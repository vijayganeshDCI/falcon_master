package com.dci.falcon.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.adapter.ProfilePageAdapter;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.fragment.DonationFragment;
import com.dci.falcon.fragment.LocationFragment;
import com.dci.falcon.fragment.RequestBloodFragment;
import com.dci.falcon.fragment.UserDetailFragment;
import com.dci.falcon.model.DonationDetails;
import com.dci.falcon.model.GetIndiUserResponse;
import com.dci.falcon.model.GetUserInfo;
import com.dci.falcon.model.IndiRegNonDonorResponse;
import com.dci.falcon.model.IndiUpdateNonDonorResponse;
import com.dci.falcon.model.IndiUserUpdateResponse;
import com.dci.falcon.model.IndiduvalRegister;
import com.dci.falcon.model.IndiduvalRegisterResponse;
import com.dci.falcon.model.LocationDetails;
import com.dci.falcon.model.UserDetails;
import com.dci.falcon.model.UserInfoEditResponse;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.CustomViewPager;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.ProfileActivityLisitener;
import com.dci.falcon.utills.UserDetailListener;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomRadioButton;
import com.dci.falcon.view.CustomTextView;
import com.github.siyamed.shapeimageview.CircularImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.falcon.app.FalconApplication.getContext;

/**
 * Created by vijayaganesh on 11/1/2017.
 */

public class ProfileActivity extends BaseActivity implements ProfileActivityLisitener {
    @BindView(R.id.pager)
    CustomViewPager pager;
    @BindView(R.id.image_profile_pic)
    CircularImageView imageProfilePic;
    @BindView(R.id.text_edit_photo)
    CustomTextView textEditPhoto;
    @BindView(R.id.radio_button_donor)
    CustomRadioButton radioButtonDonor;
    @BindView(R.id.radio_button_non_donor)
    CustomRadioButton radioButtonNonDonor;
    @BindView(R.id.radio_donor_rule)
    RadioGroup radioDonorRule;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    private static final int MY_REQUEST_CODE_CAMERA = 5;
    private static final int MY_REQUEST_CODE_GALLERY = 6;
    @BindView(R.id.text_phone_number)
    CustomTextView textPhoneNumber;
    @BindView(R.id.image_mantitory_4)
    ImageView imageMantitory4;
    @BindView(R.id.text_label_manditatory)
    CustomTextView textLabelManditatory;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.appbar)
    AppBarLayout appbar;
    @BindView(R.id.main_content)
    CoordinatorLayout mainContent;
    @BindView(R.id.text_pro_name)
    CustomTextView textProName;
    @BindView(R.id.image_right_down_arrow)
    ImageView imageRightDownArrow;
    @BindView(R.id.spinner_active_status)
    Spinner spinnerActiveStatus;
    @BindView(R.id.cons_active_status)
    ConstraintLayout consActiveStatus;
    private int REQUEST_CAMERA = 1;
    private int SELECT_FILE = 2;
    private Uri uri;
    UserDetailListener userDetailListener;
    ProfilePageAdapter adapter;
    UserDetailFragment userDetailFragment;
    LocationFragment locationFragment;
    DonationFragment donationFragment;
    public static int radiocheckedId = 0;
    LinearLayout tabChild;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    public static boolean fromBaseActvity = false;
    public static boolean isForLogin = false;
    public static boolean enableView = false;
    FloatingActionButton floatingActionButton;
    MenuItem save, edit;
    View contentView;
    UserError userError;
    @Inject
    FalconAPI falconAPI;
    IndiduvalRegisterResponse indiduvalRegisterResponse;
    IndiUserUpdateResponse indiUserUpdateResponse;
    IndiRegNonDonorResponse indiRegNonDonorResponse;
    IndiUpdateNonDonorResponse indiUpdateNonDonorResponse;
    String emptyList;
    SharedPreferences fcmKeyPreferences;
    GetIndiUserResponse getIndiUserResponse;
    UserDetails userDetails;
    LocationDetails locationDetails;
    DonationDetails donationDetails;
    UserInfoEditResponse userInfoEditResponse;
    String phoneNumber;
    String proFilePicture;
    private String proFilePictureFromImageview;
    private boolean forLogin;
    private int activeStatus;


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        BaseActivity.currentPage = 1;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        contentView = inflater.inflate(R.layout.activity_profile, null, false);
        ButterKnife.bind(this, contentView);
        FalconApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        userError = new UserError();
        fcmKeyPreferences = getSharedPreferences(FalconConstants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
        mDrawerLayout.addView(contentView, 0);
        tabChild = ((LinearLayout) tabLayout.getChildAt(0));
        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab_request_blood);
        floatingActionButton.setVisibility(View.GONE);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                push(new RequestBloodFragment(), getString(R.string.Request_Blood));
            }
        });
        Intent intent = getIntent();
        if (intent != null) {
            fromBaseActvity = intent.getBooleanExtra("isFromBaseActivity", false);
            phoneNumber = intent.getStringExtra("phoneNumber");
            isForLogin = intent.getBooleanExtra("forLogin", false);
        }

        setTitle(R.string.Profile);
        radioButtonDonor.setChecked(true);

        ArrayAdapter<String> bloodgroupAdapter = new ArrayAdapter<String>(this, R.layout.custom_spinner_profile_header, getResources().getStringArray(R.array.active_status));
        spinnerActiveStatus.setAdapter(bloodgroupAdapter);


        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.details_header)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.location_header)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.donation_header)));
        changeTabsFont();
        adapter = new ProfilePageAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        pager.setAdapter(adapter);
        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        userDetailFragment = (UserDetailFragment) adapter.instantiateItem(pager, 0);
        locationFragment = (LocationFragment) adapter.instantiateItem(pager, 1);
        donationFragment = (DonationFragment) adapter.instantiateItem(pager, 2);
        pager.setPagingEnabled(false);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition());
                userDetailFragment = (UserDetailFragment) adapter.instantiateItem(pager, 0);
                locationFragment = (LocationFragment) adapter.instantiateItem(pager, 1);
                donationFragment = (DonationFragment) adapter.instantiateItem(pager, 2);
                if (tab.getPosition() == 2) {
                    adapter.getItem(0);
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                userDetailFragment = (UserDetailFragment) adapter.instantiateItem(pager, 0);
                locationFragment = (LocationFragment) adapter.instantiateItem(pager, 1);
                donationFragment = (DonationFragment) adapter.instantiateItem(pager, 2);

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                userDetailFragment = (UserDetailFragment) adapter.instantiateItem(pager, 0);
                locationFragment = (LocationFragment) adapter.instantiateItem(pager, 1);
                donationFragment = (DonationFragment) adapter.instantiateItem(pager, 2);
            }
        });
        consActiveStatus.setVisibility(View.VISIBLE);
        radioDonorRule.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
//                    Donor-0
//                    NonDonor-1
                    case R.id.radio_button_donor:
                        radiocheckedId = 0;
                        consActiveStatus.setVisibility(View.VISIBLE);
                        pager.setCurrentItem(0);
                        for (int i = 0; i < tabChild.getChildCount(); i++) {
                            if (i == 2) {
                                tabChild.getChildAt(i).setVisibility(View.VISIBLE);

                            }
                        }

                        break;
                    case R.id.radio_button_non_donor:
                        radiocheckedId = 1;
                        consActiveStatus.setVisibility(View.GONE);
                        pager.setCurrentItem(0);
                        for (int i = 0; i < tabChild.getChildCount(); i++) {
                            if (i == 2) {
                                tabChild.getChildAt(i).setVisibility(View.GONE);
                            }
                        }
                        break;
                }
            }
        });

        spinnerActiveStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                1-Active
//                0-InActive
                if (position == 0)
                    activeStatus = 1;
                else
                    activeStatus = 0;


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //fromBaseActivity
        // fromNavigationDrawer
        // for also login purpose
        if (fromBaseActvity) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
            setViewAndChildrenEnabled(contentView, false);
            enableView = false;
            textPhoneNumber.setVisibility(View.VISIBLE);
            textProName.setVisibility(View.VISIBLE);

            getProfile();

//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    try {
//                        Thread.sleep(10000);
//                        showProgress();
//                        getProfile();
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//
//                }
//            }).start();
        } else {
//            fromLoginActivity
            //for signUp
            setViewAndChildrenEnabled(contentView, true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
            textPhoneNumber.setVisibility(View.GONE);
            textProName.setVisibility(View.GONE);
            enableView = true;
            if (!Utils.isNetworkAvailable()) {
                userError.message = getString(R.string.network_error_message);
                showError(ERROR_SHOW_TYPE_DIALOG, userError);
            }

        }


    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.save, menu);
        save = menu.findItem(R.id.menu_bar_save);
        edit = menu.findItem(R.id.menu_bar_edit);
        if (fromBaseActvity) {
            save.setVisible(false);
            edit.setVisible(true);
        } else {
            save.setVisible(true);
            edit.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_bar_save:
                if (Utils.isNetworkAvailable()) {
                    if (!fromBaseActvity) {
                        if (radiocheckedId == 0) {
                            pager.setCurrentItem(0);
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        showProgress();
                                        Thread.sleep(2000);
                                        ProfileActivity.this.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                if (userDetailFragment.isUserDetailNotNull() && locationFragment.isLocationNotNull()) {
                                                    android.support.v7.app.AlertDialog.Builder alertDialog1 = new android.support.v7.app.AlertDialog.Builder(ProfileActivity.this);
                                                    alertDialog1.setMessage(getString(R.string.alert_register) + " " + userDetailFragment.interfaceUserDetails().getFirstName() + " " + "?");
                                                    alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialogInterface, int i) {

                                                            new Thread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    registerIndiduvalUser();

                                                                }
                                                            }).start();
                                                        }
                                                    });
                                                    alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialogInterface, int i) {
                                                            dialogInterface.dismiss();
                                                            hideProgress();
                                                        }
                                                    });
                                                    alertDialog1.show();


                                                } else {
                                                    alertDialog();
//                                                    hideProgress();
                                                    switch (sharedPreferences.getInt(FalconConstants.NOTNULL, 2)) {
                                                        case 0:
                                                            pager.setCurrentItem(0);

                                                            break;
                                                        case 1:
                                                            pager.setCurrentItem(1);
                                                            break;
                                                        //                                            case 2:
                                                        //                                                pager.setCurrentItem(2);
                                                        //                                                break;
                                                    }
                                                }
                                            }
                                        });

                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }).start();

                        } else {
                            ProfileActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (userDetailFragment.isUserDetailNotNull() && locationFragment.isLocationNotNull()) {
                                        android.support.v7.app.AlertDialog.Builder alertDialog1 = new android.support.v7.app.AlertDialog.Builder(ProfileActivity.this);
                                        alertDialog1.setMessage(getString(R.string.alert_register) + " " + userDetailFragment.interfaceUserDetails().getFirstName() + " " + "?");
                                        alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                new Thread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        registerIndiduvalUser();

                                                    }
                                                }).start();
                                            }
                                        });
                                        alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                                hideProgress();
                                            }
                                        });
                                        alertDialog1.show();
                                    } else {
                                        alertDialog();
//                                        hideProgress();
                                        switch (sharedPreferences.getInt(FalconConstants.NOTNULL, 0)) {
                                            case 0:
                                                pager.setCurrentItem(0);
                                                break;
                                            case 1:
                                                pager.setCurrentItem(1);
                                                break;
                                        }
                                    }
                                }
                            });

                        }
                    } else {
                        if (radiocheckedId == 0) {
                            pager.setCurrentItem(0);
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        showProgress();
                                        Thread.sleep(2000);
                                        ProfileActivity.this.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                if (userDetailFragment.isUserDetailNotNull() && locationFragment.isLocationNotNull()) {
                                                    android.support.v7.app.AlertDialog.Builder alertDialog1 = new android.support.v7.app.AlertDialog.Builder(ProfileActivity.this);
                                                    alertDialog1.setMessage(getString(R.string.alert_update));
                                                    alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialogInterface, int i) {
                                                            new Thread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    updateProfile();
                                                                }
                                                            }).start();
                                                        }
                                                    });
                                                    alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialogInterface, int i) {
                                                            dialogInterface.dismiss();
                                                            hideProgress();
                                                        }
                                                    });
                                                    alertDialog1.show();

                                                } else {
                                                    alertDialog();
//                                                    hideProgress();
                                                    switch (sharedPreferences.getInt(FalconConstants.NOTNULL, 2)) {
                                                        case 0:
                                                            pager.setCurrentItem(0);
                                                            break;
                                                        case 1:
                                                            pager.setCurrentItem(1);
                                                            break;
                                                        //                                                    case 2:
                                                        //                                                        pager.setCurrentItem(2);
                                                        //                                                        break;
                                                    }
                                                }
                                            }
                                        });

                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }).start();
                        } else {
                            ProfileActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (userDetailFragment.isUserDetailNotNull() && locationFragment.isLocationNotNull()) {
                                        android.support.v7.app.AlertDialog.Builder alertDialog1 = new android.support.v7.app.AlertDialog.Builder(ProfileActivity.this);
                                        alertDialog1.setMessage(getString(R.string.alert_update));
                                        alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                new Thread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        updateProfile();
                                                    }
                                                }).start();
                                            }
                                        });
                                        alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                                hideProgress();
                                            }
                                        });
                                        alertDialog1.show();
                                    } else {
                                        alertDialog();
//                                        hideProgress();
                                        switch (sharedPreferences.getInt(FalconConstants.NOTNULL, 0)) {
                                            case 0:
                                                pager.setCurrentItem(0);
                                                break;
                                            case 1:
                                                pager.setCurrentItem(1);
                                                break;
                                        }
                                    }
                                }
                            });

                        }
                    }
                } else {
                    userError.message = getString(R.string.network_error_message);
                    showError(ERROR_SHOW_TYPE_DIALOG, userError);
                }


                break;
            case R.id.menu_bar_edit:
                if (Utils.isNetworkAvailable()) {
                    save.setVisible(true);
                    edit.setVisible(false);
                    setViewAndChildrenEnabled(contentView, true);
                    enableView = true;
                } else {
//                    alertDialog();
                    userError.message = getString(R.string.network_error_message);
                    showError(ERROR_SHOW_TYPE_DIALOG, userError);
                }
                break;
        }
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return true;
    }

    private void alertDialog() {
        ProfileActivity.this.runOnUiThread(new Runnable() {
            public void run() {
                android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(ProfileActivity.this);
                alertDialog.setMessage(getString(R.string.alert_mes_profile));
                alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        hideProgress();
                    }
                });
                alertDialog.show();
            }
        });
    }

    private void changeTabsFont() {

        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "font/Montserrat-Regular.ttf");
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(font, Typeface.NORMAL);
                }
            }

            if (j != 2) {
                ViewGroup vgTab1 = (ViewGroup) vg.getChildAt(j);
                int tabChildsCount1 = vgTab.getChildCount();
                for (int i = 0; i < tabChildsCount1; i++) {
                    View tabViewChild = vgTab1.getChildAt(i);
                    if (tabViewChild instanceof TextView) {
                        ((TextView) tabViewChild).setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.icon_right_arrow_tab, 0);
                        ((TextView) tabViewChild).setCompoundDrawablePadding(8);
                    }
                }
            }
        }
    }

    @OnClick({R.id.image_profile_pic, R.id.text_edit_photo})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_profile_pic:
                getProfilePicture();
                break;
            case R.id.text_edit_photo:
                getProfilePicture();
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_REQUEST_CODE_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, REQUEST_CAMERA);
            } else {
                // Your app will not have this permission. Turn off all functions
                // that require this permission or it will force close like your
                // original question
            }
        } else if (requestCode == MY_REQUEST_CODE_GALLERY) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);//
                startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
            } else {

            }
        }
    }

    private void getProfilePicture() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add photo");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(ProfileActivity.this, Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA},
                                    MY_REQUEST_CODE_CAMERA);
                        } else {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, REQUEST_CAMERA);
                        }
                    } else {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, REQUEST_CAMERA);
                    }

                } else if (options[item].equals("Choose from Gallery")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(ProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                    MY_REQUEST_CODE_GALLERY);
                        } else {
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);//
                            startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
                        }
                    } else {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);//
                        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
                    }

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        imageProfilePic.setImageBitmap(bm);
//        uploadUserImage(data.getData());
        proFilePicture = getBase64Image(bm);
        uri = data.getData();
    }

    private String convertToBitMap() {
        Bitmap bm = ((BitmapDrawable) imageProfilePic.getDrawable()).getBitmap();
        proFilePictureFromImageview = getBase64Image(bm);
        return proFilePictureFromImageview;
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(getExternalFilesDir(null),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        imageProfilePic.setImageBitmap(thumbnail);
//        uploadUserImage(Uri.fromFile(destination));
        proFilePicture = getBase64Image(thumbnail);
        uri = Uri.fromFile(destination);
    }


    private void registerIndiduvalUser() {
        final IndiduvalRegister indiduvalRegister = new IndiduvalRegister();
        indiduvalRegister.setDeviceID(sharedPreferences.getString(FalconConstants.DEVICEID, ""));
        indiduvalRegister.setAppID(sharedPreferences.getString(FalconConstants.APPID, ""));
        indiduvalRegister.setAppVersion(sharedPreferences.getString(FalconConstants.APPVERSION, ""));
        indiduvalRegister.setPhoneNumber(phoneNumber);
        indiduvalRegister.setNonsmartPhone(false);
        indiduvalRegister.setFCMKey(fcmKeyPreferences.getString(FalconConstants.FCMTOKEN, ""));
        indiduvalRegister.setAuthorizer(userDetailFragment.interfaceUserDetails().getAuthorizer());
        indiduvalRegister.setUserType(radiocheckedId);
        if (proFilePicture == null) {
            indiduvalRegister.setProfilePicture(convertToBitMap());
        } else {
            indiduvalRegister.setProfilePicture(proFilePicture);
        }
        indiduvalRegister.setFirstName(userDetailFragment.interfaceUserDetails().getFirstName());
        indiduvalRegister.setLastName(userDetailFragment.interfaceUserDetails().getLastName());
        indiduvalRegister.setBloodGroupID(userDetailFragment.interfaceUserDetails().getBloodGroup());
        indiduvalRegister.setAge(userDetailFragment.interfaceUserDetails().getAge());
        indiduvalRegister.setEmailID(userDetailFragment.interfaceUserDetails().getEmailID());
        indiduvalRegister.setPassword(userDetailFragment.interfaceUserDetails().getPassword());
        indiduvalRegister.setDOB(userDetailFragment.interfaceUserDetails().getDateOfBirth());
        indiduvalRegister.setGender(userDetailFragment.interfaceUserDetails().getGender());
        indiduvalRegister.setSecQuesID(userDetailFragment.interfaceUserDetails().getSequrityQuestionID());
        indiduvalRegister.setSecQuesAns(userDetailFragment.interfaceUserDetails().getSequrityQuestionAnswer());
        indiduvalRegister.setOrganizationType(userDetailFragment.interfaceUserDetails().getOrgType());
        indiduvalRegister.setOrganizationID(userDetailFragment.interfaceUserDetails().getOrgID());
        indiduvalRegister.setOrgAddressDetail(userDetailFragment.interfaceUserDetails().getOrgaddress());
        indiduvalRegister.setOrgName(userDetailFragment.interfaceUserDetails().getOrgName());
        indiduvalRegister.setOrgDetail(userDetailFragment.interfaceUserDetails().getOrgDetail());
        indiduvalRegister.setOrgPhoneNumber(userDetailFragment.interfaceUserDetails().getOrgphoneNumber());
        indiduvalRegister.setNearHospitalLocationHome(locationFragment.interafceLocationDetails().getNearHome());
        indiduvalRegister.setNearHospitalLocationOffice(locationFragment.interafceLocationDetails().getNearOffice());
        indiduvalRegister.setNearHospitalLocationFre(locationFragment.interafceLocationDetails().getNearFrequent());
        indiduvalRegister.setNearHospitalLocationHomeLat(locationFragment.interafceLocationDetails().getNearHomeLat());
        indiduvalRegister.setNearHospitalLocationHomeLng(locationFragment.interafceLocationDetails().getNearHomeLng());
        indiduvalRegister.setNearHospitalLocationOfficeLat(locationFragment.interafceLocationDetails().getNearOfficeLat());
        indiduvalRegister.setNearHospitalLocationOfficeLng(locationFragment.interafceLocationDetails().getNearOfficeLng());
        indiduvalRegister.setNearHospitalLocationFreLat(locationFragment.interafceLocationDetails().getNearFrequentLat());
        indiduvalRegister.setNearHospitalLocationFreLng(locationFragment.interafceLocationDetails().getNearFrequentLng());
        if (radiocheckedId == 0) {
            indiduvalRegister.setActiveStatus(activeStatus);
            indiduvalRegister.setLastDonationDate(donationFragment.interfaceDonationDetails().getLastDonated());
//            if (donationFragment.interfaceDonationDetails().getLastDonated() != null)
//                indiduvalRegister.setLastDonationDate(donationFragment.interfaceDonationDetails().getLastDonated());
//            else
//                indiduvalRegister.setLastDonationDate("");
            indiduvalRegister.setDonateAt(donationFragment.interfaceDonationDetails().getDonateAt());
            indiduvalRegister.setDND(donationFragment.donNotDisturbDate());
        } else {
            indiduvalRegister.setActiveStatus(0);
            indiduvalRegister.setLastDonationDate("");
            indiduvalRegister.setDonateAt("");
            indiduvalRegister.setDND("");
        }

//        if (Utils.isNetworkAvailable()) {
        showProgress();
        if (radiocheckedId == 0) {
            falconAPI.registerIndiduvalUser(indiduvalRegister).enqueue(new Callback<IndiduvalRegisterResponse>() {
                @Override
                public void onResponse(Call<IndiduvalRegisterResponse> call, Response<IndiduvalRegisterResponse> response) {
                    hideProgress();
                    if (response.body() != null && response.isSuccessful()) {
                        indiduvalRegisterResponse = response.body();
                        android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(ProfileActivity.this);
                        alertDialog.setMessage(getString(R.string.successfully_update));
                        alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(ProfileActivity.this, HomeActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }
                        });
                        alertDialog.show();
                        editor.putInt(FalconConstants.LOGINSTATUSCODE, 2).commit();
                        editor.putInt(FalconConstants.USEDID, indiduvalRegisterResponse.getUserInfo().getId()).commit();
//                        editor.putInt(FalconConstants.INDIDUVALLIVES, indiduvalRegisterResponse.getUserInfo().getLives()).commit();
//                        editor.putInt(FalconConstants.INDIDUVALUNITS, indiduvalRegisterResponse.getUserInfo().getIndUnits()).commit();
//                        editor.putInt(FalconConstants.FALCONTOTALUNITS, indiduvalRegisterResponse.getTotalIndUnits()).commit();
                        editor.putString(FalconConstants.BLOODGROUP, indiduvalRegisterResponse.getUserInfo().getBloodGroupID()).commit();
                        editor.putString(FalconConstants.USERNAME, indiduvalRegisterResponse.getUserInfo().getFirstName()).commit();
                        editor.putInt(FalconConstants.AUTHORIZER, indiduvalRegisterResponse.getUserInfo().getAuthorizer()).commit();
                        editor.putString(FalconConstants.PHONENUMBER, indiduvalRegisterResponse.getUserInfo().getPhoneNumber()).commit();
                        editor.putString(FalconConstants.PROFILEPICTURE, indiduvalRegisterResponse.getUserInfo().getProfilePicture()).commit();
                        editor.putInt(FalconConstants.USERTYPE, indiduvalRegisterResponse.getUserInfo().getUserType()).commit();


                    } else {
                        Toast.makeText(ProfileActivity.this, getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<IndiduvalRegisterResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(ProfileActivity.this, getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            falconAPI.registerIndiduvalNonDonorUser(indiduvalRegister).enqueue(new Callback<IndiRegNonDonorResponse>() {
                @Override
                public void onResponse(Call<IndiRegNonDonorResponse> call, Response<IndiRegNonDonorResponse> response) {
                    hideProgress();
                    if (response.body() != null && response.isSuccessful()) {
                        indiRegNonDonorResponse = response.body();
                        editor.putInt(FalconConstants.LOGINSTATUSCODE, 2).commit();
                        android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(ProfileActivity.this);
                        alertDialog.setMessage(getString(R.string.successfully_update));
                        alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(ProfileActivity.this, HomeActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }
                        });
                        alertDialog.show();
                        editor.putInt(FalconConstants.LOGINSTATUSCODE, 2).commit();
                        editor.putInt(FalconConstants.USEDID, indiRegNonDonorResponse.getUserInfo().getId()).commit();
//                        editor.putInt(FalconConstants.INDIDUVALLIVES, indiRegNonDonorResponse.getUserInfo().getLives()).commit();
//                        editor.putInt(FalconConstants.INDIDUVALUNITS, indiRegNonDonorResponse.getUserInfo().getIndUnits()).commit();
//                        editor.putInt(FalconConstants.FALCONTOTALUNITS, indiRegNonDonorResponse.getTotalIndUnits()).commit();
                        editor.putString(FalconConstants.BLOODGROUP, indiRegNonDonorResponse.getUserInfo().getBloodGroupID()).commit();
                        editor.putString(FalconConstants.USERNAME, indiRegNonDonorResponse.getUserInfo().getFirstName()).commit();
                        editor.putInt(FalconConstants.AUTHORIZER, indiRegNonDonorResponse.getUserInfo().getAuthorizer()).commit();
                        editor.putString(FalconConstants.PHONENUMBER, indiRegNonDonorResponse.getUserInfo().getPhoneNumber()).commit();
                        editor.putString(FalconConstants.PROFILEPICTURE, indiRegNonDonorResponse.getUserInfo().getProfilePicture()).commit();
                        editor.putInt(FalconConstants.USERTYPE, indiRegNonDonorResponse.getUserInfo().getUserType()).commit();
                    } else {
                        Toast.makeText(ProfileActivity.this, getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<IndiRegNonDonorResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(ProfileActivity.this, getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                }
            });
        }
//        } else {
//            userError.message = getString(R.string.network_error_message);
//            showError(ERROR_SHOW_TYPE_DIALOG, userError);
//        }

    }

    private void getProfile() {
        final GetUserInfo getUserInfo = new GetUserInfo();
        getUserInfo.setUserID(sharedPreferences.getInt(FalconConstants.USEDID, 0));
        if (Utils.isNetworkAvailable()) {
            showProgress();
            falconAPI.getUserProfile(getUserInfo).enqueue(new Callback<GetIndiUserResponse>() {
                @Override
                public void onResponse(Call<GetIndiUserResponse> call, Response<GetIndiUserResponse> response) {
                    hideProgress();
                    getIndiUserResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && getIndiUserResponse.getStatusCode() == 200) {
                        userDetails = new UserDetails();
                        locationDetails = new LocationDetails();
                        donationDetails = new DonationDetails();
                        String pro = getIndiUserResponse.getUserInfo().getProfilePicture();
                        if (pro != null) {
                            universalImageLoader((ImageView) findViewById(R.id.image_profile_pic), pro);
//                            picasoImageLoader((ImageView) findViewById(R.id.image_profile_pic),pro,ProfileActivity.this);
                        } else {
                            imageProfilePic.setImageResource(R.mipmap.img_sample_pro_pic);
                        }
                        textProName.setText(getIndiUserResponse.getUserInfo().getFirstName());
                        textPhoneNumber.setText(getIndiUserResponse.getUserInfo().getPhoneNumber());


                        //Donor-1
                        if (getIndiUserResponse.getUserInfo().getUserType() == 0) {
                            radioButtonDonor.setChecked(true);
                            consActiveStatus.setVisibility(View.VISIBLE);
//                            donationDetails.setActiveStatus(getIndiUserResponse.getUserInfo().getActiveStatus());
                            donationDetails.setDonateAt(getIndiUserResponse.getUserInfo().getDonateAt());
                            donationDetails.setLastDonated(getIndiUserResponse.getUserInfo().getLastDonationDate());
                            donationDetails.setDndInfo(getIndiUserResponse.getDndInfo());
                            donationFragment.getDonationDetail(donationDetails);
                            if (getIndiUserResponse.getUserInfo().getActiveStatus() == 1)
//                            Active
                                spinnerActiveStatus.setSelection(0);
                            else
//                            Inactive
                                spinnerActiveStatus.setSelection(1);

                        } else {
                            //Non Donor-0
                            radioButtonNonDonor.setChecked(true);
                            consActiveStatus.setVisibility(View.GONE);

                        }
                        userDetails.setFirstName(getIndiUserResponse.getUserInfo().getFirstName());
                        userDetails.setLastName(getIndiUserResponse.getUserInfo().getLastName());
                        userDetails.setBloodGroup(getIndiUserResponse.getUserInfo().getBloodGroupID());
                        userDetails.setAge(getIndiUserResponse.getUserInfo().getAge());
                        userDetails.setEmailID(getIndiUserResponse.getUserInfo().getEmailID());
                        userDetails.setDateOfBirth(getIndiUserResponse.getUserInfo().getDOB());
                        userDetails.setGender(getIndiUserResponse.getUserInfo().getGender());
                        userDetails.setSequrityQuestionID(getIndiUserResponse.getUserInfo().getSecQuesID());
                        userDetails.setSequrityQuestionAnswer(getIndiUserResponse.getUserInfo().getSecQuesAns());
                        userDetails.setOrgID(getIndiUserResponse.getUserInfo().getOrganizationID());
                        userDetails.setOrgType(getIndiUserResponse.getUserInfo().getOrganizationType());
                        userDetails.setAuthorizer(getIndiUserResponse.getUserInfo().getAuthorizer());
                        if (getIndiUserResponse.getOrg() != null) {
                            userDetails.setOrgName(getIndiUserResponse.getOrg().getName());
                            userDetails.setOrgDetail(getIndiUserResponse.getOrg().getDetails());
                            userDetails.setOrgaddress(getIndiUserResponse.getOrg().getAddressDetails());
                            userDetails.setOrgphoneNumber(getIndiUserResponse.getOrg().getPhoneNumber());
                            userDetails.setStatus(getIndiUserResponse.getOrg().getStatus());
                        }
                        locationDetails.setNearHome(getIndiUserResponse.getUserInfo().getNearHospitalLocationHome());
                        locationDetails.setNearHomeLng(getIndiUserResponse.getUserInfo().getNearHospitalLocationHomeLng());
                        locationDetails.setNearHomeLat(getIndiUserResponse.getUserInfo().getNearHospitalLocationHomeLat());
                        locationDetails.setNearOffice(getIndiUserResponse.getUserInfo().getNearHospitalLocationOffice());
                        locationDetails.setNearOfficeLng(getIndiUserResponse.getUserInfo().getNearHospitalLocationOfficeLng());
                        locationDetails.setNearOfficeLat(getIndiUserResponse.getUserInfo().getNearHospitalLocationOfficeLat());
                        locationDetails.setNearFrequent(getIndiUserResponse.getUserInfo().getNearHospitalLocationFre());
                        locationDetails.setNearFrequentLng(getIndiUserResponse.getUserInfo().getNearHospitalLocationFreLng());
                        locationDetails.setNearFrequentLat(getIndiUserResponse.getUserInfo().getNearHospitalLocationFreLat());
                        userDetailFragment.getUserDetails(userDetails);
                        UserDetailFragment.userdetails = userDetails;
                        locationFragment.getLocationDetail(locationDetails);

                    } else {
                        Toast.makeText(ProfileActivity.this, getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<GetIndiUserResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(ProfileActivity.this, getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            alertDialog();
//            userError.message = getString(R.string.network_error_message);
//            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }
    }

    private void updateProfile() {
        IndiduvalRegister indiduvalRegister = new IndiduvalRegister();
        indiduvalRegister.setId(sharedPreferences.getInt(FalconConstants.USEDID, 0));
        indiduvalRegister.setDeviceID(sharedPreferences.getString(FalconConstants.DEVICEID, ""));
        indiduvalRegister.setAppID(sharedPreferences.getString(FalconConstants.APPID, ""));
        indiduvalRegister.setAppVersion(sharedPreferences.getString(FalconConstants.APPVERSION, ""));
        indiduvalRegister.setPhoneNumber(sharedPreferences.getString(FalconConstants.PHONENUMBER, ""));
        indiduvalRegister.setNonsmartPhone(false);
        indiduvalRegister.setFCMKey(fcmKeyPreferences.getString(FalconConstants.FCMTOKEN, ""));
        indiduvalRegister.setAuthorizer(userDetailFragment.interfaceUserDetails().getAuthorizer());
        indiduvalRegister.setUserType(radiocheckedId);
        if (proFilePicture == null) {
            //when not change image
            indiduvalRegister.setProfilePicture(convertToBitMap());
        } else {
            //when change image
            indiduvalRegister.setProfilePicture(proFilePicture);
        }
        indiduvalRegister.setFirstName(userDetailFragment.interfaceUserDetails().getFirstName());
        indiduvalRegister.setLastName(userDetailFragment.interfaceUserDetails().getLastName());
        indiduvalRegister.setBloodGroupID(userDetailFragment.interfaceUserDetails().getBloodGroup());
        indiduvalRegister.setAge(userDetailFragment.interfaceUserDetails().getAge());
        indiduvalRegister.setEmailID(userDetailFragment.interfaceUserDetails().getEmailID());
        indiduvalRegister.setDOB(userDetailFragment.interfaceUserDetails().getDateOfBirth());
        indiduvalRegister.setGender(userDetailFragment.interfaceUserDetails().getGender());
        indiduvalRegister.setSecQuesID(userDetailFragment.interfaceUserDetails().getSequrityQuestionID());
        indiduvalRegister.setSecQuesAns(userDetailFragment.interfaceUserDetails().getSequrityQuestionAnswer());
        indiduvalRegister.setOrganizationType(userDetailFragment.interfaceUserDetails().getOrgType());
        indiduvalRegister.setOrganizationID(userDetailFragment.interfaceUserDetails().getOrgID());
        indiduvalRegister.setOrgAddressDetail(userDetailFragment.interfaceUserDetails().getOrgaddress());
        indiduvalRegister.setOrgName(userDetailFragment.interfaceUserDetails().getOrgName());
        indiduvalRegister.setOrgDetail(userDetailFragment.interfaceUserDetails().getOrgDetail());
        indiduvalRegister.setOrgPhoneNumber(userDetailFragment.interfaceUserDetails().getOrgphoneNumber());
        indiduvalRegister.setNearHospitalLocationHome(locationFragment.interafceLocationDetails().getNearHome());
        indiduvalRegister.setNearHospitalLocationOffice(locationFragment.interafceLocationDetails().getNearOffice());
        indiduvalRegister.setNearHospitalLocationFre(locationFragment.interafceLocationDetails().getNearFrequent());
        indiduvalRegister.setNearHospitalLocationHomeLat(locationFragment.interafceLocationDetails().getNearHomeLat());
        indiduvalRegister.setNearHospitalLocationHomeLng(locationFragment.interafceLocationDetails().getNearHomeLng());
        indiduvalRegister.setNearHospitalLocationOfficeLat(locationFragment.interafceLocationDetails().getNearOfficeLat());
        indiduvalRegister.setNearHospitalLocationOfficeLng(locationFragment.interafceLocationDetails().getNearOfficeLng());
        indiduvalRegister.setNearHospitalLocationFreLat(locationFragment.interafceLocationDetails().getNearFrequentLat());
        indiduvalRegister.setNearHospitalLocationFreLng(locationFragment.interafceLocationDetails().getNearFrequentLng());
//        Donor
        if (radiocheckedId == 0) {
            indiduvalRegister.setActiveStatus(activeStatus);
            if (donationFragment.interfaceDonationDetails().getLastDonated() != null)
                indiduvalRegister.setLastDonationDate(donationFragment.interfaceDonationDetails().getLastDonated());
            else
                indiduvalRegister.setLastDonationDate("");
            indiduvalRegister.setDonateAt(donationFragment.interfaceDonationDetails().getDonateAt());
            indiduvalRegister.setDND(donationFragment.donNotDisturbDate());
        } else {
//            Non Donor
            indiduvalRegister.setActiveStatus(0);
            indiduvalRegister.setLastDonationDate("");
            indiduvalRegister.setDonateAt("");
            indiduvalRegister.setDND("");
        }
//        if (Utils.isNetworkAvailable()) {

        showProgress();
        if (radiocheckedId == 0) {
            falconAPI.editUserInfo(indiduvalRegister).enqueue(new Callback<IndiUserUpdateResponse>() {
                @Override
                public void onResponse(Call<IndiUserUpdateResponse> call, Response<IndiUserUpdateResponse> response) {
                    hideProgress();
                    indiUserUpdateResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && indiUserUpdateResponse.getStatusCode() == 200) {
                        android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(ProfileActivity.this);
                        alertDialog.setMessage(getString(R.string.successfully_update));
                        alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (isForLogin) {
                                    editor.putInt(FalconConstants.LOGINSTATUSCODE, 2).commit();
                                    Intent intent = new Intent(ProfileActivity.this, HomeActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    Toast.makeText(ProfileActivity.this, getString(R.string.successfully_logged), Toast.LENGTH_SHORT).show();
                                } else
                                    onBackPressed();
                            }
                        });
                        alertDialog.show();
                        editor.putInt(FalconConstants.USEDID, indiUserUpdateResponse.getUserInfo().getId()).commit();
//                        editor.putInt(FalconConstants.INDIDUVALLIVES, indiUserUpdateResponse.getUserInfo().getLives()).commit();
//                        editor.putInt(FalconConstants.INDIDUVALUNITS, indiUserUpdateResponse.getUserInfo().getIndUnits()).commit();
//                        editor.putInt(FalconConstants.FALCONTOTALUNITS, indiUserUpdateResponse.getTotalUnits()).commit();
                        editor.putString(FalconConstants.BLOODGROUP, indiUserUpdateResponse.getUserInfo().getBloodGroupID()).commit();
                        editor.putString(FalconConstants.USERNAME, indiUserUpdateResponse.getUserInfo().getFirstName()).commit();
                        editor.putInt(FalconConstants.AUTHORIZER, indiUserUpdateResponse.getUserInfo().getAuthorizer()).commit();
                        editor.putString(FalconConstants.PHONENUMBER, indiUserUpdateResponse.getUserInfo().getPhoneNumber()).commit();
                        editor.putString(FalconConstants.PROFILEPICTURE, indiUserUpdateResponse.getUserInfo().getProfilePicture()).commit();
                        editor.putInt(FalconConstants.USERTYPE, indiUserUpdateResponse.getUserInfo().getUserType()).commit();

                    } else {
                        Toast.makeText(ProfileActivity.this, getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<IndiUserUpdateResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(ProfileActivity.this, getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();

                }
            });
        } else {
            falconAPI.editNonDonorUserInfo(indiduvalRegister).enqueue(new Callback<IndiUpdateNonDonorResponse>() {
                @Override
                public void onResponse(Call<IndiUpdateNonDonorResponse> call, Response<IndiUpdateNonDonorResponse> response) {

                    hideProgress();
                    indiUpdateNonDonorResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && indiUpdateNonDonorResponse.getStatusCode() == 200) {
                        android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(ProfileActivity.this);
                        alertDialog.setMessage(getString(R.string.successfully_update));
                        alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                onBackPressed();
                            }
                        });
                        alertDialog.show();
                        editor.putInt(FalconConstants.USEDID, indiUpdateNonDonorResponse.getUserInfo().getId()).commit();
//                        editor.putInt(FalconConstants.INDIDUVALLIVES, indiUpdateNonDonorResponse.getUserInfo().getLives()).commit();
//                        editor.putInt(FalconConstants.INDIDUVALUNITS, indiUpdateNonDonorResponse.getUserInfo().getIndUnits()).commit();
//                        editor.putInt(FalconConstants.FALCONTOTALUNITS, indiUpdateNonDonorResponse.getTotalUnits()).commit();
                        editor.putString(FalconConstants.BLOODGROUP, indiUpdateNonDonorResponse.getUserInfo().getBloodGroupID()).commit();
                        editor.putString(FalconConstants.USERNAME, indiUpdateNonDonorResponse.getUserInfo().getFirstName()).commit();
                        editor.putInt(FalconConstants.AUTHORIZER, indiUpdateNonDonorResponse.getUserInfo().getAuthorizer()).commit();
                        editor.putString(FalconConstants.PHONENUMBER, indiUpdateNonDonorResponse.getUserInfo().getPhoneNumber()).commit();
                        editor.putString(FalconConstants.PROFILEPICTURE, indiUpdateNonDonorResponse.getUserInfo().getProfilePicture()).commit();
                        editor.putInt(FalconConstants.USERTYPE, indiUpdateNonDonorResponse.getUserInfo().getUserType()).commit();

                    } else {
                        Toast.makeText(ProfileActivity.this, getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<IndiUpdateNonDonorResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(ProfileActivity.this, getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                }
            });
        }

//        } else {
//            userError.message = getString(R.string.network_error_message);
//            showError(ERROR_SHOW_TYPE_DIALOG, userError);
//        }


    }


    @Override
    public void pageNumber() {
        pager.setCurrentItem(pager.getCurrentItem() + 1);
    }
}
