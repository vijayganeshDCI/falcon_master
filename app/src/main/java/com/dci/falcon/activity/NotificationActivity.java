package com.dci.falcon.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.dci.falcon.R;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.fragment.AcceptRequestFragment;
import com.dci.falcon.fragment.HistoryFragment;
import com.dci.falcon.fragment.NotificationFragment;
import com.dci.falcon.model.NotificationUpdate;
import com.dci.falcon.model.NotificationUpdateResponse;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.Utils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vijayaganesh on 1/31/2018.
 */

public class NotificationActivity extends BaseActivity {

    @BindView(R.id.frame_notify_container)
    FrameLayout frameNotifyContainer;
    @BindView(R.id.cons_notify)
    ConstraintLayout consNotify;
    @Inject
    SharedPreferences sharedPreferences;
    UserError userError;
    private NotificationUpdateResponse notificationUpdateResponse;
    String notificationType;
    boolean isNotify = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_notification, null, false);
        ButterKnife.bind(this, contentView);
        FalconApplication.getContext().getComponent().inject(this);
        mDrawerLayout.addView(contentView, 0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        userError = new UserError();
        Intent intent = getIntent();
        if (intent != null){
            notificationType = intent.getStringExtra("notificationType");
            isNotify = intent.getBooleanExtra("isNotify", false);
        }
        if (isNotify)
            updateNotification(0);


        if (Utils.isNetworkAvailable()) {
            //     Donor-0
//                    NonDonor-1
            if (sharedPreferences.getInt(FalconConstants.ROLEID, 0) == 0) {

//            INdi
                NotificationFragment notificationFragment = new NotificationFragment();
//                Bundle bundle = new Bundle();
//                bundle.putString("notificationType", notificationType);
//                notificationFragment.setArguments(bundle);
                push(notificationFragment);
//            if (sharedPreferences.getInt(FalconConstants.USERTYPE, 0) == 0 || sharedPreferences.getInt(FalconConstants.AUTHORIZER, 0) == 1)
//                push(new NotificationFragment());
//            else
//                push(new AcceptRequestFragment());
            } else {
//            Hosp
                push(new AcceptRequestFragment());
            }
        } else {
            userError.message = getString(R.string.network_error_message);
            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void push(Fragment fragment, String title) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        String tag = fragment.getClass().getCanonicalName();

        if (title != null) {
            try {
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_notify_container, fragment, tag)
                        .addToBackStack(title)
                        .commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_notify_container, fragment, tag)
                        .addToBackStack(title)
                        .commitAllowingStateLoss();
            }
        } else {
            try {
                fragmentManager.beginTransaction().replace(R.id.frame_notify_container, fragment, tag).commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction().replace(R.id.frame_notify_container, fragment, tag).commitAllowingStateLoss();
            }
        }

//        if (title != null) {
//            ActionBar actionBar = getSupportActionBar();
//            if (actionBar != null) {
//                actionBar.setTitle(title);
//            }
//        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(NotificationActivity.this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void push(Fragment fragment) {
        if (fragment == null) {
            return;
        }
        push(fragment, null);
    }

    private void updateNotification(int status) {
        NotificationUpdate notificationUpdate = new NotificationUpdate();
        notificationUpdate.setStatus(status);
        if (sharedPreferences.getInt(FalconConstants.ROLEID, 0) == 0) {
            notificationUpdate.setUser_id(sharedPreferences.getInt(FalconConstants.USEDID, 0));
            notificationUpdate.setUser_type(1);
        } else {
            notificationUpdate.setUser_id(sharedPreferences.getInt(FalconConstants.HOSPITALUSERID, 0));
            notificationUpdate.setUser_type(0);
        }

//        if (Utils.isNetworkAvailable()) {

        falconAPI.updateNotification(notificationUpdate).enqueue(new Callback<NotificationUpdateResponse>() {
            @Override
            public void onResponse(Call<NotificationUpdateResponse> call, Response<NotificationUpdateResponse> response) {
                notificationUpdateResponse = response.body();
                if (response.body() != null && response.isSuccessful() && notificationUpdateResponse.getStatusCode() == 200) {
//                        notifyBilicking.setVisible(false);
//                        notify.setVisible(true);
                }
            }

            @Override
            public void onFailure(Call<NotificationUpdateResponse> call, Throwable t) {

            }
        });

//        } else {
//            userError.message = getString(R.string.network_error_message);
//            showError(ERROR_SHOW_TYPE_DIALOG, userError);
//        }


    }
}
