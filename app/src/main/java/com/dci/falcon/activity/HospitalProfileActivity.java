package com.dci.falcon.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.dci.falcon.R;
import com.dci.falcon.fragment.HospitalProfileFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by vijayaganesh on 11/16/2017.
 */

public class HospitalProfileActivity extends BaseActivity {

    boolean fromActvity;
    View contentView;
    @BindView(R.id.frame_hos_profile_container)
    FrameLayout frameHosProfileContainer;
    @BindView(R.id.cons_hos_profile)
    ConstraintLayout consHosProfile;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        BaseActivity.currentPage = 1;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        contentView = inflater.inflate(R.layout.activity_hospital_profile, null, false);
        ButterKnife.bind(this, contentView);
        mDrawerLayout.addView(contentView, 0);
        Intent intent = getIntent();
        if (intent != null) {
            fromActvity = intent.getBooleanExtra("isFromBaseActivity", false);
        }
        //fromBaseActivity
        if (fromActvity) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
        } else {
//            fromLoginActivity
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
        }
        push(new HospitalProfileFragment());

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void push(Fragment fragment, String title) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        String tag = fragment.getClass().getCanonicalName();

        if (title != null) {
            try {
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_hos_profile_container, fragment, tag)
                        .addToBackStack(title)
                        .commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_hos_profile_container, fragment, tag)
                        .addToBackStack(title)
                        .commitAllowingStateLoss();
            }
        } else {
            try {
                fragmentManager.beginTransaction().replace(R.id.frame_hos_profile_container, fragment, tag).commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction().replace(R.id.frame_hos_profile_container, fragment, tag).commitAllowingStateLoss();
            }
        }

//        if (title != null) {
//            ActionBar actionBar = getSupportActionBar();
//            if (actionBar != null) {
//                actionBar.setTitle(title);
//            }
//        }
    }

    public void push(Fragment fragment) {
        if (fragment == null) {
            return;
        }
        push(fragment, null);
    }
}
