package com.dci.falcon.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.Settings;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.dci.falcon.BuildConfig;
import com.dci.falcon.R;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.fragment.AlertDialogFragment;
import com.dci.falcon.fragment.LoginFragment;
import com.dci.falcon.fragment.RequestBloodFragment;
import com.dci.falcon.fragment.UserProfileSelectionFragment;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.UiUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.dci.falcon.app.FalconApplication.getContext;

/**
 * Created by vijayaganesh on 10/19/2017.
 */

public class LoginActivity extends BaseActivity {
    @BindView(R.id.frame_login_container)
    FrameLayout frameLoginContainer;
    @BindView(R.id.cons_login)
    ConstraintLayout consLogin;
    FloatingActionButton floatingActionButton;
    String deviceID, appID, appVersion;

    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        FalconApplication.getContext().getComponent().inject(this);
        deviceID = Settings.Secure.getString(getContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        appID = getApplication().getPackageName();
        appVersion = BuildConfig.VERSION_NAME;
        editor = sharedPreferences.edit();
        editor.putString(FalconConstants.DEVICEID, deviceID).commit();
        editor.putString(FalconConstants.APPID, appID).commit();
        editor.putString(FalconConstants.APPVERSION, appVersion).commit();
//        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab_request_blood);
//        floatingActionButton.setVisibility(View.GONE);
        push(new UserProfileSelectionFragment());
    }


    public void push(Fragment fragment, String title) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        String tag = fragment.getClass().getCanonicalName();

        if (title != null) {
            try {
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_login_container, fragment, tag)
                        .addToBackStack(title)
                        .commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_login_container, fragment, tag)
                        .addToBackStack(title)
                        .commitAllowingStateLoss();
            }
        } else {
            try {
                fragmentManager.beginTransaction().replace(R.id.frame_login_container, fragment, tag).commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction().replace(R.id.frame_login_container, fragment, tag).commitAllowingStateLoss();
            }
        }

//        if (title != null) {
//            ActionBar actionBar = getSupportActionBar();
//            if (actionBar != null) {
//                actionBar.setTitle(title);
//            }
//        }
    }

    public void push(Fragment fragment) {
        if (fragment == null) {
            return;
        }
        push(fragment, null);
    }


    public void showError(int showType, UserError error) {
        switch (showType) {
            case ERROR_SHOW_TYPE_LOG:
                Log.e("BaseActivity", "Error title : " + error.title + "; Message : " + error.message);
                break;
            case ERROR_SHOW_TYPE_TOAST:
                if (!TextUtils.isEmpty(error.message)) {
                    Toast.makeText(this, error.message, Toast.LENGTH_LONG).show();
                }
                break;
            case ERROR_SHOW_TYPE_DIALOG:
                dismissDialogFragment(DIALOG_API_ERROR_TAG);
                getSupportFragmentManager().beginTransaction()
                        .add(AlertDialogFragment.newOkDialog(error.title, error.message), DIALOG_API_ERROR_TAG)
                        .commitAllowingStateLoss();
                break;
        }
    }

    public void dismissDialogFragment(String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        DialogFragment fragment = (DialogFragment) fragmentManager.findFragmentByTag(tag);
        if (fragment != null && fragment.isVisible()) {
            fragment.dismissAllowingStateLoss();
        }
    }


    // API Call handling
    public void showProgress() {
        showProgress(R.string.loading);
    }

    public void showProgress(int messageId) {
        showProgress(getString(messageId));
    }

    public void showProgress(final CharSequence message) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                UiUtils.hideKeyboard(LoginActivity.this);
                mDialog.getWindow().setBackgroundDrawable(new
                        ColorDrawable(android.graphics.Color.TRANSPARENT));
                mDialog.setCancelable(false);
                mDialog.setIndeterminate(true);
                mDialog.show();
                mDialog.setContentView(R.layout.custom_progress_view);
            }
        });
    }

    public void hideProgress() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mDialog.dismiss();
            }
        });
    }

    public void popBackStackImmediate() {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack();
        fragmentManager.executePendingTransactions();
    }

    public void popAllBackstack() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        int backCount = fragmentManager.getBackStackEntryCount();
        if (backCount > 0) {
            fragmentManager.popBackStackImmediate(fragmentManager.getBackStackEntryAt(0).getId(),
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    public void popBackStack() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        int backStackCount = getSupportFragmentManager().getBackStackEntryCount();
        for (int i = 0; i < backStackCount; i++) {// Get the back stack fragment id.
            int backStackId = getSupportFragmentManager().getBackStackEntryAt(i).getId();
            fragmentManager.popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }


}
