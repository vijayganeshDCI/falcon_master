package com.dci.falcon.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.dci.falcon.R;
import com.dci.falcon.fragment.AddtionalUsersFragment;
import com.dci.falcon.fragment.FamilyMembersDonationsFragment;
import com.dci.falcon.view.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by vijayaganesh on 1/17/2018.
 */

public class FacilitatedDonationActivity extends BaseActivity {
    @BindView(R.id.text_label_family)
    CustomTextView textLabelFamily;
    @BindView(R.id.view_family)
    View viewFamily;
    @BindView(R.id.cons_family)
    ConstraintLayout consFamily;
    @BindView(R.id.text_label_others)
    CustomTextView textLabelOthers;
    @BindView(R.id.view_others)
    View viewOthers;
    @BindView(R.id.cons_others)
    ConstraintLayout consOthers;
    @BindView(R.id.cons_parent_view_1)
    ConstraintLayout consParentView;
    @BindView(R.id.frame_fac_don_container)
    FrameLayout frameFacDonContainer;
    @BindView(R.id.cons_facili_dona)
    LinearLayout consFaciliDona;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_facilitated_donations, null, false);
        mDrawerLayout.addView(contentView, 0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        ButterKnife.bind(this, contentView);
        viewOthers.setVisibility(View.INVISIBLE);
        viewFamily.setVisibility(View.VISIBLE);
        push(new FamilyMembersDonationsFragment());
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
////        switch (item.getItemId()) {
////            case R.id.menu_bar_back:
////                finish();
////                break;
////            default:
////                break;
////        }
//
//        if (mDrawerToggle.onOptionsItemSelected(item)) {
//            return true;
//        }
//
//        return true;
//    }
//
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
////        MenuInflater inflater = getMenuInflater();
////        inflater.inflate(R.menu.fac_don_back, menu);
//        return true;
//    }

    @OnClick({R.id.cons_family, R.id.cons_others})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cons_family:
                viewOthers.setVisibility(View.INVISIBLE);
                viewFamily.setVisibility(View.VISIBLE);
                push(new FamilyMembersDonationsFragment());
                break;
            case R.id.cons_others:
                viewOthers.setVisibility(View.VISIBLE);
                viewFamily.setVisibility(View.INVISIBLE);
                push(new AddtionalUsersFragment());
                break;
        }
    }

    public void push(Fragment fragment, String title) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        String tag = fragment.getClass().getCanonicalName();

        if (title != null) {
            try {
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_fac_don_container, fragment, tag)
                        .addToBackStack(title)
                        .commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_fac_don_container, fragment, tag)
                        .addToBackStack(title)
                        .commitAllowingStateLoss();
            }
        } else {
            try {
                fragmentManager.beginTransaction().replace(R.id.frame_fac_don_container, fragment, tag).commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction().replace(R.id.frame_fac_don_container, fragment, tag).commitAllowingStateLoss();
            }
        }

//        if (title != null) {
//            ActionBar actionBar = getSupportActionBar();
//            if (actionBar != null) {
//                actionBar.setTitle(title);
//            }
//        }
    }

    public void push(Fragment fragment) {
        if (fragment == null) {
            return;
        }
        push(fragment, null);
    }
}
