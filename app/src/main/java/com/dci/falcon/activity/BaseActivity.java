package com.dci.falcon.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.adapter.NavDrawerListViewAdapter;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.fragment.AlertDialogFragment;
import com.dci.falcon.model.Logout;
import com.dci.falcon.model.LogoutResponse;
import com.dci.falcon.model.NavDrawerItem;
import com.dci.falcon.model.QuickRegistration;
import com.dci.falcon.model.TrackRequest;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.UiUtils;
import com.dci.falcon.utills.Utils;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressWarnings("ResourceType")
public class BaseActivity extends AppCompatActivity {

    private static final float PICTURE_SIZE = 640;
    protected Handler mHandler = new Handler();
    protected ProgressDialog mDialog;
    public static final int ERROR_SHOW_TYPE_LOG = 0;
    public static final int ERROR_SHOW_TYPE_TOAST = 1;
    public static final int ERROR_SHOW_TYPE_DIALOG = 2;
    public static final String DIALOG_API_ERROR_TAG = "apiTag";
    private String[] mPlanetTitles;
    public ListView mDrawerList;
    private TypedArray mDrawerIcon;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListViewAdapter adapter;
    public DrawerLayout mDrawerLayout;
    public ActionBarDrawerToggle mDrawerToggle;
    public CoordinatorLayout coordinatorLayout;
    public FloatingActionButton floatingActionButton;
    public AdView adView;
    public TextView userName, bloodGroup;
    ViewGroup header;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    FalconAPI falconAPI;
    int REQUEST_STORAGE = 1;
    ImageView imageProPic;
    String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_SMS, Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
    UserError userError;
    LogoutResponse logoutResponse;
    FrameLayout frameLayout;
    private static final float END_SCALE = 0.7f;
    public static int currentPage=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        mDialog = new ProgressDialog(BaseActivity.this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermissions();
        } else {
            checkLocationPermission();
        }
        FalconApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        userError = new UserError();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_menu);
        frameLayout = (FrameLayout) findViewById(R.id.base_container);
        mDrawerList = (ListView) findViewById(R.id.nav_menu);
        adView = (AdView) findViewById(R.id.adView);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_base);
        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab_request_blood);
        mPlanetTitles = getResources().getStringArray(R.array.nav_listitem);
        mDrawerIcon = getResources().obtainTypedArray(R.array.nav_listicon);
        LayoutInflater nav_inflater = getLayoutInflater();
        header = (ViewGroup) nav_inflater.inflate(R.layout.nav_header, mDrawerList, false);
        userName = (TextView) header.findViewById(R.id.text_nav_header_user_name);
        bloodGroup = (TextView) header.findViewById(R.id.text_nav_header_blood_group);
        imageProPic = (ImageView) header.findViewById(R.id.image_profile_pic_nav);
        if (sharedPreferences.getString(FalconConstants.PROFILEPICTURE, "") != null) {
            picasoImageLoader(imageProPic, sharedPreferences.getString(FalconConstants.PROFILEPICTURE, ""), BaseActivity.this);
        } else {
            imageProPic.setImageResource(R.mipmap.img_sample_pro_pic);
        }
        userName.setText(sharedPreferences.getString(FalconConstants.USERNAME, "Falcon"));
        bloodGroup.setVisibility(View.VISIBLE);
        if (sharedPreferences.getInt(FalconConstants.ROLEID, 0) == 0) {
            bloodGroup.setText("Blood Group: " + sharedPreferences.getString(FalconConstants.BLOODGROUP, "Blood Group:"));
        } else {
            bloodGroup.setText(sharedPreferences.getString(FalconConstants.HOSPITALREGISTERID, ""));
        }
        mDrawerList.addHeaderView(header, null, false);
//        AdRequest adRequest = new AdRequest.Builder()
//                .build();
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                // Check the LogCat to get your test device ID
                .addTestDevice("1EA63B76DBC601B0CEF8C0145342AFEE")
                .build();
        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
//                Toast.makeText(getApplicationContext(), "Ad is loaded!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdClosed() {
//                Toast.makeText(getApplicationContext(), "Ad is closed!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
//                Toast.makeText(getApplicationContext(), "Ad failed to load! error code: " + errorCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
//                Toast.makeText(getApplicationContext(), "Ad left application!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
//                Toast.makeText(getApplicationContext(), "Ad is opened!", Toast.LENGTH_SHORT).show();
            }
        });

        switch (sharedPreferences.getInt(FalconConstants.ROLEID, 0)) {
            case 0:
                navDrawerItems = new ArrayList<NavDrawerItem>();
                //Home
                navDrawerItems.add(new NavDrawerItem(mPlanetTitles[0], mDrawerIcon.getResourceId(0, 0)));
                //Profile
                navDrawerItems.add(new NavDrawerItem(mPlanetTitles[1], mDrawerIcon.getResourceId(1, 0)));
//        <item>Track Request</item>
                navDrawerItems.add(new NavDrawerItem(mPlanetTitles[2], mDrawerIcon.getResourceId(2, 0)));
                //<item>History</item>

//                navDrawerItems.add(new NavDrawerItem(mPlanetTitles[3], mDrawerIcon.getResourceId(3, 0)));
//                        <item>FAQ</item>

                navDrawerItems.add(new NavDrawerItem(mPlanetTitles[4], mDrawerIcon.getResourceId(4, 0)));
//                        <item>Language</item>

//                navDrawerItems.add(new NavDrawerItem(mPlanetTitles[5], mDrawerIcon.getResourceId(5, 0)));
//                        <item>Make Donation</item>

//                navDrawerItems.add(new NavDrawerItem(mPlanetTitles[6], mDrawerIcon.getResourceId(6, 0)));
//                        <item>Share App</item>

                navDrawerItems.add(new NavDrawerItem(mPlanetTitles[7], mDrawerIcon.getResourceId(7, 0)));
//                        <item>Change Password</item>

                navDrawerItems.add(new NavDrawerItem(mPlanetTitles[8], mDrawerIcon.getResourceId(8, 0)));
//Quick Reg
                navDrawerItems.add(new NavDrawerItem(mPlanetTitles[9], mDrawerIcon.getResourceId(9, 0)));
//                        <item>Logout</item>

                navDrawerItems.add(new NavDrawerItem(mPlanetTitles[10], mDrawerIcon.getResourceId(10, 0)));

                break;
            case 1:
                navDrawerItems = new ArrayList<NavDrawerItem>();
                navDrawerItems.add(new NavDrawerItem(mPlanetTitles[0], mDrawerIcon.getResourceId(0, 0)));
                navDrawerItems.add(new NavDrawerItem(mPlanetTitles[1], mDrawerIcon.getResourceId(1, 0)));
                navDrawerItems.add(new NavDrawerItem(mPlanetTitles[2], mDrawerIcon.getResourceId(2, 0)));
//                navDrawerItems.add(new NavDrawerItem(mPlanetTitles[3], mDrawerIcon.getResourceId(3, 0)));
                navDrawerItems.add(new NavDrawerItem(mPlanetTitles[4], mDrawerIcon.getResourceId(4, 0)));
//                navDrawerItems.add(new NavDrawerItem(mPlanetTitles[5], mDrawerIcon.getResourceId(5, 0)));
                navDrawerItems.add(new NavDrawerItem(mPlanetTitles[7], mDrawerIcon.getResourceId(7, 0)));
                navDrawerItems.add(new NavDrawerItem(mPlanetTitles[8], mDrawerIcon.getResourceId(8, 0)));
                navDrawerItems.add(new NavDrawerItem(mPlanetTitles[10], mDrawerIcon.getResourceId(10, 0)));
                break;
        }

        adapter = new NavDrawerListViewAdapter(getApplicationContext(), navDrawerItems);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new BaseActivity.DrawerItemClickListener());
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);

            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (sharedPreferences.getString(FalconConstants.PROFILEPICTURE, "") != null) {
                    picasoImageLoader(imageProPic, sharedPreferences.getString(FalconConstants.PROFILEPICTURE, ""), BaseActivity.this);
                } else {
                    imageProPic.setImageResource(R.mipmap.img_sample_pro_pic);
                }
                userName.setText(sharedPreferences.getString(FalconConstants.USERNAME, "Falcon"));
                if (sharedPreferences.getInt(FalconConstants.ROLEID, 0) == 0) {
                    bloodGroup.setText("Blood Group: " + sharedPreferences.getString(FalconConstants.BLOODGROUP, "Blood Group:"));
                } else {
                    bloodGroup.setText(sharedPreferences.getString(FalconConstants.HOSPITALREGISTERID, ""));

                }


            }
        };
        mDrawerLayout.addDrawerListener(mDrawerToggle);

        mDrawerLayout.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
                                            @Override
                                            public void onDrawerSlide(View drawerView, float slideOffset) {
                                                frameLayout.setVisibility(slideOffset > 0 ? View.VISIBLE : View.GONE);

                                                // Scale the View based on current slide offset
                                                final float diffScaledOffset = slideOffset * (1 - END_SCALE);
                                                final float offsetScale = 1 - diffScaledOffset;
                                                frameLayout.setScaleX(offsetScale);
                                                frameLayout.setScaleY(offsetScale);

                                                // Translate the View, accounting for the scaled width
                                                final float xOffset = drawerView.getWidth() * slideOffset;
                                                final float xOffsetDiff = frameLayout.getWidth() * diffScaledOffset / 2;
                                                final float xTranslation = xOffset - xOffsetDiff;
                                                frameLayout.setTranslationX(xTranslation);
                                            }

                                            @Override
                                            public void onDrawerClosed(View drawerView) {
                                                frameLayout.setVisibility(View.GONE);
                                            }
                                        }
        );

    }

    @Override
    public void onBackPressed() {
        if (this.mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            this.mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onPause() {
        if (adView != null) {
            adView.pause();
        }
        if (mDialog != null) {
            mDialog.dismiss();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adView != null) {
            adView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (adView != null) {
            adView.destroy();
        }
        super.onDestroy();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Storage permission is enabled
            } else if (ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)
                    && ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                    || ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.READ_SMS)
                    || ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    || ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.CAMERA)) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(BaseActivity.this);
                alertDialog.setMessage(R.string.location_from_dialog);
                alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        requestPermission();
                    }
                });
                alertDialog.show();

            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(BaseActivity.this);
                alertDialog.setMessage(R.string.location_settings);
                alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", BaseActivity.this.getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);
                    }
                });
                alertDialog.show();
            }
        }

    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {


        @Override
        public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
            selectItem(position);
            switch (sharedPreferences.getInt(FalconConstants.ROLEID, 0)) {
                //Indiduval login
                case 0:
                    switch (position) {
                        case 1:
                            new Handler().postDelayed(new Runnable() {
                                public void run() {
                                    if (position!=currentPage){
                                        currentPage=position;
                                        Intent intentHome = new Intent(BaseActivity.this, HomeActivity.class);
                                        intentHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intentHome);
                                    } }
                            }, 100);


                            break;
                        case 2:
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (position!=currentPage){
                                        currentPage=position;
                                        Intent intentProfile = new Intent(BaseActivity.this, ProfileActivity.class);
                                        intentProfile.putExtra("isFromBaseActivity", true);
                                        intentProfile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intentProfile);
                                    }

                                }
                            }, 100);
                            break;
                        case 3:
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (position!=currentPage){
                                        currentPage=position;
                                        Intent intentProfile = new Intent(BaseActivity.this, TrackRequestActivity.class);
                                        intentProfile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intentProfile);
                                    }

                                }
                            }, 100);
                            break;
                        case 4:
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
//                                    if (position!=currentPage){
//                                        currentPage=position;
//                                        Intent intentProfile = new Intent(BaseActivity.this, HistoryActivity.class);
//                                        intentProfile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                        startActivity(intentProfile);
//                                    }
                                    if (position!=currentPage){
                                        currentPage=position;
                                        Intent intentProfile = new Intent(BaseActivity.this, FAQActivity.class);
                                        intentProfile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intentProfile);
                                    }

                                }
                            }, 100);

                            break;
                        case 5:
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Intent i = new Intent(Intent.ACTION_SEND);
                                        i.setType("text/plain");
                                        i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                                        String sAux = "\nLet me recommend you this application\n";
                                        sAux = sAux + "https://play.google.com \n\n";
                                        i.putExtra(Intent.EXTRA_TEXT, sAux);
                                        startActivity(Intent.createChooser(i, "Choose one"));
                                    } catch (Exception e) {
                                        //e.toString();
                                    }

//                                    if (position!=currentPage){
//                                        currentPage=position;
//                                        Intent intentProfile = new Intent(BaseActivity.this, FAQActivity.class);
//                                        intentProfile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                        startActivity(intentProfile);
//                                    }

                                }
                            }, 100);
                            break;
                        case 6:
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
//                                    if (position!=currentPage){
//                                        currentPage=position;
//                                        Intent intentProfile = new Intent(BaseActivity.this, LanguageActivity.class);
//                                        intentProfile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                        startActivity(intentProfile);
//                                    }
                                    if (position!=currentPage){
                                        currentPage=position;
                                        Intent intentProfile = new Intent(BaseActivity.this, ChangePasswordActivity.class);
                                        intentProfile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intentProfile);
                                    }

                                }
                            }, 100);
                            break;
                        case 7:
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
//                                    if (position!=currentPage){
//                                        currentPage=position;
//                                        Intent intentProfile = new Intent(BaseActivity.this, MakeDonationActivity.class);
//                                        intentProfile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                        startActivity(intentProfile);
//                                    }

                                    if (position!=currentPage){
                                        currentPage=position;
                                        Intent intentProfile = new Intent(BaseActivity.this, QuickRegistrationActivity.class);
                                        intentProfile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intentProfile);
                                    }

                                }
                            }, 100);
                            break;
                        case 8:
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
//                                    try {
//                                        Intent i = new Intent(Intent.ACTION_SEND);
//                                        i.setType("text/plain");
//                                        i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
//                                        String sAux = "\nLet me recommend you this application\n";
//                                        sAux = sAux + "https://play.google.com/store/apps/details?id=Orion.Soft \n\n";
//                                        i.putExtra(Intent.EXTRA_TEXT, sAux);
//                                        startActivity(Intent.createChooser(i, "Choose one"));
//                                    } catch (Exception e) {
//                                        //e.toString();
//                                    }
                                    AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(BaseActivity.this);
                                    alertDialog1.setMessage(getString(R.string.alert_log_out));
                                    alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            logout(position);

                                        }
                                    });
                                    alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                                    alertDialog1.show();
                                }
                            }, 100);
                            break;
                        case 9:
//                            new Handler().postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//                                    if (position!=currentPage){
//                                        currentPage=position;
//                                        Intent intentProfile = new Intent(BaseActivity.this, ChangePasswordActivity.class);
//                                        intentProfile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                        startActivity(intentProfile);
//                                    }
//
//                                }
//                            }, 100);
                            break;
                        case 10:
//                            new Handler().postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//                                    if (position!=currentPage){
//                                        currentPage=position;
//                                        Intent intentProfile = new Intent(BaseActivity.this, QuickRegistrationActivity.class);
//                                        intentProfile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                        startActivity(intentProfile);
//                                    }
//
//                                }
//                            }, 100);
                            break;
                        case 11:
//                            new Handler().postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//                                    AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(BaseActivity.this);
//                                    alertDialog1.setMessage(getString(R.string.alert_log_out));
//                                    alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialogInterface, int i) {
//                                            logout(position);
//
//                                        }
//                                    });
//                                    alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialogInterface, int i) {
//                                            dialogInterface.dismiss();
//                                        }
//                                    });
//                                    alertDialog1.show();
//                                }
//                            }, 100);
                            break;
                    }
                    break;
                case 1:

                    switch (position) {
                        case 1:
                            new Handler().postDelayed(new Runnable() {
                                public void run() {
                                    if (position!=currentPage){
                                        currentPage=position;
                                        Intent intentHome = new Intent(BaseActivity.this, HomeActivity.class);
                                        intentHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intentHome);
                                    }

                                }
                            }, 100);

                            break;
                        case 2:
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (position!=currentPage){
                                        currentPage=position;
                                        Intent intentProfile = new Intent(BaseActivity.this, HospitalProfileActivity.class);
                                        intentProfile.putExtra("isFromBaseActivity", true);
                                        intentProfile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intentProfile);
                                    }

                                }
                            }, 100);
                            break;
                        case 3:

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (position!=currentPage){
                                        currentPage=position;
                                        Intent intentProfile = new Intent(BaseActivity.this, TrackRequestActivity.class);
                                        intentProfile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intentProfile);
                                    }

                                }
                            }, 100);
                            break;
                        case 4:
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
//                                    if (position!=currentPage){
//                                        currentPage=position;
//                                        Intent intentProfile = new Intent(BaseActivity.this, HistoryActivity.class);
//                                        intentProfile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                        startActivity(intentProfile);
//                                    }
                                    if (position!=currentPage){
                                        currentPage=position;
                                        Intent intentProfile = new Intent(BaseActivity.this, FAQActivity.class);
                                        intentProfile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intentProfile);
                                    }

                                }
                            }, 100);
                            break;
                        case 5:
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
//                                    if (position!=currentPage){
//                                        currentPage=position;
//                                        Intent intentProfile = new Intent(BaseActivity.this, FAQActivity.class);
//                                        intentProfile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                        startActivity(intentProfile);
//                                    }
                                    try {
                                        Intent i = new Intent(Intent.ACTION_SEND);
                                        i.setType("text/plain");
                                        i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                                        String sAux = "\nLet me recommend you this application\n\n";
                                        sAux = sAux + "https://play.google.com \n\n";
                                        i.putExtra(Intent.EXTRA_TEXT, sAux);
                                        startActivity(Intent.createChooser(i, "Choose one"));
                                    } catch (Exception e) {
                                        //e.toString();
                                    }

                                }
                            }, 100);
                        case 6:
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
//                                    if (position!=currentPage){
//                                        currentPage=position;
//                                        Intent intentProfile = new Intent(BaseActivity.this, LanguageActivity.class);
//                                        intentProfile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                        startActivity(intentProfile);
//                                    }

                                    if (position!=currentPage){
                                        currentPage=position;
                                        Intent intentProfile = new Intent(BaseActivity.this, ChangePasswordActivity.class);
                                        intentProfile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intentProfile);
                                    }
                                }
                            }, 100);
                            break;
                        case 7:

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
//                                    try {
//                                        Intent i = new Intent(Intent.ACTION_SEND);
//                                        i.setType("text/plain");
//                                        i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
//                                        String sAux = "\nLet me recommend you this application\n\n";
//                                        sAux = sAux + "https://play.google.com \n\n";
//                                        i.putExtra(Intent.EXTRA_TEXT, sAux);
//                                        startActivity(Intent.createChooser(i, "Choose one"));
//                                    } catch (Exception e) {
//                                        //e.toString();
//                                    }
                                    AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(BaseActivity.this);
                                    alertDialog1.setMessage(getString(R.string.alert_log_out));
                                    alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            logout(position);

                                        }
                                    });
                                    alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                                    alertDialog1.show();
                                }
                            }, 100);
                            break;
                        case 8:
//                            new Handler().postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//                                    if (position!=currentPage){
//                                        currentPage=position;
//                                        Intent intentProfile = new Intent(BaseActivity.this, ChangePasswordActivity.class);
//                                        intentProfile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                        startActivity(intentProfile);
//                                    }
//
//                                }
//                            }, 100);
                            break;
                        case 9:
//                            new Handler().postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//                                    AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(BaseActivity.this);
//                                    alertDialog1.setMessage(getString(R.string.alert_log_out));
//                                    alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialogInterface, int i) {
//                                            logout(position);
//
//                                        }
//                                    });
//                                    alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialogInterface, int i) {
//                                            dialogInterface.dismiss();
//                                        }
//                                    });
//                                    alertDialog1.show();
//                                }
//                            }, 100);
                            break;

                    }

                    break;
            }


        }
    }



    private void logout(final int position) {
        Logout logout = new Logout();

        if (sharedPreferences.getInt(FalconConstants.ROLEID, 0) == 0) {
            logout.setId(sharedPreferences.getInt(FalconConstants.USEDID, 0));
            logout.setType(1);
        } else {
            logout.setId(sharedPreferences.getInt(FalconConstants.HOSPITALUSERID, 0));
            logout.setType(2);

        }
        if (Utils.isNetworkAvailable()) {
            showProgress();
            falconAPI.logout(logout).enqueue(new Callback<LogoutResponse>() {
                @Override
                public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {
                    hideProgress();
                    logoutResponse = response.body();
                    if (response.isSuccessful() && response.body() != null && logoutResponse.getStatusCode() == 200) {
                        if (position!=currentPage){
                            currentPage=1;
                            Intent intent_admin_logout = new Intent(BaseActivity.this, LoginActivity.class);
                            intent_admin_logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            finish();
                            startActivity(intent_admin_logout);
                            editor.putInt(FalconConstants.LOGINSTATUSCODE, 0).commit();
                            Toast.makeText(BaseActivity.this, getString(R.string.logged_out), Toast.LENGTH_SHORT).show();
                        }


                    } else {
                        Toast.makeText(BaseActivity.this, getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<LogoutResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(BaseActivity.this, getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                }
            });


        } else {
            userError.message = getString(R.string.network_error_message);
            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }
    }

    private void selectItem(int position) {
        // Highlight the selected item, update the title, and close the drawer
        mDrawerList.setItemChecked(position, true);
        // setTitle(mPlanetTitles[position]);
        mDrawerLayout.closeDrawer(mDrawerList);


    }

    // API Call handling
    public void showProgress() {
        showProgress(R.string.loading);
    }

    public void showProgress(int messageId) {
        showProgress(getString(messageId));
    }

    public void showProgress(final CharSequence message) {

        if (getApplicationContext() != null) {
            mHandler.post(new Runnable() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void run() {
                    UiUtils.hideKeyboard(BaseActivity.this);
                    mDialog.getWindow().setBackgroundDrawable(new
                            ColorDrawable(android.graphics.Color.TRANSPARENT));
                    mDialog.setCancelable(false);
                    mDialog.setIndeterminate(true);
                    mDialog.show();
                    mDialog.setContentView(R.layout.custom_progress_view);
                }
            });
        }

    }

    public void hideProgress() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mDialog.dismiss();
            }
        });
    }

    public void showError(int showType, UserError error) {
        switch (showType) {
            case ERROR_SHOW_TYPE_LOG:
                Log.e("BaseActivity", "Error title : " + error.title + "; Message : " + error.message);
                break;
            case ERROR_SHOW_TYPE_TOAST:
                if (!TextUtils.isEmpty(error.message)) {
                    Toast.makeText(this, error.message, Toast.LENGTH_LONG).show();
                }
                break;
            case ERROR_SHOW_TYPE_DIALOG:
                dismissDialogFragment(DIALOG_API_ERROR_TAG);
                getSupportFragmentManager().beginTransaction()
                        .add(AlertDialogFragment.newOkDialog(error.title, error.message), DIALOG_API_ERROR_TAG)
                        .commitAllowingStateLoss();
                break;
        }
    }

    public void dismissDialogFragment(String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        DialogFragment fragment = (DialogFragment) fragmentManager.findFragmentByTag(tag);
        if (fragment != null && fragment.isVisible()) {
            fragment.dismissAllowingStateLoss();
        }
    }

    public void popBackStackImmediate() {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack();
        fragmentManager.executePendingTransactions();
    }

    public void popAllBackstack() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        int backCount = fragmentManager.getBackStackEntryCount();
        if (backCount > 0) {
            fragmentManager.popBackStackImmediate(fragmentManager.getBackStackEntryAt(0).getId(),
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    public void push(Fragment fragment, String title) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        String tag = fragment.getClass().getCanonicalName();

        if (title != null) {
            try {
                fragmentManager.beginTransaction()
                        .replace(R.id.base_container, fragment, tag)
                        .addToBackStack(title)
                        .commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction()
                        .replace(R.id.base_container, fragment, tag)
                        .addToBackStack(title)
                        .commitAllowingStateLoss();
            }
        } else {
            try {
                fragmentManager.beginTransaction().replace(R.id.base_container, fragment, tag).commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction().replace(R.id.base_container, fragment, tag).commitAllowingStateLoss();
            }
        }

//        if (title != null) {
//            ActionBar actionBar = getSupportActionBar();
//            if (actionBar != null) {
//                actionBar.setTitle(title);
//            }
//        }
    }

    public void push(android.support.v4.app.Fragment fragment) {
        if (fragment == null) {
            return;
        }
        push(fragment, null);
    }


    private void checkPermissions() {
        if (!hasPermissionGranted()) {
            requestPermission();
        }
    }

    public boolean hasPermissionGranted() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    public boolean hasLocationPermissionGranted() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, REQUEST_STORAGE);
        }
    }

    public void checkLocationPermission() {
        if (this != null && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
    }

    public static void setViewAndChildrenEnabled(View view, boolean enabled) {
        view.setEnabled(enabled);
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View child = viewGroup.getChildAt(i);
                setViewAndChildrenEnabled(child, enabled);
            }
        }
    }

    public String getBase64Image(Bitmap bitmap) {
        Matrix matrix = new Matrix();
        matrix.setRectToRect(new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight()),
                new RectF(0, 0, PICTURE_SIZE, PICTURE_SIZE),
                Matrix.ScaleToFit.CENTER);
        Bitmap scaledBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        byte[] bytes = stream.toByteArray();
        return Base64.encodeToString(bytes, Base64.NO_WRAP);
    }

    public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    public void picasoImageLoader(ImageView imageView, String url, Context context) {
        Picasso.with(context)
                .load(getString(R.string.falcon_image_url) + url)
                .placeholder(R.mipmap.img_sample_pro_pic)   // optional
//                                    .error(R.drawable.ic_error_fallback)      // optional
//                                    .resize(250, 200)                        // optional
//                                    .rotate(90)                             // optional
                .into(imageView);
    }

    public void universalImageLoader(ImageView imageView, String url) {
        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisk(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(R.mipmap.img_sample_pro_pic)
                .showImageOnFail(R.mipmap.img_sample_pro_pic)
                .showImageOnLoading(R.mipmap.img_sample_pro_pic).build();
        imageLoader.displayImage(getString(R.string.falcon_image_url) + url, imageView, options);
    }
}
