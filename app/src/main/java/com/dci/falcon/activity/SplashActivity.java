package com.dci.falcon.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.dci.falcon.R;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.view.CustomTextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by vijayaganesh on 10/19/2017.
 */

public class SplashActivity extends Activity {

    @Inject
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        FalconApplication.getContext().getComponent().inject(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for sInstance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                    switch (sharedPreferences.getInt(FalconConstants.LOGINSTATUSCODE, 0)) {
                        case 0:
                            Intent intentLogin = new Intent(SplashActivity.this, LoginActivity.class);
                            startActivity(intentLogin);
                            break;
                        case 1:
                            Intent intentProfile = new Intent(SplashActivity.this, ProfileActivity.class);
                            startActivity(intentProfile);
                            break;
                        case 2:
                            Intent intentHome = new Intent(SplashActivity.this, HomeActivity.class);
                            startActivity(intentHome);
                            break;
                    }

//                    Intent intent = new Intent(SplashActivity.this, ProfileActivity.class);
//                    startActivity(intent);
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }).start();
    }
}
