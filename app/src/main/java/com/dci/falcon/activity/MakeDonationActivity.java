package com.dci.falcon.activity;

import android.content.Context;

import instamojo.library.InstapayListener;
import instamojo.library.InstamojoPay;
import instamojo.library.Config;

import android.content.IntentFilter;
import android.app.Activity;

import org.json.JSONObject;
import org.json.JSONException;

import android.widget.Toast;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;

import com.dci.falcon.R;
import com.dci.falcon.fragment.MakeDonationFragment;
import com.dci.falcon.fragment.RequestBloodFragment;

import butterknife.ButterKnife;

/**
 * Created by vijayaganesh on 11/10/2017.
 */

public class MakeDonationActivity extends BaseActivity {

    InstamojoPay instamojoPay;

    private void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername) {
//        final Activity activity = this;
        instamojoPay = new InstamojoPay();
        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", purpose);
            pay.put("amount", amount);
            pay.put("name", buyername);
            pay.put("send_sms", true);
            pay.put("send_email", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(MakeDonationActivity.this, pay, listener);
    }

    InstapayListener listener;


    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {
                Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG)
                        .show();
            }

            @Override
            public void onFailure(int code, String reason) {
                Toast.makeText(getApplicationContext(), "Failed: " + reason, Toast.LENGTH_LONG)
                        .show();
            }
        };
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(instamojoPay);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_make_donation, null, false);
        ButterKnife.bind(this, contentView);
        mDrawerLayout.addView(contentView, 0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

//        push(new MakeDonationFragment());
        callInstamojoPay("vijayaganesh.jeevanandham@dci.in", "8870410990", "10", "official", "vijay");
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        BaseActivity.currentPage = 1;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

//    public void push(Fragment fragment, String title) {
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        String tag = fragment.getClass().getCanonicalName();
//
//        if (title != null) {
//            try {
//                fragmentManager.beginTransaction()
//                        .replace(R.id.frame_make_donation_container, fragment, tag)
//                        .addToBackStack(title)
//                        .commit();
//            } catch (IllegalStateException ile) {
//                fragmentManager.beginTransaction()
//                        .replace(R.id.frame_make_donation_container, fragment, tag)
//                        .addToBackStack(title)
//                        .commitAllowingStateLoss();
//            }
//        } else {
//            try {
//                fragmentManager.beginTransaction().replace(R.id.frame_make_donation_container, fragment, tag).commit();
//            } catch (IllegalStateException ile) {
//                fragmentManager.beginTransaction().replace(R.id.frame_make_donation_container, fragment, tag).commitAllowingStateLoss();
//            }
//        }
//
////        if (title != null) {
////            ActionBar actionBar = getSupportActionBar();
////            if (actionBar != null) {
////                actionBar.setTitle(title);
////            }
////        }
//    }
//
//    public void push(Fragment fragment) {
//        if (fragment == null) {
//            return;
//        }
//        push(fragment, null);
//    }
}
