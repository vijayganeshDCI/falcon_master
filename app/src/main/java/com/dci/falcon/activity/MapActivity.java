package com.dci.falcon.activity;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.widget.FrameLayout;

import com.dci.falcon.R;
import com.dci.falcon.fragment.FalconMapFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MapActivity extends BaseActivity {
    @BindView(R.id.frame_map_container)
    FrameLayout frameMapContainer;
    @BindView(R.id.cons_map)
    ConstraintLayout consMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        ButterKnife.bind(this);
        setTitle(R.string.Find_route);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        push(new FalconMapFragment());
    }

    public void push(Fragment fragment, String title) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        String tag = fragment.getClass().getCanonicalName();

        if (title != null) {
            try {
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_map_container, fragment, tag)
                        .addToBackStack(title)
                        .commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_map_container, fragment, tag)
                        .addToBackStack(title)
                        .commitAllowingStateLoss();
            }
        } else {
            try {
                fragmentManager.beginTransaction().replace(R.id.frame_map_container, fragment, tag).commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction().replace(R.id.frame_map_container, fragment, tag).commitAllowingStateLoss();
            }
        }

//        if (title != null) {
//            ActionBar actionBar = getSupportActionBar();
//            if (actionBar != null) {
//                actionBar.setTitle(title);
//            }
//        }
    }

    public void push(Fragment fragment) {
        if (fragment == null) {
            return;
        }
        push(fragment, null);
    }
}
