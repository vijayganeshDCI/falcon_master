package com.dci.falcon.firebase;

import android.content.SharedPreferences;
import android.widget.Toast;

import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.utills.FalconConstants;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import javax.inject.Inject;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by vijayaganesh on 11/13/2017.
 */

public class FirebaseInstanceID extends FirebaseInstanceIdService {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        //Getting registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        editor.putString(FalconConstants.FCMTOKEN,refreshedToken).commit();
//        fcmDeviceKey=refreshedToken;
        System.out.println("==========" + refreshedToken + "=======");

    }

    @Override
    public void onCreate() {
        super.onCreate();
        sharedPreferences=getSharedPreferences(FalconConstants.FCMKEYSHAREDPERFRENCES,MODE_PRIVATE);
        editor=sharedPreferences.edit();
    }
}
