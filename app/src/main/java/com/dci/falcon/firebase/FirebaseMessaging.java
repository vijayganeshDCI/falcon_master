package com.dci.falcon.firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Config;
import android.util.Log;

import com.dci.falcon.R;
import com.dci.falcon.activity.NotificationActivity;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import static com.nostra13.universalimageloader.core.ImageLoader.TAG;

/**
 * Created by vijayaganesh on 11/13/2017.
 */

public class FirebaseMessaging extends com.google.firebase.messaging.FirebaseMessagingService {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    public static String notifyPage="1";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        //Calling method to generate notification
        sendNotification(remoteMessage);


    }


    //This method is only generating push notification
    //It is same as we did in earlier posts
    private void sendNotification(RemoteMessage remoteMessage) {


        Intent intentNo = new Intent("notificationListener");
        intentNo.putExtra("notify", true);
        sendBroadcast(intentNo);

        notifyPage = remoteMessage.getData().get("id");
        Intent intent = new Intent(this, NotificationActivity.class);
        intent.putExtra("notificationType", remoteMessage.getData().get("id"));

        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.icon_notification_small);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent,
                PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.notification_item);
//        contentView.setTextViewText(R.id.text_notification_title,messageBody.getTitle());
//        contentView.setTextViewText(R.id.text_notification_subtitle,messageBody.getBody());
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setLargeIcon(largeIcon)
                .setSmallIcon(R.mipmap.icon_notification_small)
                .setContentTitle(remoteMessage.getData().get("title"))
                .setContentText(remoteMessage.getData().get("body"))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        int notificationID = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        notificationManager.notify(notificationID, notificationBuilder.build());
    }


    @Override
    public void onCreate() {
        super.onCreate();

    }
}
