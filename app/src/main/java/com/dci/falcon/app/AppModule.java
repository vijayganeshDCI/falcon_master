package com.dci.falcon.app;


import android.content.Context;
import android.content.SharedPreferences;

import com.dci.falcon.retrofit.FalconAPI;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;


//provides instance or objects for a class
//because of this no need to create objects by using new keyword
@Module
public class AppModule {

    public static final String FALCON_PREFS = "Falcon";

    private final FalconApplication fApp;

    public AppModule(FalconApplication app) {
        this.fApp = app;
    }
    //provides dependencies (return application class objects)
    @Provides
    @Singleton
    Context provideApplicationContext() {
        return fApp;
    }

    //provides dependencies (return sharedprference objects)
    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences() {
        return fApp.getSharedPreferences(FALCON_PREFS, MODE_PRIVATE);
    }

    //provides dependencies (return API class objects)
    @Provides
    public FalconAPI provideFalconApiInterface(Retrofit retrofit) {
        return retrofit.create(FalconAPI.class);
    }


}
