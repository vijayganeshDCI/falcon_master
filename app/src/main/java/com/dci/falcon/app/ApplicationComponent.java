package com.dci.falcon.app;


import android.content.SharedPreferences;

import com.dci.falcon.activity.BaseActivity;
import com.dci.falcon.activity.ChangePasswordActivity;
import com.dci.falcon.activity.LoginActivity;
import com.dci.falcon.activity.NotificationActivity;
import com.dci.falcon.activity.ProfileActivity;
import com.dci.falcon.activity.SplashActivity;
import com.dci.falcon.adapter.AuthorizerRequestAdapter;
import com.dci.falcon.adapter.BloodRequestListAdapter;
import com.dci.falcon.adapter.HistoryAdapter;
import com.dci.falcon.adapter.RespondRequestAdapter;
import com.dci.falcon.adapter.TrackMyRequestAdapter;
import com.dci.falcon.adapter.AcceptedRequestAdapter;
import com.dci.falcon.adapter.ViewMemberInfoAdapter;
import com.dci.falcon.fragment.AcceptRequestFragment;
import com.dci.falcon.fragment.AddMemberFragment;
import com.dci.falcon.fragment.AddtionalUsersFragment;
import com.dci.falcon.fragment.AuthorizerRequestFragment;
import com.dci.falcon.fragment.BloodRequestListFragment;
import com.dci.falcon.fragment.ChangeNumberFragment;
import com.dci.falcon.fragment.DashBoardFragment;
import com.dci.falcon.fragment.DonationFragment;
import com.dci.falcon.fragment.FAQFragment;
import com.dci.falcon.fragment.FacililatedDonationFragment;
import com.dci.falcon.fragment.FamilyMembersDonationsFragment;
import com.dci.falcon.fragment.FacililatedDonationDetailFragment;
import com.dci.falcon.fragment.FalconMapFragment;
import com.dci.falcon.fragment.ForgetPasswordFragment;
import com.dci.falcon.fragment.ForgetPasswordHospFragment;
import com.dci.falcon.fragment.HistoryFragment;
import com.dci.falcon.fragment.HospitalProfileFragment;
import com.dci.falcon.fragment.LanguageFragment;
import com.dci.falcon.fragment.LocationFragment;
import com.dci.falcon.fragment.LoginFragment;
import com.dci.falcon.fragment.NotificationFragment;
import com.dci.falcon.fragment.OtpFragment;
import com.dci.falcon.fragment.OtpVerificationFragment;
import com.dci.falcon.fragment.QuickRegistrationFragment;
import com.dci.falcon.fragment.QuickRequestFragment;
import com.dci.falcon.fragment.RequestBloodFragment;
import com.dci.falcon.fragment.ResetPasswordIndiduvalUserFragment;
import com.dci.falcon.fragment.RespondRequestFragment;
import com.dci.falcon.fragment.SequrityQuestionFragment;
import com.dci.falcon.fragment.TrackMyRequestDetailFragment;
import com.dci.falcon.fragment.TrackMyRequestFragment;
import com.dci.falcon.fragment.UserActivityFragment;
import com.dci.falcon.fragment.UserDetailFragment;
import com.dci.falcon.fragment.UserProfileSelectionFragment;
import com.dci.falcon.fragment.ViewInformationFragment;
import com.dci.falcon.fragment.UpdateRequestStatusFragment;
import com.dci.falcon.retrofit.RetrofitModule;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;


@Singleton
// modules for perform dependency injection on below classes
@Component(modules = {AppModule.class, RetrofitModule.class})
public interface ApplicationComponent {

    void inject(FalconApplication application);

    SharedPreferences sharedPreferences();

    Retrofit retrofit();

    void inject(BaseActivity baseActivity);

    void inject(LoginActivity loginActivity);

    void inject(ProfileActivity profileActivity);

    void inject(SplashActivity splashActivity);

    void inject(NotificationActivity notificationActivity);

    void inject(ChangePasswordActivity changePasswordActivity);

    void inject(OtpVerificationFragment otpVerificationFragment);

    void inject(FalconMapFragment falconMapFragment);

    void inject(UserDetailFragment profileFragment);

    void inject(UserProfileSelectionFragment userProfileSelectionFragment);

    void inject(LoginFragment loginFragment);

    void inject(DashBoardFragment dashBoardFragment);

    void inject(LanguageFragment languageFragment);

    void inject(HospitalProfileFragment hospitalProfileFragment);

    void inject(OtpFragment otpFragment);

    void inject(ChangeNumberFragment changeNumberFragment);

    void inject(FAQFragment faqFragment);

    void inject(SequrityQuestionFragment sequrityQuestionFragment);

    void inject(ResetPasswordIndiduvalUserFragment resetPasswordFragment);

    void inject(ForgetPasswordFragment forgetPasswordFragment);

    void inject(AddMemberFragment addMemberFragment);


    void inject(RequestBloodFragment requestBloodFragment);

    void inject(QuickRequestFragment quickRequestFragment);

    void inject(ViewInformationFragment viewInformationFragment);

    void inject(NotificationFragment notificationFragment);

    void inject(HistoryFragment historyFragment);

    void inject(TrackMyRequestFragment trackMyRequestFragment);

    void inject(AuthorizerRequestFragment authorizerRequestFragment);

    void inject(AuthorizerRequestAdapter authorizerRequestAdapter);

    void inject(RespondRequestFragment respondRequestFragment);

    void inject(AcceptRequestFragment acceptRequestFragment);

    void inject(UpdateRequestStatusFragment updateRequestStatusFragment);

    void inject(RespondRequestAdapter respondRequestAdapter);

    void inject(ViewMemberInfoAdapter viewMemberInfoAdapter);

    void inject(LocationFragment locationFragment);

    void inject(DonationFragment donationFragment);

    void inject(AcceptedRequestAdapter acceptedRequestAdapter);

    void inject(ForgetPasswordHospFragment forgetPasswordHospFragment);

    void inject(QuickRegistrationFragment quickRegistrationFragment);

    void inject(UserActivityFragment userActivityFragment);

    void inject(TrackMyRequestAdapter trackMyRequestAdapter);

    void inject(TrackMyRequestDetailFragment trackMyRequestDetailFragment);

    void inject(FamilyMembersDonationsFragment familyMembersDonationsFragment);

    void inject(FacililatedDonationDetailFragment facililatedDonationDetailFragment);

    void inject(HistoryAdapter historyAdapter);

    void inject(AddtionalUsersFragment addtionalUsersFragment);

    void inject(FacililatedDonationFragment facililatedDonationFragment);

    void inject(BloodRequestListAdapter bloodRequestListAdapter);
    void inject(BloodRequestListFragment bloodRequestListFragment);
}
