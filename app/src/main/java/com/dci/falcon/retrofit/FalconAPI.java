package com.dci.falcon.retrofit;


import com.dci.falcon.model.AcceptRequest;
import com.dci.falcon.model.AddMember;
import com.dci.falcon.model.AddMemberResponse;
import com.dci.falcon.model.AdditionalUsersResponse;
import com.dci.falcon.model.AuthorizerRequestUpdate;
import com.dci.falcon.model.AuthorizerRequestUpdateResponse;
import com.dci.falcon.model.ChangeHospPassword;
import com.dci.falcon.model.ChangeNumber;
import com.dci.falcon.model.ChangeNumberResponse;
import com.dci.falcon.model.CheckQandA;
import com.dci.falcon.model.CheckSeqQandAResponse;
import com.dci.falcon.model.CloseRequirement;
import com.dci.falcon.model.FacilitatedDonationDetailResponse;
import com.dci.falcon.model.ForgetPassSequrityQuestionResponse;
import com.dci.falcon.model.GetAddMemberResponse;
import com.dci.falcon.model.GetFamilyMemberResponse;
import com.dci.falcon.model.GetHospitalInfo;
import com.dci.falcon.model.GetIndiUserResponse;
import com.dci.falcon.model.GetUserInfo;
import com.dci.falcon.model.HistoryResponse;
import com.dci.falcon.model.HospitalHistoryResponse;
import com.dci.falcon.model.HospitalLogin;
import com.dci.falcon.model.HospitalLoginResponse;
import com.dci.falcon.model.HospitalRegisterResponse;
import com.dci.falcon.model.HospitalRegister;
import com.dci.falcon.model.IndiRegNonDonorResponse;
import com.dci.falcon.model.IndiUpdateNonDonorResponse;
import com.dci.falcon.model.IndiUserUpdateResponse;
import com.dci.falcon.model.IndiduvalLogin;
import com.dci.falcon.model.IndiduvalLoginResponse;
import com.dci.falcon.model.IndiduvalRegister;
import com.dci.falcon.model.IndiduvalRegisterResponse;
import com.dci.falcon.model.Logout;
import com.dci.falcon.model.LogoutResponse;
import com.dci.falcon.model.MemberDelete;
import com.dci.falcon.model.NotificationUpdate;
import com.dci.falcon.model.NotificationUpdateResponse;
import com.dci.falcon.model.OrgResponse;
import com.dci.falcon.model.Phonenumber;
import com.dci.falcon.model.QuickRegistrationResponse;
import com.dci.falcon.model.RejectRequest;
import com.dci.falcon.model.RequestBlood;
import com.dci.falcon.model.RequestBloodResponse;
import com.dci.falcon.model.RequestList;
import com.dci.falcon.model.RequestListResponse;
import com.dci.falcon.model.ResetPassword;
import com.dci.falcon.model.ResetPasswordResponse;
import com.dci.falcon.model.AuthorizerRequestResponse;
import com.dci.falcon.model.RespondReqResponse;
import com.dci.falcon.model.SecurityQuestionResponse;
import com.dci.falcon.model.SequrityQuestion;
import com.dci.falcon.model.StaticPage;
import com.dci.falcon.model.StaticPageResponse;
import com.dci.falcon.model.TrackMyRequestDetailResponse;
import com.dci.falcon.model.TrackMyRequestHospDetailResponse;
import com.dci.falcon.model.TrackMyRequestHospResponse;
import com.dci.falcon.model.TrackMyRequestResponse;
import com.dci.falcon.model.TrackRequest;
import com.dci.falcon.model.AcceptRequestResponse;
import com.dci.falcon.model.UniqueNumber;
import com.dci.falcon.model.UniqueNumberResponse;
import com.dci.falcon.model.UpdateFcmToken;
import com.dci.falcon.model.UpdateMemberResponse;
import com.dci.falcon.model.UpdateRespondRequest;
import com.dci.falcon.model.UpdateRespondRequestResponse;
import com.dci.falcon.model.UserActivityUpdate;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;


public interface FalconAPI {


    @POST("hospitalinfo")
    public Call<HospitalRegisterResponse> registerHospitalUser(@Body HospitalRegister hospitalRegister);

    @POST("hosplogin")
    public Call<HospitalLoginResponse> loginHospitalUser(@Body HospitalLogin hospitalLogin);

    @POST("secques")
    public Call<SecurityQuestionResponse> sequrityQuestions();

    @POST("organization")
    public Call<OrgResponse> orgnaizationList();

    @POST("userinfo")
    public Call<IndiduvalRegisterResponse> registerIndiduvalUser(@Body IndiduvalRegister indiduvalRegister);

    @POST("userinfo")
    public Call<IndiRegNonDonorResponse> registerIndiduvalNonDonorUser(@Body IndiduvalRegister indiduvalRegister);

    @POST("userinfo")
    public Call<QuickRegistrationResponse> quickRegistration(@Body IndiduvalRegister indiduvalRegister);

    @POST("userinfoedit")
    public Call<IndiUserUpdateResponse> editUserInfo(@Body IndiduvalRegister indiduvalRegister);

    @POST("userinfoedit")
    public Call<IndiUpdateNonDonorResponse> editNonDonorUserInfo(@Body IndiduvalRegister indiduvalRegister);


    @POST("userlogin")
    public Call<IndiduvalLoginResponse> loginIndiduvalUser(@Body IndiduvalLogin indiduvalLogin);

    @POST("updatephoneno")
    public Call<ChangeNumberResponse> changeNumber(@Body ChangeNumber changeNumber);

    @POST("getstaticcontent")
    public Call<StaticPageResponse> getStaticPage(@Body StaticPage staticPage);


    @POST("getsecques")
    public Call<ForgetPassSequrityQuestionResponse> getSequrityQuestion(@Body SequrityQuestion sequrityQuestion);

    @POST("checksecans")
    public Call<CheckSeqQandAResponse> checkSeqQandA(@Body CheckQandA checkQandA);


    @POST("checkUniqueMobile")
    public Call<UniqueNumberResponse> checkUniqueNumber(@Body UniqueNumber uniqueNumber);

    @POST("updatepassword")
    public Call<ResetPasswordResponse> resetPassword(@Body ResetPassword resetPassword);

    @POST("membercreate")
    public Call<AddMemberResponse> addMember(@Body AddMember addMember);

    @POST("memberdelete")
    public Call<Object> memberDelete(@Body MemberDelete memberDelete);

    @POST("memberupdate")
    public Call<UpdateMemberResponse> memberEdit(@Body AddMember addMember);

    @POST("memberDetail")
    public Call<GetAddMemberResponse> memberInfo(@Body MemberDelete memberDelete);

    @POST("getUserProfile")
    public Call<GetIndiUserResponse> getUserProfile(@Body GetUserInfo getUserInfo);


    @POST("bloodrequire")
    public Call<RequestBloodResponse> requestBlood(@Body RequestBlood requestBlood);

    @POST("getFamilyMembers")
    public Call<GetFamilyMemberResponse> getFamilyMembers(@Body GetUserInfo getUserInfo);

    @POST("getHospitalInfo")
    public Call<HospitalLoginResponse> getHospInfo(@Body GetHospitalInfo getHospitalInfo);

    @POST("hospitalinfoedit")
    public Call<HospitalRegisterResponse> getHospInfoEdit(@Body HospitalRegister hospitalRegister);

    @POST("getHistory")
    public Call<HistoryResponse> getUserHistory(@Body GetUserInfo getUserInfo);

    @POST("trackMyRequest")
    public Call<TrackMyRequestResponse> trackRequest(@Body GetUserInfo getUserInfo);

    @POST("AcceptRequest")
    public Call<AcceptRequestResponse> acceptedRequest(@Body AcceptRequest acceptRequest);

    @POST("RespondRequest")
    public Call<AuthorizerRequestResponse> authorizerRequest(@Body GetUserInfo getUserInfo);

    @POST("RespondRequestUpdate")
    public Call<AuthorizerRequestUpdateResponse> authorizerRequestUpdate(@Body AuthorizerRequestUpdate authorizerRequestUpdate);

    @POST("requireBlood")
    public Call<RespondReqResponse> respondRequest(@Body TrackRequest trackRequest);

    @POST("requireBloodResponse")
    public Call<UpdateRespondRequestResponse> updateRespondRequest(@Body UpdateRespondRequest updateRespondRequest);

    @POST("logout")
    public Call<LogoutResponse> logout(@Body Logout logout);

    @POST("indCloseReq")
    public Call<LogoutResponse> closeRequirement(@Body CloseRequirement closeRequirement);


    @POST("updatePasswordHspt")
    public Call<LogoutResponse> changeHospPassword(@Body ChangeHospPassword changeHospPassword);

    @POST("mobileLogin")
    public Call<GetIndiUserResponse> loginWithPhonenumber(@Body Phonenumber phonenumber);

    @POST("trackMyRequestDetail")
    public Call<TrackMyRequestDetailResponse> trackDetail(@Body TrackRequest trackRequest);

    @POST("getHosHistory")
    public Call<HospitalHistoryResponse> getHospitalHistory(@Body GetUserInfo getUserInfo);

    @POST("hosTrackMyRequest")
    public Call<TrackMyRequestHospResponse> getHosTrackMyRequest(@Body GetUserInfo getUserInfo);

    @POST("hosTrackMyRequestDetail")
    public Call<TrackMyRequestHospDetailResponse> getHosTrackMyRequestDetail(@Body TrackRequest trackRequest);

    @POST("hosBloodReqStatusUpdate")
    public Call<LogoutResponse> updateUserActivity(@Body UserActivityUpdate userActivityUpdate);

    @POST("getFamilyMembersDonations")
    public Call<FacilitatedDonationDetailResponse> getFacilitatedDonationDetail(@Body TrackRequest trackRequest);

    @POST("indDeclineReq")
    public Call<LogoutResponse> rejectRequest(@Body RejectRequest rejectRequest);

    @POST("getAddusers")
    public Call<AdditionalUsersResponse> getAdditionalUsers(@Body TrackRequest trackRequest);

    @POST("updateFCM")
    public Call<LogoutResponse> updateFcmToken(@Body UpdateFcmToken updateFcmToken);


    @POST("updatenot")
    public Call<NotificationUpdateResponse> updateNotification(@Body NotificationUpdate notificationUpdate);

    @POST("getbloodrequest")
    public Call<RequestListResponse> getBloodRequestList(@Body RequestList requestList);



}

