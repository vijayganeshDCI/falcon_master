package com.dci.falcon.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.dci.falcon.R;
import com.dci.falcon.activity.LoginActivity;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.utills.FalconConstants;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by vijayaganesh on 10/20/2017.
 */

public class UserProfileSelectionFragment extends BaseFragment {
    @BindView(R.id.button_indiduval)
    Button buttonIndiduval;
    @BindView(R.id.button_hospital)
    Button buttonHospital;
    Unbinder unbinder;
    LoginActivity loginActivity;
    LoginFragment loginFragment;
    Bundle bundle;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_type, container, false);
        unbinder = ButterKnife.bind(this, view);
        FalconApplication.getContext().getComponent().inject(this);
        loginActivity = (LoginActivity) getActivity();
        ((LoginActivity) getActivity()).getSupportActionBar().hide();
        editor=sharedPreferences.edit();
        loginFragment = new LoginFragment();
        bundle = new Bundle();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.button_indiduval, R.id.button_hospital})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button_indiduval:
                editor.putInt(FalconConstants.ROLEID,0).commit();
                bundle.putBoolean("isFromIndiduvalUser", true);
                loginFragment.setArguments(bundle);
                loginActivity.push(loginFragment, getString(R.string.login));
                break;
            case R.id.button_hospital:
                editor.putInt(FalconConstants.ROLEID,1).commit();
                bundle.putBoolean("isFromIndiduvalUser", false);
                loginFragment.setArguments(bundle);
                loginActivity.push(loginFragment, getString(R.string.login));
                break;
        }
    }
}
