package com.dci.falcon.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.activity.BaseActivity;
import com.dci.falcon.activity.HomeActivity;
import com.dci.falcon.adapter.ViewMemberInfoAdapter;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.model.FamilyMembersInfoItem;
import com.dci.falcon.model.GetFamilyMemberResponse;
import com.dci.falcon.model.GetUserInfo;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomTextView;
import com.dci.falcon.view.CustomTextViewBold;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

/**
 * Created by vijayaganesh on 11/15/2017.
 */

public class ViewInformationFragment extends BaseFragment
//        implements RecyclerItemTouchHelper.RecyclerItemTouchHelperListener
{
    @BindView(R.id.image_profile_pic)
    CircularImageView imageProfilePic;
    @BindView(R.id.text_view_header_name)
    CustomTextViewBold textViewHeaderName;
    @BindView(R.id.text_view_header_blood_group)
    CustomTextView textViewHeaderBloodGroup;
    @BindView(R.id.cons_view_info_header)
    ConstraintLayout consViewInfoHeader;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.appbar)
    AppBarLayout appbar;
    @BindView(R.id.text_facilitated_units)
    CustomTextViewBold textFacilitatedUnits;
    @BindView(R.id.cons_lives_item)
    ConstraintLayout consLivesItem;
    @BindView(R.id.text_label_facilitated_units)
    CustomTextView textLabelFacilitatedUnits;
    @BindView(R.id.view_lives)
    View viewLives;
    @BindView(R.id.cons_lives)
    ConstraintLayout consLives;
    @BindView(R.id.card_lives)
    CardView cardLives;
    @BindView(R.id.text_indiduval_units)
    CustomTextViewBold textIndiduvalUnits;
    @BindView(R.id.cons_units_item)
    ConstraintLayout consUnitsItem;
    @BindView(R.id.text_label_indiduval_units)
    CustomTextView textLabelIndiduvalUnits;
    @BindView(R.id.view_units)
    View viewUnits;
    @BindView(R.id.cons_units)
    ConstraintLayout consUnits;
    @BindView(R.id.card_units)
    CardView cardUnits;
    @BindView(R.id.recycle_list_add_member)
    RecyclerView recycleListAddMember;
    @BindView(R.id.main_content)
    CoordinatorLayout mainContent;
    @BindView(R.id.cons_view_information)
    ConstraintLayout consViewInformation;
    Unbinder unbinder;
    ViewMemberInfoAdapter viewInfoMemberAdapter;
    List<FamilyMembersInfoItem> viewInfoMemberListItemList;
    int age[] = {18, 20, 21, 55};
    HomeActivity homeActivity;
    FloatingActionButton floatingActionButton;
    @Inject
    FalconAPI falconAPI;
    @Inject
    SharedPreferences sharedPreferences;
    GetFamilyMemberResponse getFamilyMemberResponse;
    UserError userError;
    String userName;
    int userID;
    @BindView(R.id.image_empty_member)
    ImageView imageEmptyMember;
    @BindView(R.id.text_label_empty_member)
    CustomTextView textLabelEmptyMember;
    @BindView(R.id.cons_empty_list)
    ConstraintLayout consEmptyList;
    BaseActivity baseActivity;
    @BindView(R.id.text_view_header_phone_number)
    CustomTextView textViewHeaderPhoneNumber;
    @BindView(R.id.text_view_header_role)
    CustomTextView textViewHeaderRole;
    @BindView(R.id.cons_indi_view_info)
    ConstraintLayout consIndiViewInfo;
    @BindView(R.id.cons_hosp_view_info)
    ConstraintLayout consHospViewInfo;
    @BindView(R.id.cons_family_mem)
    ConstraintLayout consFamilyMem;
    @BindView(R.id.image_profile_pic_hosp)
    PorterShapeImageView imageProfilePicHosp;
    @BindView(R.id.text_view_header_name_hosp)
    CustomTextViewBold textViewHeaderNameHosp;
    @BindView(R.id.view_bar)
    View viewBar;
    @BindView(R.id.text_view_header_phone_number_hosp)
    CustomTextView textViewHeaderPhoneNumberHosp;
    @BindView(R.id.text_view_header_blood_group_hosp)
    CustomTextView textViewHeaderBloodGroupHosp;
    @BindView(R.id.text_facilitated_hosp)
    CustomTextViewBold textFacilitatedHosp;
    @BindView(R.id.cons_lives_item_hosp)
    ConstraintLayout consLivesItemHosp;
    @BindView(R.id.text_label_facilitated_hosp)
    CustomTextView textlabelFacilitatedHosp;
    @BindView(R.id.view_lives_hosp)
    View viewLivesHosp;
    @BindView(R.id.cons_lives_hosp)
    ConstraintLayout consLivesHosp;
    @BindView(R.id.card_lives_hosp)
    CardView cardLivesHosp;
    @BindView(R.id.text_label_units_hosp)
    CustomTextViewBold textLabelUnitsHosp;
    @BindView(R.id.cons_units_item_hosp)
    ConstraintLayout consUnitsItemHosp;
    @BindView(R.id.text_label_units_decription_hosp)
    CustomTextView textLabelUnitsDecriptionHosp;
    @BindView(R.id.view_units_hosp)
    View viewUnitsHosp;
    @BindView(R.id.cons_units_hosp)
    ConstraintLayout consUnitsHosp;
    @BindView(R.id.card_units_hosp)
    CardView cardUnitsHosp;
    @BindView(R.id.fab_add_member)
    FloatingActionButton fabAddMember;
    @BindView(R.id.text_label_view_info_head)
    CustomTextView textLabelViewInfoHead;
    @BindView(R.id.text_re_date)
    CustomTextViewBold textReDate;
    @BindView(R.id.cons_re_dates)
    ConstraintLayout consReDates;
    @BindView(R.id.text_label_re_date)
    CustomTextView textLabelReDate;
    @BindView(R.id.view_re_date)
    View viewReDate;
    @BindView(R.id.cons_remaining_dates)
    ConstraintLayout consRemainingDates;
    @BindView(R.id.card_remaining_dates)
    CardView cardRemainingDates;
    @BindView(R.id.bottom_view)
    ConstraintLayout bottomView;

    @Override
    public void onResume() {
        super.onResume();
        if (sharedPreferences.getInt(FalconConstants.ROLEID, 0) == 0) {
//            Indiduval user
            consIndiViewInfo.setVisibility(View.VISIBLE);
            consHospViewInfo.setVisibility(View.GONE);
            if (sharedPreferences.getInt(FalconConstants.USERTYPE, 0) == 0) {
//            Donor
                consUnits.setVisibility(View.VISIBLE);
                consRemainingDates.setVisibility(View.VISIBLE);
//               Indiduval Donated Units
                startCountAnimation(sharedPreferences.getInt(FalconConstants.INDIDUVALUNITS, 0), textIndiduvalUnits);
//                Remaining days to donate blood
                startCountAnimation(sharedPreferences.getInt(FalconConstants.REMAINIG_DAYS_TO_DONATE_BLOOD, 0), textReDate);
                if (sharedPreferences.getInt(FalconConstants.INDIDUVALUNITS, 0) > 1) {
                    textLabelIndiduvalUnits.setText(getString(R.string.No_of_indi_units));
                } else {
                    textLabelIndiduvalUnits.setText(getString(R.string.No_of_indi_unit));
                }
                if (sharedPreferences.getInt(FalconConstants.REMAINIG_DAYS_TO_DONATE_BLOOD, 0) > 1) {
                    textLabelReDate.setText(getString(R.string.No_of_re_days));
                } else {
                    textLabelReDate.setText(getString(R.string.No_of_re_day));
                }

            } else {
//                non donor
                consUnits.setVisibility(View.GONE);
                consRemainingDates.setVisibility(View.GONE);

            }
//            Facilitated Units
            startCountAnimation(sharedPreferences.getInt(FalconConstants.FACILITATEDDONATIONTOTALUNITS, 0),
                    textFacilitatedUnits);
            if (sharedPreferences.getInt(FalconConstants.FACILITATEDDONATIONTOTALUNITS, 0) > 1)
                textLabelFacilitatedUnits.setText(getString(R.string.No_of_faciliteds));
            else
                textLabelFacilitatedUnits.setText(getString(R.string.No_of_facilitated));
            textViewHeaderBloodGroup.setText("Blood Group:" + sharedPreferences.getString(FalconConstants.BLOODGROUP, ""));
            if (sharedPreferences.getInt(FalconConstants.AUTHORIZER, 0) == 1) {
                textViewHeaderName.setText(sharedPreferences.getString(FalconConstants.USERNAME, "") + " - " + getString(R.string.auth));
            } else {
                textViewHeaderName.setText(sharedPreferences.getString(FalconConstants.USERNAME, "") + " - " + getString(R.string.non_auth));
            }
            textViewHeaderPhoneNumber.setText(sharedPreferences.getString(FalconConstants.PHONENUMBER, ""));
            if (sharedPreferences.getString(FalconConstants.PROFILEPICTURE, "") != null) {
                picasoImageLoader(imageProfilePic, sharedPreferences.getString(FalconConstants.PROFILEPICTURE, ""), getActivity());
            } else {
                imageProfilePic.setImageResource(R.mipmap.img_sample_pro_pic);
            }
            getFamilyMembers();

        } else {
//            Hospital User
            consIndiViewInfo.setVisibility(View.GONE);
            consHospViewInfo.setVisibility(View.VISIBLE);
            consUnitsHosp.setVisibility(View.GONE);
            if (sharedPreferences.getInt(FalconConstants.FACILITATEDHOSPDONATIONTOTALUNITS, 0) > 1)
                textlabelFacilitatedHosp.setText(getString(R.string.No_of_lives_hosp));
            else
                textlabelFacilitatedHosp.setText(getString(R.string.No_of_live_hosp));
            startCountAnimation(sharedPreferences.getInt(FalconConstants.FACILITATEDHOSPDONATIONTOTALUNITS, 0), textFacilitatedHosp);
            textViewHeaderBloodGroupHosp.setText(sharedPreferences.getString(FalconConstants.HOSPITALREGISTERID, ""));
            textViewHeaderNameHosp.setText(sharedPreferences.getString(FalconConstants.USERNAME, ""));
            textViewHeaderPhoneNumberHosp.setText(sharedPreferences.getString(FalconConstants.PHONENUMBER, ""));
            if (sharedPreferences.getString(FalconConstants.PROFILEPICTURE, "") != null) {
                picasoImageLoader(imageProfilePic, sharedPreferences.getString(FalconConstants.PROFILEPICTURE, ""), getActivity());
            } else {
                imageProfilePicHosp.setImageResource(R.mipmap.img_sample_pro_pic);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_information, container, false);
        unbinder = ButterKnife.bind(this, view);
        FalconApplication.getContext().getComponent().inject(this);
        if (sharedPreferences.getInt(FalconConstants.ROLEID, 0) == 0) {
            getActivity().setTitle(getString(R.string.View_Info));
        }
        else {
            getActivity().setTitle(getString(R.string.View_Info_hosp));
        }

        homeActivity = (HomeActivity) getActivity();
        baseActivity = (BaseActivity) getActivity();
        userError = new UserError();
        viewInfoMemberListItemList = new ArrayList<FamilyMembersInfoItem>();
//        notificationActivity.getSupportActionBar().hide();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        linearLayoutManager.setAutoMeasureEnabled(true);
        recycleListAddMember.setLayoutManager(linearLayoutManager);


        floatingActionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab_request_blood);
        floatingActionButton.setVisibility(View.GONE);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeActivity.push(new RequestBloodFragment(), getString(R.string.Request_Blood));
            }
        });
        recycleListAddMember.setOnScrollListener(onScrollListener);

//
//        recycleListAddMember.addOnItemTouchListener(
//                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(View view, int position) {
//                        final FamilyMembersInfoItem familyMembersInfoItem = viewInfoMemberListItemList.get(position);
//
//                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                        alertDialog.setMessage(getString(R.string.alert_give_blood_req_from_view_info) + " " + familyMembersInfoItem.getFirstName() + " ?");
//                        alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                RequestBloodFragment requestBloodFragment = new RequestBloodFragment();
//                                Bundle bundle = new Bundle();
//                                bundle.putBoolean("isFromQuestRequest", true);
//                                bundle.putString("bloodGroup", familyMembersInfoItem.getBloodGroupID());
//                                bundle.putString("patientName", familyMembersInfoItem.getFirstName());
//                                homeActivity.push(requestBloodFragment, getString(R.string.Request_Blood));
//                                requestBloodFragment.setArguments(bundle);
//
//                            }
//                        });
//                        alertDialog.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.dismiss();
//                            }
//                        });
//                        alertDialog.show();
//
//                    }
//                }));

        return view;
    }

    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            // Could hide open views here if you wanted. //
        }
    };


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void getFamilyMembers() {
        GetUserInfo getUserInfo = new GetUserInfo();
        getUserInfo.setUserID(sharedPreferences.getInt(FalconConstants.USEDID, 0));
        if (Utils.isNetworkAvailable()) {
            showProgress();
            viewInfoMemberListItemList.clear();
            falconAPI.getFamilyMembers(getUserInfo).enqueue(new Callback<GetFamilyMemberResponse>() {
                @Override
                public void onResponse(Call<GetFamilyMemberResponse> call, Response<GetFamilyMemberResponse> response) {
                    hideProgress();
                    getFamilyMemberResponse = response.body();
                    if (isAdded()) {
                        if (response.isSuccessful() && response.body() != null && getFamilyMemberResponse.getStatusCode() == 200) {
                            consEmptyList.setVisibility(View.GONE);
                            consFamilyMem.setVisibility(View.VISIBLE);
                            for (int i = 0; i < getFamilyMemberResponse.getMembersInfo().size(); i++) {
                                viewInfoMemberListItemList.add(new FamilyMembersInfoItem(getFamilyMemberResponse.getMembersInfo().get(i).getFirstName(),
                                        getFamilyMemberResponse.getMembersInfo().get(i).getBloodGroupID(),
                                        getFamilyMemberResponse.getMembersInfo().get(i).getPhoneNumber(),
                                        getFamilyMemberResponse.getMembersInfo().get(i).getId(),
                                        getFamilyMemberResponse.getMembersInfo().get(i).getLastName(),
                                        getFamilyMemberResponse.getMembersInfo().get(i).getProfilePicture(),
                                        getFamilyMemberResponse.getMembersInfo().get(i).getBloodCount(),
                                        getFamilyMemberResponse.getMembersInfo().get(i).getRelationship(),
                                        getFamilyMemberResponse.getMembersInfo().get(i).isLogged_in_status()));
                            }

                            viewInfoMemberAdapter = new ViewMemberInfoAdapter(getActivity(), viewInfoMemberListItemList, baseActivity, ViewInformationFragment.this);
                            recycleListAddMember.setAdapter(viewInfoMemberAdapter);
//                            recycleListAddMember.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
                            //                        viewInfoMemberAdapter.addAll(viewInfoMemberListItemList);
                            Collections.sort(viewInfoMemberListItemList, new Comparator<FamilyMembersInfoItem>() {
                                public int compare(FamilyMembersInfoItem obj1, FamilyMembersInfoItem obj2) {
                                    // ## Ascending order
                                    //                                return Integer.valueOf(obj2.getId()).compareTo(obj1.getId()); // To compare integer values
                                    // ## Descending order
                                    return obj1.getFirstName().compareToIgnoreCase(obj2.getFirstName()); // To compare string values
                                    // return Integer.valueOf(obj2.empId).compareTo(obj1.empId); // To compare integer values
                                }
                            });

                        } else {
                            consEmptyList.setVisibility(View.VISIBLE);
                            consFamilyMem.setVisibility(View.GONE);


                        }
                    }
                }

                @Override
                public void onFailure(Call<GetFamilyMemberResponse> call, Throwable t) {
                    if (isAdded()) {
                        hideProgress();
                        consEmptyList.setVisibility(View.VISIBLE);
                        consFamilyMem.setVisibility(View.GONE);

                        Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }

                }
            });
        } else {
            if (isAdded()) {
                consEmptyList.setVisibility(View.VISIBLE);
                consFamilyMem.setVisibility(View.GONE);
                userError.message = getActivity().getString(R.string.network_error_message);
                showError(ERROR_SHOW_TYPE_DIALOG, userError);
            }
        }
    }

    @OnClick(R.id.fab_add_member)
    public void onViewClicked() {
        homeActivity.push(new AddMemberFragment(), getString(R.string.add_mem));
    }
}
