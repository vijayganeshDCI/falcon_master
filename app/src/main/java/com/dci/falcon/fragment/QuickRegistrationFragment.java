package com.dci.falcon.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.activity.HomeActivity;
import com.dci.falcon.activity.QuickRegistrationActivity;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.model.QuickRegistration;
import com.dci.falcon.model.UniqueNumber;
import com.dci.falcon.model.UniqueNumberResponse;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomButton;
import com.dci.falcon.view.CustomCheckBox;
import com.dci.falcon.view.CustomEditText;
import com.dci.falcon.view.CustomRadioButton;
import com.dci.falcon.view.CustomTextView;
import com.dci.falcon.view.CustomTextViewBold;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.GsonBuilder;
import com.msg91.sendotp.library.PhoneNumberUtils;
import com.msg91.sendotp.library.internal.Iso2Phone;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import lib.kingja.switchbutton.SwitchMultiButton;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

/**
 * Created by vijayaganesh on 11/16/2017.
 */

public class QuickRegistrationFragment extends BaseFragment implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, DatePickerDialog.OnDateSetListener {
    @BindView(R.id.edit_hosp_name)
    CustomEditText editName;
    @BindView(R.id.text_input_name)
    TextInputLayout textInputName;
    @BindView(R.id.spinner_blood_group)
    Spinner spinnerBloodGroup;
    @BindView(R.id.view_blood_group)
    View viewBloodGroup;
    @BindView(R.id.edit_phone_number)
    CustomEditText editPhoneNumber;
    @BindView(R.id.text_input_phone_number)
    TextInputLayout textInputPhoneNumber;
    @BindView(R.id.button_register)
    CustomButton buttonRegister;
    @BindView(R.id.cons_quick_registration)
    ConstraintLayout consQuickRegistration;
    Unbinder unbinder;
    @BindView(R.id.text_pin_hospital)
    CustomTextView textPinHospital;
    @BindView(R.id.edit_address_home)
    CustomEditText editAddressHome;
    @BindView(R.id.text_input_add_home)
    TextInputLayout textInputAddHome;
    private static final int PLACE_PICKER_REQUEST = 3;
    private static final long FASTEST_INTERVAL = 1000 * 5;
    private static final long INTERVAL = 1000 * 10;
    @BindView(R.id.image_icon_quick_reg)
    ImageView imageIconQuickReg;
    @BindView(R.id.text_label_quick_reg)
    CustomTextViewBold textLabelQuickReg;
    @BindView(R.id.text_label_quick_reg_des)
    CustomTextView textLabelQuickRegDes;
    @BindView(R.id.image_mantitory_4)
    ImageView imageMantitory4;
    @BindView(R.id.text_label_manditatory)
    CustomTextView textLabelManditatory;
    @BindView(R.id.image_mantitory_1)
    ImageView imageMantitory1;
    @BindView(R.id.image_mantitory_2)
    ImageView imageMantitory2;
    @BindView(R.id.image_profile_pic)
    CircularImageView imageProfilePic;
    @BindView(R.id.text_edit_photo)
    CustomTextView textEditPhoto;
    @BindView(R.id.radio_button_donor)
    CustomRadioButton radioButtonDonor;
    @BindView(R.id.radio_button_non_donor)
    CustomRadioButton radioButtonNonDonor;
    @BindView(R.id.radio_donor_rule)
    RadioGroup radioDonorRule;
    @BindView(R.id.check_enable_authoriser)
    CustomCheckBox checkEnableAuthoriser;
    @BindView(R.id.scroll_quick_registration)
    ScrollView scrollQuickRegistration;
    @BindView(R.id.edit_Last_name)
    CustomEditText editLastName;
    @BindView(R.id.text_input_last_name)
    TextInputLayout textInputLastName;
    @BindView(R.id.edit_dob)
    CustomEditText editDob;
    @BindView(R.id.text_input_Dob)
    TextInputLayout textInputDob;
    @BindView(R.id.edit_gender)
    CustomEditText editGender;
    @BindView(R.id.switch_gender)
    SwitchMultiButton switchGender;
    //    @BindView(R.id.image_mantitory_5)
//    ImageView imageMantitory5;
    @BindView(R.id.edit_age)
    CustomEditText editAge;
    @BindView(R.id.text_input_age)
    TextInputLayout textInputAge;
    private LocationRequest mLocationRequest;
    Location mCurrentLocation;
    GoogleApiClient mGoogleApiClient;
    private double mLat, mLng;
    private LatLngBounds bounds;
    FloatingActionButton floatingActionButton;
    HomeActivity homeActivity;
    double nearHomeLat, nearHomeLng;
    private String bloodGroup;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    public static final String INTENT_PHONENUMBER = "phonenumber";
    public static final String INTENT_COUNTRY_CODE = "code";
    private String mCountryIso;
    @Inject
    FalconAPI falconAPI;
    private UniqueNumberResponse uniqueNumberResponse;
    private UserError userError;
    private int radiocheckedId;
    private static final int MY_REQUEST_CODE_CAMERA = 5;
    private static final int MY_REQUEST_CODE_GALLERY = 6;
    private int REQUEST_CAMERA = 1;
    private int SELECT_FILE = 2;
    private String proFilePicture;
    private Uri uri;
    private String proFilePictureFromImageview;
    private int isAuthorizer;
    private QuickRegistrationActivity quickRegistrationActivity;
    private FusedLocationProviderClient mFusedLocationClient;
    private Calendar currentD, calendardate, calendarTime, currentT;
    private String donateDate, donateTime;
    private SimpleDateFormat simpleDateFormat;
    private int gender=2;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_quick_registration, container, false);
        unbinder = ButterKnife.bind(this, view);
        getActivity().setTitle(getString(R.string.Quick_registration));
        if (getActivity() instanceof HomeActivity)
            homeActivity = (HomeActivity) getActivity();
        else
            quickRegistrationActivity = (QuickRegistrationActivity) getActivity();
        FalconApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        userError = new UserError();
        initiaieGoogleApiClient();
        editName.setFilters(new InputFilter[]{editTextRestrictLength(), editTextRestrictSmileys(), editTextRestrictSpecialCharacters()});
        editLastName.setFilters(new InputFilter[]{editTextRestrictLength(), editTextRestrictSmileys(), editTextRestrictSpecialCharacters()});
        floatingActionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab_request_blood);
        floatingActionButton.setVisibility(View.GONE);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeActivity.push(new RequestBloodFragment(), getString(R.string.Request_Blood));
            }
        });
        mCountryIso = PhoneNumberUtils.getDefaultCountryIso(getActivity());
        final ArrayAdapter<String> bloodgroupAdapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item, getResources().getStringArray(R.array.blood_group));
        spinnerBloodGroup.setAdapter(bloodgroupAdapter);
        spinnerBloodGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (adapterView.getChildCount() > 0) {
                    if (((TextView) adapterView.getChildAt(0)).getText().equals("Select Blood Group")) {
                        ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.hint_grey));
                    }
                }
                bloodGroup = adapterView.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        radioButtonDonor.setChecked(true);
        radioDonorRule.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
//                    Donor-0
//                    NonDonor-1
                    case R.id.radio_button_donor:
                        radiocheckedId = 0;

                        break;
                    case R.id.radio_button_non_donor:
                        radiocheckedId = 1;
                        break;
                }
            }
        });

        checkEnableAuthoriser.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    isAuthorizer = 1;
                else
                    isAuthorizer = 0;
            }
        });

        switchGender.setOnSwitchListener(new SwitchMultiButton.OnSwitchListener() {
            @Override
            public void onSwitch(int position, String tabText) {
                gender = position;
            }
        });

        return view;
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        calendardate = Calendar.getInstance();
        calendardate.set(Calendar.MONTH, monthOfYear);
        calendardate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        calendardate.set(Calendar.YEAR, year);
        simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        donateDate = simpleDateFormat.format(calendardate.getTime());
        editDob.setText(donateDate);
        editAge.setText(getAge(year, monthOfYear, dayOfMonth));
    }


    private void datePickerDialog() {
        currentD = Calendar.getInstance();
        Calendar maxDate = Calendar.getInstance();
        Calendar minDate = Calendar.getInstance();
        DatePickerDialog datePickerDialog =
                DatePickerDialog.newInstance(this, currentD.get(Calendar.YEAR), currentD.get(Calendar.MONTH),
                        currentD.get(Calendar.DAY_OF_MONTH));
        maxDate.add(Calendar.YEAR, -18);
        datePickerDialog.setMaxDate(maxDate);
        minDate.add(Calendar.YEAR, -60);
        datePickerDialog.setMinDate(minDate);
        datePickerDialog.setAccentColor(getResources().getColor(R.color.falcon_red));
        datePickerDialog.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_REQUEST_CODE_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, REQUEST_CAMERA);
            } else {
                // Your app will not have this permission. Turn off all functions
                // that require this permission or it will force close like your
                // original question
            }
        } else if (requestCode == MY_REQUEST_CODE_GALLERY) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);//
                startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
            } else {

            }
        }
    }

    private void getProfilePicture() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
        builder.setTitle("Add photo");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA},
                                    MY_REQUEST_CODE_CAMERA);
                        } else {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, REQUEST_CAMERA);
                        }
                    } else {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, REQUEST_CAMERA);
                    }

                } else if (options[item].equals("Choose from Gallery")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                    MY_REQUEST_CODE_GALLERY);
                        } else {
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);//
                            startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
                        }
                    } else {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);//
                        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
                    }

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        imageProfilePic.setImageBitmap(bm);
//        uploadUserImage(data.getData());
        proFilePicture = getBase64Image(bm);
        uri = data.getData();
    }

    private String convertToBitMap() {
        Bitmap bm = ((BitmapDrawable) imageProfilePic.getDrawable()).getBitmap();
        proFilePictureFromImageview = getBase64Image(bm);
        return proFilePictureFromImageview;
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(getActivity().getExternalFilesDir(null),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        imageProfilePic.setImageBitmap(thumbnail);
//        uploadUserImage(Uri.fromFile(destination));
        proFilePicture = getBase64Image(thumbnail);
        uri = Uri.fromFile(destination);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.button_register, R.id.edit_address_home, R.id.image_profile_pic, R.id.text_edit_photo, R.id.edit_dob})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button_register:

                if (isQuickRegNotNull()) {
                    AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(getActivity());
                    alertDialog1.setMessage(getString(R.string.alert_register) + " " + editName.getText().toString() + " " + "?");
                    alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            isNewNumber();
                        }
                    });
                    alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    alertDialog1.show();
                } else {
//                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                    alertDialog.setMessage(getString(R.string.alert_mes_profile));
//                    alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            dialogInterface.dismiss();
//                            hideProgress();
//                        }
//                    });
//                    alertDialog.show();
                }


                break;
            case R.id.edit_address_home:
                if (getActivity() instanceof HomeActivity) {
                    if (!homeActivity.hasPermissionGranted()) {
                        homeActivity.requestPermission();
                    } else {
                        pickYourPlace();
                    }
                } else {
                    if (!quickRegistrationActivity.hasPermissionGranted()) {
                        quickRegistrationActivity.requestPermission();
                    } else {
                        pickYourPlace();
                    }
                }

                break;
            case R.id.image_profile_pic:
                getProfilePicture();
                break;
            case R.id.text_edit_photo:
                getProfilePicture();
                break;
            case R.id.edit_dob:
                datePickerDialog();
                break;
        }
    }

    private void otpVerification(String phoneNumber) {
        QuickRegistration quickRegistration = new QuickRegistration();
        if (proFilePicture != null)
            quickRegistration.setProfilePicture(proFilePicture);
        else
            quickRegistration.setProfilePicture(convertToBitMap());
        quickRegistration.setUserType(radiocheckedId);
        quickRegistration.setFirstName(editName.getText().toString());
        quickRegistration.setBloodGroup(bloodGroup);
        quickRegistration.setPhoneNumber(editPhoneNumber.getText().toString());
        quickRegistration.setLocationHome(editAddressHome.getText().toString());
        quickRegistration.setHomelat(String.valueOf(mLat));
        quickRegistration.setHomelng(String.valueOf(mLng));
        quickRegistration.setAuthorizerStatus(isAuthorizer);
        quickRegistration.setLastName(editLastName.getText().toString());
        quickRegistration.setAge(Integer.parseInt(editAge.getText().toString()));
        Date dob = null;
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd-MMM-yyyy");
        try {
            dob = simpleDateFormat1.parse(editDob.getText().toString());
            simpleDateFormat.applyPattern("yyyy-MM-dd");
            quickRegistration.setDob(simpleDateFormat.format(dob));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        quickRegistration.setGender(gender + 1);

        OtpVerificationFragment otpVerificationFragment = new OtpVerificationFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("otpVerify", 3);
        bundle.putString("quickRegDetails", new GsonBuilder().create().toJson(quickRegistration));
        bundle.putString(INTENT_PHONENUMBER, phoneNumber);
        bundle.putString(INTENT_COUNTRY_CODE, Iso2Phone.getPhone(mCountryIso));
        otpVerificationFragment.setArguments(bundle);
        if (getActivity() instanceof HomeActivity) {
            homeActivity.push(otpVerificationFragment);
        } else {
            quickRegistrationActivity.push(otpVerificationFragment);
        }

    }

    private void isNewNumber() {
        UniqueNumber uniqueNumber = new UniqueNumber();
        uniqueNumber.setPhoneNumber(editPhoneNumber.getText().toString());
        if (Utils.isNetworkAvailable()) {
            showProgress();
            falconAPI.checkUniqueNumber(uniqueNumber).enqueue(new Callback<UniqueNumberResponse>() {
                @Override
                public void onResponse(Call<UniqueNumberResponse> call, Response<UniqueNumberResponse> response) {
                    hideProgress();
                    uniqueNumberResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && uniqueNumberResponse.getStatusCode() != 500) {
                        //Old Number
                        Toast.makeText(getActivity(), uniqueNumberResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        //New Number
                        otpVerification(editPhoneNumber.getText().toString());
                    }
                }

                @Override
                public void onFailure(Call<UniqueNumberResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            userError.message = getActivity().getString(R.string.network_error_message);
            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PLACE_PICKER_REQUEST
                && resultCode == Activity.RESULT_OK) {

            final Place place = PlacePicker.getPlace(getActivity(), data);
            final String name = place.getName().toString();
            final String address = place.getAddress().toString();
            String attributions = (String) place.getAttributions();
            if (attributions == null) {
                attributions = "";
            }
            nearHomeLat = place.getLatLng().latitude;
            nearHomeLng = place.getLatLng().longitude;
            if (nearHomeLat != 0 && nearHomeLng != 0) {
                if (!name.contains("9"))
                    editAddressHome.setText(name + "," + address);
                else
                    editAddressHome.setText(address);
            } else
                editAddressHome.setText("");

        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void initiaieGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity()).addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void startLocationUpdates() {

        if (getActivity() != null && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
//
//        mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//        if (mCurrentLocation != null) {
//            mLat = mCurrentLocation.getLatitude();
//            mLng = mCurrentLocation.getLongitude();
//        }

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        mFusedLocationClient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    mLat = location.getLatitude();
                    mLng = location.getLongitude();
                }
            }
        });
        double radiusDegrees = 0.001;
        LatLng center = new LatLng(mLat, mLng);
        LatLng northEast = new LatLng(center.latitude + radiusDegrees, center.longitude + radiusDegrees);
        LatLng southWest = new LatLng(center.latitude - radiusDegrees, center.longitude - radiusDegrees);
        bounds = LatLngBounds.builder()
                .include(northEast)
                .include(southWest)
                .build();
    }

    private void pickYourPlace() {
        try {
            if (bounds != null) {
                PlacePicker.IntentBuilder intentBuilder =
                        new PlacePicker.IntentBuilder();
                intentBuilder.setLatLngBounds(bounds);
                Intent intent = intentBuilder.build(getActivity());
                startActivityForResult(intent, PLACE_PICKER_REQUEST);
            }

        } catch (GooglePlayServicesRepairableException
                | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    private boolean showErrorFirstName() {

        if (editName.getText().toString().isEmpty()) {
            textInputName.setError(getString(R.string.req_first_name));
            return false;
        } else {
            textInputName.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorFirstName1() {

        if (editName.getText().toString().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorBloodGroup() {
        if (bloodGroup.contains("Select Blood Group")) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setMessage(getString(R.string.req_Blood));
            alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            alertDialog.show();
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorBloodGroup1() {
        if (bloodGroup.contains("Select Blood Group")) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorPhoneNumber() {
        if (editPhoneNumber.getText().toString().isEmpty()) {
            textInputPhoneNumber.setError(getString(R.string.req_pho));
            return false;
        } else if (editPhoneNumber.getText().toString().length() != 10) {
            textInputPhoneNumber.setError(getString(R.string.req_phone_number));
            return false;
        } else {
            textInputPhoneNumber.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorPhoneNumber1() {
        if (editPhoneNumber.getText().toString().isEmpty()) {
            return false;
        } else if (editPhoneNumber.getText().toString().length() > 10) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorLocation() {

        if (editAddressHome.getText().toString().isEmpty()) {
            textInputAddHome.setError(getString(R.string.req_Location));
            return false;
        } else {
            textInputAddHome.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorLocation1() {

        if (editAddressHome.getText().toString().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorDob() {
        if (editDob.getText().toString().isEmpty()) {
            textInputDob.setError(getString(R.string.req_DOB));
            return false;
        } else {
            textInputDob.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorDob1() {
        if (editDob.getText().toString().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    public boolean isQuickRegNotNull() {
        showErrorFirstName();
        showErrorPhoneNumber();
        showErrorLocation();
        showErrorBloodGroup();
        showErrorAge();
        showErrorDob();
        showErrorGender();
        if (showErrorFirstName1()
                && showErrorPhoneNumber1() && showErrorLocation1() && showErrorBloodGroup() && showErrorDob1() && showErrorAge1()
                && showErrorGender1()) {
            return true;
        } else {
            return false;
        }
    }

    private boolean showErrorAge() {
        if (editAge.getText().toString().isEmpty()) {
            textInputAge.setError(getString(R.string.req_Age));
            return false;
        } else if (Integer.parseInt(editAge.getText().toString()) < 18) {
            textInputAge.setError(getActivity().getString(R.string.min_age));
            return false;
        } else if (Integer.parseInt(editAge.getText().toString()) > 60) {
            textInputAge.setError(getActivity().getString(R.string.min_age));
            return false;
        } else {
            textInputAge.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorAge1() {
        if (editAge.getText().toString().isEmpty()) {
            return false;
        } else if (Integer.parseInt(editAge.getText().toString()) < 18) {
            return false;
        } else if (Integer.parseInt(editAge.getText().toString()) > 60) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorGender1() {

        switch (gender){
            case 0:
                return true;
            case 1:
                return true;
            case 2:
                return false;
        }
        return false;
    }

    private boolean showErrorGender() {

        switch (gender){
            case 0:
                return true;
            case 1:
                return true;
            case 2:
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setMessage(getString(R.string.req_gender));
                alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                alertDialog.show();
                return false;
        }

        return false;
    }

}
