package com.dci.falcon.fragment;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.activity.HomeActivity;
import com.dci.falcon.activity.NotificationActivity;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.model.UpdateRespondRequest;
import com.dci.falcon.model.UpdateRespondRequestResponse;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomButton;
import com.dci.falcon.view.CustomCheckBox;
import com.dci.falcon.view.CustomEditText;
import com.dci.falcon.view.CustomTextView;
import com.dci.falcon.view.CustomTextViewBold;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

/**
 * Created by vijayaganesh on 12/15/2017.
 */

public class UpdateRequestStatusFragment extends BaseFragment implements TimePickerDialog.OnTimeSetListener {
    @BindView(R.id.image_icon_res_req)
    ImageView imageIconResReq;
    @BindView(R.id.text_label_res_req)
    CustomTextViewBold textLabelResReq;
    @BindView(R.id.text_label_res_req_update)
    CustomTextView textLabelResReqUpdate;
    @BindView(R.id.edit_res_req_time)
    CustomEditText editResReqTime;
    @BindView(R.id.text_input_res_req_time)
    TextInputLayout textInputResReqTime;
    @BindView(R.id.edit_res_req_update)
    CustomEditText editResReqUpdate;
    @BindView(R.id.text_input_res_req_remarks)
    TextInputLayout textInputResReqRemarks;
    @BindView(R.id.check_on_the_way)
    CustomCheckBox checkOnTheWay;
    @BindView(R.id.button_update)
    CustomButton buttonUpdate;
    @BindView(R.id.update_request_status)
    ConstraintLayout updateRequestStatus;
    Unbinder unbinder;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    FalconAPI falconAPI;
    UserError userError;
    int ontheWayStatus;
    int id, status;
    private Calendar calendarTime, currentT;
    SimpleDateFormat simpleDateFormat;
    String donateTime;
    UpdateRespondRequestResponse updateRespondRequestResponse;
    NotificationActivity notificationActivity;
    private FloatingActionButton floatingActionButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_update_request_status, container, false);
        unbinder = ButterKnife.bind(this, view);
        FalconApplication.getContext().getComponent().inject(this);
        userError = new UserError();
        notificationActivity = (NotificationActivity) getActivity();
        notificationActivity.getSupportActionBar().hide();
        simpleDateFormat = new SimpleDateFormat();
//        editResReqUpdate.addTextChangedListener(textWatcher);
//        editResReqTime.addTextChangedListener(textWatcher);
        Bundle bundle = getArguments();
        if (bundle != null) {
            id = bundle.getInt("id");
            status = bundle.getInt("status");
        }
        checkOnTheWay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    ontheWayStatus = 1;
                } else {
                    ontheWayStatus = 0;
                }

            }
        });
        floatingActionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab_request_blood);
        floatingActionButton.setVisibility(View.GONE);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                notificationActivity.push(new RequestBloodFragment(), getString(R.string.Request_Blood));
            }
        });
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void getUpdateRequestStatus() {
        UpdateRespondRequest updateRespondRequest = new UpdateRespondRequest();
        updateRespondRequest.setId(id);
        updateRespondRequest.setNotes(editResReqUpdate.getText().toString().isEmpty() ? "" : editResReqUpdate.getText().toString());
        updateRespondRequest.setScheduleTime(editResReqTime.getText().toString().isEmpty() ? "" : editResReqTime.getText().toString());
        updateRespondRequest.setUserResStatus(status);
        updateRespondRequest.setTravellingStatus(ontheWayStatus);
        if (Utils.isNetworkAvailable()) {
            showProgress();
            falconAPI.updateRespondRequest(updateRespondRequest).enqueue(new Callback<UpdateRespondRequestResponse>() {
                @Override
                public void onResponse(Call<UpdateRespondRequestResponse> call, Response<UpdateRespondRequestResponse> response) {
                    hideProgress();
                    updateRespondRequestResponse = response.body();
                    if (response.isSuccessful() && response.isSuccessful() && updateRespondRequestResponse.getStatusCode() == 200) {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setMessage(getString(R.string.successfully_update));
                        alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                getActivity().onBackPressed();
                            }

                        });
                        alertDialog.show();
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<UpdateRespondRequestResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();

                }
            });
        } else {
            userError.message = getActivity().getString(R.string.network_error_message);
            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }
    }

    @OnClick({R.id.edit_res_req_time, R.id.button_update})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.edit_res_req_time:
                timePickerDialog();
                break;
            case R.id.button_update:
                AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(getActivity());
                alertDialog1.setMessage(getString(R.string.alert_update));
                alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getUpdateRequestStatus();
                    }
                });
                alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                alertDialog1.show();
                break;
        }
    }

    private void timePickerDialog() {
        currentT = Calendar.getInstance();
        TimePickerDialog timePickerDialog =
                TimePickerDialog.newInstance(this, currentT.get(Calendar.HOUR_OF_DAY), currentT.get(Calendar.MINUTE), currentT.get(Calendar.SECOND), false);
        timePickerDialog.setAccentColor(getResources().getColor(R.color.falcon_red));
        timePickerDialog.show(getActivity().getFragmentManager(), "Timepickerdialog");
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        calendarTime = Calendar.getInstance();
        calendarTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendarTime.set(Calendar.MINUTE, minute);
        simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        donateTime = simpleDateFormat.format(calendarTime.getTime());
        editResReqTime.setText(donateTime);
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            statusValidation();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private void statusValidation() {
        if (editResReqTime.getText().toString().isEmpty() && editResReqUpdate.getText().toString().isEmpty()) {
            buttonUpdate.setEnabled(false);
            buttonUpdate.setBackgroundColor(getResources().getColor(R.color.light_grey));
        } else {
            buttonUpdate.setEnabled(true);
            buttonUpdate.setBackgroundColor(getResources().getColor(R.color.falcon_red));
        }
    }
}
