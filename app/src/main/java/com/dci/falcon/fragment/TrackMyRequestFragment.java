package com.dci.falcon.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.activity.BaseActivity;
import com.dci.falcon.activity.HomeActivity;
import com.dci.falcon.activity.TrackRequestActivity;
import com.dci.falcon.adapter.TrackMyRequestAdapter;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.model.GetUserInfo;
import com.dci.falcon.model.TrackMyRequestHospResponse;
import com.dci.falcon.model.TrackMyRequestItem;
import com.dci.falcon.model.TrackMyRequestResponse;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomTextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

/**
 * Created by vijayaganesh on 11/10/2017.
 */

public class TrackMyRequestFragment extends BaseFragment {
    FloatingActionButton floatingActionButton;
    TrackRequestActivity trackRequestActivity;
    @BindView(R.id.list_track_my_request)
    ListView listTrackMyRequest;
    @BindView(R.id.cons_track_request)
    ConstraintLayout consTrackRequest;
    Unbinder unbinder;
    TrackMyRequestAdapter trackRequestAdapter;
    List<TrackMyRequestItem> requestItemList;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    FalconAPI falconAPI;
    UserError userError;
    TrackMyRequestResponse trackRequestResponse;
    TrackMyRequestHospResponse trackMyRequestHospResponse;
    @BindView(R.id.swiperefresh_track_req)
    SwipeRefreshLayout swiperefreshTrackReq;
    @BindView(R.id.image_empty_member)
    ImageView imageEmptyMember;
    @BindView(R.id.text_label_empty_member)
    CustomTextView textLabelEmptyMember;
    @BindView(R.id.cons_empty_list)
    ConstraintLayout consEmptyList;
    HomeActivity homeActivity;
    BaseActivity baseActivity;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_track_request, container, false);
        unbinder = ButterKnife.bind(this, view);
        FalconApplication.getContext().getComponent().inject(this);
        trackRequestActivity = (TrackRequestActivity) getActivity();
        getActivity().setTitle(getString(R.string.Track_Request));
        imageEmptyMember.setImageResource(R.mipmap.icon_empty_track_request);
        textLabelEmptyMember.setText(getString(R.string.No_More_req));
        baseActivity = (BaseActivity) getActivity();
        userError = new UserError();
        floatingActionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab_request_blood);
        floatingActionButton.setVisibility(View.GONE);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                trackRequestActivity.push(new RequestBloodFragment(), getString(R.string.Request_Blood));
            }
        });
        requestItemList = new ArrayList<TrackMyRequestItem>();
        if (sharedPreferences.getInt(FalconConstants.ROLEID, 0) == 0) {
            if (isAdded()) {
                getTrackMyRequest();
            }
        } else {
            if (isAdded()) {
                getHospitalTrackMyrequest();
            }
        }
        swiperefreshTrackReq.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swiperefreshTrackReq.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isAdded()) {
                            if (sharedPreferences.getInt(FalconConstants.ROLEID, 0) == 0)
                                getTrackMyRequest();
                            else
                                getHospitalTrackMyrequest();
                        }
                        swiperefreshTrackReq.setRefreshing(false);
                    }
                }, 3000);
            }
        });

//        listTrackMyRequest.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
//                TrackMyRequestItem trackMyRequestItem = (TrackMyRequestItem) adapterView.getItemAtPosition(position);
//                TrackMyRequestDetailFragment trackMyRequestDetailFragment=new TrackMyRequestDetailFragment();
//                Bundle bundle=new Bundle();
//                bundle.putInt("trackDetailID",trackMyRequestItem.getId());
//                trackMyRequestDetailFragment.setArguments(bundle);
//                trackRequestActivity.push(trackMyRequestDetailFragment,getString(R.string.Donor_list));
//            }
//        });


        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void getTrackMyRequest() {
        if (isAdded()) {
            GetUserInfo getUserInfo = new GetUserInfo();
            getUserInfo.setUserID(sharedPreferences.getInt(FalconConstants.USEDID, 0));
            if (Utils.isNetworkAvailable()) {
                showProgress();
                requestItemList.clear();
                falconAPI.trackRequest(getUserInfo).enqueue(new Callback<TrackMyRequestResponse>() {
                    @Override
                    public void onResponse(Call<TrackMyRequestResponse> call, Response<TrackMyRequestResponse> response) {
                        hideProgress();
                        trackRequestResponse = response.body();
                        if (isAdded()) {
                            if (response.isSuccessful() && response.body() != null && trackRequestResponse.getStatusCode() == 200
                                    && !trackRequestResponse.getUser().isEmpty()) {
                                consEmptyList.setVisibility(View.GONE);

                                for (int i = 0; i < trackRequestResponse.getUser().size(); i++) {
                                    requestItemList.add(new TrackMyRequestItem(trackRequestResponse.getUser().get(i).getPatientName(),
                                            trackRequestResponse.getUser().get(i).getUnits(),
                                            trackRequestResponse.getUser().get(i).getBloodGroupID(),
                                            trackRequestResponse.getUser().get(i).getHospitalAddress(),
                                            trackRequestResponse.getUser().get(i).getDateTime(),
                                            trackRequestResponse.getUser().get(i).getId(),
                                            trackRequestResponse.getUser().get(i).getStatus()));
                                }

                                trackRequestAdapter = new TrackMyRequestAdapter(getActivity(), requestItemList, baseActivity, TrackMyRequestFragment.this);
                                listTrackMyRequest.setAdapter(trackRequestAdapter);
                                Collections.sort(requestItemList, new Comparator<TrackMyRequestItem>() {
                                    public int compare(TrackMyRequestItem obj1, TrackMyRequestItem obj2) {
                                        // ## Ascending order
                                        //                                return Integer.valueOf(obj2.getId()).compareTo(obj1.getId()); // To compare integer values
                                        // ## Descending order
                                        return obj2.getDateTime().compareToIgnoreCase(obj1.getDateTime()); // To compare string values
                                        // return Integer.valueOf(obj2.empId).compareTo(obj1.empId); // To compare integer values
                                    }
                                });
                            } else {
                                consEmptyList.setVisibility(View.VISIBLE);

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<TrackMyRequestResponse> call, Throwable t) {
                        if (isAdded()) {
                            hideProgress();
                            consEmptyList.setVisibility(View.VISIBLE);
                            Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            } else {
                if (isAdded()) {
                    consEmptyList.setVisibility(View.VISIBLE);
                    userError.message = getActivity().getString(R.string.network_error_message);
                    showError(ERROR_SHOW_TYPE_DIALOG, userError);
                }
            }
        }
    }

    public void getHospitalTrackMyrequest() {
            GetUserInfo getUserInfo = new GetUserInfo();
            getUserInfo.setUserID(sharedPreferences.getInt(FalconConstants.HOSPITALUSERID, 0));
            if (Utils.isNetworkAvailable()) {
                showProgress();
                requestItemList.clear();
                falconAPI.getHosTrackMyRequest(getUserInfo).enqueue(new Callback<TrackMyRequestHospResponse>() {
                    @Override
                    public void onResponse(Call<TrackMyRequestHospResponse> call, Response<TrackMyRequestHospResponse> response) {
                        hideProgress();
                        trackMyRequestHospResponse = response.body();
                        if (isAdded()) {
                            if (response.isSuccessful() && response.body() != null && trackMyRequestHospResponse.getStatusCode() == 200
                                    && !trackMyRequestHospResponse.getUser().isEmpty()) {
                                consEmptyList.setVisibility(View.GONE);

                                for (int i = 0; i < trackMyRequestHospResponse.getUser().size(); i++) {
                                    requestItemList.add(new TrackMyRequestItem(trackMyRequestHospResponse.getUser().get(i).getPatientName(),
                                            trackMyRequestHospResponse.getUser().get(i).getUnits(),
                                            trackMyRequestHospResponse.getUser().get(i).getBloodGroupID(),
                                            trackMyRequestHospResponse.getUser().get(i).getHospitalAddress(),
                                            trackMyRequestHospResponse.getUser().get(i).getDateTime(),
                                            trackMyRequestHospResponse.getUser().get(i).getId(),
                                            trackMyRequestHospResponse.getUser().get(i).getStatus()));
                                }

                                trackRequestAdapter = new TrackMyRequestAdapter(getActivity(), requestItemList, baseActivity, TrackMyRequestFragment.this);
                                listTrackMyRequest.setAdapter(trackRequestAdapter);
                                Collections.sort(requestItemList, new Comparator<TrackMyRequestItem>() {
                                    public int compare(TrackMyRequestItem obj1, TrackMyRequestItem obj2) {
                                        // ## Ascending order
                                        //                                return Integer.valueOf(obj2.getId()).compareTo(obj1.getId()); // To compare integer values
                                        // ## Descending order
                                        return obj2.getDateTime().compareToIgnoreCase(obj1.getDateTime()); // To compare string values
                                        // return Integer.valueOf(obj2.empId).compareTo(obj1.empId); // To compare integer values
                                    }
                                });
                            } else {
                                consEmptyList.setVisibility(View.VISIBLE);

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<TrackMyRequestHospResponse> call, Throwable t) {
                        if (isAdded()) {
                            hideProgress();
                            consEmptyList.setVisibility(View.VISIBLE);
                            Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            } else {
                if (isAdded()) {
                    consEmptyList.setVisibility(View.VISIBLE);
                    userError.message = getActivity().getString(R.string.network_error_message);
                    showError(ERROR_SHOW_TYPE_DIALOG, userError);
                }
            }
    }
}
