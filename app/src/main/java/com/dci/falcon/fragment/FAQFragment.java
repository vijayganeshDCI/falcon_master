package com.dci.falcon.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.activity.FAQActivity;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.model.StaticPage;
import com.dci.falcon.model.StaticPageResponse;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomTextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

/**
 * Created by vijayaganesh on 11/10/2017.
 */

public class FAQFragment extends BaseFragment {
    FloatingActionButton floatingActionButton;
    FAQActivity faqActivity;
    //    @BindView(R.id.web_view_faq)
//    WebView webViewFaq;
    Unbinder unbinder;
    @BindView(R.id.text_faq)
    CustomTextView textFaq;
    @BindView(R.id.cons_faq)
    ConstraintLayout consFaq;
    @BindView(R.id.scroll_faq)
    ScrollView scrollFaq;
    @Inject
    FalconAPI falconAPI;
    UserError userError;
    StaticPageResponse staticPageResponse;
    @BindView(R.id.image_empty_faq)
    ImageView imageEmptyFaq;
    @BindView(R.id.text_label_faq_req)
    CustomTextView textLabelFaqReq;
    @BindView(R.id.cons_empty_list)
    ConstraintLayout consEmptyList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_faq, container, false);
        getActivity().setTitle(getString(R.string.FAQ));
        faqActivity = (FAQActivity) getActivity();
        FalconApplication.getContext().getComponent().inject(this);
        userError = new UserError();
        floatingActionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab_request_blood);
        floatingActionButton.setVisibility(View.GONE);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                faqActivity.push(new RequestBloodFragment(), getString(R.string.Request_Blood));
            }
        });
        unbinder = ButterKnife.bind(this, view);
        faqPage();
//        webViewFaq.loadUrl("http://www.dotcominfoway.com/");
//        WebSettings webSettings = webViewFaq.getSettings();
//        webSettings.setJavaScriptEnabled(true);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void faqPage() {
        StaticPage staticPage = new StaticPage();
        staticPage.setPage_name("FAQ");
        if (Utils.isNetworkAvailable()) {
            showProgress();
            falconAPI.getStaticPage(staticPage).enqueue(new Callback<StaticPageResponse>() {
                @Override
                public void onResponse(Call<StaticPageResponse> call, Response<StaticPageResponse> response) {
                    hideProgress();
                    consEmptyList.setVisibility(View.GONE);
                    if (isAdded()) {
                        if (response.isSuccessful() && response.body() != null) {
                            staticPageResponse = response.body();
                            textFaq.setText(staticPageResponse.getData().getContent());
                        }else {
                            Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();

                        }
                    }
                }

                @Override
                public void onFailure(Call<StaticPageResponse> call, Throwable t) {
                    if (isAdded()) {
                        hideProgress();
                        consEmptyList.setVisibility(View.VISIBLE);
                        Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }

                }
            });

        } else {
            if (isAdded()) {
                consEmptyList.setVisibility(View.VISIBLE);
                userError.message = getActivity().getString(R.string.network_error_message);
                showError(ERROR_SHOW_TYPE_DIALOG, userError);
            }
        }
    }
}
