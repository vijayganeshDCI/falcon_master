package com.dci.falcon.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.dci.falcon.R;
import com.dci.falcon.activity.HomeActivity;
import com.dci.falcon.activity.NotificationActivity;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.firebase.FirebaseMessaging;
import com.dci.falcon.model.GetHospitalInfo;
import com.dci.falcon.model.GetIndiUserResponse;
import com.dci.falcon.model.GetUserInfo;
import com.dci.falcon.model.HospitalLoginResponse;
import com.dci.falcon.model.LogoutResponse;
import com.dci.falcon.model.NotificationUpdate;
import com.dci.falcon.model.NotificationUpdateResponse;
import com.dci.falcon.model.UpdateFcmToken;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomTextView;
import com.dci.falcon.view.CustomTextViewBold;

import javax.inject.Inject;

import az.plainpie.PieView;
import az.plainpie.animation.PieAngleAnimation;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;
import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

/**
 * Created by vijayaganesh on 10/23/2017.
 */

public class DashBoardFragment extends BaseFragment {

    Unbinder unbinder;
    HomeActivity homeActivity;
    @BindView(R.id.coordinator_dashboard)
    CoordinatorLayout coordinatorDashboard;
    @BindView(R.id.image_info)
    ImageView imageInfo;
    @BindView(R.id.text_label_view_info)
    CustomTextView textLabelViewInfo;
    @BindView(R.id.view_top_dash)
    View viewTopDash;
    @BindView(R.id.image_quick_request)
    ImageView imageQuickRequest;
    @BindView(R.id.text_label_quick_request)
    CustomTextView textLabelQuickRequest;
    @BindView(R.id.view_left_dash)
    View viewLeftDash;
    @BindView(R.id.view_right_dash)
    View viewRightDash;
    @BindView(R.id.image_add_member)
    ImageView imageAddMember;
    @BindView(R.id.text_label_add_member)
    CustomTextView textLabelAddMember;
    @BindView(R.id.view_bottom_dash)
    View viewBottomDash;
    @BindView(R.id.image_enabled_donations)
    ImageView imageEnabledDonations;
    @BindView(R.id.text_label_enable_donations)
    CustomTextView textLabelEnableDonations;
    @BindView(R.id.cons_dashboard)
    ConstraintLayout consDashboard;
    @BindView(R.id.text_label_dash_title)
    CustomTextView textLabelDashTitle;
    @BindView(R.id.text_label_dash_title_sub)
    CustomTextViewBold textLabelDashTitleSub;
    FloatingActionButton floatingActionButton;
    boolean isFromHospitalProfile;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @BindView(R.id.text_label_total_app_units)
    CustomTextView textLabelTotalAppUnits;
    @BindView(R.id.text_label_view_info_indi_units)
    CustomTextView textLabelViewInfoIndiUnits;
    @BindView(R.id.text_label_total_references)
    CustomTextView textLabelTotalReferences;
    @Inject
    FalconAPI falconAPI;
    @BindView(R.id.cons_dashboard_title)
    ConstraintLayout consDashboardTitle;
    @BindView(R.id.cons_dashboard_top)
    ConstraintLayout consDashboardTop;
    @BindView(R.id.cons_dashboard_middle)
    ConstraintLayout consDashboardMiddle;
    @BindView(R.id.cons_dashboard_bottom)
    ConstraintLayout consDashboardBottom;
    @BindView(R.id.text_avg_time)
    CustomTextView textAvgTime;
    @BindView(R.id.text_label_time)
    CustomTextView textLabelTime;
    private GetIndiUserResponse getIndiUserResponse;
    UserError userError;
    private HospitalLoginResponse hospitalLoginResponse;
    SharedPreferences fcmKeyPreferences, notificationPreferences;
    SharedPreferences.Editor notificationPreferencesEditor;
    LogoutResponse logoutResponse;
    private MenuItem notify, notifyBilicking, notify_badge;
    private NotificationReceiver notificationReceiver;
    static boolean notificationView;
    NotificationUpdateResponse notificationUpdateResponse;
    private ConstraintLayout consNotify;
    private TextView notifCount;
    PieView pieView;
    PieAngleAnimation pieAngleAnimation;
    private static int percentageVisibility = 0;

    @Override
    public void onResume() {
        super.onResume();

        if (isAdded()) {
            if (Utils.isNetworkAvailable()) {
                showProgress();
                updateFcmToken();
                if (sharedPreferences.getInt(FalconConstants.ROLEID, 0) == 0)
                //     Donor-0
                //                    NonDonor-1
                {

//                    if (sharedPreferences.getInt(FalconConstants.USERTYPE, 0) == 0)
//                        textLabelViewInfoIndiUnits.setVisibility(View.VISIBLE);
//                    else
//                        textLabelViewInfoIndiUnits.setVisibility(View.INVISIBLE);

                    getIndiProfile();
                } else {
                    getHosInfo();
                }
            } else {
                userError.message = getString(R.string.network_error_message);
                showError(ERROR_SHOW_TYPE_DIALOG, userError);
            }

        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        unbinder = ButterKnife.bind(this, view);
        homeActivity = (HomeActivity) getActivity();
        homeActivity.setTitle(getString(R.string.Falcon));
        homeActivity.getSupportActionBar().show();
        setHasOptionsMenu(true);
        notificationReceiver = new NotificationReceiver();
        FalconApplication.getContext().getComponent().inject(this);
        userError = new UserError();
        editor = sharedPreferences.edit();
        IntentFilter intentFilter = new IntentFilter("notificationListener");
        getActivity().registerReceiver(notificationReceiver, intentFilter);
        textLabelViewInfoIndiUnits.setVisibility(View.INVISIBLE);
        fcmKeyPreferences = getActivity().getSharedPreferences(FalconConstants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
        notificationPreferences = getActivity().getSharedPreferences(FalconConstants.NOTIFICATIONPERFRENCES, MODE_PRIVATE);
        notificationPreferencesEditor = notificationPreferences.edit();
        imageAddMember.setImageResource(R.mipmap.icon_user_activity);
        textLabelAddMember.setText(getString(R.string.user_activity));
        switch (sharedPreferences.getInt(FalconConstants.ROLEID, 0)) {
            case 0:
//                imageQuickRequest.setImageResource(R.mipmap.icon_quick_request);
//                textLabelQuickRequest.setText(getString(R.string.quick_request));
                textLabelViewInfo.setText(getString(R.string.me_family));
                break;
            case 1:
//                imageQuickRequest.setImageResource(R.mipmap.icon_quick_registration_dashboard);
//                textLabelQuickRequest.setText(getString(R.string.quick_reg));
                textLabelViewInfo.setText(getString(R.string.view_info));
                break;
        }
        floatingActionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab_request_blood);
        floatingActionButton.setVisibility(View.VISIBLE);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeActivity.push(new RequestBloodFragment(), getString(R.string.Request_Blood));
            }
        });


//        textLabelTotalReferences.setText("You Refered"+ sharedPreferences.getInt(FalconConstants.INDIDUVALUNITS,0)+ " Units Blood.");

        pieView = (PieView) view.findViewById(R.id.pieView);
        pieView.setPercentageBackgroundColor(getResources().getColor(R.color.falcon_red));


        return view;
    }

    @Override
    public boolean onBackPressed() {
        return super.onBackPressed();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        getActivity().unregisterReceiver(notificationReceiver);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.notification, menu);
        notify = menu.findItem(R.id.menu_bar_notify);
        notifyBilicking = menu.findItem(R.id.menu_bar_notify_bilicking);
//        notify_badge = menu.findItem(R.id.badge);
//        notifCount = (Button) notify_badge.findViewById(R.id.notif_count);
//        MenuItemCompat.setActionView(notify_badge, R.layout.feed_update_count);
//        consNotify = (ConstraintLayout) MenuItemCompat.getActionView(notify_badge);
//        notifCount=(TextView)consNotify.findViewById(R.id.notify_count) ;
//        notifCount.setText(""+1);
        notify.setVisible(true);

    }


    private void perVisibility() {
        if (percentageVisibility == 0) {
            textAvgTime.setVisibility(View.INVISIBLE);
            textLabelTime.setVisibility(View.INVISIBLE);
            textLabelQuickRequest.setText(getString(R.string.completed_request));
            pieView.setVisibility(View.VISIBLE);
            pieAngleAnimation = new PieAngleAnimation(pieView);
            pieAngleAnimation.setDuration(5000);
            percentageVisibility = 1;
        } else {
            textAvgTime.setVisibility(View.VISIBLE);
            textLabelTime.setVisibility(View.VISIBLE);
            textLabelQuickRequest.setText(getString(R.string.Avg_acceptance));
            pieView.setVisibility(View.INVISIBLE);
            percentageVisibility = 0;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_bar_notify:
                notify.setVisible(true);
                notifyBilicking.setVisible(false);
                Intent intent = new Intent(getActivity(), NotificationActivity.class);
                startActivity(intent);
                break;
            case R.id.menu_bar_notify_bilicking:
//                notificationPreferencesEditor.putBoolean(FalconConstants.NOTIFICATION,false).commit();
                Intent intent1 = new Intent(getActivity(), NotificationActivity.class);
                intent1.putExtra("isNotify", true);
                intent1.putExtra("notificationType", FirebaseMessaging.notifyPage);
                startActivity(intent1);
//                updateNotification(0);

                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @OnClick({R.id.image_info, R.id.image_quick_request, R.id.image_add_member, R.id.image_enabled_donations})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_info:
                homeActivity.push(new ViewInformationFragment(), getString(R.string.View_Info));
                break;
            case R.id.image_quick_request:

//                homeActivity.push(new FalconMapFragment(), getString(R.string.Quick_req));

//                switch (sharedPreferences.getInt(FalconConstants.ROLEID, 0)) {
//                    case 0:
//                        homeActivity.push(new QuickRequestFragment(), getString(R.string.Quick_req));
//                        break;
//                    case 1:
//                        homeActivity.push(new QuickRegistrationFragment(), getString(R.string.Quick_registration));
//                        break;
//                }
                break;
            case R.id.image_add_member:
                homeActivity.push(new UserActivityFragment(), getString(R.string.User_Activity));

//                switch (sharedPreferences.getInt(FalconConstants.ROLEID, 0)) {
//                    case 0:
//                        homeActivity.push(new AddMemberFragment(), getString(R.string.Add_Member));
//                        break;
//                    case 1:
//                        homeActivity.push(new UserActivityFragment(), getString(R.string.User_Activity));
//                        break;
//                }
                break;
            case R.id.image_enabled_donations:

                homeActivity.push(new BloodRequestListFragment(), getString(R.string.request_list));

//                switch (sharedPreferences.getInt(FalconConstants.ROLEID, 0)) {
//                    case 0:
//                        Intent intent = new Intent(getActivity(), FacilitatedDonationActivity.class);
//                        startActivity(intent);
//                        break;
//                    case 1:
//                        homeActivity.push(new AddtionalUsersFragment(), getString(R.string.fac_dontions));
//                        break;
//                }
//                break;
        }
    }

    private void getIndiProfile() {
        final GetUserInfo getUserInfo = new GetUserInfo();
        getUserInfo.setUserID(sharedPreferences.getInt(FalconConstants.USEDID, 0));
//        if (Utils.isNetworkAvailable()) {
//            showProgress();
        falconAPI.getUserProfile(getUserInfo).enqueue(new Callback<GetIndiUserResponse>() {
            @Override
            public void onResponse(Call<GetIndiUserResponse> call, Response<GetIndiUserResponse> response) {
                hideProgress();
                getIndiUserResponse = response.body();
                if (isAdded()) {
                    if (response.body() != null && response.isSuccessful() && getIndiUserResponse.getStatusCode() == 200) {
                        textLabelTotalAppUnits.setText("Falcon Donated " + getIndiUserResponse.getTotalUnits() + " Units Blood");
                        textLabelTotalAppUnits.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.text_view_rotation));

                        editor.putInt(FalconConstants.USEDID, getIndiUserResponse.getUserInfo().getId()).commit();
//                        editor.putInt(FalconConstants.INDIDUVALLIVES, getIndiUserResponse.getUserInfo().getLives()).commit();
                        editor.putInt(FalconConstants.INDIDUVALUNITS, getIndiUserResponse.getIndUnits()).commit();
                        editor.putInt(FalconConstants.FACILITATEDDONATIONTOTALUNITS, getIndiUserResponse.getTotal_user_blood_count()).commit();
                        editor.putInt(FalconConstants.FALCONTOTALUNITS, getIndiUserResponse.getTotalUnits()).commit();

                        editor.putString(FalconConstants.BLOODGROUP, getIndiUserResponse.getUserInfo().getBloodGroupID()).commit();
                        editor.putString(FalconConstants.USERNAME, getIndiUserResponse.getUserInfo().getFirstName()).commit();
                        editor.putInt(FalconConstants.AUTHORIZER, getIndiUserResponse.getUserInfo().getAuthorizer()).commit();
                        editor.putString(FalconConstants.PHONENUMBER, getIndiUserResponse.getUserInfo().getPhoneNumber()).commit();
                        editor.putString(FalconConstants.PROFILEPICTURE, getIndiUserResponse.getUserInfo().getProfilePicture()).commit();
                        editor.putInt(FalconConstants.USERTYPE, getIndiUserResponse.getUserInfo().getUserType()).commit();
                        editor.putInt(FalconConstants.REMAINIG_DAYS_TO_DONATE_BLOOD,
                                getIndiUserResponse.getBlood_ramaing_days()).commit();

                        if (getIndiUserResponse.getUserInfo().getPending_notification() == 1) {
                            notifyBilicking.setVisible(true);
                            notify.setVisible(false);
                        } else {
                            notifyBilicking.setVisible(false);
                            notify.setVisible(true);
                        }

                        startCountAnimation((int) getIndiUserResponse.getTime_percentage(), textAvgTime);
                        pieView.setPercentage((int) getIndiUserResponse.getPercentage());
                        perVisibility();


                    } else if (getIndiUserResponse.getStatusCode() == 401) {
                        AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(getActivity());
                        alertDialog1.setView(R.layout.inactive_user_alert_view);
                        alertDialog1.setCancelable(false);
                        alertDialog1.show();

                    } else {
//                        Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();

                    }
                }
            }

            @Override
            public void onFailure(Call<GetIndiUserResponse> call, Throwable t) {
                if (isAdded()) {
                    hideProgress();
//                    Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                }
            }
        });

//        }
//        else {
//            userError.message = getString(R.string.network_error_message);
//            showError(ERROR_SHOW_TYPE_DIALOG, userError);
//        }
    }


    private void getHosInfo() {
        GetHospitalInfo getHospitalInfo = new GetHospitalInfo();
        getHospitalInfo.setHosID(sharedPreferences.getInt(FalconConstants.HOSPITALUSERID, 0));
//        if (Utils.isNetworkAvailable()) {
//            showProgress();
        falconAPI.getHospInfo(getHospitalInfo).enqueue(new Callback<HospitalLoginResponse>() {
            @Override
            public void onResponse(Call<HospitalLoginResponse> call, Response<HospitalLoginResponse> response) {
                hideProgress();
                hospitalLoginResponse = response.body();
                if (isAdded()) {
                    if (response.body() != null && response.isSuccessful() && hospitalLoginResponse.getStatusCode() == 200) {
                        textLabelTotalAppUnits.setText("Falcon Donated " + hospitalLoginResponse.getTotalUnits() + " Units Blood");
                        textLabelTotalAppUnits.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.text_view_rotation));
//                            textLabelViewInfoIndiUnits.setText("You Donated " + hospitalLoginResponse.getUserinfo().getIndUnits() + " Units Blood");
                        editor.putInt(FalconConstants.HOSPUNITS, hospitalLoginResponse.getUserinfo().getIndUnits()).commit();
                        editor.putInt(FalconConstants.HOSPLIVES, hospitalLoginResponse.getUserinfo().getLives()).commit();
                        editor.putInt(FalconConstants.HOSPTOTALUNITS, hospitalLoginResponse.getTotalUnits()).commit();
                        editor.putString(FalconConstants.HOSPITALREGISTERID, hospitalLoginResponse.getUserinfo().getRegisterID()).commit();
                        editor.putInt(FalconConstants.HOSPITALUSERID, hospitalLoginResponse.getUserinfo().getId()).commit();
                        editor.putInt(FalconConstants.FACILITATEDHOSPDONATIONTOTALUNITS, hospitalLoginResponse.getTotal_user_blood_count()).commit();
                        //                        editor.putInt(FalconConstants.HOSPLIVES, hospitalLoginResponse.getUserinfo().getLives()).commit();
                        editor.putString(FalconConstants.USERNAME, hospitalLoginResponse.getUserinfo().getName()).commit();
                        editor.putString(FalconConstants.PHONENUMBER, hospitalLoginResponse.getUserinfo().getPhoneNumber()).commit();
                        editor.putString(FalconConstants.PROFILEPICTURE, hospitalLoginResponse.getUserinfo().getPhoto()).commit();
                        if (hospitalLoginResponse.getUserinfo().getPending_notification() == 1) {
                            notifyBilicking.setVisible(true);
                            notify.setVisible(false);
                        } else {
                            notifyBilicking.setVisible(false);
                            notify.setVisible(true);
                        }
                        startCountAnimation((int) hospitalLoginResponse.getTime_percentage(), textAvgTime);
                        pieView.setPercentage((int) hospitalLoginResponse.getPercentage());
                        perVisibility();


                    } else if (hospitalLoginResponse.getStatusCode() == 401) {
                        AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(getActivity());
                        alertDialog1.setView(R.layout.inactive_user_alert_view);
                        alertDialog1.setCancelable(false);
                        alertDialog1.show();

                    } else {
//                        Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();

                    }
                }
            }

            @Override
            public void onFailure(Call<HospitalLoginResponse> call, Throwable t) {
                if (isAdded()) {
                    hideProgress();
//                    Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                }

            }
        });
//        } else {
//            userError.message = getString(R.string.network_error_message);
//            showError(ERROR_SHOW_TYPE_DIALOG, userError);
//        }
    }

    public void updateFcmToken() {
        UpdateFcmToken updateFcmToken = new UpdateFcmToken();
        updateFcmToken.setFCMKey(fcmKeyPreferences.getString(FalconConstants.FCMTOKEN, ""));
        if (sharedPreferences.getInt(FalconConstants.ROLEID, 0) == 0)
            updateFcmToken.setId(sharedPreferences.getInt(FalconConstants.USEDID, 0));
        else
            updateFcmToken.setId(sharedPreferences.getInt(FalconConstants.HOSPITALUSERID, 0));

        falconAPI.updateFcmToken(updateFcmToken).enqueue(new Callback<LogoutResponse>() {
            @Override
            public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {
//                hideProgress();
                logoutResponse = response.body();
                if (response.isSuccessful() && response.body() != null && logoutResponse.getStatusCode() == 200) {
//                    Toast.makeText(getActivity(),"FCM Updated Success",Toast.LENGTH_SHORT).show();
                } else {
//                    Toast.makeText(getActivity(),"FCM Updated UnSuccess",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LogoutResponse> call, Throwable t) {
//                hideProgress();
//                Toast.makeText(getActivity(),"FCM Updated Faliure",Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void updateNotification(int status) {
        NotificationUpdate notificationUpdate = new NotificationUpdate();
        notificationUpdate.setStatus(status);
        if (sharedPreferences.getInt(FalconConstants.ROLEID, 0) == 0) {
            notificationUpdate.setUser_id(sharedPreferences.getInt(FalconConstants.USEDID, 0));
            notificationUpdate.setUser_type(1);
        } else {
            notificationUpdate.setUser_id(sharedPreferences.getInt(FalconConstants.HOSPITALUSERID, 0));
            notificationUpdate.setUser_type(0);
        }

        if (Utils.isNetworkAvailable()) {

            falconAPI.updateNotification(notificationUpdate).enqueue(new Callback<NotificationUpdateResponse>() {
                @Override
                public void onResponse(Call<NotificationUpdateResponse> call, Response<NotificationUpdateResponse> response) {
                    notificationUpdateResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && notificationUpdateResponse.getStatusCode() == 200) {
                        notifyBilicking.setVisible(false);
                        notify.setVisible(true);
                    }
                }

                @Override
                public void onFailure(Call<NotificationUpdateResponse> call, Throwable t) {

                }
            });

        } else {
            userError.message = getString(R.string.network_error_message);
            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }


    }

    public class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getBooleanExtra("notify", false)) {
//                notificationView=true;
                notifyBilicking.setVisible(true);
                notify.setVisible(false);
            }
//            else {
//                notify.setVisible(true);
//                notifyBilicking.setVisible(false);
//            }
        }
    }
}

