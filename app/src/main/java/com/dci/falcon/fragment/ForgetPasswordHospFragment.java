package com.dci.falcon.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.activity.ChangePasswordActivity;
import com.dci.falcon.activity.LoginActivity;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.model.ChangeHospPassword;
import com.dci.falcon.model.LogoutResponse;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomButton;
import com.dci.falcon.view.CustomEditText;
import com.dci.falcon.view.CustomTextView;
import com.dci.falcon.view.CustomTextViewBold;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

/**
 * Created by vijayaganesh on 1/2/2018.
 */

public class ForgetPasswordHospFragment extends BaseFragment {
    @BindView(R.id.image_icon_forget)
    ImageView imageIconForget;
    @BindView(R.id.text_label_forget_password)
    CustomTextViewBold textLabelForgetPassword;
    @BindView(R.id.text_label_enter_new_pass)
    CustomTextView textLabelEnterNewPass;
    @BindView(R.id.edit_reg_id)
    CustomEditText editRegId;
    @BindView(R.id.text_input_reg_id)
    TextInputLayout textInputRegId;
    @BindView(R.id.edit_new_password)
    CustomEditText editNewPassword;
    @BindView(R.id.text_input_new_password)
    TextInputLayout textInputNewPassword;
    @BindView(R.id.edit_retype_new_password)
    CustomEditText editRetypeNewPassword;
    @BindView(R.id.text_input_retype_new_password)
    TextInputLayout textInputRetypeNewPassword;
    @BindView(R.id.button_ok)
    CustomButton buttonChange;
    @BindView(R.id.cons_forget_password_hosp)
    ConstraintLayout consForgetPasswordHosp;
    Unbinder unbinder;
    @Inject
    FalconAPI falconAPI;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    UserError userError;
    LogoutResponse changeHospPasswordResponse;
    @BindView(R.id.edit_old_password)
    CustomEditText editOldPassword;
    @BindView(R.id.text_input_old_password)
    TextInputLayout textInputOldPassword;
    private FloatingActionButton floatingActionButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forget_password_hosp, container, false);
        unbinder = ButterKnife.bind(this, view);
        FalconApplication.getContext().getComponent().inject(this);
        userError = new UserError();
        editor = sharedPreferences.edit();
        if (getActivity() instanceof ChangePasswordActivity) {
            textInputOldPassword.setVisibility(View.VISIBLE);
            editRegId.setVisibility(View.GONE);
            getActivity().setTitle(getString(R.string.reset_password));
            textLabelForgetPassword.setText(getString(R.string.reset_password));
            floatingActionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab_request_blood);
            floatingActionButton.setVisibility(View.GONE);
            floatingActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                notificationActivity.push(new RequestBloodFragment(), getString(R.string.Request_Blood));
                }
            });
        } else {
            textInputOldPassword.setVisibility(View.GONE);
            textLabelForgetPassword.setText(getString(R.string.forget_password));
            getActivity().setTitle(getString(R.string.forget_password));
            editRegId.setVisibility(View.VISIBLE);
        }
        editRetypeNewPassword.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    if (isResetPasswordNotNull()) {
                        AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(getActivity());
                        alertDialog1.setMessage(getString(R.string.alert_change_password));
                        alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                changePassword();

                            }
                        });
                        alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        alertDialog1.show();
                    } else {
//                        android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(getActivity());
//                        alertDialog.setMessage(getString(R.string.alert_mes_profile));
//                        alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                dialogInterface.dismiss();
//                                hideProgress();
//                            }
//                        });
//                        alertDialog.show();
                    }
                }
                return false;
            }
        });

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.button_ok)
    public void onViewClicked() {
        if (isResetPasswordNotNull()) {
            AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(getActivity());
            alertDialog1.setMessage(getString(R.string.alert_change_password));
            alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    changePassword();

                }
            });
            alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            alertDialog1.show();
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setMessage(getString(R.string.alert_mes_profile));
            alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    hideProgress();
                }
            });
            alertDialog.show();
        }

    }

    private void changePassword() {
        ChangeHospPassword changeHospPassword = new ChangeHospPassword();
        changeHospPassword.setPassword(editRetypeNewPassword.getText().toString());
        if (getActivity() instanceof ChangePasswordActivity) {
//            ChangePassword
            changeHospPassword.setRegisterID(sharedPreferences.getString(FalconConstants.HOSPITALREGISTERID, ""));
            changeHospPassword.setType(2);
            changeHospPassword.setOld_password(editOldPassword.getText().toString());
        } else {
//            ForgetPassword
            changeHospPassword.setType(1);
            changeHospPassword.setRegisterID(editRegId.getText().toString());
        }

        if (Utils.isNetworkAvailable()) {
            showProgress();
            falconAPI.changeHospPassword(changeHospPassword).enqueue(new Callback<LogoutResponse>() {
                @Override
                public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {
                    hideProgress();
                    changeHospPasswordResponse = response.body();
                    if (isAdded()) {
                        if (response.isSuccessful() && response.body() != null && changeHospPasswordResponse.getStatusCode() == 200) {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                            alertDialog.setMessage(getString(R.string.successfully_update_pass));
                            alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    if (getActivity() instanceof LoginActivity) {
                                        Intent intent_admin_logout = new Intent(getActivity(), LoginActivity.class);
                                        intent_admin_logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        getActivity().finish();
                                        startActivity(intent_admin_logout);
                                        editor.putInt(FalconConstants.LOGINSTATUSCODE, 0).commit();
                                    } else if (getActivity() instanceof ChangePasswordActivity) {
                                        getActivity().onBackPressed();
                                    }
                                }
                            });
                            alertDialog.show();

                        } else if (changeHospPasswordResponse.getStatusCode() == 401) {
                            AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(getActivity());
                            alertDialog1.setView(R.layout.inactive_user_alert_view);
                            alertDialog1.setCancelable(false);
                            alertDialog1.show();
                        } else if (changeHospPasswordResponse.getStatusCode() == 501) {
                            Toast.makeText(getActivity(), getString(R.string.old_password_not_mathed), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.vaild_reg_id), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<LogoutResponse> call, Throwable t) {
                    if (isAdded()) {
                        hideProgress();
                        Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }

                }
            });
        } else {
            userError.message = getActivity().getString(R.string.network_error_message);
            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }
    }


    private boolean showErrorNewPassword() {
        if (editNewPassword.getText().toString().isEmpty()) {
            textInputNewPassword.setError(getString(R.string.req_first_pasword));
            return false;
        } else if (editNewPassword.getText().length() < 8) {
            textInputNewPassword.setError(getActivity().getString(R.string.min_pass));
            return false;
        } else {
            textInputNewPassword.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorRegId() {
        if (editRegId.getText().toString().isEmpty()) {
            textInputRegId.setError(getString(R.string.req_reg_id));
            return false;
        } else {
            textInputRegId.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorRegId1() {
        if (editRegId.getText().toString().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorNewPassword1() {
        if (editNewPassword.getText().toString().isEmpty()) {
            return false;
        } else if (editNewPassword.getText().length() < 8) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorRetypePassword() {
        if (editRetypeNewPassword.getText().toString().isEmpty()) {
            textInputRetypeNewPassword.setError(getString(R.string.req_first_pasword));
            return false;
        } else if (editRetypeNewPassword.getText().length() < 8) {
            textInputRetypeNewPassword.setError(getActivity().getString(R.string.min_pass));
            return false;
        } else {
            textInputRetypeNewPassword.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorRetypePassword1() {
        if (editRetypeNewPassword.getText().toString().isEmpty()) {
            return false;
        } else if (editRetypeNewPassword.getText().length() < 8) {
            return false;
        } else {
            return true;
        }
    }

    private boolean isResetPasswordNotNull() {
        showErrorNewPassword();
        showErrorRetypePassword();
        if (getActivity() instanceof ChangePasswordActivity) {
            if (showErrorRetypePassword1() && showErrorNewPassword1()) {
                if (editNewPassword.getText().toString().matches(editRetypeNewPassword.getText().toString())) {
                    return true;
                } else {
                    userError.message = getActivity().getString(R.string.mismatch_password);
                    showError(ERROR_SHOW_TYPE_DIALOG, userError);
                    return false;
                }
            } else {
                return false;
            }
        } else {
            showErrorRegId();
            if (showErrorRetypePassword1() && showErrorNewPassword1() && showErrorRegId1()) {
                if (editNewPassword.getText().toString().matches(editRetypeNewPassword.getText().toString())) {
                    return true;
                } else {
                    userError.message = getActivity().getString(R.string.mismatch_password);
                    showError(ERROR_SHOW_TYPE_DIALOG, userError);
                    return false;
                }
            } else {
                return false;
            }
        }

    }
}
