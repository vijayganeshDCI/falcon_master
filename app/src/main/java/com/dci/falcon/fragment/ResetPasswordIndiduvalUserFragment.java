package com.dci.falcon.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.activity.ChangePasswordActivity;
import com.dci.falcon.activity.LoginActivity;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.model.ResetPassword;
import com.dci.falcon.model.ResetPasswordResponse;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomButton;
import com.dci.falcon.view.CustomEditText;
import com.dci.falcon.view.CustomTextView;
import com.dci.falcon.view.CustomTextViewBold;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

/**
 * Created by vijayaganesh on 11/30/2017.
 */

public class ResetPasswordIndiduvalUserFragment extends BaseFragment {
    @BindView(R.id.image_icon_forget)
    ImageView imageIconForget;
    @BindView(R.id.text_label_reset_password)
    CustomTextViewBold textLabelResetPassword;
    @BindView(R.id.text_label_new_password)
    CustomTextView textLabelNewPassword;
    @BindView(R.id.text_input_new_password)
    TextInputLayout textInputNewPassword;
    @BindView(R.id.text_input_retype_new_password)
    TextInputLayout textInputRetypeNewPassword;
    @BindView(R.id.button_verify)
    CustomButton buttonVerify;
    @BindView(R.id.cons_reset_password)
    ConstraintLayout consResetPassword;
    Unbinder unbinder;
    @Inject
    FalconAPI falconAPI;
    @BindView(R.id.edit_new_password)
    CustomEditText editNewPassword;
    @BindView(R.id.edit_retype_new_password)
    CustomEditText editRetypeNewPassword;
    String phoneNumber;
    UserError userError;
    ResetPasswordResponse resetPasswordResponse;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @BindView(R.id.edit_old_password)
    CustomEditText editOldPassword;
    @BindView(R.id.text_input_old_password)
    TextInputLayout textInputOldPassword;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reset_password, container, false);
        unbinder = ButterKnife.bind(this, view);
        FalconApplication.getContext().getComponent().inject(this);
        getActivity().setTitle(getString(R.string.reset_password));
        Bundle bundle = getArguments();
        editor = sharedPreferences.edit();
        userError = new UserError();
        editRetypeNewPassword.setFilters(new InputFilter[]{editTextRestrictLength(), editTextRestrictSmileys()});
        editNewPassword.setFilters(new InputFilter[]{editTextRestrictLength(), editTextRestrictSmileys()});
//        editRetypeNewPassword.addTextChangedListener(textWatcher);
//        editNewPassword.addTextChangedListener(textWatcher);
        if (getActivity() instanceof LoginActivity) {
            textInputOldPassword.setVisibility(View.GONE);
            if (bundle != null) {
                phoneNumber = bundle.getString("phoneNumber");
            }
        } else if (getActivity() instanceof ChangePasswordActivity) {
            textInputOldPassword.setVisibility(View.VISIBLE);
            phoneNumber = sharedPreferences.getString(FalconConstants.PHONENUMBER, "");
        }

        editRetypeNewPassword.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    if (isResetPasswordNotNull()) {
                        AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(getActivity());
                        alertDialog1.setMessage(getString(R.string.alert_change_password));
                        alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                resetPassword();
                            }
                        });
                        alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        alertDialog1.show();
                    } else {
//                        android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(getActivity());
//                        alertDialog.setMessage(getString(R.string.alert_mes_profile));
//                        alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                dialogInterface.dismiss();
//                                hideProgress();
//                            }
//                        });
//                        alertDialog.show();
                    }

                }
                return false;
            }
        });
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.button_verify)
    public void onViewClicked() {

        if (isResetPasswordNotNull()) {
            AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(getActivity());
            alertDialog1.setMessage(getString(R.string.alert_change_password));
            alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    resetPassword();
                }
            });
            alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            alertDialog1.show();
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setMessage(getString(R.string.alert_mes_profile));
            alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    hideProgress();
                }
            });
            alertDialog.show();
        }


    }


    private void resetPassword() {
        ResetPassword resetPassword = new ResetPassword();
        if (getActivity() instanceof LoginActivity){
            //Forget Password
            resetPassword.setPhone_no(phoneNumber);
            resetPassword.setType(1);
            resetPassword.setPassword(editRetypeNewPassword.getText().toString());
        }else{
            // ChangePassword
            resetPassword.setPhone_no(phoneNumber);
            resetPassword.setType(2);
            resetPassword.setPassword(editRetypeNewPassword.getText().toString());
            resetPassword.setOld_password(editOldPassword.getText().toString());
        }


        if (Utils.isNetworkAvailable()) {
            showProgress();
            falconAPI.resetPassword(resetPassword).enqueue(new Callback<ResetPasswordResponse>() {
                @Override
                public void onResponse(Call<ResetPasswordResponse> call, Response<ResetPasswordResponse> response) {
                    hideProgress();
                    resetPasswordResponse = response.body();
                    if (isAdded()) {
                        if (response.isSuccessful() && response.body() != null && resetPasswordResponse.getStatusCode() == 200) {

                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                            alertDialog.setMessage(getString(R.string.successfully_update_pass));
                            alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    if (getActivity() instanceof LoginActivity) {
                                        Intent intent_admin_logout = new Intent(getActivity(), LoginActivity.class);
                                        intent_admin_logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        getActivity().finish();
                                        startActivity(intent_admin_logout);
                                        editor.putInt(FalconConstants.LOGINSTATUSCODE, 0).commit();
                                    } else if (getActivity() instanceof ChangePasswordActivity) {
                                        getActivity().onBackPressed();
                                    }
                                }
                            });
                            alertDialog.show();


                        } else if (resetPasswordResponse.getStatusCode() == 501){
                            Toast.makeText(getActivity(), getString(R.string.old_password_not_mathed), Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show(); }
                    }
                }

                @Override
                public void onFailure(Call<ResetPasswordResponse> call, Throwable t) {
                    if (isAdded()) {
                        hideProgress();
                        Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }


                }
            });

        } else {
            userError.message = getActivity().getString(R.string.network_error_message);
            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }
    }

//
//    private boolean passwordValidation() {
//        if (!editNewPassword.getText().toString().isEmpty() && !editRetypeNewPassword.getText().toString().isEmpty()
//                && editRetypeNewPassword.getText().toString().equals(editNewPassword.getText().toString())) {
//            buttonVerify.setBackgroundColor(getResources().getColor(R.color.falcon_red));
//            return true;
//        } else {
//            buttonVerify.setBackgroundColor(getResources().getColor(R.color.light_grey));
//            return false;
//        }
//    }
//
//    TextWatcher textWatcher = new TextWatcher() {
//        @Override
//        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//        }
//
//        @Override
//        public void onTextChanged(CharSequence s, int start, int before, int count) {
//            passwordValidation();
//        }
//
//        @Override
//        public void afterTextChanged(Editable s) {
//
//        }
//    };

    private boolean showErrorNewPassword() {
        if (editNewPassword.getText().toString().isEmpty()) {
            textInputNewPassword.setError(getString(R.string.req_first_pasword));
            return false;
        } else if (editNewPassword.getText().length() < 8) {
            textInputNewPassword.setError(getActivity().getString(R.string.min_pass));
            return false;
        } else {
            textInputNewPassword.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorNewPassword1() {
        if (editNewPassword.getText().toString().isEmpty()) {
            return false;
        } else if (editNewPassword.getText().length() < 8) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorRetypePassword() {
        if (editRetypeNewPassword.getText().toString().isEmpty()) {
            textInputRetypeNewPassword.setError(getString(R.string.req_first_pasword));
            return false;
        } else if (editRetypeNewPassword.getText().length() < 8) {
            textInputRetypeNewPassword.setError(getActivity().getString(R.string.min_pass));
            return false;
        } else {
            textInputRetypeNewPassword.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorRetypePassword1() {
        if (editRetypeNewPassword.getText().toString().isEmpty()) {
            return false;
        } else if (editRetypeNewPassword.getText().length() < 8) {
            return false;
        } else {
            return true;
        }
    }

    private boolean isResetPasswordNotNull() {
        showErrorNewPassword();
        showErrorRetypePassword();
        if (showErrorRetypePassword1() && showErrorNewPassword1()) {
            if (editNewPassword.getText().toString().matches(editRetypeNewPassword.getText().toString())) {
                return true;
            } else {
                userError.message = getActivity().getString(R.string.mismatch_password);
                showError(ERROR_SHOW_TYPE_DIALOG, userError);
                return false;
            }
        } else {
            return false;
        }
    }
}
