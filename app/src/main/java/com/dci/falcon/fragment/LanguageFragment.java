package com.dci.falcon.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.dci.falcon.R;
import com.dci.falcon.activity.HomeActivity;
import com.dci.falcon.activity.LanguageActivity;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.view.CustomRadioButton;
import com.dci.falcon.view.CustomTextView;

import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by vijayaganesh on 11/10/2017.
 */

public class LanguageFragment extends BaseFragment {

    FloatingActionButton floatingActionButton;
    LanguageActivity languageActivity;
    @BindView(R.id.text_label_select_language)
    CustomTextView textLabelSelectLanguage;
    @BindView(R.id.radio_button_english)
    CustomRadioButton radioButtonEnglish;
    @BindView(R.id.radio_button_tamil)
    CustomRadioButton radioButtonTamil;
    @BindView(R.id.radio_button_malayalam)
    CustomRadioButton radioButtonMalayalam;
    @BindView(R.id.radio_button_telugu)
    CustomRadioButton radioButtonTelugu;
    @BindView(R.id.radio_button_kanada)
    CustomRadioButton radioButtonKanada;
    @BindView(R.id.radio_button_hindi)
    CustomRadioButton radioButtonHindi;
    @BindView(R.id.radio_button_urdu)
    CustomRadioButton radioButtonUrdu;
    @BindView(R.id.radio_language)
    RadioGroup radioLanguage;
    @BindView(R.id.cons_language)
    ConstraintLayout consLanguage;
    Unbinder unbinder;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String language="en";
    static int selectedLanguage=0;
    MenuItem save;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_language, container, false);
        unbinder = ButterKnife.bind(this, view);
        getActivity().setTitle(getString(R.string.Language));
        languageActivity = (LanguageActivity) getActivity();
        FalconApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        setHasOptionsMenu(true);
        floatingActionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab_request_blood);
        floatingActionButton.setVisibility(View.GONE);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                languageActivity.push(new RequestBloodFragment(), getString(R.string.Request_Blood));
            }
        });
        setLangauge(selectedLanguage);
        radioLanguage.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.radio_button_english:
                        selectedLanguage=0;
                        language = "en";
                        break;
                    case R.id.radio_button_tamil:
                        selectedLanguage=1;
                        language = "ta";
                        break;
                    case R.id.radio_button_malayalam:
                        selectedLanguage=2;
                        language = "ml";
                        break;
                    case R.id.radio_button_telugu:
                        selectedLanguage=3;
                        language = "te";
                        break;
                    case R.id.radio_button_kanada:
                        selectedLanguage=4;
                        language = "kn";
                        break;
                    case R.id.radio_button_hindi:
                        selectedLanguage=5;
                        language = "hi";
                        break;
                    case R.id.radio_button_urdu:
                        selectedLanguage=6;
                        language = "ur";
                        break;
                }
            }
        });
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.save, menu);
        save = menu.findItem(R.id.menu_bar_save);
        save.setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_bar_save:
                AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(getActivity());
                alertDialog1.setMessage(getString(R.string.alert_change_language));
                alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        setLocale(language);
                    }
                });
                alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                alertDialog1.show();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(getActivity(), HomeActivity.class);
        refresh.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        getActivity().finish();
        startActivity(refresh);
    }

    private void setLangauge(int selectedLanguage){
        switch (selectedLanguage){
            case 0:
                radioButtonEnglish.setChecked(true);
                break;
            case 1:
                radioButtonTamil.setChecked(true);
                break;
            case 2:
                radioButtonMalayalam.setChecked(true);
                break;
            case 3:
                radioButtonTelugu.setChecked(true);
                break;
            case 4:
                radioButtonKanada.setChecked(true);
                break;
            case 5:
                radioButtonHindi.setChecked(true);
                break;
            case 6:
                radioButtonUrdu.setChecked(true);
                break;

        }

    }
}
