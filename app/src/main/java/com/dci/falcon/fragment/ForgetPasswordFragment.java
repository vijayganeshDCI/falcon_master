package com.dci.falcon.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.activity.LoginActivity;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.model.UniqueNumber;
import com.dci.falcon.model.UniqueNumberResponse;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomButton;
import com.dci.falcon.view.CustomEditText;
import com.dci.falcon.view.CustomTextView;
import com.dci.falcon.view.CustomTextViewBold;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

/**
 * Created by vijayaganesh on 11/29/2017.
 */

public class ForgetPasswordFragment extends BaseFragment {
    @BindView(R.id.edit_forget_number)
    CustomEditText editForgetNumber;
    @BindView(R.id.text_input_forget_password_number)
    TextInputLayout textInputForgetPasswordNumber;
    @BindView(R.id.cons_forget_password)
    ConstraintLayout consForgetPassword;
    Unbinder unbinder;
    @BindView(R.id.button_ok)
    CustomButton buttonChangePassword;
    @BindView(R.id.image_icon_forget)
    ImageView imageIconForget;
    @BindView(R.id.text_label_forget_password)
    CustomTextViewBold textLabelForgetPassword;
    @BindView(R.id.text_label_enter_number)
    CustomTextView textLabelEnterNumber;
    LoginActivity loginActivity;
    @Inject
    FalconAPI falconAPI;
    UniqueNumberResponse uniqueNumberResponse;
    UserError userError;
    private FloatingActionButton floatingActionButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forget_password, container, false);
        unbinder = ButterKnife.bind(this, view);
        loginActivity = (LoginActivity) getActivity();
        FalconApplication.getContext().getComponent().inject(this);
        userError = new UserError();
        getActivity().setTitle(getString(R.string.forget_password));
//        editForgetNumber.addTextChangedListener(textWatcher);
        editForgetNumber.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    if (isForgetPassNotNull())
                        isNewNumber();
                }
                return false;
            }
        });
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.button_ok)
    public void onViewClicked() {
        if (isForgetPassNotNull())
            isNewNumber();

    }

//    TextWatcher textWatcher = new TextWatcher() {
//        @Override
//        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//        }
//
//        @Override
//        public void onTextChanged(CharSequence s, int start, int before, int count) {
//            isForgetPassNotNull();
//        }
//
//        @Override
//        public void afterTextChanged(Editable s) {
//
//        }
//    };

    private boolean isForgetPassNotNull() {
        if (editForgetNumber.getText().toString().isEmpty()) {
            textInputForgetPasswordNumber.setError(getString(R.string.req_pho));
            return false;
        } else if (editForgetNumber.getText().toString().length() != 10) {
            textInputForgetPasswordNumber.setError(getString(R.string.req_phone_number));
            return false;
        } else {
            textInputForgetPasswordNumber.setErrorEnabled(false);
            return true;
        }

    }

    private void isNewNumber() {
        UniqueNumber uniqueNumber = new UniqueNumber();
        uniqueNumber.setPhoneNumber(editForgetNumber.getText().toString());
        if (Utils.isNetworkAvailable()) {
            showProgress();
            falconAPI.checkUniqueNumber(uniqueNumber).enqueue(new Callback<UniqueNumberResponse>() {
                @Override
                public void onResponse(Call<UniqueNumberResponse> call, Response<UniqueNumberResponse> response) {
                    hideProgress();
                    uniqueNumberResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && uniqueNumberResponse.getStatusCode() != 500) {
                        //mobile number exist
                        SequrityQuestionFragment sequrityQuestionFragment = new SequrityQuestionFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("phoneNumber", editForgetNumber.getText().toString());
                        bundle.putBoolean("forChangeNumber",false);
                        sequrityQuestionFragment.setArguments(bundle);
                        loginActivity.push(sequrityQuestionFragment, getString(R.string.Verification));
                    } else if(uniqueNumberResponse.getStatusCode() == 401) {
                        AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(getActivity());
                        alertDialog1.setView(R.layout.inactive_user_alert_view);
                        alertDialog1.setCancelable(false);
                        alertDialog1.show();
                    }else {
                        // mobile number not exist
                        Toast.makeText(getActivity(), getString(R.string.sign_up_falcon), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<UniqueNumberResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            userError.message = getActivity().getString(R.string.network_error_message);
            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }
    }
}
