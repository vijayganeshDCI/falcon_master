package com.dci.falcon.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.adapter.TrackMyRequestDetailAdapter;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.model.TrackMyRequestDetail;
import com.dci.falcon.model.TrackMyRequestDetailResponse;
import com.dci.falcon.model.TrackMyRequestHospDetailResponse;
import com.dci.falcon.model.TrackRequest;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomTextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

/**
 * Created by vijayaganesh on 1/5/2018.
 */

public class TrackMyRequestDetailFragment extends BaseFragment {
    @BindView(R.id.image_empty_member)
    ImageView imageEmptyMember;
    @BindView(R.id.text_label_empty_member)
    CustomTextView textLabelEmptyMember;
    @BindView(R.id.cons_empty_list)
    ConstraintLayout consEmptyList;
    @BindView(R.id.list_track_my_request)
    ListView listTrackMyRequest;
    @BindView(R.id.swiperefresh_track_req)
    SwipeRefreshLayout swiperefreshTrackReq;
    Unbinder unbinder;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    FalconAPI falconAPI;
    UserError userError;
    List<TrackMyRequestDetail> trackMyRequestDetailList;
    private int trackID;
    TrackMyRequestDetailAdapter trackMyRequestDetailAdapter;
    TrackMyRequestDetailResponse trackMyRequestDetailResponse;
    TrackMyRequestHospDetailResponse trackMyRequestHospDetailResponse;
    private FloatingActionButton floatingActionButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_track_my_request_detail, container, false);
        unbinder = ButterKnife.bind(this, view);
        FalconApplication.getContext().getComponent().inject(this);
        userError = new UserError();
        getActivity().setTitle(R.string.Donor_list);
        trackMyRequestDetailList = new ArrayList<TrackMyRequestDetail>();
        if (getArguments() != null) {
            Bundle bundle = getArguments();
            trackID = bundle.getInt("trackDetailID", 0);
        }
        if (isAdded()) {
            if (sharedPreferences.getInt(FalconConstants.ROLEID, 0) == 0)
                getTrackDetail(trackID);
            else
                getHospTrackDetail();
        }
        swiperefreshTrackReq.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swiperefreshTrackReq.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isAdded()) {
                            if (sharedPreferences.getInt(FalconConstants.ROLEID, 0) == 0)
                                getTrackDetail(trackID);
                            else
                                getHospTrackDetail();
                        }
                        swiperefreshTrackReq.setRefreshing(false);
                    }
                }, 3000);
            }
        });
        floatingActionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab_request_blood);
        floatingActionButton.setVisibility(View.GONE);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                notificationActivity.push(new RequestBloodFragment(), getString(R.string.Request_Blood));
            }
        });
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void getTrackDetail(int trackID) {
        TrackRequest trackRequest = new TrackRequest();
        trackRequest.setId(trackID);
        if (Utils.isNetworkAvailable()) {
            showProgress();
            trackMyRequestDetailList.clear();
            falconAPI.trackDetail(trackRequest).enqueue(new Callback<TrackMyRequestDetailResponse>() {
                @Override
                public void onResponse(Call<TrackMyRequestDetailResponse> call, Response<TrackMyRequestDetailResponse> response) {
                    hideProgress();
                    trackMyRequestDetailResponse = response.body();
                    if (isAdded()) {
                        if (response.isSuccessful() && response.isSuccessful() && trackMyRequestDetailResponse.getStatusCode() == 200
                                && !trackMyRequestDetailResponse.getTrackMyRequestDetail().isEmpty()) {
                            consEmptyList.setVisibility(View.GONE);
                            for (int i = 0; i < trackMyRequestDetailResponse.getTrackMyRequestDetail().size(); i++) {
                                trackMyRequestDetailList.add(new TrackMyRequestDetail(trackMyRequestDetailResponse.getTrackMyRequestDetail().get(i).getF_user().getProfilePicture(),
                                        trackMyRequestDetailResponse.getTrackMyRequestDetail().get(i).getF_user().getFirstName(),
                                        trackMyRequestDetailResponse.getTrackMyRequestDetail().get(i).getCreatedDate(),
                                        trackMyRequestDetailResponse.getTrackMyRequestDetail().get(i).getF_user().getPhoneNumber(),
                                        trackMyRequestDetailResponse.getTrackMyRequestDetail().get(i).getUserResStatus()));
                            }
                            trackMyRequestDetailAdapter = new TrackMyRequestDetailAdapter(getActivity(), trackMyRequestDetailList);
                            listTrackMyRequest.setAdapter(trackMyRequestDetailAdapter);
                            Collections.sort(trackMyRequestDetailList, new Comparator<TrackMyRequestDetail>() {
                                public int compare(TrackMyRequestDetail obj1, TrackMyRequestDetail obj2) {
                                    // ## Ascending order
                                    //                                return Integer.valueOf(obj2.getId()).compareTo(obj1.getId()); // To compare integer values
                                    // ## Descending order
                                    return obj2.getDateTime().compareToIgnoreCase(obj1.getDateTime()); // To compare string values
                                    // return Integer.valueOf(obj2.empId).compareTo(obj1.empId); // To compare integer values
                                }
                            });
                        } else {
                            consEmptyList.setVisibility(View.VISIBLE);
                        }
                    }
                }

                @Override
                public void onFailure(Call<TrackMyRequestDetailResponse> call, Throwable t) {
                    if (isAdded()) {
                        hideProgress();
                        consEmptyList.setVisibility(View.VISIBLE);
                        Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else {
            if (isAdded()) {
                consEmptyList.setVisibility(View.VISIBLE);
                userError.message = getActivity().getString(R.string.network_error_message);
                showError(ERROR_SHOW_TYPE_DIALOG, userError);
            }
        }
    }

    private void getHospTrackDetail() {
        TrackRequest trackRequest = new TrackRequest();
        trackRequest.setId(trackID);
        if (Utils.isNetworkAvailable()) {
            showProgress();
            trackMyRequestDetailList.clear();
            falconAPI.getHosTrackMyRequestDetail(trackRequest).enqueue(new Callback<TrackMyRequestHospDetailResponse>() {
                @Override
                public void onResponse(Call<TrackMyRequestHospDetailResponse> call, Response<TrackMyRequestHospDetailResponse> response) {
                    hideProgress();
                    trackMyRequestHospDetailResponse = response.body();
                    if (isAdded()) {
                        if (response.isSuccessful() && response.isSuccessful() && trackMyRequestHospDetailResponse.getStatusCode() == 200
                                && !trackMyRequestHospDetailResponse.getTrackMyRequestDetail().isEmpty()) {
                            consEmptyList.setVisibility(View.GONE);
                            for (int i = 0; i < trackMyRequestHospDetailResponse.getTrackMyRequestDetail().size(); i++) {
                                trackMyRequestDetailList.add(new TrackMyRequestDetail(trackMyRequestHospDetailResponse.getTrackMyRequestDetail().get(i).getF_user().getProfilePicture(),
                                        trackMyRequestHospDetailResponse.getTrackMyRequestDetail().get(i).getF_user().getFirstName(),
                                        trackMyRequestHospDetailResponse.getTrackMyRequestDetail().get(i).getCreatedDate(),
                                        trackMyRequestHospDetailResponse.getTrackMyRequestDetail().get(i).getF_user().getPhoneNumber(),
                                        trackMyRequestHospDetailResponse.getTrackMyRequestDetail().get(i).getUserResStatus()));
                            }
                            trackMyRequestDetailAdapter = new TrackMyRequestDetailAdapter(getActivity(), trackMyRequestDetailList);
                            listTrackMyRequest.setAdapter(trackMyRequestDetailAdapter);
                            Collections.sort(trackMyRequestDetailList, new Comparator<TrackMyRequestDetail>() {
                                public int compare(TrackMyRequestDetail obj1, TrackMyRequestDetail obj2) {
                                    // ## Ascending order
                                    //                                return Integer.valueOf(obj2.getId()).compareTo(obj1.getId()); // To compare integer values
                                    // ## Descending order
                                    return obj2.getDateTime().compareToIgnoreCase(obj1.getDateTime()); // To compare string values
                                    // return Integer.valueOf(obj2.empId).compareTo(obj1.empId); // To compare integer values
                                }
                            });
                        } else {
                            consEmptyList.setVisibility(View.VISIBLE);
                        }
                    }
                }

                @Override
                public void onFailure(Call<TrackMyRequestHospDetailResponse> call, Throwable t) {
                    if (isAdded()) {
                        hideProgress();
                        consEmptyList.setVisibility(View.VISIBLE);
                        Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else {
            if (isAdded()) {
                consEmptyList.setVisibility(View.VISIBLE);
                userError.message = getActivity().getString(R.string.network_error_message);
                showError(ERROR_SHOW_TYPE_DIALOG, userError);
            }
        }
    }


}
