package com.dci.falcon.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.activity.BaseActivity;
import com.dci.falcon.activity.HomeActivity;
import com.dci.falcon.adapter.AuthorizerRequestAdapter;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.model.AuthorizerRequestItem;
import com.dci.falcon.model.AuthorizerRequestResponse;
import com.dci.falcon.model.GetUserInfo;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomTextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

/**
 * Created by vijayaganesh on 11/14/2017.
 */

public class AuthorizerRequestFragment extends BaseFragment {


    @BindView(R.id.list_respond_req)
    ListView listAuthorizerReq;
    @BindView(R.id.cons_respond_req)
    ConstraintLayout consRespondReq;
    Unbinder unbinder;
    AuthorizerRequestAdapter requestAdapter;
    List<AuthorizerRequestItem> authorizerRequestItemList;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    FalconAPI falconAPI;
    UserError userError;
    AuthorizerRequestResponse authorizerRequestResponse;
    @BindView(R.id.swiperefresh_respond_req)
    SwipeRefreshLayout swiperefreshAuthorizerReq;
    BaseActivity baseActivity;
    @BindView(R.id.image_empty_auth_req)
    ImageView imageEmptyAuthReq;
    @BindView(R.id.text_label_auth_req)
    CustomTextView textLabelAuthReq;
    @BindView(R.id.cons_empty_list)
    ConstraintLayout consEmptyList;
    private FloatingActionButton floatingActionButton;
    private HomeActivity homeActivity;
    private Boolean isStarted = false;
    private Boolean isVisible = false;

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isVisible)
            getAuthorizerRequest();

    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted)
            getAuthorizerRequest();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_authorizer_request, container, false);
        unbinder = ButterKnife.bind(this, view);
        authorizerRequestItemList = new ArrayList<AuthorizerRequestItem>();
        FalconApplication.getContext().getComponent().inject(this);
        userError = new UserError();
        baseActivity = (BaseActivity) getActivity();
//        notificationActivity = (HomeActivity) getActivity();
        floatingActionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab_request_blood);
        floatingActionButton.setVisibility(View.GONE);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                notificationActivity.push(new RequestBloodFragment(), getString(R.string.Request_Blood));
            }
        });
        consEmptyList.setVisibility(View.VISIBLE);

        swiperefreshAuthorizerReq.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swiperefreshAuthorizerReq.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isAdded()) {
                            getAuthorizerRequest();
                            swiperefreshAuthorizerReq.setRefreshing(false);
                        }
                    }
                }, 3000);
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }




    public void getAuthorizerRequest() {
        GetUserInfo getUserInfo = new GetUserInfo();
        getUserInfo.setUserID(sharedPreferences.getInt(FalconConstants.USEDID, 0));
        if (Utils.isNetworkAvailable()) {
            authorizerRequestItemList.clear();
            showProgress();
            falconAPI.authorizerRequest(getUserInfo).enqueue(new Callback<AuthorizerRequestResponse>() {
                @Override
                public void onResponse(Call<AuthorizerRequestResponse> call, Response<AuthorizerRequestResponse> response) {
                    hideProgress();
                    authorizerRequestResponse = response.body();
                    if (isAdded()) {
                        if (response.body() != null && response.isSuccessful() && authorizerRequestResponse.getStatusCode() == 200) {
                            consEmptyList.setVisibility(View.GONE);
                            for (int i = 0; i < authorizerRequestResponse.getUser().size(); i++) {
                                authorizerRequestItemList.add(new AuthorizerRequestItem(
                                        authorizerRequestResponse.getUser().get(i).getT_user().getFirstName(),
                                        authorizerRequestResponse.getUser().get(i).getT_user().getPhoneNumber(),
                                        authorizerRequestResponse.getUser().get(i).getReid().getBloodGroupID(),
                                        authorizerRequestResponse.getUser().get(i).getReid().getPatientName(),
                                        authorizerRequestResponse.getUser().get(i).getReid().getUnits(),
                                        authorizerRequestResponse.getUser().get(i).getReid().getHospitalAddress(),
                                        authorizerRequestResponse.getUser().get(i).getReid().getDateTime(),
                                        authorizerRequestResponse.getUser().get(i).getId(),
                                        authorizerRequestResponse.getUser().get(i).getUserResStatus(),
                                        authorizerRequestResponse.getUser().get(i).getReid().getId(),
                                        authorizerRequestResponse.getUser().get(i).getT_user().getProfilePicture(), "", "", 0,
                                        authorizerRequestResponse.getUser().get(i).getReid().getAlternateName(),
                                        authorizerRequestResponse.getUser().get(i).getReid().getAlternateNumber(),"",""));
                            }
                            for (int j = 0; j < authorizerRequestResponse.getHospital().size(); j++) {
                                authorizerRequestItemList.add(new AuthorizerRequestItem(authorizerRequestResponse.getHospital().get(j).getT_hspt().getName(),
                                        authorizerRequestResponse.getHospital().get(j).getT_hspt().getPhoneNumber(),
                                        authorizerRequestResponse.getHospital().get(j).getReid().getBloodGroupID(),
                                        authorizerRequestResponse.getHospital().get(j).getReid().getPatientName(),
                                        authorizerRequestResponse.getHospital().get(j).getReid().getUnits(),
                                        authorizerRequestResponse.getHospital().get(j).getReid().getHospitalAddress(),
                                        authorizerRequestResponse.getHospital().get(j).getReid().getDateTime(),
                                        authorizerRequestResponse.getHospital().get(j).getId(),
                                        authorizerRequestResponse.getHospital().get(j).getUserResStatus(),
                                        authorizerRequestResponse.getHospital().get(j).getReid().getId(),
                                        authorizerRequestResponse.getHospital().get(j).getT_hspt().getPhoto(), "", "", 0,
                                        authorizerRequestResponse.getHospital().get(j).getReid().getAlternateName(),
                                        authorizerRequestResponse.getHospital().get(j).getReid().getAlternateNumber(),"",""

                                ));
                            }

                            requestAdapter = new AuthorizerRequestAdapter(getActivity(), authorizerRequestItemList, baseActivity, AuthorizerRequestFragment.this);
                            listAuthorizerReq.setAdapter(requestAdapter);
                            Collections.sort(authorizerRequestItemList, new Comparator<AuthorizerRequestItem>() {
                                public int compare(AuthorizerRequestItem obj1, AuthorizerRequestItem obj2) {
                                    // ## Ascending order
                                    //                                return Integer.valueOf(obj2.getId()).compareTo(obj1.getId()); // To compare integer values
                                    // ## Descending order
                                    return obj2.getDate().compareToIgnoreCase(obj1.getDate()); // To compare string values
                                    // return Integer.valueOf(obj2.empId).compareTo(obj1.empId); // To compare integer values
                                }
                            });

                        } else {
                            consEmptyList.setVisibility(View.VISIBLE);
                        }
                    }
                }

                @Override
                public void onFailure(Call<AuthorizerRequestResponse> call, Throwable t) {
                    if (isAdded()) {
                        hideProgress();
                        consEmptyList.setVisibility(View.VISIBLE);
                        Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }

                }
            });


        } else {
            if (isAdded()) {
                consEmptyList.setVisibility(View.VISIBLE);
//                    userError.message = getActivity().getString(R.string.network_error_message);
//                    showError(ERROR_SHOW_TYPE_DIALOG, userError);
            }
        }
    }
}
