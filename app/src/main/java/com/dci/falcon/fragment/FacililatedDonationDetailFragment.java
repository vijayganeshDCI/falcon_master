package com.dci.falcon.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.activity.HomeActivity;
import com.dci.falcon.adapter.FacilitatedDonationDetailAdapter;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.model.FacilitatedDonationDetail;
import com.dci.falcon.model.FacilitatedDonationDetailResponse;
import com.dci.falcon.model.TrackMyRequestItem;
import com.dci.falcon.model.TrackRequest;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomTextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;


/*** Created by vijayaganesh on 1/8/2018 ***/

public class FacililatedDonationDetailFragment extends BaseFragment {
    @BindView(R.id.image_empty_member)
    ImageView imageEmptyMember;
    @BindView(R.id.text_label_empty_member)
    CustomTextView textLabelEmptyMember;
    @BindView(R.id.cons_empty_list)
    ConstraintLayout consEmptyList;
    @BindView(R.id.list_facilated_donations_detail)
    ListView listFacilatedDonationsDetail;
    @BindView(R.id.swiperefresh_facilitated_detail)
    SwipeRefreshLayout swiperefreshFacilitatedDetail;
    @BindView(R.id.cons_facilitated_dona_detail)
    ConstraintLayout consFacilitatedDonaDetail;
    Unbinder unbinder;
    List<FacilitatedDonationDetail> facilitatedDonationDetailList;
    UserError userError;
    @Inject
    FalconAPI falconAPI;
    @Inject
    SharedPreferences sharedPreferences;
    FacilitatedDonationDetailResponse facilitatedDonationDetailResponse;
    FacilitatedDonationDetailAdapter facilitatedDonationDetailAdapter;
    int memberID;
    private FloatingActionButton floatingActionButton;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fac_don_back, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_bar_back:
                getActivity().onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_facili_dona_detail, container, false);
        unbinder = ButterKnife.bind(this, view);
        getActivity().setTitle(getString(R.string.donated_history));
        FalconApplication.getContext().getComponent().inject(this);
        userError = new UserError();
        setHasOptionsMenu(true);
        facilitatedDonationDetailList = new ArrayList<FacilitatedDonationDetail>();
        if (getArguments() != null) {
            Bundle bundle = getArguments();
            memberID = bundle.getInt("memberID", 0);
        }
        getDonatedHistory();
        swiperefreshFacilitatedDetail.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swiperefreshFacilitatedDetail.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getDonatedHistory();
                        swiperefreshFacilitatedDetail.setRefreshing(false);
                    }
                }, 3000);
            }
        });
        if (getActivity() instanceof HomeActivity) {
            floatingActionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab_request_blood);
            floatingActionButton.setVisibility(View.GONE);
            floatingActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //                notificationActivity.push(new RequestBloodFragment(), getString(R.string.Request_Blood));
                }
            });
        }

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        return true;
                    }
                }
                return false;
            }
        });
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void getDonatedHistory() {
        TrackRequest trackRequest = new TrackRequest();
        trackRequest.setId(memberID);
        if (Utils.isNetworkAvailable()) {
            showProgress();
            facilitatedDonationDetailList.clear();
            falconAPI.getFacilitatedDonationDetail(trackRequest).enqueue(new Callback<FacilitatedDonationDetailResponse>() {
                @Override
                public void onResponse(Call<FacilitatedDonationDetailResponse> call, Response<FacilitatedDonationDetailResponse> response) {
                    hideProgress();
                    consEmptyList.setVisibility(View.GONE);
                    facilitatedDonationDetailResponse = response.body();
                    if (isAdded()) {
                        if (response.isSuccessful() && response.body() != null && facilitatedDonationDetailResponse.getStatusCode() == 200) {
                            for (int i = 0; i < facilitatedDonationDetailResponse.getBloodReqInd().size(); i++) {
                                facilitatedDonationDetailList.add(new FacilitatedDonationDetail(
                                        facilitatedDonationDetailResponse.getBloodReqInd().get(i).getPatientName(),
                                        facilitatedDonationDetailResponse.getBloodReqInd().get(i).getProfilePicture(),
                                        facilitatedDonationDetailResponse.getBloodReqInd().get(i).getBloodGroupID(),
                                        facilitatedDonationDetailResponse.getBloodReqInd().get(i).getUnits(),
                                        facilitatedDonationDetailResponse.getBloodReqInd().get(i).getHospitalAddress(),
                                        facilitatedDonationDetailResponse.getBloodReqInd().get(i).getDateTime(),
                                        facilitatedDonationDetailResponse.getBloodReqInd().get(i).getPhoneNumber(),
                                        facilitatedDonationDetailResponse.getBloodReqInd().get(i).getPatientID(),
                                        facilitatedDonationDetailResponse.getBloodReqInd().get(i).getFirstName()
                                ));
                            }

                            for (int i = 0; i < facilitatedDonationDetailResponse.getBloodReqHos().size(); i++) {
                                facilitatedDonationDetailList.add(new FacilitatedDonationDetail(
                                        facilitatedDonationDetailResponse.getBloodReqHos().get(i).getPatientName(),
                                        facilitatedDonationDetailResponse.getBloodReqHos().get(i).getPhoto(),
                                        facilitatedDonationDetailResponse.getBloodReqHos().get(i).getBloodGroupID(),
                                        facilitatedDonationDetailResponse.getBloodReqHos().get(i).getUnits(),
                                        facilitatedDonationDetailResponse.getBloodReqHos().get(i).getHospitalAddress(),
                                        facilitatedDonationDetailResponse.getBloodReqHos().get(i).getDateTime(),
                                        facilitatedDonationDetailResponse.getBloodReqHos().get(i).getPhoneNumber(),
                                        facilitatedDonationDetailResponse.getBloodReqHos().get(i).getPatientID(),
                                        facilitatedDonationDetailResponse.getBloodReqHos().get(i).getName()
                                ));
                            }

                            facilitatedDonationDetailAdapter = new FacilitatedDonationDetailAdapter(getActivity(), facilitatedDonationDetailList);
                            listFacilatedDonationsDetail.setAdapter(facilitatedDonationDetailAdapter);
                            Collections.sort(facilitatedDonationDetailList, new Comparator<FacilitatedDonationDetail>() {
                                public int compare(FacilitatedDonationDetail obj1, FacilitatedDonationDetail obj2) {
                                    // ## Ascending order
                                    //                                return Integer.valueOf(obj2.getId()).compareTo(obj1.getId()); // To compare integer values
                                    // ## Descending order
                                    return obj2.getDate().compareToIgnoreCase(obj1.getDate()); // To compare string values
                                    // return Integer.valueOf(obj2.empId).compareTo(obj1.empId); // To compare integer values
                                }
                            });

                        } else {
                            consEmptyList.setVisibility(View.VISIBLE);
                        }
                    }

                }

                @Override
                public void onFailure(Call<FacilitatedDonationDetailResponse> call, Throwable t) {
                    if (isAdded()) {
                        hideProgress();
                        Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                        consEmptyList.setVisibility(View.VISIBLE);
                    }

                }
            });

        } else {
            if (isAdded()) {
                consEmptyList.setVisibility(View.VISIBLE);
                userError.message = getActivity().getString(R.string.network_error_message);
                showError(ERROR_SHOW_TYPE_DIALOG, userError);
            }
        }
    }
}
