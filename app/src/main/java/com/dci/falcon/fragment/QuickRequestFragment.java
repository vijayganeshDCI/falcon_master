package com.dci.falcon.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.activity.HomeActivity;
import com.dci.falcon.adapter.QuickRequestAdapter;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.model.FamilyMembersInfoItem;
import com.dci.falcon.model.GetFamilyMemberResponse;
import com.dci.falcon.model.GetUserInfo;
import com.dci.falcon.model.HistoryResponseItem;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomTextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

/**
 * Created by vijayaganesh on 11/15/2017.
 */

public class QuickRequestFragment extends BaseFragment {

    @BindView(R.id.list_quick_request)
    ListView listQuickRequest;
    @BindView(R.id.cons_quick_request)
    ConstraintLayout consQuickRequest;
    Unbinder unbinder;
    QuickRequestAdapter quickRequestAdapter;
    List<FamilyMembersInfoItem> quickRequestItemList;
    FloatingActionButton floatingActionButton;
    HomeActivity homeActivity;
    @Inject
    FalconAPI falconAPI;
    @Inject
    SharedPreferences sharedPreferences;
    GetFamilyMemberResponse getFamilyMemberResponse;
    UserError userError;
    @BindView(R.id.image_empty_member)
    ImageView imageEmptyMember;
    @BindView(R.id.text_label_empty_member)
    CustomTextView textLabelEmptyMember;
    @BindView(R.id.cons_empty_list)
    ConstraintLayout consEmptyList;
    @BindView(R.id.swiperefresh_quick_req)
    SwipeRefreshLayout swiperefreshQuickReq;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_quick_request, container, false);
        unbinder = ButterKnife.bind(this, view);
        getActivity().setTitle(getString(R.string.Quick_req));
        homeActivity = (HomeActivity) getActivity();
        userError = new UserError();
        FalconApplication.getContext().getComponent().inject(this);
        quickRequestItemList = new ArrayList<FamilyMembersInfoItem>();
        floatingActionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab_request_blood);
        floatingActionButton.setVisibility(View.GONE);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeActivity.push(new RequestBloodFragment(), getString(R.string.Request_Blood));
            }
        });
        getFamilyMembers();

        swiperefreshQuickReq.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swiperefreshQuickReq.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getFamilyMembers();
                        swiperefreshQuickReq.setRefreshing(false);
                    }
                }, 3000);
            }
        });

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void getFamilyMembers() {
        GetUserInfo getUserInfo = new GetUserInfo();
        getUserInfo.setUserID(sharedPreferences.getInt(FalconConstants.USEDID, 0));
        if (Utils.isNetworkAvailable()) {
            showProgress();
            quickRequestItemList.clear();
            falconAPI.getFamilyMembers(getUserInfo).enqueue(new Callback<GetFamilyMemberResponse>() {
                @Override
                public void onResponse(Call<GetFamilyMemberResponse> call, Response<GetFamilyMemberResponse> response) {
                    if (isAdded()) {
                        hideProgress();
                        getFamilyMemberResponse = response.body();
                        if (response.isSuccessful() && response.body() != null && getFamilyMemberResponse.getStatusCode() == 200) {
                            consEmptyList.setVisibility(View.GONE);
                            for (int i = 0; i < getFamilyMemberResponse.getMembersInfo().size(); i++) {
                                quickRequestItemList.add(new FamilyMembersInfoItem(getFamilyMemberResponse.getMembersInfo().get(i).getFirstName(),
                                        getFamilyMemberResponse.getMembersInfo().get(i).getBloodGroupID(),
                                        getFamilyMemberResponse.getMembersInfo().get(i).getPhoneNumber(),
                                        getFamilyMemberResponse.getMembersInfo().get(i).getId(),
                                        getFamilyMemberResponse.getMembersInfo().get(i).getLastName(),
                                        getFamilyMemberResponse.getMembersInfo().get(i).getProfilePicture(),
                                        getFamilyMemberResponse.getMembersInfo().get(i).getBloodCount(),
                                        getFamilyMemberResponse.getMembersInfo().get(i).getRelationship(),false));
                            }
                            quickRequestAdapter = new QuickRequestAdapter(getActivity(), quickRequestItemList, QuickRequestFragment.this);
                            listQuickRequest.setAdapter(quickRequestAdapter);
                            Collections.sort(quickRequestItemList, new Comparator<FamilyMembersInfoItem>() {
                                public int compare(FamilyMembersInfoItem obj1, FamilyMembersInfoItem obj2) {
                                    // ## Ascending order
                                    //                                return Integer.valueOf(obj2.getId()).compareTo(obj1.getId()); // To compare integer values
                                    // ## Descending order
                                    return obj1.getFirstName().compareToIgnoreCase(obj2.getFirstName()); // To compare string values
                                    // return Integer.valueOf(obj2.empId).compareTo(obj1.empId); // To compare integer values
                                }
                            });

                        } else {
                            consEmptyList.setVisibility(View.VISIBLE);
                        }
                    }
                }

                @Override
                public void onFailure(Call<GetFamilyMemberResponse> call, Throwable t) {
                    if (isAdded()) {
                        hideProgress();
                        consEmptyList.setVisibility(View.VISIBLE);
                        Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }

                }
            });
        } else {
            if (isAdded()) {
                consEmptyList.setVisibility(View.VISIBLE);
                userError.message = getActivity().getString(R.string.network_error_message);
                showError(ERROR_SHOW_TYPE_DIALOG, userError);
            }
        }
    }


}
