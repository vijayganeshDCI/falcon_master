package com.dci.falcon.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.activity.ProfileActivity;
import com.dci.falcon.adapter.OrgListAdapter;
import com.dci.falcon.adapter.SequrityQuesSpinnerAdapter;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.model.OrgListItem;
import com.dci.falcon.model.OrgResponse;
import com.dci.falcon.model.SecquesItem;
import com.dci.falcon.model.SecurityQuestionResponse;
import com.dci.falcon.model.UserDetails;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.UserDetailListener;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomCheckBox;
import com.dci.falcon.view.CustomEditText;
import com.dci.falcon.view.CustomTextView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.tasks.OnSuccessListener;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lib.kingja.switchbutton.SwitchMultiButton;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

/**
 * Created by vijayaganesh on 10/23/2017.
 */

public class UserDetailFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, UserDetailListener {


    @BindView(R.id.edit_first_name)
    CustomEditText editFirstName;
    @BindView(R.id.text_input_first_name)
    TextInputLayout textInputFirstName;
    @BindView(R.id.edit_last_name)
    CustomEditText editLastName;
    @BindView(R.id.text_input_last_name)
    TextInputLayout textInputLastName;
    @BindView(R.id.spinner_blood_group)
    Spinner spinnerBloodGroup;
    @BindView(R.id.edit_age)
    CustomEditText editAge;
    @BindView(R.id.text_input_age)
    TextInputLayout textInputAge;
    @BindView(R.id.edit_email)
    CustomEditText editEmail;
    @BindView(R.id.text_input_email)
    TextInputLayout textInputEmail;
    @BindView(R.id.edit_password)
    CustomEditText editPassword;
    @BindView(R.id.text_input_password)
    TextInputLayout textInputPassword;
    @BindView(R.id.edit_dob)
    CustomEditText editDob;
    @BindView(R.id.edit_gender)
    CustomEditText editGender;
    @BindView(R.id.spinner_org)
    Spinner spinnerOrg;
    //    @BindView(R.id.text_input_Dob)
//    TextInputLayout textInputDob;
    @BindView(R.id.view_blood_group)
    View viewBloodGroup;
    @BindView(R.id.view_org)
    View viewOrg;
    @BindView(R.id.edit_org_name)
    CustomEditText editOrgName;
    @BindView(R.id.text_input_org_name)
    TextInputLayout textInputOrgName;
    @BindView(R.id.edit_org_detail)
    CustomEditText editOrgDetail;
    @BindView(R.id.text_input_org_detail)
    TextInputLayout textInputOrgDetail;
    @BindView(R.id.edit_org_add)
    CustomEditText editOrgAdd;
    @BindView(R.id.text_input_org_add)
    TextInputLayout textInputOrgAdd;
    @BindView(R.id.edit_org_phone_number)
    CustomEditText editOrgPhoneNumber;
    @BindView(R.id.text_input_org_phone_number)
    TextInputLayout textInputOrgPhoneNumber;
    @BindView(R.id.scroll_profile)
    NestedScrollView scrollProfile;
    Uri uri;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    SimpleDateFormat simpleDateFormat;
    @BindView(R.id.spinner_sequrity_question)
    Spinner spinnerSequrityQuestion;
    @BindView(R.id.view_ques)
    View viewQues;
    @BindView(R.id.edit_answer)
    CustomEditText editAnswer;
    @BindView(R.id.text_input_Dob)
    TextInputLayout textInputDob;
    @BindView(R.id.cons_user_detail)
    ConstraintLayout consUserDetail;
    @BindView(R.id.text_input_answer)
    TextInputLayout textInputAnswer;
    @BindView(R.id.check_enable_authoriser)
    CustomCheckBox checkEnableAuthoriser;
    @BindView(R.id.image_mantitory)
    ImageView imageMantitory;
    @BindView(R.id.switch_gender)
    SwitchMultiButton switchGender;
    @BindView(R.id.image_mantitory_1)
    ImageView imageMantitory1;
    @BindView(R.id.image_mantitory_2)
    ImageView imageMantitory2;
    @BindView(R.id.button_update_location)
    CustomTextView buttonUpdateLocation;
    //    @BindView(R.id.edit_edit_re_password)
//    CustomEditText editEditRePassword;
    //    @BindView(R.id.spinner_org)
//    SearchableSpinner spinnerOrg;
    private Calendar currentD, calendardate, calendarTime, currentT;
    private String donateDate, donateTime;
    private static final int PLACE_PICKER_REQUEST = 3;
    private static final long FASTEST_INTERVAL = 1000 * 5;
    private static final long INTERVAL = 1000 * 10;
    private LocationRequest mLocationRequest;
    Location mCurrentLocation;
    GoogleApiClient mGoogleApiClient;
    private double mLat, mLng;
    private LatLngBounds bounds;
    public static UserDetailListener userDetailListener;
    private String bloodGroup;
    private int gender = 2;
    ProfileActivity profileActivity;
    @Inject
    FalconAPI falconAPI;
    UserError userError;
    SecurityQuestionResponse securityQuestionResponse;
    List<SecquesItem> sequrityQuestionsList;
    SequrityQuesSpinnerAdapter sequrityQuestionAdapter;
    String sequrityQuestion;
    static int sequrityQuestionID;
    OrgResponse orgResponse;
    OrgListAdapter orgListAdapter;
    List<OrgListItem> orgListItemList;
    String orgName, orgAddress, orgDetails, orgPhoneNumber;
    static int orgID;
    ArrayAdapter<OrgListItem> arrayAdapter;
    View view;
    public static UserDetails userdetails;
    public static String firstName;
    private FusedLocationProviderClient mFusedLocationClient;
    private int authorizer;

    private Boolean isStarted = false;
    private Boolean isVisible = false;



    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        mGoogleApiClient.connect();
        if (isVisible) {
            if (Utils.isNetworkAvailable()) {
                organaizationList();
                sequrityQuestions();
            } else {
                userError.message = getActivity().getString(R.string.network_error_message);
                showError(ERROR_SHOW_TYPE_DIALOG, userError);
            }
        }


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {
            isStarted = true;
            if (Utils.isNetworkAvailable()) {
                organaizationList();
                sequrityQuestions();
            } else {
                userError.message = getActivity().getString(R.string.network_error_message);
                showError(ERROR_SHOW_TYPE_DIALOG, userError);
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
        mGoogleApiClient.disconnect();

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_user_detail, container, false);
        ButterKnife.bind(this, view);
        setRetainInstance(true);
        initiaieGoogleApiClient();
        FalconApplication.getContext().getComponent().inject(this);
        sequrityQuestionsList = new ArrayList<SecquesItem>();
        orgListItemList = new ArrayList<OrgListItem>();
        userError = new UserError();
        editPassword.setFilters(new InputFilter[]{editTextRestrictLength(), editTextRestrictSmileys()});
        editFirstName.setFilters(new InputFilter[]{editTextRestrictLength(), editTextRestrictSmileys(), editTextRestrictSpecialCharacters()});
        editLastName.setFilters(new InputFilter[]{editTextRestrictLength(), editTextRestrictSmileys(), editTextRestrictSpecialCharacters()});
        editOrgName.setFilters(new InputFilter[]{editTextRestrictLength(), editTextRestrictSmileys(), editTextRestrictSpecialCharacters()});



        profileActivity = (ProfileActivity) getActivity();
        editor = sharedPreferences.edit();
        simpleDateFormat = new SimpleDateFormat();
        ArrayAdapter<String> bloodgroupAdapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item, getResources().getStringArray(R.array.blood_group));
        spinnerBloodGroup.setAdapter(bloodgroupAdapter);
        switchGender.setOnSwitchListener

                (new SwitchMultiButton.OnSwitchListener() {
                    @Override
                    public void onSwitch(int position, String tabText) {
                        gender = position;
                    }
                });


        spinnerBloodGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                bloodGroup = adapterView.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerOrg.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                OrgListItem orgListItem = (OrgListItem) adapterView.getItemAtPosition(position);
                orgName = orgListItem.getName();
                orgID = orgListItem.getId();
                orgAddress = orgListItem.getAddressDetails();
                orgDetails = orgListItem.getDetails();
                orgPhoneNumber = orgListItem.getPhoneNumber();
                if (orgName.contains(getString(R.string.others))) {
                    textInputOrgName.setVisibility(View.VISIBLE);
//                    textInputOrgName.requestFocus();
                } else {
                    textInputOrgName.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerSequrityQuestion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                SecquesItem secquesItem = (SecquesItem) adapterView.getItemAtPosition(position);
                sequrityQuestion = secquesItem.getQuestion();
                sequrityQuestionID = secquesItem.getId();
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (ProfileActivity.fromBaseActvity) {
            if (ProfileActivity.isForLogin)
                textInputPassword.setVisibility(View.VISIBLE);
            else
                textInputPassword.setVisibility(View.GONE);
        } else {
            textInputPassword.setVisibility(View.VISIBLE);
        }

        if (!ProfileActivity.enableView) {
            setViewAndChildrenEnabled(view, false);

        } else {
            setViewAndChildrenEnabled(view, true);

        }


        checkEnableAuthoriser.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                Authorizer >> (0-Not authorize, 1-authorize)
                if (isChecked)
                    authorizer = 1;
                else
                    authorizer = 0;

            }
        });
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        spinnerOrg.requestFocus();
        spinnerBloodGroup.requestFocus();
        spinnerOrg.setFocusable(true);
        spinnerBloodGroup.setFocusable(true);

    }


    @Override
    public void onDetach() {
        super.onDetach();
    }


    @OnClick({R.id.edit_dob, R.id.edit_org_add, R.id.button_update_location})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.edit_dob:
                datePickerDialog();
                break;
            case R.id.edit_org_add:
                if (!profileActivity.hasLocationPermissionGranted()) {
                    profileActivity.requestPermission();
                } else {
                    pickYourPlace();
                }
                break;
            case R.id.button_update_location:
                profileActivity.pageNumber();
                break;

        }
    }


    private void datePickerDialog() {
        currentD = Calendar.getInstance();
        Calendar maxDate = Calendar.getInstance();
        Calendar minDate = Calendar.getInstance();
        DatePickerDialog datePickerDialog =
                DatePickerDialog.newInstance(this, currentD.get(Calendar.YEAR), currentD.get(Calendar.MONTH),
                        currentD.get(Calendar.DAY_OF_MONTH));
        maxDate.add(Calendar.YEAR, -18);
        datePickerDialog.setMaxDate(maxDate);
        minDate.add(Calendar.YEAR, -60);
        datePickerDialog.setMinDate(minDate);
        datePickerDialog.setAccentColor(getResources().getColor(R.color.falcon_red));
        datePickerDialog.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }



    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PLACE_PICKER_REQUEST
                && resultCode == Activity.RESULT_OK) {

            final Place place = PlacePicker.getPlace(getActivity(), data);
            final String name = place.getName().toString();
            final String address = place.getAddress().toString();
            String attributions = (String) place.getAttributions();
            if (attributions == null) {
                attributions = "";
            }
            if (place.getLatLng().latitude != 0 && place.getLatLng().longitude != 0) {
                if (!name.contains("9"))
                    editOrgAdd.setText(name + "," + address);
                else
                    editOrgAdd.setText(address);
            } else
                editOrgAdd.setText("");

        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void initiaieGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity()).addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void startLocationUpdates() {

        if (getActivity() != null && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        mFusedLocationClient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    mLat = location.getLatitude();
                    mLng = location.getLongitude();
                }
            }
        });

        double radiusDegrees = 0.001;
        LatLng center = new LatLng(mLat, mLng);
        LatLng northEast = new LatLng(center.latitude + radiusDegrees, center.longitude + radiusDegrees);
        LatLng southWest = new LatLng(center.latitude - radiusDegrees, center.longitude - radiusDegrees);
        bounds = LatLngBounds.builder()
                .include(northEast)
                .include(southWest)
                .build();
    }

    private void pickYourPlace() {
        try {
            if (bounds != null) {
                PlacePicker.IntentBuilder intentBuilder =
                        new PlacePicker.IntentBuilder();
                intentBuilder.setLatLngBounds(bounds);
                Intent intent = intentBuilder.build(getActivity());
                startActivityForResult(intent, PLACE_PICKER_REQUEST);
            }

        } catch (GooglePlayServicesRepairableException
                | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isUserDetailNotNull() {
        if (isAdded()) {

            showErrorFirstName();
            showErrorLastName();
            showErrorBloodGroup();
            showErrorAge();
            showErrorGender();
            showErrorDob();
            showErrorSecQuestion();
            showErrorAnswer();
            showErrorOrg();

            if (!ProfileActivity.fromBaseActvity) {
                //                LoginActivity

                showErrorPassword();
                if (showErrorFirstName1() && showErrorLastName1() && showErrorPassword1() && showErrorBloodGroup1() && showErrorAge1()
                        && showErrorGender1() && showErrorDob1() && showErrorSecQuestion1()
                        && showErrorAnswer1() && showErrorOrg1()) {
                    return true;
                } else {
                    editor.putInt(FalconConstants.NOTNULL, 0).commit();
                    return false;
                }
            } else {
                //HomeActivity
                if (showErrorFirstName1() && showErrorLastName1() && showErrorBloodGroup1() && showErrorAge1()
                        && showErrorGender1() && showErrorDob1() && showErrorSecQuestion1()
                        && showErrorAnswer1() && showErrorOrg1()) {
                    return true;
                } else {
                    editor.putInt(FalconConstants.NOTNULL, 0).commit();
                    return false;
                }
            }
        }
        return false;
    }

    @Override
    public UserDetails interfaceUserDetails() {
        UserDetails userDetails = new UserDetails();
        userDetails.setFirstName(editFirstName.getText().toString());
        userDetails.setLastName(editLastName.getText().toString());
        userDetails.setBloodGroup(bloodGroup);
        userDetails.setAge(Integer.parseInt(editAge.getText().toString()));
        userDetails.setEmailID(editEmail.getText().toString());
        userDetails.setPassword(editPassword.getText().toString());
        userDetails.setAuthorizer(authorizer);
        Date dob = null;
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd-MMM-yyyy");
        try {
            dob = simpleDateFormat1.parse(editDob.getText().toString());
            simpleDateFormat.applyPattern("yyyy-MM-dd");
            userDetails.setDateOfBirth(simpleDateFormat.format(dob));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        userDetails.setGender(gender + 1);
        userDetails.setSequrityQuestionID(sequrityQuestionID);
        userDetails.setSequrityQuestionAnswer(editAnswer.getText().toString());
        userDetails.setOrgDetail("");
        userDetails.setOrgaddress("");
        userDetails.setOrgphoneNumber("");

        if (orgName.contains(getString(R.string.others))) {
            //New Org
            userDetails.setOrgID(orgID);
            userDetails.setOrgType(2);
            userDetails.setOrgName(editOrgName.getText().toString());

        } else {
            //Old Org
            userDetails.setOrgID(orgID);
            userDetails.setOrgType(1);
            userDetails.setOrgName(orgName);
        }
        return userDetails;
    }

    @Override
    public void getUserDetails(final UserDetails userDetails) {
        if (isAdded() && userDetails != null) {
            editFirstName.setText(userDetails.getFirstName());
            firstName = editFirstName.getText().toString();
            editFirstName.setText(firstName);
            editLastName.setText(userDetails.getLastName());
            editAge.setText("" + userDetails.getAge());
            editEmail.setText(userDetails.getEmailID());
            Date dob = null;
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
            try {
                dob = simpleDateFormat1.parse(userDetails.getDateOfBirth());
                simpleDateFormat.applyPattern("dd-MMM-yyyy");
                editDob.setText(simpleDateFormat.format(dob));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            editAnswer.setText(userDetails.getSequrityQuestionAnswer());
            selectBloodSpinnerItemByValue(spinnerBloodGroup, userDetails.getBloodGroup());

//            Male-1 Female-2
            if (userDetails.getGender() == 2) {
                switchGender.setSelectedTab(1);
            } else {
                switchGender.setSelectedTab(0);
            }

            if (userDetails.getOrgType() == 2) {
                textInputOrgName.setVisibility(View.VISIBLE);
                editOrgName.setText(userDetails.getOrgName());
            } else {
                textInputOrgName.setVisibility(View.GONE);
            }

            if (userDetails.getAuthorizer() == 1)
                checkEnableAuthoriser.setChecked(true);
            else
                checkEnableAuthoriser.setChecked(false);
//            for (int i = 0; i < sequrityQuestionsList.size(); i++) {
//                if (sequrityQuestionsList.get(i).getId() == userDetails.getSequrityQuestionID()) {
//                    spinnerSequrityQuestion.setSelection(i);
//                    break;
//                }
//            }
//            for (int i = 0; i < orgListItemList.size(); i++) {
//                if (orgListItemList.get(i).getId() == userDetails.getOrgID()) {
//                    spinnerOrg.setSelection(i);
//                    break;
//                }
//            }

        }
    }

    public static void setViewAndChildrenEnabled(View view, boolean enabled) {
        view.setEnabled(enabled);
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View child = viewGroup.getChildAt(i);
                setViewAndChildrenEnabled(child, enabled);
            }
        }
    }


    public void sequrityQuestions() {
        showProgress();
        sequrityQuestionsList.clear();
        sequrityQuestionsList.add(0, new SecquesItem(0, "", getString(R.string.question), 0));
        falconAPI.sequrityQuestions().enqueue(new Callback<SecurityQuestionResponse>() {
            @Override
            public void onResponse(Call<SecurityQuestionResponse> call, Response<SecurityQuestionResponse> response) {
                if (isAdded()) {
                    if (response.body() != null && response.isSuccessful()) {
                        hideProgress();
                        securityQuestionResponse = response.body();
                        if (securityQuestionResponse.getSecques() != null) {
                            for (int i = 0; i < securityQuestionResponse.getSecques().size(); i++) {
                                sequrityQuestionsList.add(new SecquesItem(securityQuestionResponse.getSecques().get(i).getStatus(),
                                        securityQuestionResponse.getSecques().get(i).getCreated_at(),
                                        securityQuestionResponse.getSecques().get(i).getQuestion(),
                                        securityQuestionResponse.getSecques().get(i).getId()));
                            }


                        }
                        sequrityQuestionAdapter = new SequrityQuesSpinnerAdapter(getActivity(), sequrityQuestionsList);
                        spinnerSequrityQuestion.setAdapter(sequrityQuestionAdapter);
//                        if (ProfileActivity.fromBaseActvity) {
//                            getUserDetails(userdetails);
//
//                        }

                        if (userdetails != null) {
                            for (int i = 0; i < sequrityQuestionsList.size(); i++) {
                                if (sequrityQuestionsList.get(i).getId() == userdetails.getSequrityQuestionID()) {
                                    spinnerSequrityQuestion.setSelection(i);
                                    break;
                                }
                            }
                        }

                        if (sequrityQuestionID != 0) {
                            for (int i = 0; i < sequrityQuestionsList.size(); i++) {
                                if (sequrityQuestionsList.get(i).getId() == sequrityQuestionID) {
                                    spinnerSequrityQuestion.setSelection(i);
                                    break;
                                }
                            }
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<SecurityQuestionResponse> call, Throwable t) {
                hideProgress();
                Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void organaizationList() {
        showProgress();
        orgListItemList.clear();
        orgListItemList.add(0, new OrgListItem(1, "", "", "", "", 0, "", 0, getString(R.string.Organization)));
        orgListItemList.add(1, new OrgListItem(1, "", "", "", "", 0, "", 1, getString(R.string.others)));
        falconAPI.orgnaizationList().enqueue(new Callback<OrgResponse>() {
            @Override
            public void onResponse(Call<OrgResponse> call, Response<OrgResponse> response) {
                if (isAdded()) {
                    if (response.body() != null && response.isSuccessful()) {
                        hideProgress();
                        orgResponse = response.body();
                        if (orgResponse.getOrgList() != null) {
                            for (int i = 0; i < orgResponse.getOrgList().size(); i++) {
                                orgListItemList.add(new OrgListItem(orgResponse.getOrgList().get(i).getStatus(),
                                        orgResponse.getOrgList().get(i).getDetails(),
                                        orgResponse.getOrgList().get(i).getUpdated_at(),
                                        orgResponse.getOrgList().get(i).getAddressDetails(),
                                        orgResponse.getOrgList().get(i).getPhoneNumber(),
                                        orgResponse.getOrgList().get(i).getCreateUserID(),
                                        orgResponse.getOrgList().get(i).getCreated_at(),
                                        orgResponse.getOrgList().get(i).getId(),
                                        orgResponse.getOrgList().get(i).getName()));
                            }


                        }
                        orgListAdapter = new OrgListAdapter(getActivity(), orgListItemList);
                        spinnerOrg.setAdapter(orgListAdapter);
//Call When get Details
                        if (userdetails != null) {
                            if (userdetails.getOrgType() == 2) {
                                //New Org
                                for (int i = 0; i < orgListItemList.size(); i++) {
                                    if (orgListItemList.get(i).getName().equalsIgnoreCase(getString(R.string.others))) {
                                        spinnerOrg.setSelection(i);
                                        break;
                                    }
                                }
                            } else {
                                //Old Org
                                for (int i = 0; i < orgListItemList.size(); i++) {
                                    if (orgListItemList.get(i).getName().equalsIgnoreCase(userdetails.getOrgName())) {
                                        spinnerOrg.setSelection(i);
                                        break;
                                    }
                                }
                            }
                        }
//                   Call When tab change position
                        if (orgID != 0) {
                            for (int i = 0; i < orgListItemList.size(); i++) {
                                if (orgListItemList.get(i).getName().equalsIgnoreCase(orgName)) {
                                    spinnerOrg.setSelection(i);
                                    break;
                                }
                            }
                        }


                    }
                }
            }

            @Override
            public void onFailure(Call<OrgResponse> call, Throwable t) {
                hideProgress();
                Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();

            }
        });
    }


    public static void selectBloodSpinnerItemByValue(Spinner spnr, String bloodGroup) {
        ArrayAdapter adapter = (ArrayAdapter) spnr.getAdapter();
        for (int position = 0; position < adapter.getCount(); position++) {
            if (adapter.getItem(position).equals(bloodGroup)) {
                spnr.setSelection(position);
                return;
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        null.unbind();
//        null.unbind();
    }

    private boolean showErrorFirstName() {

        if (editFirstName.getText().toString().isEmpty()) {
            textInputFirstName.setError(getString(R.string.req_first_name));
            return false;
        } else {
            textInputFirstName.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorFirstName1() {

        if (editFirstName.getText().toString().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorLastName() {
        if (editLastName.getText().toString().isEmpty()) {
            textInputLastName.setError(getString(R.string.req_Last_name));
            return false;
        } else {
            textInputLastName.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorLastName1() {
        if (editLastName.getText().toString().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorAge() {
        if (editAge.getText().toString().isEmpty()) {
            textInputAge.setError(getString(R.string.req_Age));
            return false;
        } else if (Integer.parseInt(editAge.getText().toString()) < 18) {
            textInputAge.setError(getActivity().getString(R.string.min_age));
            return false;
        } else if (Integer.parseInt(editAge.getText().toString()) > 60) {
            textInputAge.setError(getActivity().getString(R.string.min_age));
            return false;
        } else {
            textInputAge.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorAge1() {
        if (editAge.getText().toString().isEmpty()) {
            return false;
        } else if (Integer.parseInt(editAge.getText().toString()) < 18) {
            return false;
        } else if (Integer.parseInt(editAge.getText().toString()) > 60) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorPassword() {
        if (editPassword.getText().toString().isEmpty()) {
            textInputPassword.setError(getString(R.string.req_first_pasword));
            return false;
        } else if (editPassword.getText().length() < 8) {
            textInputPassword.setError(getActivity().getString(R.string.min_pass));
            return false;
        } else {
            textInputPassword.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorPassword1() {
        if (editPassword.getText().toString().isEmpty()) {
            return false;
        } else if (editPassword.getText().length() < 8) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorEmail() {
        if (editEmail.getText().toString().isEmpty()) {
            textInputEmail.setError(getString(R.string.req_Email));
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(editEmail.getText().toString()).matches()) {
            textInputEmail.setError(getActivity().getString(R.string.enter_valid_email_add));
            return false;
        } else {
            textInputEmail.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorEmail1() {
        if (editEmail.getText().toString().isEmpty()) {
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(editEmail.getText().toString()).matches()) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorDob() {
        if (editDob.getText().toString().isEmpty()) {
            textInputDob.setError(getString(R.string.req_DOB));
            return false;
        } else {
            textInputDob.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorDob1() {
        if (editDob.getText().toString().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorAnswer() {
        if (editAnswer.getText().toString().isEmpty()) {
            textInputAnswer.setError(getString(R.string.req_ans));
            return false;
        } else {
            textInputAnswer.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorAnswer1() {
        if (editAnswer.getText().toString().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorSecQuestion() {
        if (sequrityQuestion.contains(getString(R.string.question))) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setMessage(getString(R.string.req_sec));
            alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            alertDialog.show();
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorSecQuestion1() {
        if (sequrityQuestion.contains(getString(R.string.question))) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorBloodGroup() {
        if (bloodGroup.contains("Select Blood Group*")) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setMessage(getString(R.string.req_Blood));
            alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            alertDialog.show();
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorBloodGroup1() {
        if (bloodGroup.contains("Select Blood Group*")) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorOrg() {
        if (orgName.contains(getString(R.string.Organization))) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setMessage(getString(R.string.req_org));
            alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            alertDialog.show();
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorGender1() {

        switch (gender) {
            case 0:
                return true;
            case 1:
                return true;
            case 2:
                return false;
        }
        return false;
    }

    private boolean showErrorGender() {

        switch (gender) {
            case 0:
                return true;
            case 1:
                return true;
            case 2:
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setMessage(getString(R.string.req_gender));
                alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                alertDialog.show();
                return false;
        }

        return false;
    }

    private boolean showErrorOrg1() {
        if (orgName.contains(getString(R.string.Organization))) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        calendardate = Calendar.getInstance();
        calendardate.set(Calendar.MONTH, monthOfYear);
        calendardate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        calendardate.set(Calendar.YEAR, year);
        simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        donateDate = simpleDateFormat.format(calendardate.getTime());
        editDob.setText(donateDate);
        editAge.setText(getAge(year, monthOfYear, dayOfMonth));
    }


}
