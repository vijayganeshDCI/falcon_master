package com.dci.falcon.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dci.falcon.R;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.model.MapDirectionResponse;
import com.dci.falcon.retrofit.RetrofitMaps;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class FalconMapFragment extends BaseFragment implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    SupportMapFragment mMapReportAccident;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    private static final long INTERVAL = 1000 * 10;
    private static final long FASTEST_INTERVAL = 1000 * 5;
    FusedLocationProviderClient mCurrentLocation;
    //    @BindView(R.id.text_currentlocation)
//    TextView mTextCurrentLocation;
//    @BindView(R.id.image_set_location)
//    ImageView mImageSetAccidentLocation;
//    @BindView(R.id.linear_currentlocation)
//    LinearLayout linearCurrentlocation;
//    @BindView(R.id.linear_set_location)
//    LinearLayout linearSetLocation;
//    @BindView(R.id.button_location_select)
//    Button buttonLocationSelect;
    @BindView(R.id.constraint_map)
    ConstraintLayout constraintMap;
    @BindView(R.id.show_distance_time)
    TextView showDistanceTime;
    private double mLat, mLng;
    private LatLng mCurrentLatLng,mDesinationLatLng;
    Geocoder mGeocoder;
    @Inject
    SharedPreferences mPrefs;
    SharedPreferences.Editor mEditor;
    UserError mUserError;
    String mMapaddress;
    private int PROXIMITY_RADIUS = 100;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    GoogleMap mGoogleMap;
    private double latitude;
    private double longitude;
    private ArrayList<LatLng> latLngArrayList;
    private LatLng currentLatLng;
    private LatLng destinationLatLng;
    private Polyline line;
    private FloatingActionButton floatingActionButton;
    Double desinationLat, desitinationLng;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.falcon_map, null);
        ButterKnife.bind(this, view);
        FalconApplication.getContext().getComponent().inject(this);
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity()).addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mMapReportAccident = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map_falcon);
        mGeocoder = new Geocoder(getActivity(), Locale.getDefault());
        mUserError = new UserError();
        mEditor = mPrefs.edit();
        latLngArrayList = new ArrayList<>();
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

//        floatingActionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab_request_blood);
//        floatingActionButton.setVisibility(View.GONE);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }

        // Initializing

        //show error dialog if Google Play Services not available
        if (!isGooglePlayServicesAvailable()) {
            Log.d("onCreate", "Google Play Services not available. Ending Test case.");
            getActivity().finish();
        } else {
            Log.d("onCreate", "Google Play Services available. Continuing.");
        }

        Intent bundle=getActivity().getIntent();
        if (getActivity().getIntent()!=null){
            desinationLat = Double.valueOf(bundle.getStringExtra("lat"));
            desitinationLng = Double.valueOf(bundle.getStringExtra("lang"));
        }
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        null.unbind();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    protected void startLocationUpdates() {

        if (getActivity() != null && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mCurrentLocation = LocationServices.getFusedLocationProviderClient(getActivity());
        mCurrentLocation.getLastLocation()
                .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations, this can be null.
                        if (location != null) {
                            // Logic to handle location object
                            updateMap(location);
                        }
                    }
                });
    }

    private void updateMap(Location mCurrentLocation) {
        mLat = mCurrentLocation.getLatitude();
        mLng = mCurrentLocation.getLongitude();
        mCurrentLatLng = new LatLng(mLat, mLng);
        mDesinationLatLng = new LatLng(desinationLat, desitinationLng);

        mMapReportAccident.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(final GoogleMap googleMap) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mGoogleMap = googleMap;
                mGoogleMap.setMyLocationEnabled(true);
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(mDesinationLatLng));
                mGoogleMap.animateCamera( CameraUpdateFactory.newLatLngZoom( mCurrentLatLng,19  ));
                mGoogleMap.addMarker(new MarkerOptions().position(mCurrentLatLng).title("Current Location")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                mGoogleMap.addMarker(new MarkerOptions().position(mDesinationLatLng).title("Donor Location")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

                View mLocationButton =
                        ((View) mMapReportAccident.getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(
                                Integer.parseInt("2"));
                RelativeLayout.LayoutParams mRelativeLayoutParams =
                        (RelativeLayout.LayoutParams) mLocationButton.getLayoutParams();
                mRelativeLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                mRelativeLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                mRelativeLayoutParams.setMargins(0, 0, 30, 180);


                latLngArrayList.add(mCurrentLatLng);
                latLngArrayList.add(mDesinationLatLng);
                currentLatLng = latLngArrayList.get(0);
                destinationLatLng = latLngArrayList.get(1);

                getPath("driving");

//                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mCurrentLatLng, 11.0f));
//                googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
//                MarkerOptions markerOptions = new MarkerOptions();
//                markerOptions.position(mCurrentLatLng);
//                markerOptions.title("Current Position");
//                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
//                mCurrLocationMarker = gMap.addMarker(markerOptions);
//                String Hospital = "hospital";
//                String url = getUrl(mLat, mLng, Hospital);
//                Object[] DataTransfer = new Object[2];
//                DataTransfer[0] = googleMap;
//                DataTransfer[1] = url;
//                Log.d("onClick", url);
//                GetNearbyPlacesData getNearbyPlacesData = new GetNearbyPlacesData();
//                getNearbyPlacesData.execute(DataTransfer);

//                googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
//                    @Override
//                    public void onCameraIdle() {
//                        CameraPosition cameraPosition = googleMap.getCameraPosition();
//                        LatLng cameraCurrentPosition = cameraPosition.target;
//                        mLat = cameraCurrentPosition.latitude;
//                        mLng = cameraCurrentPosition.longitude;
//                        try {
//
//                            getAddress(mLat, mLng, new Handler.Callback() {
//                                @Override
//                                public boolean handleMessage(final Message msg) {
//                                    if (mTextCurrentLocation != null) {
//                                        mTextCurrentLocation.post(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                mMapaddress = msg.getData().getString(FalconConstants.ADDRESS).toString();
//                                                if (msg.getData().getString(FalconConstants.ADDRESS).length() > 45) {
//                                                    mTextCurrentLocation.setText(mMapaddress.substring(0, 45) + "....");
//                                                } else {
//                                                    mTextCurrentLocation.setText(msg.getData().getString(FalconConstants.ADDRESS));
//                                                }
//
//                                            }
//                                        });
//                                    }
//                                    return false;
//                                }
//                            });
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                });
            }
        });
    }

    //    private String getUrl(double latitude, double longitude, String nearbyPlace) {
//
//        StringBuilder googlePlacesUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
//        googlePlacesUrl.append("location=" + latitude + "," + longitude);
//        googlePlacesUrl.append("&radius=" + PROXIMITY_RADIUS);
//        googlePlacesUrl.append("&type=" + nearbyPlace);
//        googlePlacesUrl.append("&sensor=true");
//        googlePlacesUrl.append("&key=" + getString(R.string.google_maps_api_key));
//        return (googlePlacesUrl.toString());
//    }
//
//    @NonNull
//    private void getAddress(final double desinationLat, final double desitinationLng, final Handler.Callback callback)
//            throws IOException {
//        Runnable runnable = new Runnable() {
//            public void run() {
//                try {
//
//                    String address = "";
//                    List<Address> addresses = mGeocoder.getFromLocation(desinationLat, desitinationLng, 1);
//                    if (addresses.size() > 0) {
//                        Address address1 = addresses.get(0);
//                        StringBuffer sb = new StringBuffer();
//                        for (int i = 0; i < address1.getMaxAddressLineIndex() + 1; i++) {
//                            sb.append(address1.getAddressLine(i));
//                            if (i != address1.getMaxAddressLineIndex() - 1) {
//                                sb.append(",");
//                            }
//                            address = sb.toString();
//
//                        }
//                    }
//                    Message message = new Message();
//                    Bundle bundle = new Bundle();
//                    bundle.putString(FalconConstants.ADDRESS, address);
//                    message.setData(bundle);
//                    callback.handleMessage(message);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        };
//        new Thread(runnable).start();
//    }
//
//
////    @OnClick(R.id.button_location_select)
////    public void onViewClicked() {
////        mEditor.putString(FalconConstants.MAPADDRESS, mMapaddress).commit();
////        getActivity().onBackPressed();
////    }
//
    @Override
    public void onLocationChanged(Location location) {
//        mLastLocation = location;
//        if (mCurrLocationMarker != null) {
//            mCurrLocationMarker.remove();
//        }
//
//        //Place current location marker
//        latitude = location.getLatitude();
//        longitude = location.getLongitude();
//        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
//        MarkerOptions markerOptions = new MarkerOptions();
//        markerOptions.position(latLng);
//        markerOptions.title("Current Position");
//        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
//        mCurrLocationMarker = gMap.addMarker(markerOptions);
//
////        move map camera
//        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,50.0f));
//        gMap.animateCamera(CameraUpdateFactory.zoomTo(25));

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private void getPath(String type) {

        String url = "https://maps.googleapis.com/maps/";

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RetrofitMaps service = retrofit.create(RetrofitMaps.class);

        Call<MapDirectionResponse> call = service.getDistanceDuration
                ("metric", currentLatLng.latitude + "," + currentLatLng.longitude,
                        destinationLatLng.latitude + "," + destinationLatLng.longitude, type);

        call.enqueue(new Callback<MapDirectionResponse>() {
            @Override
            public void onResponse(Call<MapDirectionResponse> call, Response<MapDirectionResponse> response) {
                try {
                    //Remove previous line from map
                    if (line != null) {
                        line.remove();
                    }
                    // This loop will go through all the results and add marker on each location.
                    for (int i = 0; i < response.body().getRoutes().size(); i++) {
                        String distance = response.body().getRoutes().get(i).getLegs().get(i).getDistance().getText();
                        String time = response.body().getRoutes().get(i).getLegs().get(i).getDuration().getText();
                        showDistanceTime.setText("Distance:" + distance + ", Duration:" + time);
                        String encodedString = response.body().getRoutes().get(0).getOverviewPolyline().getPoints();
                        List<LatLng> list = decodePoly(encodedString);
                        line = mGoogleMap.addPolyline(new PolylineOptions()
                                .addAll(list)
                                .width(15)
                                .color(getResources().getColor(R.color.orange))
                                .geodesic(true)
                        );
                    }
                } catch (Exception e) {
                    Log.d("onResponse", "There is an error");
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MapDirectionResponse> call, Throwable t) {

            }

        });

    }

    private List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }

    // Checking if Google Play Services Available or not
    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(getActivity());
        if (result != ConnectionResult.SUCCESS) {
            if (googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(getActivity(), result,
                        0).show();
            }
            return false;
        }
        return true;
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }
}
