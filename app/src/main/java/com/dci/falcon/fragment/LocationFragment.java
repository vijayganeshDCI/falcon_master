package com.dci.falcon.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dci.falcon.R;
import com.dci.falcon.activity.ProfileActivity;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.model.LocationDetails;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.LocationDetailListener;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomButton;
import com.dci.falcon.view.CustomEditText;
import com.dci.falcon.view.CustomTextView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

/**
 * Created by vijayaganesh on 11/1/2017.
 */

public class LocationFragment extends BaseFragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationDetailListener {
    //    @BindView(R.id.spinner_city)
//    Spinner spinnerCity;
//    @BindView(R.id.spinner_state)
//    Spinner spinnerState;
    @BindView(R.id.text_input_add_home)
    TextInputLayout textInputAddHome;
    @BindView(R.id.edit_address_office)
    CustomEditText editAddressOffice;
    @BindView(R.id.text_input_add_office)
    TextInputLayout textInputAddOffice;
    @BindView(R.id.edit_address_freq)
    CustomEditText editAddressFreq;
    @BindView(R.id.text_input_add_freq)
    TextInputLayout textInputAddFreq;
    @BindView(R.id.cons_location)
    ConstraintLayout consLocation;
    @BindView(R.id.scroll_location)
    NestedScrollView scrollLocation;
    Unbinder unbinder;
    private static final int PLACE_PICKER_REQUEST = 3;
    private static final long FASTEST_INTERVAL = 1000 * 5;
    private static final long INTERVAL = 1000 * 10;
    @BindView(R.id.text_pin_hospital)
    CustomTextView textPinHospital;
    @BindView(R.id.image_mantitory)
    ImageView imageMantitory;
    @BindView(R.id.edit_address_home)
    CustomEditText editAddressHome;
    @BindView(R.id.edit_address_home_lat)
    CustomEditText editAddressHomeLat;
    @BindView(R.id.text_input_add_home_lat)
    TextInputLayout textInputAddHomeLat;
    @BindView(R.id.button_update_donation)
    CustomTextView buttonUpdateDonation;
    private LocationRequest mLocationRequest;
    Location mCurrentLocation;
    GoogleApiClient mGoogleApiClient;
    private double mLat, mLng;
    private LatLngBounds bounds;
    private static int editLocationClicked = 0;
    private String state, city;
    ProfileActivity profileActivity;
    double nearHomeLat, nearHomeLng;
    double nearHomeLat1, nearHomeLng1;
    double nearFreqLat1, nearFreqLng1;
    double nearOfficeLat1, nearOfficeLng1;
    double nearOfficeLat, nearOfficeLng;
    double nearFreqLat, nearFreqLng;
    View view;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private FusedLocationProviderClient mFusedLocationClient;
    Place place;
    String name, address;

    private Boolean isStarted = false;
    private Boolean isVisible = false;



    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        mGoogleApiClient.connect();
        if (isVisible) {
        }


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {
            isStarted = true;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
        mGoogleApiClient.disconnect();

    }




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_loaction, container, false);
        ButterKnife.bind(this, view);
        FalconApplication.getContext().getComponent().inject(this);
        profileActivity = (ProfileActivity) getActivity();
        editor = sharedPreferences.edit();
        initiaieGoogleApiClient();

        if (!ProfileActivity.enableView) {
            setViewAndChildrenEnabled(view, false);
        } else {
            setViewAndChildrenEnabled(view, true);
        }

        return view;
    }



    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @OnClick({R.id.edit_address_home, R.id.edit_address_office, R.id.edit_address_freq,R.id.button_update_donation})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.edit_address_home:
                if (!profileActivity.hasLocationPermissionGranted()) {
                    profileActivity.requestPermission();
                } else {
                    editLocationClicked = 1;
                    pickYourPlace();
                }

                break;
            case R.id.edit_address_office:
                if (!profileActivity.hasLocationPermissionGranted()) {
                    profileActivity.requestPermission();
                } else {
                    editLocationClicked = 2;
                    pickYourPlace();
                }

                break;
            case R.id.edit_address_freq:
                if (!profileActivity.hasLocationPermissionGranted()) {
                    profileActivity.requestPermission();
                } else {
                    editLocationClicked = 3;
                    pickYourPlace();
                }
                break;
            case R.id.button_update_donation:
                profileActivity.pageNumber();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PLACE_PICKER_REQUEST
                && resultCode == Activity.RESULT_OK) {

            place = PlacePicker.getPlace(getActivity(), data);
            name = place.getName().toString();
            address = place.getAddress().toString();
            String attributions = (String) place.getAttributions();
            if (attributions == null) {
                attributions = "";
            }
            switch (editLocationClicked) {
                case 1:
                    nearHomeLat = place.getLatLng().latitude;
                    nearHomeLng = place.getLatLng().longitude;
                    if (nearHomeLat != 0.0 && nearHomeLng != 0.0) {
                        if (!name.contains("9"))
                            editAddressHome.setText(name + "," + address);
                        else
                            editAddressHome.setText(address);
                    } else {
                        editAddressHome.setText("");
                    }
                    break;
                case 2:
                    nearOfficeLat = place.getLatLng().latitude;
                    nearOfficeLng = place.getLatLng().longitude;
                    if (nearOfficeLat != 0 && nearOfficeLng != 0) {
                        if (!name.contains("9"))
                            editAddressOffice.setText(name + "," + address);
                        else
                            editAddressOffice.setText(address);
                    } else {
                        editAddressOffice.setText("");
                    }
                    break;
                case 3:
                    nearFreqLat = place.getLatLng().latitude;
                    nearFreqLng = place.getLatLng().longitude;
                    if (nearFreqLat != 0 && nearFreqLng != 0) {
                        if (!name.contains("9"))
                            editAddressFreq.setText(name + "," + address);
                        else
                            editAddressFreq.setText(address);
                    } else {

                        editAddressFreq.setText("");
                    }
                    break;
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void initiaieGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity()).addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void startLocationUpdates() {

        if (getActivity() != null && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

//        mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//        if (mCurrentLocation != null) {
//            mLat = mCurrentLocation.getLatitude();
//            mLng = mCurrentLocation.getLongitude();
//        }

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        mFusedLocationClient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    mLat = location.getLatitude();
                    mLng = location.getLongitude();
                }
            }
        });
        double radiusDegrees = 0.001;
        LatLng center = new LatLng(mLat, mLng);
        LatLng northEast = new LatLng(center.latitude + radiusDegrees, center.longitude + radiusDegrees);
        LatLng southWest = new LatLng(center.latitude - radiusDegrees, center.longitude - radiusDegrees);
        bounds = LatLngBounds.builder()
                .include(northEast)
                .include(southWest)
                .build();
    }

    private void pickYourPlace() {
        try {
            if (bounds != null) {
                PlacePicker.IntentBuilder intentBuilder =
                        new PlacePicker.IntentBuilder();
                intentBuilder.setLatLngBounds(bounds);
                Intent intent = intentBuilder.build(getActivity());
                startActivityForResult(intent, PLACE_PICKER_REQUEST);
            }

        } catch (GooglePlayServicesRepairableException
                | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isLocationNotNull() {
        if (editAddressHome.getText().toString().isEmpty()) {
            textInputAddHome.setError(getString(R.string.req_Location));
            return false;
        } else {
            editor.putInt(FalconConstants.NOTNULL, 1).commit();
            textInputAddHome.setErrorEnabled(false);
            return true;
        }
    }


    @Override
    public LocationDetails interafceLocationDetails() {
        LocationDetails locationDetails = new LocationDetails();
        locationDetails.setNearHome(editAddressHome.getText().toString());
        locationDetails.setNearOffice(editAddressOffice.getText().toString());
        locationDetails.setNearFrequent(editAddressFreq.getText().toString());

        if (nearHomeLat == 0.0 && nearHomeLng == 0.0) {
            getLatLngFromAddress(getActivity(), editAddressHome.getText().toString(), 1);
            locationDetails.setNearHomeLat(String.valueOf(nearHomeLat1));
            locationDetails.setNearHomeLng(String.valueOf(nearHomeLng1));
        } else {
            locationDetails.setNearHomeLat(String.valueOf(nearHomeLat));
            locationDetails.setNearHomeLng(String.valueOf(nearHomeLng));
        }
        if (nearOfficeLat == 0.0 && nearOfficeLng == 0.0) {
            getLatLngFromAddress(getActivity(), editAddressOffice.getText().toString(), 2);
            locationDetails.setNearOfficeLat(String.valueOf(nearOfficeLat1));
            locationDetails.setNearOfficeLng(String.valueOf(nearOfficeLng1));
        } else {
            locationDetails.setNearOfficeLat(String.valueOf(nearOfficeLat));
            locationDetails.setNearOfficeLng(String.valueOf(nearOfficeLng));
        }
        if (nearFreqLat == 0.0 && nearFreqLng == 0.0) {
            getLatLngFromAddress(getActivity(), editAddressFreq.getText().toString(), 3);
            locationDetails.setNearFrequentLat(String.valueOf(nearFreqLat1));
            locationDetails.setNearFrequentLng(String.valueOf(nearFreqLng1));
        } else {
            locationDetails.setNearFrequentLat(String.valueOf(nearFreqLat));
            locationDetails.setNearFrequentLng(String.valueOf(nearFreqLng));
        }

        return locationDetails;
    }

    public LatLng getLatLngFromAddress(Context context, String strAddress, int number) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            if (address.size() > 0) {
                Address location = address.get(0);
                switch (number) {
                    case 1:
                        nearHomeLat1 = location.getLatitude();
                        nearHomeLng1 = location.getLongitude();
                        break;
                    case 2:
                        nearOfficeLat1 = location.getLatitude();
                        nearOfficeLng1 = location.getLongitude();
                        break;
                    case 3:
                        nearFreqLat1 = location.getLatitude();
                        nearFreqLng1 = location.getLongitude();
                        break;
                }


                p1 = new LatLng(location.getLatitude(), location.getLongitude());
            }


        } catch (IOException ex) {

            ex.printStackTrace();
        }

        return p1;
    }

    @Override
    public void getLocationDetail(LocationDetails locationDetails) {
        if (isAdded() && locationDetails != null) {
            editAddressHome.setText(locationDetails.getNearHome());
            editAddressOffice.setText(locationDetails.getNearOffice());
            editAddressFreq.setText(locationDetails.getNearFrequent());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        null.unbind();
//        null.unbind();
    }
}
