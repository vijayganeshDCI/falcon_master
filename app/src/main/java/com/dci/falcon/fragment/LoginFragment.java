package com.dci.falcon.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.activity.HomeActivity;
import com.dci.falcon.activity.HospitalProfileActivity;
import com.dci.falcon.activity.LoginActivity;
import com.dci.falcon.activity.ProfileActivity;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.model.HospitalLogin;
import com.dci.falcon.model.HospitalLoginResponse;
import com.dci.falcon.model.IndiduvalLogin;
import com.dci.falcon.model.IndiduvalLoginResponse;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomEditText;
import com.dci.falcon.view.CustomTextView;
import com.dci.falcon.view.CustomTextViewBold;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;
import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

/**
 * Created by vijayaganesh on 10/20/2017.
 */

public class LoginFragment extends BaseFragment {

    @BindView(R.id.edit_login_phone_number)
    EditText editLoginPhoneNumber;
    @BindView(R.id.button_login)
    Button buttonLogin;
    @BindView(R.id.image_falcon_icon)
    ImageView imageFalconIcon;
    @BindView(R.id.text_label_sign_in)
    CustomTextView textLabelSignIn;
    @BindView(R.id.text_input_username)
    TextInputLayout textInputUsername;
    @BindView(R.id.edit_login_password)
    EditText editLoginPassword;
    @BindView(R.id.text_input_password)
    TextInputLayout textInputPassword;
    @BindView(R.id.text_label_forget_password)
    CustomTextView textLabelForgetPassword;
    @BindView(R.id.text_label_new_user)
    CustomTextView textLabelNewUser;
    LoginActivity loginActivity;
    @BindView(R.id.text_login_title)
    CustomTextViewBold textLoginTitle;
    @BindView(R.id.text_label_sign_up)
    CustomTextViewBold textLabelSignUp;
    @BindView(R.id.text_login_change_number)
    CustomTextViewBold textLoginChangeNumber;
    @BindView(R.id.cons_login_frag)
    ConstraintLayout consLoginFrag;
    Bundle bundle;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    FalconAPI falconAPI;
    UserError userError;
    HospitalLoginResponse hospitalLoginResponse;
    IndiduvalLoginResponse indiduvalLoginResponse;
    @BindView(R.id.edit_login_hos_phone_number)
    CustomEditText editLoginHosPhoneNumber;
    @BindView(R.id.text_input_hos_username)
    TextInputLayout textInputHosUsername;
    SharedPreferences fcmKeyPreferences;
    @BindView(R.id.text_label_or)
    CustomTextView textLabelOr;
    @BindView(R.id.text_label_login_otp)
    CustomTextView textLabelLoginOtp;
    private InputFilter restrictSmileys;
    private InputFilter.LengthFilter setLength;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);
        loginActivity = (LoginActivity) getActivity();
        getActivity().setTitle(getString(R.string.login));
        ((LoginActivity) getActivity()).getSupportActionBar().show();
        FalconApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        userError = new UserError();
        textInputPassword.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "font/Montserrat-Regular.ttf"));
        fcmKeyPreferences = getActivity().getSharedPreferences(FalconConstants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
        editLoginPassword.setFilters(new InputFilter[]{editTextRestrictLength(), editTextRestrictSmileys()});
        switch (sharedPreferences.getInt(FalconConstants.ROLEID, 0)) {
            case 0:
//                Indiduval User
                textInputUsername.setVisibility(View.VISIBLE);
                textInputHosUsername.setVisibility(View.INVISIBLE);
                textLoginChangeNumber.setVisibility(View.VISIBLE);
                textLabelOr.setVisibility(View.VISIBLE);
                textLabelLoginOtp.setVisibility(View.VISIBLE);
//                editLoginPassword.addTextChangedListener(textWatcherIndi);
//                editLoginPhoneNumber.addTextChangedListener(textWatcherIndi);
                break;
            case 1:
//                Hosp User
                textInputUsername.setVisibility(View.INVISIBLE);
                textInputHosUsername.setVisibility(View.VISIBLE);
                textLoginChangeNumber.setVisibility(View.GONE);
                textLabelOr.setVisibility(View.GONE);
                textLabelLoginOtp.setVisibility(View.GONE);
//                editLoginPassword.addTextChangedListener(textWatcherHos);
//                editLoginHosPhoneNumber.addTextChangedListener(textWatcherHos);
                break;

        }
        editLoginPassword.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {

                    if (isUserLoginNotNull()) {
                        switch (sharedPreferences.getInt(FalconConstants.ROLEID, 0)) {
                            case 0:
                                loginIndiduvalUser();
                                break;
                            case 1:
//                            editor.putInt(FalconConstants.LOGINSTATUSCODE, 2).commit();
//                            Intent intent = new Intent(getActivity(), HomeActivity.class);
//                            startActivity(intent);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                loginHospitalUser();
                                break;
                        }
                    }
                }
                return false;
            }
        });

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        unbinder.unbind();
//        null.unbind();
//        null.unbind();
    }


    @OnClick({R.id.text_label_forget_password, R.id.button_login, R.id.text_label_sign_up, R.id.text_login_change_number,
            R.id.text_label_login_otp})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.text_label_forget_password:
                if (sharedPreferences.getInt(FalconConstants.ROLEID, 0) == 0)
                    loginActivity.push(new ForgetPasswordFragment(), getString(R.string.change_password));
                else
                    loginActivity.push(new ForgetPasswordHospFragment(), getString(R.string.forget_password));
                break;
            case R.id.text_login_change_number:
                loginActivity.push(new ChangeNumberFragment(), getString(R.string.login_change_number));
                break;
            case R.id.button_login:
                if (isUserLoginNotNull()) {
                    switch (sharedPreferences.getInt(FalconConstants.ROLEID, 0)) {
                        case 0:
                            loginIndiduvalUser();
                            break;
                        case 1:
//                            editor.putInt(FalconConstants.LOGINSTATUSCODE, 2).commit();
//                            Intent intent = new Intent(getActivity(), HomeActivity.class);
//                            startActivity(intent);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            loginHospitalUser();
                            break;
                    }
                }
                break;
            case R.id.text_label_sign_up:
                switch (sharedPreferences.getInt(FalconConstants.ROLEID, 0)) {
                    case 0:
                        OtpFragment otpFragment = new OtpFragment();
                        Bundle bundle = new Bundle();
                        bundle.putBoolean("isForSignUp", true);
                        otpFragment.setArguments(bundle);
                        loginActivity.push(otpFragment, getString(R.string.Registration));
//                        Intent intent = new Intent(getActivity(), ProfileActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        startActivity(intent);
                        break;
                    case 1:
                        Intent intent2 = new Intent(getActivity(), HospitalProfileActivity.class);
//                        intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent2);
                        break;
                }
                break;
            case R.id.text_label_login_otp:
                loginActivity.push(new OtpFragment(), getString(R.string.Registration));
                break;
        }
    }


//    public boolean loginIndiValidation() {
//        if (!editLoginPhoneNumber.getText().toString().isEmpty() && !editLoginPassword.getText().toString().isEmpty()) {
//            buttonLogin.setEnabled(true);
//            buttonLogin.setBackgroundColor(getResources().getColor(R.color.falcon_red));
//            return true;
//        } else {
//            buttonLogin.setEnabled(false);
//            buttonLogin.setBackgroundColor(getResources().getColor(R.color.light_grey));
//            return false;
//        }
//    }

//    public boolean loginHosValidation() {
//        if (!editLoginHosPhoneNumber.getText().toString().isEmpty() && !editLoginPassword.getText().toString().isEmpty()) {
//            buttonLogin.setEnabled(true);
//            buttonLogin.setBackgroundColor(getResources().getColor(R.color.falcon_red));
//            return true;
//        } else {
//            buttonLogin.setEnabled(false);
//            buttonLogin.setBackgroundColor(getResources().getColor(R.color.light_grey));
//            return false;
//        }
//    }
//
//    TextWatcher textWatcherIndi = new TextWatcher() {
//        @Override
//        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//        }
//
//        @Override
//        public void onTextChanged(CharSequence s, int start, int before, int count) {
//            loginIndiValidation();
//        }
//
//        @Override
//        public void afterTextChanged(Editable s) {
//
//        }
//    };
//    TextWatcher textWatcherHos = new TextWatcher() {
//        @Override
//        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//        }
//
//        @Override
//        public void onTextChanged(CharSequence s, int start, int before, int count) {
//            loginHosValidation();
//        }
//
//        @Override
//        public void afterTextChanged(Editable s) {
//
//        }
//    };

    private void loginHospitalUser() {


        HospitalLogin hospitalLogin = new HospitalLogin();
        hospitalLogin.setRegisterID(editLoginHosPhoneNumber.getText().toString());
        hospitalLogin.setPassword(editLoginPassword.getText().toString());
        hospitalLogin.setFCMKey(fcmKeyPreferences.getString(FalconConstants.FCMTOKEN, ""));


        if (Utils.isNetworkAvailable()) {
            showProgress();
            falconAPI.loginHospitalUser(hospitalLogin).enqueue(new Callback<HospitalLoginResponse>() {
                @Override
                public void onResponse(Call<HospitalLoginResponse> call, Response<HospitalLoginResponse> response) {
                    hideProgress();
                    hospitalLoginResponse = response.body();
                    if (response.isSuccessful() && response.body() != null) {
                        if (isAdded()) {
                            if (hospitalLoginResponse.getStatusCode() == 200) {
                                editor.putInt(FalconConstants.LOGINSTATUSCODE, 2).commit();
                                editor.putInt(FalconConstants.HOSPITALUSERID, hospitalLoginResponse.getUserinfo().getId()).commit();
                                editor.putString(FalconConstants.HOSPITALREGISTERID, hospitalLoginResponse.getUserinfo().getRegisterID()).commit();
                                editor.putInt(FalconConstants.HOSPUNITS, hospitalLoginResponse.getUserinfo().getIndUnits()).commit();
//                                editor.putInt(FalconConstants.HOSPLIVES, hospitalLoginResponse.getUserinfo().getLives()).commit();
                                editor.putInt(FalconConstants.HOSPTOTALUNITS, hospitalLoginResponse.getTotalUnits()).commit();
                                editor.putString(FalconConstants.USERNAME, hospitalLoginResponse.getUserinfo().getName()).commit();
                                editor.putString(FalconConstants.PHONENUMBER, hospitalLoginResponse.getUserinfo().getPhoneNumber()).commit();
                                editor.putString(FalconConstants.PROFILEPICTURE, hospitalLoginResponse.getUserinfo().getPhoto()).commit();
                                Intent intent = new Intent(getActivity(), HomeActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                Toast.makeText(getActivity(), getString(R.string.successfully_logged), Toast.LENGTH_SHORT).show();
                            } else if (hospitalLoginResponse.getStatusCode() == 401) {
                                AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(getActivity());
                                alertDialog1.setView(R.layout.inactive_user_alert_view);
                                alertDialog1.setCancelable(true);
                                alertDialog1.show();
                            } else if (hospitalLoginResponse.getStatusCode() == 408) {
                                Toast.makeText(getActivity(), hospitalLoginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), hospitalLoginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                        }

                    }
                }

                @Override
                public void onFailure(Call<HospitalLoginResponse> call, Throwable t) {
                    if (isAdded()) {
                        hideProgress();
                        Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }

                }
            });

        } else {
            userError.message = getActivity().getString(R.string.network_error_message);
            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }


    }

    private void loginIndiduvalUser() {
        if (Utils.isNetworkAvailable()) {
            IndiduvalLogin indiduvalLogin = new IndiduvalLogin();
            indiduvalLogin.setPhoneNumber(editLoginPhoneNumber.getText().toString());
            indiduvalLogin.setPassword(editLoginPassword.getText().toString());
            indiduvalLogin.setFCMKey(fcmKeyPreferences.getString(FalconConstants.FCMTOKEN, ""));

            showProgress();
            falconAPI.loginIndiduvalUser(indiduvalLogin).enqueue(new Callback<IndiduvalLoginResponse>() {
                @Override
                public void onResponse(Call<IndiduvalLoginResponse> call, Response<IndiduvalLoginResponse> response) {
                    hideProgress();
                    indiduvalLoginResponse = response.body();
                    if (response.body() != null && response.isSuccessful()) {
                        if (isAdded()) {
                            if (indiduvalLoginResponse.getStatusCode() == 200) {
                                editor.putInt(FalconConstants.LOGINSTATUSCODE, 2).commit();
                                editor.putInt(FalconConstants.USEDID, indiduvalLoginResponse.getUserInfo().getId()).commit();
//                                editor.putInt(FalconConstants.INDIDUVALLIVES, indiduvalLoginResponse.getUserInfo().getLives()).commit();
//                                editor.putInt(FalconConstants.INDIDUVALUNITS, indiduvalLoginResponse.getUserInfo().getIndUnits()).commit();
//                                editor.putInt(FalconConstants.FALCONTOTALUNITS, indiduvalLoginResponse.getTotalUnits()).commit();

                                editor.putString(FalconConstants.BLOODGROUP, indiduvalLoginResponse.getUserInfo().getBloodGroupID()).commit();
                                editor.putString(FalconConstants.USERNAME, indiduvalLoginResponse.getUserInfo().getFirstName()).commit();
                                editor.putInt(FalconConstants.AUTHORIZER, indiduvalLoginResponse.getUserInfo().getAuthorizer()).commit();
                                editor.putString(FalconConstants.PHONENUMBER, indiduvalLoginResponse.getUserInfo().getPhoneNumber()).commit();
                                editor.putString(FalconConstants.PROFILEPICTURE, indiduvalLoginResponse.getUserInfo().getProfilePicture()).commit();
                                editor.putInt(FalconConstants.USERTYPE, indiduvalLoginResponse.getUserInfo().getUserType()).commit();
                                Intent intent = new Intent(getActivity(), HomeActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                Toast.makeText(getActivity(), getString(R.string.successfully_logged), Toast.LENGTH_SHORT).show();
                            } else if (indiduvalLoginResponse.getStatusCode() == 401) {
                                AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(getActivity());
                                alertDialog1.setView(R.layout.inactive_user_alert_view);
                                alertDialog1.setCancelable(true);
                                alertDialog1.show();
                            } else {
                                Toast.makeText(getActivity(), indiduvalLoginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }else {
                        Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<IndiduvalLoginResponse> call, Throwable t) {
                    if (isAdded()) {
                        Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                        hideProgress();
                    }

                }
            });

        } else {
            userError.message = getActivity().getString(R.string.network_error_message);
            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }
    }

    private boolean showErrorPhoneNumber() {
        if (editLoginPhoneNumber.getText().toString().isEmpty()) {
            textInputUsername.setError(getString(R.string.req_pho));
            return false;
        } else if (editLoginPhoneNumber.getText().toString().length() != 10) {
            textInputUsername.setError(getString(R.string.req_phone_number));
            return false;
        } else {
            textInputUsername.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorHosUserName() {
        if (editLoginHosPhoneNumber.getText().toString().isEmpty()) {
            textInputHosUsername.setError(getString(R.string.req_user_name));
            return false;
        } else {
            textInputHosUsername.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorHosUserName1() {
        if (editLoginHosPhoneNumber.getText().toString().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorPhoneNumber1() {
        if (editLoginPhoneNumber.getText().toString().isEmpty()) {
            return false;
        } else if (editLoginPhoneNumber.getText().toString().length() > 10) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorPassword() {
        if (editLoginPassword.getText().toString().isEmpty()) {
            textInputPassword.setError(getString(R.string.req_first_pasword));
            return false;
        } else if (editLoginPassword.getText().length() < 8) {
            textInputPassword.setError(getActivity().getString(R.string.min_pass));
            return false;
        } else {
            textInputPassword.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorPassword1() {
        if (editLoginPassword.getText().toString().isEmpty()) {
            return false;
        } else if (editLoginPassword.getText().length() < 8) {
            return false;
        } else {
            return true;
        }
    }

    private boolean isUserLoginNotNull() {
        showErrorPassword();
        switch (sharedPreferences.getInt(FalconConstants.ROLEID, 0)) {

            case 0:
                showErrorPhoneNumber();
                if (showErrorPhoneNumber1() && showErrorPassword1()) {
                    return true;
                } else {
                    return false;
                }
            case 1:
                showErrorHosUserName();
                if (showErrorHosUserName1() && showErrorPassword1()) {
                    return true;
                } else {
                    return false;
                }
        }
        return false;

    }

}
