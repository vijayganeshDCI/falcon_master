package com.dci.falcon.fragment;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.activity.HomeActivity;
import com.dci.falcon.activity.LoginActivity;
import com.dci.falcon.activity.ProfileActivity;
import com.dci.falcon.activity.QuickRegistrationActivity;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.model.ChangeNumber;
import com.dci.falcon.model.ChangeNumberResponse;
import com.dci.falcon.model.GetIndiUserResponse;
import com.dci.falcon.model.IndiduvalRegister;
import com.dci.falcon.model.Phonenumber;
import com.dci.falcon.model.QuickRegistration;
import com.dci.falcon.model.QuickRegistrationResponse;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomButton;
import com.dci.falcon.view.CustomTextView;
import com.dci.falcon.view.CustomTextViewBold;
import com.google.gson.GsonBuilder;
import com.msg91.sendotp.library.SendOtpVerification;
import com.msg91.sendotp.library.Verification;
import com.msg91.sendotp.library.VerificationListener;
import com.mukesh.OtpView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;
import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;
import static com.dci.falcon.fragment.OtpFragment.INTENT_COUNTRY_CODE;
import static com.dci.falcon.fragment.OtpFragment.INTENT_PHONENUMBER;


/**
 * Created by vijayaganesh on 10/23/2017.
 */

public class OtpVerificationFragment extends BaseFragment implements ActivityCompat.OnRequestPermissionsResultCallback, VerificationListener {

    Unbinder unbinder;
    private static final String TAG = Verification.class.getSimpleName();
    @BindView(R.id.text_label_verify)
    CustomTextViewBold textLabelVerify;
    @BindView(R.id.text_otp_mob_number)
    CustomTextView textOtpMobNumber;
    @BindView(R.id.text_label_enter_otp)
    CustomTextView textLabelEnterOtp;
    @BindView(R.id.edit_otp)
    OtpView editOtp;
    @BindView(R.id.button_verify_otp)
    CustomButton buttonVerifyOtp;
    @BindView(R.id.text_resend_otp)
    CustomTextViewBold textResendOtp;
    @BindView(R.id.cons_otp_verify)
    ConstraintLayout consOtpVerify;
    @BindView(R.id.image_icon_verify_number)
    ImageView imageIconVerifyNumber;
    private Verification mVerification;
    Bundle bundle;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String countryCode, phoneNumber;
    LoginActivity loginActivity;
    UserError userError;
    boolean isNewUser = true;
    @Inject
    FalconAPI falconAPI;
    ChangeNumberResponse changeNumberResponse;
    private GetIndiUserResponse getIndiUserResponse;
    private int otpVerify;
    SharedPreferences fcmKeyPreferences;
    private String quickRegDetails;
    QuickRegistrationResponse quickRegistrationResponse;
    private HomeActivity homeActivity;
    private QuickRegistrationActivity quickRegistrationActivity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_otp_verify, container, false);
        unbinder = ButterKnife.bind(this, view);
        FalconApplication.getContext().getComponent().inject(this);
        bundle = getArguments();
        editor = sharedPreferences.edit();
        if (getActivity() instanceof LoginActivity) {
            loginActivity = (LoginActivity) getActivity();
            ((LoginActivity) getActivity()).getSupportActionBar().show();
            fcmKeyPreferences = getActivity().getSharedPreferences(FalconConstants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);

        } else if (getActivity() instanceof HomeActivity) {
            homeActivity = (HomeActivity) getActivity();
            ((HomeActivity) getActivity()).getSupportActionBar().show();

        } else {
            quickRegistrationActivity = (QuickRegistrationActivity) getActivity();
            ((QuickRegistrationActivity) getActivity()).getSupportActionBar().show();
        }
        getActivity().setTitle(getString(R.string.Verification));

        userError = new UserError();
        if (bundle != null) {
            otpVerify = bundle.getInt("otpVerify");
            isNewUser = bundle.getBoolean("isNewUser");
            quickRegDetails = bundle.getString("quickRegDetails");
        }
        initiateVerification();
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onInitiated(String response) {
        hideProgress();
    }

    @Override
    public void onInitiationFailed(Exception exception) {
    }

    @Override
    public void onVerified(String response) {
        Log.d(TAG, "Verified!\n" + response);
        hideKeypad();
        hideProgress();
        editor.putBoolean(FalconConstants.ISLOOGED, true).commit();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setMessage(R.string.Otp_verified);
        alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (otpVerify) {
                    case 0:
                        //                For signup
                        editor.putInt(FalconConstants.LOGINSTATUSCODE, 0).commit();
                        Intent intent = new Intent(getActivity(), ProfileActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.putExtra("phoneNumber", phoneNumber);
                        intent.putExtra("isFromBaseActivity", false);
                        startActivity(intent);
                        break;
                    case 1:
                        //                    for login with otp
                        getProfile();
                        break;
                    case 2:
//                        for change number
                        changeNumber();
                        break;
                    case 3:
//                        for quick registration
                        Quickregistration();
                        break;
                }
            }
        });
        alertDialog.show();

    }

    @Override
    public void onVerificationFailed(Exception exception) {
        Log.e(TAG, "Verification failed: " + exception.getMessage());
        hideProgress();
        userError.message = getActivity().getString(R.string.Otp_un_verified);
        showError(ERROR_SHOW_TYPE_DIALOG, userError);
    }


    private void Quickregistration() {
        final QuickRegistration quickRegistration = new GsonBuilder().create().fromJson(quickRegDetails, QuickRegistration.class);
        final IndiduvalRegister indiduvalRegister = new IndiduvalRegister();
        indiduvalRegister.setDeviceID(sharedPreferences.getString(FalconConstants.DEVICEID, ""));
        indiduvalRegister.setAppID(sharedPreferences.getString(FalconConstants.APPID, ""));
        indiduvalRegister.setAppVersion(sharedPreferences.getString(FalconConstants.APPVERSION, ""));
        indiduvalRegister.setNonsmartPhone(true);
        indiduvalRegister.setAuthorizer(quickRegistration.isAuthorizerStatus());
        indiduvalRegister.setPhoneNumber(quickRegistration.getPhoneNumber());
        indiduvalRegister.setUserType(quickRegistration.getUserType());
        indiduvalRegister.setProfilePicture(quickRegistration.getProfilePicture());
        indiduvalRegister.setFirstName(quickRegistration.getFirstName());
        indiduvalRegister.setBloodGroupID(quickRegistration.getBloodGroup());
        indiduvalRegister.setNearHospitalLocationHome(quickRegistration.getLocationHome());
        indiduvalRegister.setNearHospitalLocationHomeLng(quickRegistration.getHomelng());
        indiduvalRegister.setNearHospitalLocationHomeLat(quickRegistration.getHomelat());
        indiduvalRegister.setLastName(quickRegistration.getLastName());
        indiduvalRegister.setFCMKey("");
        indiduvalRegister.setAge(quickRegistration.getAge());
        indiduvalRegister.setEmailID("");
        indiduvalRegister.setPassword("");
        indiduvalRegister.setDOB(quickRegistration.getDob());
        indiduvalRegister.setGender(quickRegistration.getGender());
        indiduvalRegister.setSecQuesID(0);
        indiduvalRegister.setSecQuesAns("");
        indiduvalRegister.setOrganizationType(0);
        indiduvalRegister.setOrganizationID(0);
        indiduvalRegister.setOrgAddressDetail("");
        indiduvalRegister.setOrgName("");
        indiduvalRegister.setOrgDetail("");
        indiduvalRegister.setOrgPhoneNumber("");
        indiduvalRegister.setNearHospitalLocationOffice("");
        indiduvalRegister.setNearHospitalLocationFre("");
        indiduvalRegister.setNearHospitalLocationOfficeLat("");
        indiduvalRegister.setNearHospitalLocationOfficeLng("");
        indiduvalRegister.setNearHospitalLocationFreLat("");
        indiduvalRegister.setNearHospitalLocationFreLng("");
        indiduvalRegister.setActiveStatus(0);
        indiduvalRegister.setLastDonationDate("");
        indiduvalRegister.setDonateAt("");
        indiduvalRegister.setDND("");
        indiduvalRegister.setAddUser(1);
//        ReqUserType >> (0-User, 1-Hospital)
        if (sharedPreferences.getInt(FalconConstants.ROLEID, 0) == 0) {
            indiduvalRegister.setReqUserType(0);
            indiduvalRegister.setUserID(sharedPreferences.getInt(FalconConstants.USEDID, 0));
        } else {
            indiduvalRegister.setReqUserType(1);
            indiduvalRegister.setUserID(sharedPreferences.getInt(FalconConstants.HOSPITALUSERID, 0));
        }
        if (Utils.isNetworkAvailable()) {
            showProgress();
            falconAPI.quickRegistration(indiduvalRegister).enqueue(new Callback<QuickRegistrationResponse>() {
                @Override
                public void onResponse(Call<QuickRegistrationResponse> call, Response<QuickRegistrationResponse> response) {
                    hideProgress();
                    quickRegistrationResponse = response.body();
                    if (isAdded()) {
                        if (response.body() != null && response.isSuccessful() && quickRegistrationResponse.getStatusCode() == 200) {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                            alertDialog.setMessage(getString(R.string.successfully_regis));
                            alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    getActivity().onBackPressed();
                                }
                            });
                            alertDialog.show();
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                        }
                    }

                }

                @Override
                public void onFailure(Call<QuickRegistrationResponse> call, Throwable t) {
                    if (isAdded()) {
                        hideProgress();
                        Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }

                }
            });
        } else {
            userError.message = getString(R.string.network_error_message);
            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }

    }

    private void getProfile() {
        final Phonenumber getUserInfo = new Phonenumber();
        getUserInfo.setPhoneNumber(phoneNumber);
        getUserInfo.setFCMKey(fcmKeyPreferences.getString(FalconConstants.FCMTOKEN, ""));
        if (Utils.isNetworkAvailable()) {
            showProgress();
            falconAPI.loginWithPhonenumber(getUserInfo).enqueue(new Callback<GetIndiUserResponse>() {
                @Override
                public void onResponse(Call<GetIndiUserResponse> call, Response<GetIndiUserResponse> response) {
                    hideProgress();
                    getIndiUserResponse = response.body();
                    if (isAdded()) {
                        if (response.body() != null && response.isSuccessful() && getIndiUserResponse.getStatusCode() == 200) {
                            editor.putInt(FalconConstants.USEDID, getIndiUserResponse.getUserInfo().getId()).commit();
                            if (!getIndiUserResponse.getUserInfo().getFirstName().isEmpty() && !getIndiUserResponse.getUserInfo().getLastName().isEmpty()
                                    && getIndiUserResponse.getUserInfo().getGender() != 0 && getIndiUserResponse.getUserInfo().getAge() != 0
                                    && !getIndiUserResponse.getUserInfo().getBloodGroupID().contains("Select Blood Group")
                                    && !getIndiUserResponse.getUserInfo().getDOB().isEmpty()
                                    && !getIndiUserResponse.getUserInfo().getEmailID().isEmpty()
                                    && !getIndiUserResponse.getUserInfo().getNearHospitalLocationHome().isEmpty()
                                    && getIndiUserResponse.getUserInfo().getSecQuesID() != 0
                                    && !getIndiUserResponse.getUserInfo().getSecQuesAns().isEmpty()
                                    && getIndiUserResponse.getOrg() != null) {
                                editor.putInt(FalconConstants.LOGINSTATUSCODE, 2).commit();
//                                editor.putInt(FalconConstants.INDIDUVALLIVES, getIndiUserResponse.getUserInfo().getLives()).commit();
//                                editor.putInt(FalconConstants.INDIDUVALUNITS, getIndiUserResponse.getUserInfo().getIndUnits()).commit();
//                                editor.putInt(FalconConstants.FALCONTOTALUNITS, getIndiUserResponse.getTotalUnits()).commit();
                                editor.putString(FalconConstants.BLOODGROUP, getIndiUserResponse.getUserInfo().getBloodGroupID()).commit();
                                editor.putString(FalconConstants.USERNAME, getIndiUserResponse.getUserInfo().getFirstName()).commit();
                                editor.putInt(FalconConstants.AUTHORIZER, getIndiUserResponse.getUserInfo().getAuthorizer()).commit();
                                editor.putString(FalconConstants.PHONENUMBER, getIndiUserResponse.getUserInfo().getPhoneNumber()).commit();
                                editor.putString(FalconConstants.PROFILEPICTURE, getIndiUserResponse.getUserInfo().getProfilePicture()).commit();
                                Intent intent = new Intent(getActivity(), HomeActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                Toast.makeText(getActivity(), getString(R.string.successfully_logged), Toast.LENGTH_SHORT).show();
                            } else {
                                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.putExtra("phoneNumber", phoneNumber);
                                intent.putExtra("isFromBaseActivity", true);
                                intent.putExtra("forLogin", true);
                                startActivity(intent);
                                editor.putInt(FalconConstants.LOGINSTATUSCODE, 1).commit();
                                Toast.makeText(getActivity(), getString(R.string.fill_manditatory_fileds), Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<GetIndiUserResponse> call, Throwable t) {
                    if (isAdded()) {
                        hideProgress();
                        Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }
                }
            });

        } else {
            userError.message = getString(R.string.network_error_message);
            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }
    }


    private void hideKeypad() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    void createVerification(String phoneNumber, boolean skipPermissionCheck, String countryCode) {
        if (!skipPermissionCheck && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_SMS) ==
                PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_SMS}, 0);
        } else {
            mVerification = SendOtpVerification.createSmsVerification
                    (SendOtpVerification
                            .config(countryCode + phoneNumber)
                            .context(getActivity())
                            .autoVerification(false)
                            .otplength("4")
                            .expiry("5")
                            .build(), this);
            mVerification.initiate();
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permissions[0])) {
                Toast.makeText(getActivity(), "This application needs permission to read your SMS to automatically verify your "
                        + "phone, you may disable the permission once you have been verified.", Toast.LENGTH_LONG)
                        .show();
            }
        }
        initiateVerificationAndSuppressPermissionCheck();
    }

    void initiateVerification() {
        initiateVerification(false);
    }

    void initiateVerificationAndSuppressPermissionCheck() {
        initiateVerification(true);
    }

    void initiateVerification(boolean skipPermissionCheck) {
        Intent intent = getActivity().getIntent();
        if (intent != null) {
            countryCode = bundle.getString(INTENT_COUNTRY_CODE);
            phoneNumber = bundle.getString(INTENT_PHONENUMBER);
            textOtpMobNumber.setText("+" + 91 + "-" + phoneNumber);
            createVerification(phoneNumber, skipPermissionCheck, countryCode);
        }
    }


    @OnClick({R.id.button_verify_otp, R.id.text_resend_otp})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button_verify_otp:
//                getProfile();
                if (Utils.isNetworkAvailable()) {
                    if (editOtp.getOTP() != null && mVerification != null) {
                        mVerification.verify(editOtp.getOTP());
                        showProgress();
                    }
                } else {
                    userError.message = getActivity().getString(R.string.network_error_message);
                    showError(ERROR_SHOW_TYPE_DIALOG, userError);
                }
                break;
            case R.id.text_resend_otp:
                if (Utils.isNetworkAvailable()) {
                    if (editOtp.getOTP() != null && mVerification != null) {
                        mVerification.resend("voice");
                        showProgress();
                    }
                } else {
                    userError.message = getActivity().getString(R.string.network_error_message);
                    showError(ERROR_SHOW_TYPE_DIALOG, userError);
                }
                break;
        }
    }

    private void changeNumber() {

        ChangeNumber changeNumber = new ChangeNumber();
        changeNumber.setOld_phone_no(sharedPreferences.getString(FalconConstants.OLDPHONENUMBER, ""));
        changeNumber.setNew_phone_no(sharedPreferences.getString(FalconConstants.NEWPHONENUMBER, ""));
        if (Utils.isNetworkAvailable()) {
            showProgress();
            falconAPI.changeNumber(changeNumber).enqueue(new Callback<ChangeNumberResponse>() {
                @Override
                public void onResponse(Call<ChangeNumberResponse> call, Response<ChangeNumberResponse> response) {
                    hideProgress();
                    changeNumberResponse = response.body();
                    if (isAdded()) {
                        if (response.body() != null && response.isSuccessful() && changeNumberResponse.getStatusCode() == 200) {
                            //                        editor.putInt(FalconConstants.USEDID, changeNumberResponse.getData().get(0).getId()).commit();
                            //                        editor.putInt(FalconConstants.INDIDUVALLIVES, changeNumberResponse.getData().get(0).getLives()).commit();
                            //                        editor.putInt(FalconConstants.INDIDUVALUNITS, changeNumberResponse.getData().get(0).getUnits()).commit();
                            //                        editor.putString(FalconConstants.BLOODGROUP, changeNumberResponse.getData().get(0).getBloodGroupID()).commit();
                            //                        editor.putString(FalconConstants.USERNAME, changeNumberResponse.getData().get(0).getFirstName()).commit();
                            Intent intent_admin_logout = new Intent(getActivity(), LoginActivity.class);
                            intent_admin_logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            getActivity().finish();
                            startActivity(intent_admin_logout);
                            Toast.makeText(getActivity(), changeNumberResponse.getStatus(), Toast.LENGTH_SHORT).show();
                            editor.putInt(FalconConstants.LOGINSTATUSCODE, 0).commit();
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                        }
                    }

                }

                @Override
                public void onFailure(Call<ChangeNumberResponse> call, Throwable t) {
                    if (isAdded()) {
                        hideProgress();
                        Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }

                }
            });

        } else {
            userError.message = getActivity().getString(R.string.network_error_message);
            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }
    }
}
