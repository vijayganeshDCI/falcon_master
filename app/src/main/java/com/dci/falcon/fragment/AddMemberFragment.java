package com.dci.falcon.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.text.InputFilter;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.activity.HomeActivity;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.model.AddMember;
import com.dci.falcon.model.AddMemberResponse;
import com.dci.falcon.model.GetAddMemberResponse;
import com.dci.falcon.model.MemberDelete;
import com.dci.falcon.model.UpdateMemberResponse;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomButton;
import com.dci.falcon.view.CustomCheckBox;
import com.dci.falcon.view.CustomEditText;
import com.dci.falcon.view.CustomRadioButton;
import com.dci.falcon.view.CustomTextView;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.tasks.OnSuccessListener;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import lib.kingja.switchbutton.SwitchMultiButton;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

/**
 * Created by vijayaganesh on 11/14/2017.
 */

public class AddMemberFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    @BindView(R.id.image_profile_pic)
    CircularImageView imageProfilePic;
    @BindView(R.id.edit_first_name)
    CustomEditText editFirstName;
    @BindView(R.id.text_input_first_name)
    TextInputLayout textInputFirstName;
    @BindView(R.id.edit_last_name)
    CustomEditText editLastName;
    @BindView(R.id.text_input_last_name)
    TextInputLayout textInputLastName;
    @BindView(R.id.spinner_blood_group)
    Spinner spinnerBloodGroup;
    @BindView(R.id.view_blood_group)
    View viewBloodGroup;
    @BindView(R.id.edit_age)
    CustomEditText editAge;
    @BindView(R.id.text_input_age)
    TextInputLayout textInputAge;
    @BindView(R.id.edit_email)
    CustomEditText editEmail;
    @BindView(R.id.text_input_email)
    TextInputLayout textInputEmail;
    @BindView(R.id.edit_phone_number)
    CustomEditText editPhoneNumber;
    @BindView(R.id.text_input_phone_number)
    TextInputLayout textInputPhoneNumber;
    @BindView(R.id.edit_dob)
    CustomEditText editDob;
    @BindView(R.id.text_input_Dob)
    TextInputLayout textInputDob;
    @BindView(R.id.edit_gender)
    CustomEditText editGender;
    @BindView(R.id.switch_gender)
    SwitchMultiButton switchGender;
    @BindView(R.id.edit_address_home)
    CustomEditText editAddressHome;
    @BindView(R.id.text_input_add_home)
    TextInputLayout textInputAddHome;
    @BindView(R.id.button_add)
    CustomButton buttonAdd;
    @BindView(R.id.cons_add_member)
    ConstraintLayout consAddMember;
    Unbinder unbinder;
    @BindView(R.id.text_pin_hospital)
    CustomTextView textPinHospital;
    @BindView(R.id.edit_address_office)
    CustomEditText editAddressOffice;
    @BindView(R.id.text_input_add_office)
    TextInputLayout textInputAddOffice;
    @BindView(R.id.edit_address_freq)
    CustomEditText editAddressFreq;
    @BindView(R.id.text_input_add_freq)
    TextInputLayout textInputAddFreq;
    @BindView(R.id.scroll_add_member)
    ScrollView scrollAddMember;
    @BindView(R.id.spinner_relation_ship)
    Spinner spinnerRelationShip;
    @BindView(R.id.view_relation_ship)
    View viewRelationShip;
    @BindView(R.id.check_auth_phonenumber)
    CustomCheckBox checkAuthPhonenumber;
    @BindView(R.id.text_edit_photo)
    CustomTextView textEditPhoto;
    @BindView(R.id.radio_button_donor)
    CustomRadioButton radioButtonDonor;
    @BindView(R.id.radio_button_non_donor)
    CustomRadioButton radioButtonNonDonor;
    @BindView(R.id.radio_donor_rule)
    RadioGroup radioDonorRule;
    @BindView(R.id.image_mantitory_4)
    ImageView imageMantitory4;
    @BindView(R.id.text_label_manditatory)
    CustomTextView textLabelManditatory;
    @BindView(R.id.image_mantitory)
    ImageView imageMantitory;
    @BindView(R.id.image_mantitory_1)
    ImageView imageMantitory1;
    @BindView(R.id.image_mantitory_2)
    ImageView imageMantitory2;
    @BindView(R.id.text_label_active)
    CustomTextView textLabelActive;
    @BindView(R.id.switch_active)
    SwitchCompat switchActive;
    @BindView(R.id.image_right_down_arrow)
    ImageView imageRightDownArrow;
    @BindView(R.id.spinner_active_status)
    Spinner spinnerActiveStatus;
    @BindView(R.id.cons_active_status)
    ConstraintLayout consActiveStatus;
    @BindView(R.id.edit_last_donated_date)
    CustomEditText editLastDonatedDate;
    @BindView(R.id.text_input_last_donated_date)
    TextInputLayout textInputLastDonatedDate;
    private Calendar currentD, calendardate, calendarTime, currentT;
    private String dob, donateTime;
    SimpleDateFormat simpleDateFormat;
    private static final int PLACE_PICKER_REQUEST = 3;
    private static final long FASTEST_INTERVAL = 1000 * 5;
    private static final long INTERVAL = 1000 * 10;
    private static final int MY_REQUEST_CODE_CAMERA = 5;
    private static final int MY_REQUEST_CODE_GALLERY = 6;
    private int REQUEST_CAMERA = 1;
    private int SELECT_FILE = 2;
    private LocationRequest mLocationRequest;
    Location mCurrentLocation;
    GoogleApiClient mGoogleApiClient;
    private double mLat, mLng;
    private LatLngBounds bounds;
    FloatingActionButton floatingActionButton;
    HomeActivity homeActivity;
    public static int editLocation;
    double nearHomeLat, nearHomeLng;
    double nearOfficeLat, nearOfficeLng;
    double nearFreqLat, nearFreqLng;
    @Inject
    FalconAPI falconAPI;
    @Inject
    SharedPreferences sharedPreferences;
    String bloodGroup;
    String realtionShip;
    int gender = 2;
    UserError userError;
    AddMemberResponse addMemberResponse;
    UpdateMemberResponse updateMemberResponse;
    String profilePicture;
    private Uri uri;
    private String proFilePictureFromImageview;
    int memberId;
    boolean isFromViewInfo = false;
    boolean isLogged = false;
    GetAddMemberResponse getAddMemberResponse;
    MenuItem edit, save;
    View view;
    private int radiocheckedId;
    private FusedLocationProviderClient mFusedLocationClient;
    private int authenticateUser = 0;
    double nearHomeLat1, nearHomeLng1;
    double nearFreqLat1, nearFreqLng1;
    double nearOfficeLat1, nearOfficeLng1;
    private int isActiveChecked = 1;
    private int dateSelected = 0;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_add_member, container, false);
        unbinder = ButterKnife.bind(this, view);
        simpleDateFormat = new SimpleDateFormat();
        getActivity().setTitle(getString(R.string.Add_Member));
        homeActivity = (HomeActivity) getActivity();
        FalconApplication.getContext().getComponent().inject(this);
        userError = new UserError();
        final ArrayAdapter<String> bloodgroupAdapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item, getResources().getStringArray(R.array.blood_group));
        spinnerBloodGroup.setAdapter(bloodgroupAdapter);
        final ArrayAdapter<String> relationshipAdapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item, getResources().getStringArray(R.array.relation_ship));
        spinnerRelationShip.setAdapter(relationshipAdapter);
        ArrayAdapter<String> activeStatus = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item, getResources().getStringArray(R.array.active_status));
        spinnerActiveStatus.setAdapter(activeStatus);
        initiaieGoogleApiClient();
        editFirstName.setFilters(new InputFilter[]{editTextRestrictLength(), editTextRestrictSmileys(), editTextRestrictSpecialCharacters()});
        editLastName.setFilters(new InputFilter[]{editTextRestrictLength(), editTextRestrictSmileys(), editTextRestrictSpecialCharacters()});
        floatingActionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab_request_blood);
        floatingActionButton.setVisibility(View.GONE);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeActivity.push(new RequestBloodFragment(), getString(R.string.Request_Blood));
            }
        });
        Bundle bundle = getArguments();
        if (bundle != null) {
            memberId = bundle.getInt("memberID");
            isFromViewInfo = bundle.getBoolean("isFromViewInfo");
            isLogged = bundle.getBoolean("isLogged");
        }
        if (isFromViewInfo) {
            //for update
            if (isLogged) {
                setViewAndChildrenEnabled(view, false);
                buttonAdd.setVisibility(View.GONE);
            } else {
                buttonAdd.setText(getActivity().getString(R.string.update));
                buttonAdd.setVisibility(View.VISIBLE);
            }
            getMemberInfo();
//            enableView(false);
//            setHasOptionsMenu(true);
        } else {
//            for create
            setHasOptionsMenu(false);
//            enableView(true);
//            setViewAndChildrenEnabled(view, true);
            buttonAdd.setText(getActivity().getString(R.string.Add_Member));
            switchActive.setChecked(true);
        }

//        editAge.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus) {
//                    if (!editAge.getText().toString().isEmpty()) {
//                        if (Integer.parseInt(editAge.getText().toString()) >= 18 && Integer.parseInt(editAge.getText().toString()) <= 60) {
//                            textInputAge.setErrorEnabled(false);
//                        } else {
//                            textInputAge.setError(getActivity().getString(R.string.min_age));
//                        }
//                    }
//                }
//            }
//        });
//
//        editEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus) {
//                    if (!editEmail.getText().toString().isEmpty()) {
//                        if (!Patterns.EMAIL_ADDRESS.matcher(editEmail.getText().toString()).matches()) {
//                            textInputEmail.setError(getActivity().getString(R.string.enter_valid_email_add));
//                        } else {
//                            textInputEmail.setErrorEnabled(false);
//                        }
//                    }
//                }
//            }
//        });

        spinnerRelationShip.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (((TextView) adapterView.getChildAt(0)).getText().equals("Select Your Relationship")) {
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.hint_grey));
                }
//                addMemberValidation();
                realtionShip = adapterView.getSelectedItem().toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerBloodGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (((TextView) adapterView.getChildAt(0)).getText().equals("Select Blood Group")) {
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.hint_grey));
                }
//                addMemberValidation();
                bloodGroup = adapterView.getSelectedItem().toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        switchGender.setOnSwitchListener(new SwitchMultiButton.OnSwitchListener() {
            @Override
            public void onSwitch(int position, String tabText) {
                gender = position;
            }
        });


        checkAuthPhonenumber.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    authenticateUser = 1;
                else
                    authenticateUser = 0;

            }
        });
        radioButtonDonor.setChecked(true);
        consActiveStatus.setVisibility(View.VISIBLE);
        radioDonorRule.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
//                    Donor-0
//                    NonDonor-1
                    case R.id.radio_button_donor:
                        radiocheckedId = 0;
                        consActiveStatus.setVisibility(View.VISIBLE);
//                        switchActive.setEnabled(true);
//                        switchActive.setChecked(true);
//                        checkAuthPhonenumber.setEnabled(true);
                        break;
                    case R.id.radio_button_non_donor:
                        radiocheckedId = 1;
                        consActiveStatus.setVisibility(View.GONE);
//                        switchActive.setEnabled(false);
//                        switchActive.setChecked(false);
//                        checkAuthPhonenumber.setEnabled(false);
//                        checkAuthPhonenumber.setChecked(false);
                        break;
                }
            }
        });

        spinnerActiveStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //                1-Active
//                0-InActive
                if (position == 0)
                    isActiveChecked = 1;
                else
                    isActiveChecked = 0;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//        checkEnable();
//        switchActive.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//                    isActiveChecked = 1;
////                    checkDontDisturb.setEnabled(true);
//                } else {
//                    isActiveChecked = 0;
////                    checkDontDisturb.setEnabled(false);
////                    checkDontDisturb.setChecked(false);
//                }
//            }
//        });
//        editFirstName.addTextChangedListener(textWatcher);
//        editLastName.addTextChangedListener(textWatcher);
//        editAge.addTextChangedListener(textWatcher);
//        editEmail.addTextChangedListener(textWatcher);
//        editPhoneNumber.addTextChangedListener(textWatcher);
//        editDob.addTextChangedListener(textWatcher);
//        editAddressHome.addTextChangedListener(textWatcher);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.edit_dob, R.id.edit_address_home, R.id.button_add, R.id.image_profile_pic
            , R.id.edit_address_office, R.id.edit_address_freq, R.id.text_edit_photo, R.id.edit_last_donated_date})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.edit_dob:
                dateSelected = 0;
                datePickerDialog();
                break;
            case R.id.edit_last_donated_date:
                dateSelected = 1;
                datePickerDialog();
                break;
            case R.id.edit_address_home:
                if (!homeActivity.hasPermissionGranted()) {
                    homeActivity.requestPermission();
                } else {
                    pickYourPlace();
                    editLocation = 1;
                }
                break;
            case R.id.image_profile_pic:
                getProfilePicture();
                break;
            case R.id.text_edit_photo:
                getProfilePicture();
                break;
            case R.id.button_add:
                if (isMemberDetailNotNull()) {
                    AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(getActivity());
                    if (isFromViewInfo)
                        alertDialog1.setMessage(getString(R.string.alert_update));
                    else
                        alertDialog1.setMessage(getString(R.string.alert_add_member) + " " + editFirstName.getText().toString() + " " + "?");
                    alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            showProgress();
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    addMember();
                                }
                            }).start();
                        }
                    });
                    alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    alertDialog1.show();
                } else {
//                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                    alertDialog.setMessage(getString(R.string.alert_mes_profile));
//                    alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            dialogInterface.dismiss();
//                            hideProgress();
//                        }
//                    });
//                    alertDialog.show();
                }

                break;
            case R.id.edit_address_office:
                if (!homeActivity.hasPermissionGranted()) {
                    homeActivity.requestPermission();
                } else {
                    pickYourPlace();
                    editLocation = 2;
                }
                break;
            case R.id.edit_address_freq:
                if (!homeActivity.hasPermissionGranted()) {
                    homeActivity.requestPermission();
                } else {
                    pickYourPlace();
                    editLocation = 3;
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
        if (requestCode == PLACE_PICKER_REQUEST
                && resultCode == Activity.RESULT_OK) {

            final Place place = PlacePicker.getPlace(getActivity(), data);
            final String name = place.getName().toString();
            final String address = place.getAddress().toString();
            String attributions = (String) place.getAttributions();
            if (attributions == null) {
                attributions = "";
            }
            switch (editLocation) {
                case 1:
                    nearHomeLat = place.getLatLng().latitude;
                    nearHomeLng = place.getLatLng().longitude;
                    if (nearHomeLat != 0.0 && nearHomeLng != 0.0) {
                        if (!name.contains("9"))
                            editAddressHome.setText(name + "," + address);
                        else
                            editAddressHome.setText(address);
                    } else {
                        editAddressHome.setText("");
                    }
                    break;
                case 2:
                    nearOfficeLat = place.getLatLng().latitude;
                    nearOfficeLng = place.getLatLng().longitude;
                    if (nearOfficeLat != 0 && nearOfficeLng != 0) {
                        if (!name.contains("9"))
                            editAddressOffice.setText(name + "," + address);
                        else
                            editAddressOffice.setText(address);
                    } else {
                        editAddressOffice.setText("");
                    }
                    break;
                case 3:
                    nearFreqLat = place.getLatLng().latitude;
                    nearFreqLng = place.getLatLng().longitude;
                    if (nearFreqLat != 0 && nearFreqLng != 0) {
                        if (!name.contains("9"))
                            editAddressFreq.setText(name + "," + address);
                        else
                            editAddressFreq.setText(address);
                    } else {

                        editAddressFreq.setText("");
                    }
                    break;
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        super.onCreateOptionsMenu(menu, inflater);
//        inflater.inflate(R.menu.save, menu);
//        edit = menu.findItem(R.id.menu_bar_edit);
//        edit.setVisible(true);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.menu_bar_edit:
////                setViewAndChildrenEnabled(view, true);
////                enableView(true);
//                edit.setVisible(false);
//                buttonAdd.setEnabled(true);
//                buttonAdd.setBackgroundColor(getResources().getColor(R.color.falcon_red));
//                break;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_REQUEST_CODE_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, REQUEST_CAMERA);
            } else {
                // Your app will not have this permission. Turn off all functions
                // that require this permission or it will force close like your
                // original question
            }
        } else if (requestCode == MY_REQUEST_CODE_GALLERY) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);//
                startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
            } else {

            }
        }
    }

    private void getProfilePicture() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
        builder.setTitle("Add photo");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA},
                                    MY_REQUEST_CODE_CAMERA);
                        } else {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, REQUEST_CAMERA);
                        }
                    } else {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, REQUEST_CAMERA);
                    }

                } else if (options[item].equals("Choose from Gallery")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                    MY_REQUEST_CODE_GALLERY);
                        } else {
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);//
                            startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
                        }
                    } else {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);//
                        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
                    }

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        imageProfilePic.setImageBitmap(bm);
//        uploadUserImage(data.getData());
        profilePicture = getBase64Image(bm);
        uri = data.getData();
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(getActivity().getExternalFilesDir(null),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        imageProfilePic.setImageBitmap(thumbnail);
//        uploadUserImage(Uri.fromFile(destination));
        profilePicture = getBase64Image(thumbnail);
        uri = Uri.fromFile(destination);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

        calendardate = Calendar.getInstance();
        calendardate.set(Calendar.MONTH, monthOfYear);
        calendardate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        calendardate.set(Calendar.YEAR, year);
        simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        dob = simpleDateFormat.format(calendardate.getTime());
        if (dateSelected == 0) {
//            Date of Birth
            editDob.setText(dob);
            editAge.setText(getAge(year, monthOfYear, dayOfMonth));
        } else {
//            Last Donated Date
            editLastDonatedDate.setText(dob);
        }


    }


    private void datePickerDialog() {
        currentD = Calendar.getInstance();
        Calendar maxDate = Calendar.getInstance();
        Calendar minDate = Calendar.getInstance();
        DatePickerDialog datePickerDialog =
                DatePickerDialog.newInstance(this, currentD.get(Calendar.YEAR), currentD.get(Calendar.MONTH),
                        currentD.get(Calendar.DAY_OF_MONTH));
        if (dateSelected == 0) {
//            for date of birth
            maxDate.add(Calendar.YEAR, -18);
            datePickerDialog.setMaxDate(maxDate);
            minDate.add(Calendar.YEAR, -60);
            datePickerDialog.setMinDate(minDate);
        } else {
//            for last donated date
            datePickerDialog.setMaxDate(Calendar.getInstance());
        }

        datePickerDialog.setAccentColor(getResources().getColor(R.color.falcon_red));
        datePickerDialog.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private void initiaieGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity()).addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void startLocationUpdates() {

        if (getActivity() != null && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

//        mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//        if (mCurrentLocation != null) {
//            mLat = mCurrentLocation.getLatitude();
//            mLng = mCurrentLocation.getLongitude();
//        }

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        mFusedLocationClient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    mLat = location.getLatitude();
                    mLng = location.getLongitude();
                }
            }
        });

        double radiusDegrees = 0.001;
        LatLng center = new LatLng(mLat, mLng);
        LatLng northEast = new LatLng(center.latitude + radiusDegrees, center.longitude + radiusDegrees);
        LatLng southWest = new LatLng(center.latitude - radiusDegrees, center.longitude - radiusDegrees);
        bounds = LatLngBounds.builder()
                .include(northEast)
                .include(southWest)
                .build();
    }

    private void pickYourPlace() {
        try {
            if (bounds != null) {
                PlacePicker.IntentBuilder intentBuilder =
                        new PlacePicker.IntentBuilder();
                intentBuilder.setLatLngBounds(bounds);
                Intent intent = intentBuilder.build(getActivity());
                startActivityForResult(intent, PLACE_PICKER_REQUEST);
            }

        } catch (GooglePlayServicesRepairableException
                | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    private String convertToBitMap() {
        Bitmap bm = ((BitmapDrawable) imageProfilePic.getDrawable()).getBitmap();
        proFilePictureFromImageview = getBase64Image(bm);
        return proFilePictureFromImageview;
    }

    private void addMember() {
        AddMember addMember = new AddMember();
        if (profilePicture == null) {
            addMember.setProfilePicture(convertToBitMap());
        } else {
            addMember.setProfilePicture(profilePicture);
        }
        addMember.setUser_id(sharedPreferences.getInt(FalconConstants.USEDID, 0));
        addMember.setFirstName(editFirstName.getText().toString());
        addMember.setLastName(editLastName.getText().toString());
        addMember.setBloodGroupID(bloodGroup);
        addMember.setRelationshipID(realtionShip);
        addMember.setAge(Integer.parseInt(editAge.getText().toString()));
        addMember.setEmailID(editEmail.getText().toString());
        addMember.setPhoneNumber(editPhoneNumber.getText().toString());
        addMember.setActiveStatus(isActiveChecked);
        Date dob = null, lastDonatedDate = null;
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd-MMM-yyyy");
        try {
            dob = simpleDateFormat1.parse(editDob.getText().toString());
            lastDonatedDate = simpleDateFormat1.parse(editLastDonatedDate.getText().toString());
            simpleDateFormat.applyPattern("yyyy-MM-dd");
            addMember.setDOB(simpleDateFormat.format(dob));
            addMember.setLastDonationDate(simpleDateFormat.format(lastDonatedDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        addMember.setGender(gender + 1);
        addMember.setNearHospitalLocationHome(editAddressHome.getText().toString());
        addMember.setNearHospitalLocationOffice(editAddressOffice.getText().toString());
        addMember.setNearHospitalLocationFre(editAddressFreq.getText().toString());

        if (nearHomeLat == 0.0 && nearHomeLng == 0.0) {
            getLatLngFromAddress(getActivity(), editAddressHome.getText().toString(), 1);
            addMember.setNearHospitalLocationHomeLat(String.valueOf(nearHomeLat1));
            addMember.setNearHospitalLocationHomeLng(String.valueOf(nearHomeLng1));
        } else {
            addMember.setNearHospitalLocationHomeLat(String.valueOf(nearHomeLat));
            addMember.setNearHospitalLocationHomeLng(String.valueOf(nearHomeLng));
        }
        if (nearOfficeLat == 0.0 && nearOfficeLng == 0.0) {
            getLatLngFromAddress(getActivity(), editAddressOffice.getText().toString(), 2);
            addMember.setNearHospitalLocationOfficeLng(String.valueOf(nearOfficeLng1));
            addMember.setNearHospitalLocationOfficeLat(String.valueOf(nearOfficeLat1));
        } else {
            addMember.setNearHospitalLocationOfficeLng(String.valueOf(nearOfficeLng));
            addMember.setNearHospitalLocationOfficeLat(String.valueOf(nearOfficeLat));
        }
        if (nearFreqLat == 0.0 && nearFreqLng == 0.0) {
            getLatLngFromAddress(getActivity(), editAddressFreq.getText().toString(), 3);
            addMember.setNearHospitalLocationFreLng(String.valueOf(nearFreqLng1));
            addMember.setNearHospitalLocationFreLat(String.valueOf(nearFreqLat1));
        } else {
            addMember.setNearHospitalLocationFreLng(String.valueOf(nearFreqLng));
            addMember.setNearHospitalLocationFreLat(String.valueOf(nearFreqLat));
        }
//        Donor-0
//                    NonDonor-1

        addMember.setMember_id(memberId);
        addMember.setUserType(radiocheckedId);
        addMember.setRefererID(authenticateUser);

        if (Utils.isNetworkAvailable()) {
            showProgress();

            if (!isFromViewInfo) {
                falconAPI.addMember(addMember).enqueue(new Callback<AddMemberResponse>() {
                    @Override
                    public void onResponse(Call<AddMemberResponse> call, Response<AddMemberResponse> response) {
                        hideProgress();
                        addMemberResponse = response.body();
                        if (isAdded()) {
                            if (response.body() != null && response.isSuccessful() && addMemberResponse.getStatusCode() == 200) {
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                                alertDialog.setMessage(getString(R.string.successfully_added));
                                alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        homeActivity.push(new DashBoardFragment());
                                    }
                                });
                                alertDialog.show();
                            } else {
                                Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<AddMemberResponse> call, Throwable t) {
                        if (isAdded()) {
                            hideProgress();
                            Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                        }

                    }
                });
            } else {
                falconAPI.memberEdit(addMember).enqueue(new Callback<UpdateMemberResponse>() {
                    @Override
                    public void onResponse(Call<UpdateMemberResponse> call, Response<UpdateMemberResponse> response) {
                        hideProgress();
                        updateMemberResponse = response.body();
                        if (isAdded()) {
                            if (response.body() != null && response.isSuccessful() && updateMemberResponse.getStatusCode() == 200) {
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                                alertDialog.setMessage(getString(R.string.successfully_update));
                                alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        getActivity().onBackPressed();
                                    }
                                });
                                alertDialog.show();
                            } else {
                                Toast.makeText(getActivity(), getString(R.string.register_error_message), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<UpdateMemberResponse> call, Throwable t) {
                        if (isAdded()) {
                            hideProgress();
                            Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                        }

                    }
                });
            }
        } else {
            userError.message = getString(R.string.network_error_message);
            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }
    }

    public LatLng getLatLngFromAddress(Context context, String strAddress, int number) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            if (address.size() > 0) {
                Address location = address.get(0);
                switch (number) {
                    case 1:
                        nearHomeLat1 = location.getLatitude();
                        nearHomeLng1 = location.getLongitude();
                        break;
                    case 2:
                        nearOfficeLat1 = location.getLatitude();
                        nearOfficeLng1 = location.getLongitude();
                        break;
                    case 3:
                        nearFreqLat1 = location.getLatitude();
                        nearFreqLng1 = location.getLongitude();
                        break;
                }


                p1 = new LatLng(location.getLatitude(), location.getLongitude());
            }


        } catch (IOException ex) {

            ex.printStackTrace();
        }

        return p1;
    }
//    private void addMemberValidation() {
//        if (!isFromViewInfo) {
//            if (!editFirstName.getText().toString().isEmpty() && !editLastName.getText().toString().isEmpty()
//                    && !bloodGroup.contains("Select Blood Group") && !editAge.getText().toString().isEmpty()
//                    && !editEmail.getText().toString().isEmpty() && !realtionShip.contains("Select Your Relationship")
//                    && !editDob.getText().toString().isEmpty() && !editAddressHome.getText().toString().isEmpty()
//                    && !editPhoneNumber.getText().toString().isEmpty()
//                    ) {
//                buttonAdd.setEnabled(true);
//                buttonAdd.setBackgroundColor(getResources().getColor(R.color.falcon_red));
//            } else {
//                buttonAdd.setEnabled(false);
//                buttonAdd.setBackgroundColor(getResources().getColor(R.color.light_grey));
//            }
//        }
//    }

//    TextWatcher textWatcher = new TextWatcher() {
//        @Override
//        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//        }
//
//        @Override
//        public void onTextChanged(CharSequence s, int start, int before, int count) {
//            addMemberValidation();
//        }
//
//        @Override
//        public void afterTextChanged(Editable s) {
//
//        }
//    };

    private void getMemberInfo() {
        MemberDelete memberDelete = new MemberDelete();
        memberDelete.setMember_id(memberId);
        if (Utils.isNetworkAvailable()) {
            showProgress();
            falconAPI.memberInfo(memberDelete).enqueue(new Callback<GetAddMemberResponse>() {
                @Override
                public void onResponse(Call<GetAddMemberResponse> call, Response<GetAddMemberResponse> response) {
                    hideProgress();
                    getAddMemberResponse = response.body();
                    if (isAdded()) {
                        if (response.body() != null && response.isSuccessful() && getAddMemberResponse.getStatusCode() == 200) {
                            if (getAddMemberResponse.getUserInfo().getProfilePicture() != null) {
                                picasoImageLoader(imageProfilePic, getAddMemberResponse.getUserInfo().getProfilePicture(), getActivity());
                            } else {
                                imageProfilePic.setImageResource(R.mipmap.img_sample_pro_pic);
                            }
                            editFirstName.setText(getAddMemberResponse.getUserInfo().getFirstName());
                            editLastName.setText(getAddMemberResponse.getUserInfo().getLastName());
                            selectBloodSpinnerItemByValue(spinnerBloodGroup, getAddMemberResponse.getUserInfo().getBloodGroupID());
                            selectRelationSpinnerItemByValue(spinnerRelationShip, getAddMemberResponse.getMemberInfo().getRelationshipID());
                            editAge.setText("" + getAddMemberResponse.getUserInfo().getAge());
                            editEmail.setText(getAddMemberResponse.getUserInfo().getEmailID());
                            editPhoneNumber.setText(getAddMemberResponse.getUserInfo().getPhoneNumber());
                            Date dob = null, lastDonatedDate = null;
                            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
                            try {
                                dob = simpleDateFormat1.parse(getAddMemberResponse.getUserInfo().getDOB());
                                lastDonatedDate = simpleDateFormat1.parse(getAddMemberResponse.getUserInfo().getLastDonationDate());
                                simpleDateFormat.applyPattern("dd-MMM-yyyy");
                                editDob.setText(simpleDateFormat.format(dob));
                                editLastDonatedDate.setText(simpleDateFormat.format(lastDonatedDate));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            //                        Male-1 Female-2
                            if (getAddMemberResponse.getUserInfo().getGender() == 2) {
                                switchGender.setSelectedTab(1);
                            } else {
                                switchGender.setSelectedTab(0);
                            }
                            editAddressHome.setText(getAddMemberResponse.getUserInfo().getNearHospitalLocationHome());
                            editAddressFreq.setText(getAddMemberResponse.getUserInfo().getNearHospitalLocationFre());
                            editAddressOffice.setText(getAddMemberResponse.getUserInfo().getNearHospitalLocationOffice());
                            if (getAddMemberResponse.getUserInfo().getActiveStatus() == 1)
//                                switchActive.setChecked(true);
                                spinnerActiveStatus.setSelection(0);
                            else
                                spinnerActiveStatus.setSelection(1);

//                                switchActive.setChecked(false);

                            if (getAddMemberResponse.getUserInfo().getRefererID() == sharedPreferences.getInt(FalconConstants.USEDID, 0))
                                checkAuthPhonenumber.setChecked(true);
                            else
                                checkAuthPhonenumber.setChecked(false);
//                            Donor-0
//                    NonDonor-1
                            if (getAddMemberResponse.getUserInfo().getUserType() == 1) {

                                radioButtonNonDonor.setChecked(true);
                                consActiveStatus.setVisibility(View.VISIBLE);
                            } else {

                                radioButtonDonor.setChecked(true);
                                consActiveStatus.setVisibility(View.GONE);
                            }

                        } else {
                            Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<GetAddMemberResponse> call, Throwable t) {
                    if (isAdded()) {
                        hideProgress();
                        Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }
                }
            });

        } else {
            userError.message = getString(R.string.network_error_message);
            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }
    }

    public static void selectBloodSpinnerItemByValue(Spinner spnr, String bloodGroup) {
        ArrayAdapter adapter = (ArrayAdapter) spnr.getAdapter();
        for (int position = 0; position < adapter.getCount(); position++) {
            if (adapter.getItem(position).equals(bloodGroup)) {
                spnr.setSelection(position);
                return;
            }
        }
    }

    public static void selectRelationSpinnerItemByValue(Spinner spnr, String relation) {
        ArrayAdapter adapter = (ArrayAdapter) spnr.getAdapter();
        for (int position = 0; position < adapter.getCount(); position++) {
            if (adapter.getItem(position).equals(relation)) {
                spnr.setSelection(position);
                return;
            }
        }
    }

    public boolean isMemberDetailNotNull() {
        showErrorFirstName();
        showErrorLastName();
        showErrorBloodGroup();
        showErrorAge();
//        showErrorEmail();
        showErrorDob();
//        showErrorPhoneNumber();
        showErrorLocation();
        showErrorRelative();
        showErrorGender();

        if (showErrorFirstName1() && showErrorLastName1() && showErrorBloodGroup1() && showErrorAge1()
                && showErrorDob1() && showErrorLocation1() && showErrorRelative1() && showErrorGender1()) {
            return true;
        } else {
            return false;
        }
    }

    private boolean showErrorFirstName() {

        if (editFirstName.getText().toString().isEmpty()) {
            textInputFirstName.setError(getString(R.string.req_first_name));
            return false;
        } else {
            textInputFirstName.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorLocation() {

        if (editAddressHome.getText().toString().isEmpty()) {
            textInputAddHome.setError(getString(R.string.req_Location));
            return false;
        } else {
            textInputAddHome.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorLocation1() {

        if (editAddressHome.getText().toString().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorPhoneNumber() {
        if (editPhoneNumber.getText().toString().isEmpty()) {
            textInputPhoneNumber.setError(getString(R.string.req_pho));
            return false;
        } else if (editPhoneNumber.getText().toString().length() != 10) {
            textInputPhoneNumber.setError(getString(R.string.req_phone_number));
            return false;
        } else {
            textInputPhoneNumber.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorPhoneNumber1() {
        if (editPhoneNumber.getText().toString().isEmpty()) {
            return false;
        } else if (editPhoneNumber.getText().toString().length() > 10) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorFirstName1() {

        if (editFirstName.getText().toString().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorLastName() {
        if (editLastName.getText().toString().isEmpty()) {
            textInputLastName.setError(getString(R.string.req_Last_name));
            return false;
        } else {
            textInputLastName.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorLastName1() {
        if (editLastName.getText().toString().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorAge() {
        if (editAge.getText().toString().isEmpty()) {
            textInputAge.setError(getString(R.string.req_Age));
            return false;
        } else if (Integer.parseInt(editAge.getText().toString()) < 18) {
            textInputAge.setError(getActivity().getString(R.string.min_age));
            return false;
        } else if (Integer.parseInt(editAge.getText().toString()) > 60) {
            textInputAge.setError(getActivity().getString(R.string.min_age));
            return false;
        } else {
            textInputAge.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorAge1() {
        if (editAge.getText().toString().isEmpty()) {
            return false;
        } else if (Integer.parseInt(editAge.getText().toString()) < 18) {
            return false;
        } else if (Integer.parseInt(editAge.getText().toString()) > 60) {
            return false;
        } else {
            return true;
        }
    }


    private boolean showErrorEmail() {
        if (editEmail.getText().toString().isEmpty()) {
            textInputEmail.setError(getString(R.string.req_Email));
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(editEmail.getText().toString()).matches()) {
            textInputEmail.setError(getActivity().getString(R.string.enter_valid_email_add));
            return false;
        } else {
            textInputEmail.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorEmail1() {
        if (editEmail.getText().toString().isEmpty()) {
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(editEmail.getText().toString()).matches()) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorDob() {
        if (editDob.getText().toString().isEmpty()) {
            textInputDob.setError(getString(R.string.req_DOB));
            return false;
        } else {
            textInputDob.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorDob1() {
        if (editDob.getText().toString().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }


    private boolean showErrorBloodGroup() {
        if (bloodGroup.contains("Select Blood Group")) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setMessage(getString(R.string.req_Blood));
            alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            alertDialog.show();
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorRelative() {
        if (realtionShip.contains("Select Your Relationship")) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setMessage(getString(R.string.req_relation));
            alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            alertDialog.show();
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorRelative1() {
        if (realtionShip.contains("Select Your Relationship")) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorBloodGroup1() {
        if (bloodGroup.contains("Select Blood Group")) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorGender1() {

        switch (gender) {
            case 0:
                return true;
            case 1:
                return true;
            case 2:
                return false;
        }
        return false;
    }

    private boolean showErrorGender() {

        switch (gender) {
            case 0:
                return true;
            case 1:
                return true;
            case 2:
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setMessage(getString(R.string.req_gender));
                alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                alertDialog.show();
                return false;
        }

        return false;
    }

    private void enableView(boolean isEnable) {
        imageProfilePic.setEnabled(isEnable);
        radioButtonDonor.setEnabled(isEnable);
        radioButtonNonDonor.setEnabled(isEnable);
        textInputFirstName.setEnabled(isEnable);
        textInputLastName.setEnabled(isEnable);
        spinnerBloodGroup.setEnabled(isEnable);
        spinnerRelationShip.setEnabled(isEnable);
        textInputAge.setEnabled(isEnable);
        textInputEmail.setEnabled(isEnable);
        textInputPhoneNumber.setEnabled(isEnable);
        textInputDob.setEnabled(isEnable);
        switchGender.setEnabled(isEnable);
        textInputAddHome.setEnabled(isEnable);
        textInputAddOffice.setEnabled(isEnable);
        textInputAddFreq.setEnabled(isEnable);
        checkAuthPhonenumber.setEnabled(isEnable);

        if (isFromViewInfo) {
//            NonDonor-1
            if (getAddMemberResponse.getUserInfo().getUserType() == 1) {
                switchActive.setEnabled(false);
//                switchActive.setChecked(false);
//                checkAuthPhonenumber.setChecked(false);
//                checkAuthPhonenumber.setEnabled(false);
            } else {
//                Donor-0
                switchActive.setEnabled(true);
//                switchActive.setChecked(true);
//                checkAuthPhonenumber.setEnabled(true);
            }

        } else {
            switchActive.setEnabled(isEnable);
            switchActive.setChecked(isEnable);
            checkAuthPhonenumber.setEnabled(isEnable);
        }
    }


}
