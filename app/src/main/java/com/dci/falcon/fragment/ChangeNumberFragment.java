package com.dci.falcon.fragment;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.activity.LoginActivity;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.model.UniqueNumber;
import com.dci.falcon.model.UniqueNumberResponse;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomButton;
import com.dci.falcon.view.CustomEditText;
import com.dci.falcon.view.CustomTextView;
import com.dci.falcon.view.CustomTextViewBold;
import com.msg91.sendotp.library.PhoneNumberFormattingTextWatcher;
import com.msg91.sendotp.library.PhoneNumberUtils;
import com.msg91.sendotp.library.internal.Iso2Phone;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

/**
 * Created by vijayaganesh on 11/29/2017.
 */

public class ChangeNumberFragment extends BaseFragment {
    @BindView(R.id.text_title_change_number)
    CustomTextViewBold textTitleChangeNumber;
    @BindView(R.id.edit_old_num)
    CustomEditText editOldNum;
    @BindView(R.id.text_input_old_number)
    TextInputLayout textInputOldNumber;
    @BindView(R.id.edit_new_num)
    CustomEditText editNewNum;
    @BindView(R.id.text_input_new_number)
    TextInputLayout textInputNewNumber;
    @BindView(R.id.button_change_password)
    CustomButton buttonChangePassword;
    @BindView(R.id.cons_chnage_password)
    ConstraintLayout consChnagePassword;
    Unbinder unbinder;
    LoginActivity loginActivity;

    @BindView(R.id.image_icon_change_number)
    ImageView imageIconChangeNumber;
    @BindView(R.id.text_label_change_number)
    CustomTextView textLabelChangeNumber;
    private TextWatcher mNumberTextWatcher;
    private String mCountryIso;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    UserError userError;
    private FloatingActionButton floatingActionButton;
    private UniqueNumberResponse uniqueNumberResponse;
    @Inject
    FalconAPI falconAPI;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_change_nuber, container, false);
        unbinder = ButterKnife.bind(this, view);
        getActivity().setTitle(getString(R.string.login_change_number));
        loginActivity = (LoginActivity) getActivity();
        FalconApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        userError = new UserError();
//        editOldNum.addTextChangedListener(textWatcher);
//        editNewNum.addTextChangedListener(textWatcher);
        mCountryIso = PhoneNumberUtils.getDefaultCountryIso(getActivity());
//        resetNumberTextWatcher(mCountryIso);
//        mNumberTextWatcher.afterTextChanged(editNewNum.getText());
//        tryAndPrefillPhoneNumber();
        editNewNum.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    if (isChangeNumberNotNull()) {
                        AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(getActivity());
                        alertDialog1.setMessage(getString(R.string.alert_change_number));
                        alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (isChangeNumberNotNull())
                                    isOldNumber();
                            }
                        });
                        alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        alertDialog1.show();
                    }

                }
                return false;
            }
        });
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.button_change_password)
    public void onViewClicked() {
        if (Utils.isNetworkAvailable()) {
            if (isChangeNumberNotNull()) {
                AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(getActivity());
                alertDialog1.setMessage(getString(R.string.alert_change_number));
                alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (isChangeNumberNotNull())
                            isOldNumber();
                    }
                });
                alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                alertDialog1.show();
            }
        } else {
            userError.message = getActivity().getString(R.string.network_error_message);
            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }
    }


    private boolean showErrorOldNumber() {
        if (editOldNum.getText().toString().isEmpty()) {
            textInputOldNumber.setError(getString(R.string.req_pho));
            return false;
        } else if (editOldNum.getText().toString().length() != 10) {
            textInputOldNumber.setError(getString(R.string.req_phone_number));
            return false;
        } else {
            textInputOldNumber.setErrorEnabled(false);
            return true;
        }

    }

    private boolean showErrorOldNumber1() {
        if (editOldNum.getText().toString().isEmpty()) {
            return false;
        } else if (editOldNum.getText().toString().length() != 10) {
            return false;
        } else {
            return true;
        }

    }

    private boolean showErrorNewNumber() {
        if (editNewNum.getText().toString().isEmpty()) {
            textInputNewNumber.setError(getString(R.string.req_pho));
            return false;
        } else if (editNewNum.getText().toString().length() != 10) {
            textInputNewNumber.setError(getString(R.string.req_phone_number));
            return false;
        } else {
            textInputNewNumber.setErrorEnabled(false);
            return true;
        }

    }

    private boolean showErrorNewNumber1() {
        if (editNewNum.getText().toString().isEmpty()) {
            return false;
        } else if (editNewNum.getText().toString().length() != 10) {
            return false;
        } else {
            return true;
        }

    }

    private boolean isChangeNumberNotNull() {
        showErrorNewNumber();
        showErrorOldNumber();
        if (showErrorNewNumber1() && showErrorOldNumber1()) {
            return true;
        } else {
            return false;
        }
    }

//    TextWatcher textWatcher = new TextWatcher() {
//        @Override
//        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//        }
//
//        @Override
//        public void onTextChanged(CharSequence s, int start, int before, int count) {
//            changeNumberValidation();
//        }
//
//        @Override
//        public void afterTextChanged(Editable s) {
//
//        }
//    };
//
//    private void changeNumberValidation() {
//        if (!editNewNum.getText().toString().isEmpty() && !editOldNum.getText().toString().isEmpty()) {
//            buttonChangePassword.setEnabled(true);
//            buttonChangePassword.setBackgroundColor(getResources().getColor(R.color.falcon_red));
//        } else {
//            buttonChangePassword.setEnabled(false);
//            buttonChangePassword.setBackgroundColor(getResources().getColor(R.color.light_grey));
//        }
//    }


    private void setButtonsEnabled(boolean enabled) {
        buttonChangePassword.setEnabled(enabled);
    }


    private void resetNumberTextWatcher(String countryIso) {

        if (mNumberTextWatcher != null) {
            editNewNum.removeTextChangedListener(mNumberTextWatcher);
        }

        mNumberTextWatcher = new PhoneNumberFormattingTextWatcher(countryIso) {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                super.onTextChanged(s, start, before, count);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                super.beforeTextChanged(s, start, count, after);
            }

            @Override
            public synchronized void afterTextChanged(Editable s) {
                super.afterTextChanged(s);
                if (isPossiblePhoneNumber()) {
                    setButtonsEnabled(true);
                    editNewNum.setTextColor(Color.BLACK);
                } else {
                    setButtonsEnabled(false);
                    editNewNum.setTextColor(Color.RED);
                }
            }
        };

        editNewNum.addTextChangedListener(mNumberTextWatcher);
    }

    private boolean isPossiblePhoneNumber() {
        return PhoneNumberUtils.isPossibleNumber(editNewNum.getText().toString(), mCountryIso);
    }

    private String getE164Number() {
        return editNewNum.getText().toString().replaceAll("\\D", "").trim();
    }

    private void tryAndPrefillPhoneNumber() {
        if (getActivity().checkCallingOrSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            TelephonyManager manager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            editNewNum.setText(manager.getLine1Number());
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, 0);
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            tryAndPrefillPhoneNumber();
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permissions[0])) {
                Toast.makeText(getActivity(), "This application needs permission to read your phone number to automatically "
                        + "pre-fill it", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void isNewNumber() {
        final UniqueNumber uniqueNumber = new UniqueNumber();
        uniqueNumber.setPhoneNumber(editNewNum.getText().toString());
        if (Utils.isNetworkAvailable()) {
            showProgress();
            falconAPI.checkUniqueNumber(uniqueNumber).enqueue(new Callback<UniqueNumberResponse>() {
                @Override
                public void onResponse(Call<UniqueNumberResponse> call, Response<UniqueNumberResponse> response) {
                    hideProgress();
                    uniqueNumberResponse = response.body();
                    if (isAdded()) {
                        if (response.body() != null && response.isSuccessful()) {
                            if (uniqueNumberResponse.getStatusCode() == 500) {
                                //mobile number not exist\\
                                SequrityQuestionFragment sequrityQuestionFragment = new SequrityQuestionFragment();
                                Bundle bundle = new Bundle();
                                bundle.putBoolean("forChangeNumber", true);
                                bundle.putString("newNumber", editNewNum.getText().toString());
                                bundle.putString("oldNumber", editOldNum.getText().toString());
                                sequrityQuestionFragment.setArguments(bundle);
                                loginActivity.push(sequrityQuestionFragment);


                            } else {
                                //mobile number already exist
                                Toast.makeText(getActivity(), getString(R.string.new_number_already_there), Toast.LENGTH_SHORT).show();

                            }
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<UniqueNumberResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            userError.message = getActivity().getString(R.string.network_error_message);
            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }
    }

    private void isOldNumber() {
        final UniqueNumber uniqueNumber = new UniqueNumber();
        uniqueNumber.setPhoneNumber(editOldNum.getText().toString());
        if (Utils.isNetworkAvailable()) {
            showProgress();
            falconAPI.checkUniqueNumber(uniqueNumber).enqueue(new Callback<UniqueNumberResponse>() {
                @Override
                public void onResponse(Call<UniqueNumberResponse> call, Response<UniqueNumberResponse> response) {
                    hideProgress();
                    uniqueNumberResponse = response.body();
                    if (isAdded()) {
                        if (response.body() != null && response.isSuccessful()) {
                            if (uniqueNumberResponse.getStatusCode() == 500) {
                                //mobile number not exist
                                Toast.makeText(getActivity(), getString(R.string.old_number_not_there), Toast.LENGTH_SHORT).show();
                            }else if (uniqueNumberResponse.getStatusCode() == 404){
                                AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(getActivity());
                                alertDialog1.setView(R.layout.inactive_user_alert_view);
                                alertDialog1.setCancelable(false);
                                alertDialog1.show();
                            } else {
                                //mobile number already exist
                                isNewNumber();

                            }
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<UniqueNumberResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            userError.message = getActivity().getString(R.string.network_error_message);
            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }
    }

}
