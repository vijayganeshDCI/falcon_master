package com.dci.falcon.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.widget.ArrayAdapter;

import com.dci.falcon.R;
import com.dci.falcon.app.FalconApplication;


public class AlertDialogFragment extends DialogFragment implements DialogInterface.OnClickListener {

    public static final String CLOSE_AFTER_DISMISS = "closeAfterDismiss";

    public static final String ALERT_DIALOG_TITLE = "title";
    public static final String ALERT_DIALOG_MESSAGE = "message";
    public static final String ALERT_DIALOG_POSITIVE = "positive";
    public static final String ALERT_DIALOG_NEGATIVE = "negative";
    public static final String ALERT_DIALOG_NEUTRAL = "neutral";
    public static final String ALERT_DIALOG_RES_ID = "resId";
    public static final String ALERT_TARGET_ACTIVITY = "targetActivity";

    public static AlertDialogFragment newOkDialog(CharSequence message) {
        return newDialog(null, message, getResString(R.string.Alert_Ok), null);
    }

    public static AlertDialogFragment newOkDialog(CharSequence title, CharSequence message) {
        return newDialog(title, message, getResString(R.string.Alert_Ok), null);
    }

    public static AlertDialogFragment newDialog(CharSequence title, CharSequence message, CharSequence ok, CharSequence negative) {
        return newDialog(title, message, ok, negative, -1);
    }

    public static AlertDialogFragment newDialog(CharSequence title, CharSequence message, CharSequence ok, CharSequence negative, int resId) {
        Bundle args = new Bundle();
        args.putCharSequence(ALERT_DIALOG_TITLE, title);
        args.putCharSequence(ALERT_DIALOG_MESSAGE, message);
        args.putCharSequence(ALERT_DIALOG_POSITIVE, ok);
        args.putCharSequence(ALERT_DIALOG_NEGATIVE, negative);
        args.putInt(ALERT_DIALOG_RES_ID, resId);
        AlertDialogFragment fragment = new AlertDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private DialogInterface.OnClickListener mListener;

    public void setListener(DialogInterface.OnClickListener listener) {
        mListener = listener;
    }

    private boolean mCloseAfterDismiss = false;

    public AlertDialogFragment withCloseAfterDismiss(boolean closeAfterDismiss) {
        mCloseAfterDismiss = closeAfterDismiss;
        return this;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();
        if (args == null) {
            return super.onCreateDialog(savedInstanceState);
        }
        if (savedInstanceState != null) {
            mCloseAfterDismiss = savedInstanceState.getBoolean(CLOSE_AFTER_DISMISS, false);
        }

        Activity activity = getActivity();
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, getTheme());
        CharSequence title = args.getCharSequence(ALERT_DIALOG_TITLE);
        if (!TextUtils.isEmpty(title)) {
            builder.setTitle(title);
        }
        builder.setMessage(args.getCharSequence(ALERT_DIALOG_MESSAGE));

        CharSequence positive = args.getCharSequence(ALERT_DIALOG_POSITIVE);
        if (!TextUtils.isEmpty(positive)) {
            builder.setPositiveButton(positive, this);
        }
        CharSequence negative = args.getCharSequence(ALERT_DIALOG_NEGATIVE);
        if (!TextUtils.isEmpty(negative)) {
            builder.setNegativeButton(negative, this);
        }
        int resId = args.getInt(ALERT_DIALOG_RES_ID, -1);
        if (resId != -1) {
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(activity,
                    resId, android.R.layout.simple_list_item_1);
            builder.setAdapter(adapter, this);
        }

        return builder.create();
    }

    private static CharSequence getResString(int resId) {
        return getAppContext().getString(resId);
    }

    private static Context getAppContext() {
        return FalconApplication.getContext();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        dismiss();
        Bundle args = getArguments();
        if (which == DialogInterface.BUTTON_POSITIVE && args != null
                && args.containsKey(ALERT_TARGET_ACTIVITY)) {
            Class<? extends Activity> clazz = (Class<? extends Activity>) args.getSerializable(ALERT_TARGET_ACTIVITY);
            Activity activity = getActivity();
            if (activity != null && clazz != null) {
                activity.startActivity(new Intent(activity, clazz));
            }
        }
        if (mListener != null) {
            try {
                mListener.onClick(dialog, which);
            }catch (IllegalStateException e){

            }
        }
        if (mCloseAfterDismiss) {
            Activity activity = getActivity();
            if (activity != null) {
                Intent intent = NavUtils.getParentActivityIntent(activity);
                if (intent != null) {
                    NavUtils.navigateUpTo(activity, intent);
                } else {
                    activity.finish();
                }
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(CLOSE_AFTER_DISMISS, mCloseAfterDismiss);
    }
}
