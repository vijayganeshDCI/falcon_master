package com.dci.falcon.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.dci.falcon.R;
import com.dci.falcon.activity.ProfileActivity;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.model.DoNotDisturbList;
import com.dci.falcon.model.DonNotDisturbItem;
import com.dci.falcon.model.DonationDetails;
import com.dci.falcon.utills.DonationDetailListener;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.view.CustomEditText;
import com.dci.falcon.view.CustomTextView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by vijayaganesh on 11/1/2017.
 */

public class DonationFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, DonationDetailListener {
    @BindView(R.id.text_label_active)
    CustomTextView textLabelActive;
    @BindView(R.id.switch_active)
    SwitchCompat switchActive;
    @BindView(R.id.edit_last_donated)
    CustomEditText editLastDonated;
    @BindView(R.id.edit_dont_schedule_donation)
    CustomEditText editScheduleDonation;
    @BindView(R.id.text_input_last_donated)
    TextInputLayout textInputLastDonated;
    @BindView(R.id.text_input_schedule_donation)
    TextInputLayout textInputScheduleDonation;
    @BindView(R.id.text_label_dont_disturb)
    CustomTextView textLabelDontDisturb;
    @BindView(R.id.text_label_scedule_donation)
    CustomTextView textLabelSceduleDonation;
    @BindView(R.id.check_dont_disturb)
    CheckBox checkDontDisturb;
    @BindView(R.id.text_input_dont_disturb_time__from)
    TextInputLayout textInputSelectFromTime;
    @BindView(R.id.edit_dont_disturb_time_from)
    CustomEditText editSelectFromTime;
    @BindView(R.id.text_input_dont_disturb_time_to)
    TextInputLayout textInputSelectToTime;
    @BindView(R.id.edit_dont_disturb_time_to)
    CustomEditText editSelectToTime;
    @BindView(R.id.scroll_donation)
    NestedScrollView scrollDonation;
    Unbinder unbinder;
    @BindView(R.id.text_label_days)
    CustomTextView textLabelDays;
    @BindView(R.id.check_sunday)
    CheckBox checkSunday;
    @BindView(R.id.check_monday)
    CheckBox checkMonday;
    @BindView(R.id.check_tuesday)
    CheckBox checkTuesday;
    @BindView(R.id.check_wednesday)
    CheckBox checkWednesday;
    @BindView(R.id.check_thursday)
    CheckBox checkThursday;
    @BindView(R.id.check_friday)
    CheckBox checkFriday;
    @BindView(R.id.check_saturday)
    CheckBox checkSaturday;
    @BindView(R.id.cons_select_days)
    ConstraintLayout consSelectDays;

    SimpleDateFormat simpleDateFormat;
    private Calendar currentD, calendardate, calendarTime, currentT;
    private String donateDate, donateTime, donateTime1;
    public static int editDateClicked = 0;
    public static int editTimeClicked = 0;
    private int isActiveChecked = 1;
    View donationView;
    List<DonNotDisturbItem> donNotDisturbItemList;
    List<String> selectedDays;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    private Boolean isStarted = false;
    private Boolean isVisible = false;



    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isVisible) {
        }


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {
            isStarted = true;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        donationView = inflater.inflate(R.layout.fragment_donation, container, false);
        ButterKnife.bind(this, donationView);
        FalconApplication.getContext().getComponent().inject(this);
        simpleDateFormat = new SimpleDateFormat();
        selectedDays = new ArrayList<String>();
        editor = sharedPreferences.edit();
        donNotDisturbItemList = new ArrayList<DonNotDisturbItem>();
        switchActive.setVisibility(View.GONE);
        textLabelActive.setVisibility(View.GONE);
        switchActive.setChecked(true);
//        checkEnable();
        switchActive.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    checkEnable();
                    isActiveChecked = 1;
//                    checkDontDisturb.setEnabled(true);
                } else {

                    checkDisable();
                    isActiveChecked = 0;
//                    checkDontDisturb.setEnabled(false);
//                    checkDontDisturb.setChecked(false);
                }
            }
        });

//        checkDontDisturb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//                    checkEnable();
//                } else {
//                    checkDisable();
//                }
//            }
//        });

        checkMonday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selectedDays.add("Mon");
                } else {
                    selectedDays.remove("Mon");
                }

            }
        });
        checkTuesday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selectedDays.add("Tues");
                } else {
                    selectedDays.remove("Tues");
                }
            }
        });
        checkWednesday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selectedDays.add("Wed");
                } else {
                    selectedDays.remove("Wed");
                }
            }
        });
        checkThursday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selectedDays.add("Thurs");
                } else {
                    selectedDays.remove("Thurs");
                }
            }
        });
        checkFriday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selectedDays.add("Fri");
                } else {
                    selectedDays.remove("Fri");
                }
            }
        });
        checkSaturday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selectedDays.add("Sat");
                } else {
                    selectedDays.remove("Sat");
                }
            }
        });
        checkSunday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selectedDays.add("Sun");
                } else {
                    selectedDays.remove("Sun");
                }
            }
        });

        if (!ProfileActivity.enableView) {
            setViewAndChildrenEnabled(donationView, false);
        } else {
            setViewAndChildrenEnabled(donationView, true);
        }
        return donationView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @OnClick({R.id.edit_last_donated, R.id.edit_dont_disturb_time_from, R.id.edit_dont_disturb_time_to, R.id.edit_dont_schedule_donation})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.edit_last_donated:
                editDateClicked = 1;
                datePickerDialog();
                break;
            case R.id.edit_dont_schedule_donation:
                editDateClicked = 2;
                datePickerDialog();
                break;
            case R.id.edit_dont_disturb_time_from:
                editTimeClicked = 1;
                timePickerDialog();
                break;
            case R.id.edit_dont_disturb_time_to:
                editTimeClicked = 2;
                timePickerDialog();
                break;
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        calendardate = Calendar.getInstance();
        calendardate.set(Calendar.MONTH, monthOfYear);
        calendardate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        calendardate.set(Calendar.YEAR, year);
        switch (editDateClicked) {
            case 1:
                simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
                donateDate = simpleDateFormat.format(calendardate.getTime());
                editLastDonated.setText(donateDate);
                break;
            case 2:
                simpleDateFormat = new SimpleDateFormat("dd-MMM");
                donateDate = simpleDateFormat.format(calendardate.getTime());
                editScheduleDonation.setText(donateDate);
                break;
        }

    }


    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        calendarTime = Calendar.getInstance();
        calendarTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendarTime.set(Calendar.MINUTE, minute);
        simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        donateTime = simpleDateFormat.format(calendarTime.getTime());
        switch (editTimeClicked) {
            case 1:
                editSelectFromTime.setText(donateTime);
                break;
            case 2:
                editSelectToTime.setText(donateTime);
            default:
                return;
        }

    }

    private void datePickerDialog() {
        currentD = Calendar.getInstance();
        DatePickerDialog datePickerDialog = null;
        datePickerDialog =
                DatePickerDialog.newInstance(this, currentD.get(Calendar.YEAR), currentD.get(Calendar.MONTH),
                        currentD.get(Calendar.DAY_OF_MONTH));
        if (editDateClicked == 1) {
            datePickerDialog.setMaxDate(Calendar.getInstance());
        }
        datePickerDialog.setAccentColor(getResources().getColor(R.color.falcon_red));
        datePickerDialog.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }

    private void timePickerDialog() {
        currentT = Calendar.getInstance();
        TimePickerDialog timePickerDialog =
                TimePickerDialog.newInstance(this, currentT.get(Calendar.HOUR_OF_DAY), currentT.get(Calendar.MINUTE), currentT.get(Calendar.SECOND), false);
        timePickerDialog.setAccentColor(getResources().getColor(R.color.falcon_red));
        timePickerDialog.show(getActivity().getFragmentManager(), "Timepickerdialog");
    }

    @Override
    public boolean isDonationNotNull() {
//        if (switchActive.isChecked()) {
//            if (!editLastDonated.getText().toString().isEmpty()) {
//                if (isAdded())
//                    textInputLastDonated.setErrorEnabled(false);
//                return true;
//            } else {
//                editor.putInt(FalconConstants.NOTNULL, 2).commit();
//                if (isAdded())
//                    textInputLastDonated.setError(getString(R.string.req_Last));
//                return false;
//            }
//        } else {
//            return true;
//        }
        return false;
    }

    @Override
    public DonationDetails interfaceDonationDetails() {
        DonationDetails donationDetails = new DonationDetails();
//        donationDetails.setActiveStatus(isActiveChecked);
        Date dob = null;
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd-MMM-yyyy");
        try {
            dob = simpleDateFormat1.parse(editLastDonated.getText().toString());
            simpleDateFormat.applyPattern("yyyy-MM-dd");
            donationDetails.setLastDonated(simpleDateFormat.format(dob));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        donationDetails.setDonateAt(editScheduleDonation.getText().toString());
//        if (isActiveChecked == 1) {
//            donationDetails.setLastDonated(editLastDonated.getText().toString());
//            donationDetails.setDonateAt(editScheduleDonation.getText().toString());
//        }
        return donationDetails;
    }


    @Override
    public String donNotDisturbDate() {
        DoNotDisturbList doNotDisturbList = new DoNotDisturbList();

        JSONArray donotDistrubjson = new JSONArray();
        for (int i = 0; i < selectedDays.size(); i++) {

            try {
                JSONObject individulelement = new JSONObject();
                individulelement.put("Day", selectedDays.get(i).toString());
                individulelement.put("StartTime", editSelectFromTime.getText().toString());
                individulelement.put("EndTime", editSelectToTime.getText().toString());
                donotDistrubjson.put(individulelement);
            } catch (Exception e) {

            }


        }
        return donotDistrubjson.toString();

    }

    @Override
    public void getDonationDetail(DonationDetails donationDetails) {
        if (isAdded() && donationDetails != null) {
            if (donationDetails.getActiveStatus() == 1) {
                switchActive.setChecked(true);
            } else if (donationDetails.getActiveStatus() == 0) {
                switchActive.setChecked(false);
            }
            Date scduleDonation = null;
            Date lastDonated = null;
            Date from = null;
            Date to = null;
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
//            SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("hh:mm:ss");
            try {
                lastDonated = simpleDateFormat1.parse(donationDetails.getLastDonated());
                simpleDateFormat.applyPattern("dd-MMM-yyyy");
                editLastDonated.setText(simpleDateFormat.format(lastDonated));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            try {
                scduleDonation = simpleDateFormat1.parse(donationDetails.getDonateAt());
                simpleDateFormat.applyPattern("dd-MMM");
                editScheduleDonation.setText(simpleDateFormat.format(scduleDonation));
            } catch (ParseException e) {
                e.printStackTrace();
            }


            if (donationDetails.getDndInfo().size() > 0) {
                for (int i = 0; i < donationDetails.getDndInfo().size(); i++) {
                    switch (donationDetails.getDndInfo().get(i).getDay()) {
                        case "Mon":
                            checkMonday.setChecked(true);
                            break;
                        case "Tues":
                            checkTuesday.setChecked(true);
                            break;
                        case "Wed":
                            checkWednesday.setChecked(true);
                            break;
                        case "Thurs":
                            checkThursday.setChecked(true);
                            break;
                        case "Fri":
                            checkFriday.setChecked(true);
                            break;
                        case "Sat":
                            checkSaturday.setChecked(true);
                            break;
                        case "Sun":
                            checkSunday.setChecked(true);
                            break;
                    }
                }
//                try {
////                    from = simpleDateFormat2.parse(donationDetails.getDndInfo().get(0).getStartTime());
////                    to = simpleDateFormat2.parse(donationDetails.getDndInfo().get(0).getEndTime());
////                    simpleDateFormat1.applyPattern("hh:mm:ss a");
////                    editSelectFromTime.setText(simpleDateFormat1.format(from));
////                    editSelectToTime.setText(simpleDateFormat1.format(to));
//                    ;
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
                editSelectFromTime.setText(donationDetails.getDndInfo().get(0).getStartTime());
                editSelectToTime.setText(donationDetails.getDndInfo().get(0).getEndTime());

            }
        }
    }


    private void checkDisable() {
//        checkSunday.setChecked(false);
//        checkMonday.setChecked(false);
//        checkTuesday.setChecked(false);
//        checkWednesday.setChecked(false);
//        checkThursday.setChecked(false);
//        checkFriday.setChecked(false);
//        checkSaturday.setChecked(false);
        checkSunday.setEnabled(false);
        checkMonday.setEnabled(false);
        checkTuesday.setEnabled(false);
        checkWednesday.setEnabled(false);
        checkThursday.setEnabled(false);
        checkFriday.setEnabled(false);
        checkSaturday.setEnabled(false);
        editSelectFromTime.setEnabled(false);
//        editSelectFromTime.setText("");
        editSelectToTime.setEnabled(false);
//        editSelectToTime.setText("");
//        editScheduleDonation.setText("");
        editScheduleDonation.setEnabled(false);
//        editLastDonated.setText("");
        editLastDonated.setEnabled(false);
    }

    private void checkEnable() {
        checkSunday.setEnabled(true);
        checkMonday.setEnabled(true);
        checkTuesday.setEnabled(true);
        checkWednesday.setEnabled(true);
        checkThursday.setEnabled(true);
        checkFriday.setEnabled(true);
        checkSaturday.setEnabled(true);
        editSelectFromTime.setEnabled(true);
        editSelectToTime.setEnabled(true);
        editScheduleDonation.setEnabled(true);
        editLastDonated.setEnabled(true);

    }

}
