package com.dci.falcon.fragment;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.activity.HomeActivity;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.model.LogoutResponse;
import com.dci.falcon.model.UserActivityUpdate;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomButton;
import com.dci.falcon.view.CustomEditText;
import com.dci.falcon.view.CustomTextView;
import com.dci.falcon.view.CustomTextViewBold;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

/**
 * Created by vijayaganesh on 11/16/2017.
 */

public class UserActivityFragment extends BaseFragment {
    HomeActivity homeActivity;
    FloatingActionButton floatingActionButton;
    @BindView(R.id.edit_reg_id)
    CustomEditText editRegId;
    @BindView(R.id.text_red_id)
    TextInputLayout textRedId;
    @BindView(R.id.edit_blood_units)
    CustomEditText editBloodUnits;
    @BindView(R.id.text_input_blood_units)
    TextInputLayout textInputBloodUnits;
    @BindView(R.id.button_update)
    CustomButton buttonUpdate;
    @BindView(R.id.cons_user_activity)
    ConstraintLayout consUserActivity;
    Unbinder unbinder;
    @BindView(R.id.image_icon_quick_reg)
    ImageView imageIconQuickReg;
    @BindView(R.id.text_label_quick_reg)
    CustomTextViewBold textLabelQuickReg;
    @BindView(R.id.text_label_quick_reg_des)
    CustomTextView textLabelQuickRegDes;
    @Inject
    FalconAPI falconAPI;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    UserError userError;
    LogoutResponse logoutResponse;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_activity, container, false);
        unbinder = ButterKnife.bind(this, view);
        FalconApplication.getContext().getComponent().inject(this);
        getActivity().setTitle(getString(R.string.User_Activity));
        homeActivity = (HomeActivity) getActivity();
        userError = new UserError();
        editor = sharedPreferences.edit();
        floatingActionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab_request_blood);
        floatingActionButton.setVisibility(View.GONE);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeActivity.push(new RequestBloodFragment(), getString(R.string.Request_Blood));
            }
        });

        editBloodUnits.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    if (isUserActivityNotNull())
                        updateUserActivity();
                }
                return false;
            }
        });
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.button_update)
    public void onViewClicked() {
        if (isUserActivityNotNull()) {
            AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(getActivity());
            alertDialog1.setMessage(getString(R.string.alert_update));
            alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    updateUserActivity();
                }
            });
            alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            alertDialog1.show();
        } else {
            android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(getActivity());
            alertDialog.setMessage(getString(R.string.alert_mes_profile));
            alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    hideProgress();
                }
            });
            alertDialog.show();
        }
    }

    private void updateUserActivity() {

        UserActivityUpdate userActivityUpdate = new UserActivityUpdate();
        userActivityUpdate.setUniqueID(editRegId.getText().toString());
        try {
            userActivityUpdate.setUnits(Integer.parseInt(editBloodUnits.getText().toString()));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        if (Utils.isNetworkAvailable()) {
            showProgress();
            falconAPI.updateUserActivity(userActivityUpdate).enqueue(new Callback<LogoutResponse>() {
                @Override
                public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {
                    hideProgress();
                    logoutResponse = response.body();
                    if (isAdded()) {
                        if (response.body() != null && response.isSuccessful() && logoutResponse.getStatusCode() == 200) {
                            Toast.makeText(getActivity(), getString(R.string.successfully_update), Toast.LENGTH_SHORT).show();
                            getActivity().onBackPressed();
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.enter_vaild_request_id), Toast.LENGTH_SHORT).show();

                        }
                    }
                }

                @Override
                public void onFailure(Call<LogoutResponse> call, Throwable t) {
                    if (isAdded()) {
                        hideProgress();
                        Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }
                }
            });

        } else {
            userError.message = getActivity().getString(R.string.network_error_message);
            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }

    }

    private boolean showErrorRegID() {
        if (editRegId.getText().toString().isEmpty()) {
            textRedId.setError(getString(R.string.req_request_id));
            return false;
        } else {
            textRedId.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorRegID1() {
        if (editRegId.getText().toString().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorBloodUnits() {
        if (editBloodUnits.getText().toString().isEmpty()) {
            textInputBloodUnits.setError(getString(R.string.req_units));
            return false;
        } else {
            textInputBloodUnits.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorBloodUnits1() {
        if (editBloodUnits.getText().toString().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    private boolean isUserActivityNotNull() {
        showErrorBloodUnits();
        showErrorRegID();

        if (showErrorRegID1() && showErrorBloodUnits1()) {
            return true;
        } else
            return false;

    }

}
