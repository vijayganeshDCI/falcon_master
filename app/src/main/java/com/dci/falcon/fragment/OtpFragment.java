package com.dci.falcon.fragment;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.activity.LoginActivity;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.model.UniqueNumber;
import com.dci.falcon.model.UniqueNumberResponse;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomButton;
import com.dci.falcon.view.CustomTextViewBold;
import com.msg91.sendotp.library.PhoneNumberFormattingTextWatcher;
import com.msg91.sendotp.library.PhoneNumberUtils;
import com.msg91.sendotp.library.internal.Iso2Phone;

import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

/**
 * Created by vijayaganesh on 10/27/2017.
 */

public class OtpFragment extends BaseFragment {
    @BindView(R.id.image_falcon_icon_otp_login)
    ImageView imageFalconIconOtpLogin;
    @BindView(R.id.text_input_phone_number)
    TextInputLayout textInputPhoneNumber;
    @BindView(R.id.button_get_otp)
    CustomButton buttonOtpLogin;
    @BindView(R.id.check_enable_authoriser)
    CheckBox checkEnableAuthoriser;
    @BindView(R.id.cons_otp_login)
    ConstraintLayout consOtpLogin;
    Unbinder unbinder;
    public static final String INTENT_PHONENUMBER = "phonenumber";
    public static final String INTENT_COUNTRY_CODE = "code";
    @BindView(R.id.text_login_title_otp_login)
    CustomTextViewBold textLoginTitleOtpLogin;
    @BindView(R.id.edit_otp_login_phone_number)
    EditText editOtpLoginPhoneNumber;
    private TextWatcher mNumberTextWatcher;
    private String mCountryIso;
    LoginActivity loginActivity;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    FalconAPI falconAPI;
    UserError userError;
    UniqueNumberResponse uniqueNumberResponse;
    boolean isForSignUp = false;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_otp_login, container, false);
        unbinder = ButterKnife.bind(this, view);
        FalconApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        loginActivity = (LoginActivity) getActivity();
        userError = new UserError();
        Bundle bundle = getArguments();
        if (getArguments() != null)
            isForSignUp = bundle.getBoolean("isForSignUp");

        if (isForSignUp)
            getActivity().setTitle(getString(R.string.Registration));
        else
            getActivity().setTitle(getString(R.string.Login));


//        checkEnableAuthoriser.setVisibility(View.VISIBLE);


//        if (isForSignUp)
//            //SignUp
//            checkEnableAuthoriser.setVisibility(View.VISIBLE);
//        else
//            //LoginWithOTP
//            checkEnableAuthoriser.setVisibility(View.GONE);

        ((LoginActivity) getActivity()).getSupportActionBar().show();
        mCountryIso = PhoneNumberUtils.getDefaultCountryIso(getActivity());
        final String defaultCountryName = new Locale("", mCountryIso).getDisplayName();
//        resetNumberTextWatcher(mCountryIso);
//        mNumberTextWatcher.afterTextChanged(editOtpLoginPhoneNumber.getText());
//        editOtpLoginPhoneNumber.addTextChangedListener(textWatcher);
//        tryAndPrefillPhoneNumber();
        editOtpLoginPhoneNumber.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {

//                    if (isForSignUp) {
//                        SignUp
                    if (isOtpNotNull())
                        isNewNumber();
//                    }
//                    else {
//                        //LoginwithOTP
//                        if (isOtpNotNull())
//                            otpVerification(editOtpLoginPhoneNumber.getText().toString());
//                    }
                }
                return false;
            }
        });
//        checkEnableAuthoriser.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
////                Authorizer >> (0-Not authorize, 1-authorize)
//                if (isChecked)
//                    editor.putInt(FalconConstants.AUTHORIZERSTATUS, 1).commit();
//                else
//                    editor.putInt(FalconConstants.AUTHORIZERSTATUS, 0).commit();
//
//            }
//        });
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.button_get_otp)
    public void onViewClicked() {
//        if (isForSignUp) {
        if (isOtpNotNull())
            isNewNumber();
//        }
//        else {
//            if (isOtpNotNull())
//                otpVerification(editOtpLoginPhoneNumber.getText().toString());
//        }

    }

    private void otpVerification(String phoneNumber) {
        OtpVerificationFragment otpVerificationFragment = new OtpVerificationFragment();
        Bundle bundle = new Bundle();
        bundle.putString(INTENT_PHONENUMBER, phoneNumber);
        bundle.putString(INTENT_COUNTRY_CODE, Iso2Phone.getPhone(mCountryIso));
        if (isForSignUp)
//            Signup
            bundle.putInt("otpVerify", 0);
        else
//            LoginwithOtp
            bundle.putInt("otpVerify", 1);
        otpVerificationFragment.setArguments(bundle);
        loginActivity.push(otpVerificationFragment, getString(R.string.Verification));

    }

    private boolean isOtpNotNull() {
        if (editOtpLoginPhoneNumber.getText().toString().isEmpty()) {
            textInputPhoneNumber.setError(getString(R.string.req_pho));
            return false;
        } else if (editOtpLoginPhoneNumber.getText().toString().length() != 10) {
            textInputPhoneNumber.setError(getString(R.string.req_phone_number));
            return false;
        } else {
            textInputPhoneNumber.setErrorEnabled(false);
            return true;
        }

    }

    private void setButtonsEnabled(boolean enabled) {
        buttonOtpLogin.setEnabled(enabled);
    }


    private void resetNumberTextWatcher(String countryIso) {

        if (mNumberTextWatcher != null) {
            editOtpLoginPhoneNumber.removeTextChangedListener(mNumberTextWatcher);
        }

        mNumberTextWatcher = new PhoneNumberFormattingTextWatcher(countryIso) {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                super.onTextChanged(s, start, before, count);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                super.beforeTextChanged(s, start, count, after);
            }

            @Override
            public synchronized void afterTextChanged(Editable s) {
                super.afterTextChanged(s);
                if (isPossiblePhoneNumber()) {
                    setButtonsEnabled(true);
                    editOtpLoginPhoneNumber.setTextColor(Color.BLACK);
                } else {
                    setButtonsEnabled(false);
                    editOtpLoginPhoneNumber.setTextColor(Color.RED);
                }
            }
        };

        editOtpLoginPhoneNumber.addTextChangedListener(mNumberTextWatcher);
    }

    private boolean isPossiblePhoneNumber() {
        return PhoneNumberUtils.isPossibleNumber(editOtpLoginPhoneNumber.getText().toString(), mCountryIso);
    }

    private String getE164Number() {
        return editOtpLoginPhoneNumber.getText().toString().replaceAll("\\D", "").trim();
        // return PhoneNumberUtils.formatNumberToE164(mPhoneNumber.getText().toString(), mCountryIso);
    }

    private void tryAndPrefillPhoneNumber() {
        if (getActivity().checkCallingOrSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            TelephonyManager manager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            editOtpLoginPhoneNumber.setText(manager.getLine1Number());
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, 0);
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            tryAndPrefillPhoneNumber();
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permissions[0])) {
                Toast.makeText(getActivity(), "This application needs permission to read your phone number to automatically "
                        + "pre-fill it", Toast.LENGTH_LONG).show();
            }
        }
    }

//    public boolean loginValidation() {
//        if (!editOtpLoginPhoneNumber.getText().toString().isEmpty()) {
//            buttonOtpLogin.setEnabled(true);
//            buttonOtpLogin.setBackgroundColor(getResources().getColor(R.color.falcon_red));
//            return true;
//        } else {
//            buttonOtpLogin.setEnabled(false);
//            buttonOtpLogin.setBackgroundColor(getResources().getColor(R.color.light_grey));
//            return false;
//        }
//    }
//
//    TextWatcher textWatcher = new TextWatcher() {
//        @Override
//        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//        }
//
//        @Override
//        public void onTextChanged(CharSequence s, int start, int before, int count) {
//            loginValidation();
//        }
//
//        @Override
//        public void afterTextChanged(Editable s) {
//
//        }
//    };

    private void isNewNumber() {
        final UniqueNumber uniqueNumber = new UniqueNumber();
        uniqueNumber.setPhoneNumber(editOtpLoginPhoneNumber.getText().toString());
        if (Utils.isNetworkAvailable()) {
            showProgress();
            falconAPI.checkUniqueNumber(uniqueNumber).enqueue(new Callback<UniqueNumberResponse>() {
                @Override
                public void onResponse(Call<UniqueNumberResponse> call, Response<UniqueNumberResponse> response) {
                    hideProgress();
                    uniqueNumberResponse = response.body();
                    if (isAdded()) {
                        if (response.body() != null && response.isSuccessful()) {
                            if (isForSignUp) {
                                //Signup
                                if (uniqueNumberResponse.getStatusCode() == 500) {
                                    //mobile number not exist
                                    otpVerification(editOtpLoginPhoneNumber.getText().toString());
                                } else {
                                    //mobile number already exist
                                    Toast.makeText(getActivity(), uniqueNumberResponse.getMessage(), Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                //Login with OTP
                                if (uniqueNumberResponse.getStatusCode() == 500) {
                                    //mobile number not exist
                                    Toast.makeText(getActivity(), getString(R.string.sign_up_falcon), Toast.LENGTH_SHORT).show();
                                } else if (uniqueNumberResponse.getStatusCode() == 404) {
                                    AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(getActivity());
                                    alertDialog1.setView(R.layout.inactive_user_alert_view);
                                    alertDialog1.setCancelable(false);
                                    alertDialog1.show();
                                } else {
                                    //mobile number already exist
                                    otpVerification(editOtpLoginPhoneNumber.getText().toString());

                                }
                            }

                        } else {
                            Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<UniqueNumberResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();

                }


            });

        } else {
            userError.message = getActivity().getString(R.string.network_error_message);
            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }
    }
}

