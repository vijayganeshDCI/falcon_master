package com.dci.falcon.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.InputFilter;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.activity.HospitalProfileActivity;
import com.dci.falcon.activity.LoginActivity;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.model.GetHospitalInfo;
import com.dci.falcon.model.HospitalLoginResponse;
import com.dci.falcon.model.HospitalRegister;
import com.dci.falcon.model.HospitalRegisterResponse;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomButton;
import com.dci.falcon.view.CustomEditText;
import com.dci.falcon.view.CustomTextView;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;
import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

/**
 * Created by vijayaganesh on 11/16/2017.
 */

public class HospitalProfileFragment extends BaseFragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    @BindView(R.id.image_profile_pic)
    CircularImageView imageProfilePic;
    @BindView(R.id.text_edit_photo)
    CustomTextView textEditPhoto;
    @BindView(R.id.edit_hosp_name)
    CustomEditText editName;
    @BindView(R.id.text_hosp_name)
    TextInputLayout textInputName;
    @BindView(R.id.edit_address_hos)
    CustomEditText editAddressHos;
    @BindView(R.id.text_input_address)
    TextInputLayout textInputAddress;
    @BindView(R.id.edit_phone_number)
    CustomEditText editPhoneNumber;
    @BindView(R.id.text_input_phone_num)
    TextInputLayout textInputPhoneNum;
    @BindView(R.id.edit_hos_reg_id)
    CustomEditText editHosRegId;
    @BindView(R.id.text_input_hos_reg_id)
    TextInputLayout textInputHosRegId;
    @BindView(R.id.edit_owner_name)
    CustomEditText editOwnerName;
    @BindView(R.id.text_input_owner_name)
    TextInputLayout textInputOwnerName;
    @BindView(R.id.edit_owner_phone_number)
    CustomEditText editOwnerPhoneNumber;
    @BindView(R.id.text_input_owner_phone_number)
    TextInputLayout textInputOwnerPhoneNumber;
    @BindView(R.id.edit_email)
    CustomEditText editEmail;
    @BindView(R.id.text_input_email)
    TextInputLayout textInputEmail;
    @BindView(R.id.edit_password)
    CustomEditText editPassword;
    @BindView(R.id.text_input_password)
    TextInputLayout textInputPassword;
    @BindView(R.id.button_register)
    CustomButton buttonRegister;
    @BindView(R.id.cons_hospital_details)
    ConstraintLayout consHospitalDetails;
    @BindView(R.id.scroll_hospital_details)
    ScrollView scrollHospitalDetails;
    Unbinder unbinder;
    private static final int MY_REQUEST_CODE_CAMERA = 5;
    private static final int MY_REQUEST_CODE_GALLERY = 6;
    @BindView(R.id.text_prof_name)
    CustomTextView textProfName;
    @BindView(R.id.text_phone_number)
    CustomTextView textPhoneNumber;
    @BindView(R.id.image_mantitory_4)
    ImageView imageMantitory4;
    @BindView(R.id.text_label_manditatory)
    CustomTextView textLabelManditatory;
    @BindView(R.id.image_mantitory_1)
    ImageView imageMantitory1;
    private int REQUEST_CAMERA = 1;
    private int SELECT_FILE = 2;
    private Uri uri;
    private static final int PLACE_PICKER_REQUEST = 3;
    private static final long FASTEST_INTERVAL = 1000 * 5;
    private static final long INTERVAL = 1000 * 10;
    private LocationRequest mLocationRequest;
    Location mCurrentLocation;
    GoogleApiClient mGoogleApiClient;
    private double mLat, mLng;
    private double mPickerLat, mPickerLng;
    private LatLngBounds bounds;
    private static int editLocationClicked = 0;
    HospitalProfileActivity hospitalProfileActivity;
    FloatingActionButton floatingActionButton;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    FalconAPI falconAPI;
    HospitalRegisterResponse hospitalRegisterResponse;
    UserError userError;
    SharedPreferences fcmKeyPreferences;
    boolean isFromBaseActivity;
    HospitalLoginResponse hospitalLoginResponse;
    MenuItem edit;
    View view;
    private String proFilePicture;
    private String proFilePictureFromImageview;
    private FusedLocationProviderClient mFusedLocationClient;
    private double nearHomeLat1, nearHomeLng1;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_hospital_details, container, false);
        unbinder = ButterKnife.bind(this, view);
        initiaieGoogleApiClient();
        hospitalProfileActivity = (HospitalProfileActivity) getActivity();
        FalconApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        userError = new UserError();
        fcmKeyPreferences = getActivity().getSharedPreferences(FalconConstants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
        floatingActionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab_request_blood);
        floatingActionButton.setVisibility(View.GONE);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hospitalProfileActivity.push(new RequestBloodFragment(), getString(R.string.Request_Blood));
            }
        });
        editPassword.setFilters(new InputFilter[]{editTextRestrictLength(), editTextRestrictSmileys()});
        editName.setFilters(new InputFilter[]{editTextRestrictLength(), editTextRestrictSmileys(), editTextRestrictSpecialCharacters()});
        editOwnerName.setFilters(new InputFilter[]{editTextRestrictLength(), editTextRestrictSmileys(), editTextRestrictSpecialCharacters()});
//        editName.addTextChangedListener(textWatcher);`
//        editAddressHos.addTextChangedListener(textWatcher);
//        editPhoneNumber.addTextChangedListener(textWatcher);
//        editHosRegId.addTextChangedListener(textWatcher);
//        editOwnerName.addTextChangedListener(textWatcher);
//        editOwnerPhoneNumber.addTextChangedListener(textWatcher);
//        editEmail.addTextChangedListener(textWatcher);
//        editPassword.addTextChangedListener(textWatcher);
        Intent bundle = getActivity().getIntent();
        if (bundle != null) {
            isFromBaseActivity = bundle.getBooleanExtra("isFromBaseActivity", false);
        }
        if (isFromBaseActivity) {
            getHosInfo();
            setViewAndChildrenEnabled(view, false);
            setHasOptionsMenu(true);
            editPassword.setVisibility(View.GONE);
            getActivity().setTitle(getString(R.string.Profile));
            buttonRegister.setText(getActivity().getString(R.string.update));
            textProfName.setVisibility(View.VISIBLE);
            textPhoneNumber.setVisibility(View.VISIBLE);
        } else {
            setViewAndChildrenEnabled(view, true);
            setHasOptionsMenu(false);
            editPassword.setVisibility(View.VISIBLE);
            textProfName.setVisibility(View.GONE);
            textPhoneNumber.setVisibility(View.GONE);
            getActivity().setTitle(getString(R.string.Registration));
            buttonRegister.setText(getActivity().getString(R.string.Register));
        }

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.save, menu);
        edit = menu.findItem(R.id.menu_bar_edit);
        edit.setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_bar_edit:
                setViewAndChildrenEnabled(view, true);
                edit.setVisible(false);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.image_profile_pic, R.id.text_edit_photo, R.id.edit_address_hos, R.id.button_register})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_profile_pic:
                getProfilePicture();
                break;
            case R.id.text_edit_photo:
                getProfilePicture();
                break;
            case R.id.edit_address_hos:
                if (!hospitalProfileActivity.hasPermissionGranted()) {
                    hospitalProfileActivity.requestPermission();
                } else {
                    pickYourPlace();
                }
                break;
            case R.id.button_register:
//                Intent intent = new Intent(getActivity(), HomeActivity.class);
//                startActivity(intent);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                showProgress();
                if (isHosProfileNotNull()) {
                    if (isFromBaseActivity) {
                        android.support.v7.app.AlertDialog.Builder alertDialog1 = new android.support.v7.app.AlertDialog.Builder(getActivity());
                        alertDialog1.setMessage(getString(R.string.alert_update));
                        alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                hospInfoEdit();
//                                new Thread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        hospInfoEdit();
//
//                                    }
//                                }).start();
                            }
                        });
                        alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
//                                hideProgress();
                            }
                        });
                        alertDialog1.show();
                    } else {
                        android.support.v7.app.AlertDialog.Builder alertDialog1 = new android.support.v7.app.AlertDialog.Builder(getActivity());
                        alertDialog1.setMessage(getString(R.string.alert_register) + " " + editName.getText().toString() + " " + "?");
                        alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                registerHospitalUser();
//                                new Thread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        registerHospitalUser();
//
//                                    }
//                                }).start();

                            }
                        });
                        alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
//                                hideProgress();
                            }
                        });
                        alertDialog1.show();
                    }
                } else {
//                    hideProgress();
//                    android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(getActivity());
//                    alertDialog.setMessage(getString(R.string.alert_mes_profile));
//                    alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            dialogInterface.dismiss();
//                            hideProgress();
//                        }
//                    });
//                    alertDialog.show();
                }

                break;
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_REQUEST_CODE_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, REQUEST_CAMERA);
            } else {
                // Your app will not have this permission. Turn off all functions
                // that require this permission or it will force close like your
                // original question
            }
        } else if (requestCode == MY_REQUEST_CODE_GALLERY) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);//
                startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
            } else {

            }
        }


    }

    private void getProfilePicture() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add photo");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA},
                                    MY_REQUEST_CODE_CAMERA);
                        } else {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, REQUEST_CAMERA);
                        }
                    } else {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, REQUEST_CAMERA);
                    }

                } else if (options[item].equals("Choose from Gallery")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                    MY_REQUEST_CODE_GALLERY);
                        } else {
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);//
                            startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
                        }
                    } else {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);//
                        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
                    }

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        imageProfilePic.setImageBitmap(bm);
//        uploadUserImage(data.getData());
        proFilePicture = getBase64Image(bm);

        uri = data.getData();
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(getActivity().getExternalFilesDir(null),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        imageProfilePic.setImageBitmap(thumbnail);
//        uploadUserImage(Uri.fromFile(destination));
        proFilePicture = getBase64Image(thumbnail);
        uri = Uri.fromFile(destination);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PLACE_PICKER_REQUEST
                && resultCode == Activity.RESULT_OK) {

            final Place place = PlacePicker.getPlace(getActivity(), data);
            final String name = place.getName().toString();
            final String address = place.getAddress().toString();
            String attributions = (String) place.getAttributions();
            mPickerLat = place.getLatLng().latitude;
            mPickerLng = place.getLatLng().longitude;
            if (attributions == null) {
                attributions = "";
            }
            if (mPickerLat != 0 && mPickerLng != 0) {
                if (!name.contains("9"))
                    editAddressHos.setText(name + "," + address);
                else
                    editAddressHos.setText(address);
            } else
                editAddressHos.setText("");

        } else if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void initiaieGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity()).addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void startLocationUpdates() {

        if (getActivity() != null && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

//        mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//        if (mCurrentLocation != null) {
//            mLat = mCurrentLocation.getLatitude();
//            mLng = mCurrentLocation.getLongitude();
//        }
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        mFusedLocationClient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    mLat = location.getLatitude();
                    mLng = location.getLongitude();
                }
            }
        });

        double radiusDegrees = 0.001;
        LatLng center = new LatLng(mLat, mLng);
        LatLng northEast = new LatLng(center.latitude + radiusDegrees, center.longitude + radiusDegrees);
        LatLng southWest = new LatLng(center.latitude - radiusDegrees, center.longitude - radiusDegrees);
        bounds = LatLngBounds.builder()
                .include(northEast)
                .include(southWest)
                .build();
    }

    private void pickYourPlace() {
        try {
            if (bounds != null) {
                PlacePicker.IntentBuilder intentBuilder =
                        new PlacePicker.IntentBuilder();
                intentBuilder.setLatLngBounds(bounds);
                Intent intent = intentBuilder.build(getActivity());
                startActivityForResult(intent, PLACE_PICKER_REQUEST);
            }

        } catch (GooglePlayServicesRepairableException
                | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

//    TextWatcher textWatcher = new TextWatcher() {
//        @Override
//        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//        }
//
//        @Override
//        public void onTextChanged(CharSequence s, int start, int before, int count) {
//            hospitalRegisterValidation();
//        }
//
//        @Override
//        public void afterTextChanged(Editable s) {
//
//        }
//    };

    private String convertToBitMap() {
        Bitmap bm = ((BitmapDrawable) imageProfilePic.getDrawable()).getBitmap();
        proFilePictureFromImageview = getBase64Image(bm);
        return proFilePictureFromImageview;
    }

    public LatLng getLatLngFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            if (address.size() > 0) {
                Address location = address.get(0);
                nearHomeLat1 = location.getLatitude();
                nearHomeLng1 = location.getLongitude();
                p1 = new LatLng(location.getLatitude(), location.getLongitude());
            }

        } catch (IOException ex) {

            ex.printStackTrace();
        }

        return p1;
    }

    private void registerHospitalUser() {
        HospitalRegister hospitalRegister = new HospitalRegister();
        hospitalRegister.setName(editName.getText().toString());
        hospitalRegister.setAddress(editAddressHos.getText().toString());
        hospitalRegister.setEmailID(editEmail.getText().toString());
        hospitalRegister.setOwnerName(editOwnerName.getText().toString());
        hospitalRegister.setOwnerPhoneNumber(editOwnerPhoneNumber.getText().toString());
        if (mPickerLat == 0.0 && mPickerLng == 0.0) {
            getLatLngFromAddress(getActivity(), editAddressHos.getText().toString());
            hospitalRegister.setLat(String.valueOf(nearHomeLat1));
            hospitalRegister.setLng(String.valueOf(nearHomeLng1));
        } else {
            hospitalRegister.setLat(String.valueOf(mPickerLat));
            hospitalRegister.setLng(String.valueOf(mPickerLng));
        }
        hospitalRegister.setPhoneNumber(editPhoneNumber.getText().toString());
        hospitalRegister.setPassword(editPassword.getText().toString());
        hospitalRegister.setRegisterID(editHosRegId.getText().toString());
        hospitalRegister.setDeviceID(sharedPreferences.getString(FalconConstants.DEVICEID, ""));
        hospitalRegister.setAppID(sharedPreferences.getString(FalconConstants.APPID, ""));
        hospitalRegister.setAppVersion(sharedPreferences.getString(FalconConstants.APPVERSION, ""));
        hospitalRegister.setFCMKey(fcmKeyPreferences.getString(FalconConstants.FCMTOKEN, ""));
        if (proFilePicture == null) {
            hospitalRegister.setPhoto(convertToBitMap());
        } else {
            hospitalRegister.setPhoto(proFilePicture);
        }

        if (Utils.isNetworkAvailable()) {
            showProgress();
            falconAPI.registerHospitalUser(hospitalRegister).enqueue(new Callback<HospitalRegisterResponse>() {
                @Override
                public void onResponse(Call<HospitalRegisterResponse> call, Response<HospitalRegisterResponse> response) {
                    hideProgress();
                    hospitalRegisterResponse = response.body();
                    if (isAdded()) {
                        if (response.body() != null && response.isSuccessful() && hospitalRegisterResponse.getStatusCode() == 200) {
                            editor.putInt(FalconConstants.HOSPITALUSERID, hospitalRegisterResponse.getUserinfo().getId()).commit();
                            editor.putString(FalconConstants.HOSPITALREGISTERID, hospitalRegisterResponse.getUserinfo().getRegisterID()).commit();
                            editor.putInt(FalconConstants.HOSPUNITS, hospitalRegisterResponse.getUserinfo().getIndUnits()).commit();
                            editor.putInt(FalconConstants.HOSPTOTALUNITS, hospitalRegisterResponse.getTotalUnits()).commit();
                            editor.putString(FalconConstants.USERNAME, hospitalRegisterResponse.getUserinfo().getName()).commit();
                            editor.putString(FalconConstants.PHONENUMBER, hospitalRegisterResponse.getUserinfo().getPhoneNumber()).commit();
                            editor.putString(FalconConstants.PROFILEPICTURE, hospitalRegisterResponse.getUserinfo().getPhoto()).commit();
                            android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(getActivity());
                            alertDialog.setMessage(getString(R.string.alert_hos_reg));
                            alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent intent_admin_logout = new Intent(getActivity(), LoginActivity.class);
                                    intent_admin_logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    getActivity().finish();
                                    startActivity(intent_admin_logout);
                                    editor.putInt(FalconConstants.LOGINSTATUSCODE, 0).commit();
                                }
                            });
                            alertDialog.show();

                        } else {
                            Toast.makeText(getActivity(), getString(R.string.registerID_error_message), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<HospitalRegisterResponse> call, Throwable t) {
                    if (isAdded()) {
                        hideProgress();
                        Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else {
            userError.message = getString(R.string.network_error_message);
            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }

    }

//    private void hospitalRegisterValidation() {
//        if (!editName.getText().toString().isEmpty() && !editAddressHos.getText().toString().isEmpty() &&
//                !editPhoneNumber.getText().toString().isEmpty() && !editHosRegId.getText().toString().isEmpty() && !editOwnerName.getText().toString().isEmpty() &&
//                !editOwnerPhoneNumber.getText().toString().isEmpty() && !editEmail.getText().toString().isEmpty() && !editPassword.getText().toString().isEmpty() &&
//                android.util.Patterns.EMAIL_ADDRESS.matcher(editEmail.getText().toString()).matches()) {
//            buttonRegister.setEnabled(true);
//            buttonRegister.setBackgroundColor(getResources().getColor(R.color.falcon_red));
//
//        } else {
//            buttonRegister.setEnabled(false);
//            buttonRegister.setBackgroundColor(getResources().getColor(R.color.light_grey));
//        }
//    }

    private void getHosInfo() {
        GetHospitalInfo getHospitalInfo = new GetHospitalInfo();
        getHospitalInfo.setHosID(sharedPreferences.getInt(FalconConstants.HOSPITALUSERID, 0));
        if (Utils.isNetworkAvailable()) {
            showProgress();
            falconAPI.getHospInfo(getHospitalInfo).enqueue(new Callback<HospitalLoginResponse>() {
                @Override
                public void onResponse(Call<HospitalLoginResponse> call, Response<HospitalLoginResponse> response) {
                    hideProgress();
                    hospitalLoginResponse = response.body();
                    if (isAdded()) {
                        if (response.body() != null && response.isSuccessful() && hospitalLoginResponse.getStatusCode() == 200) {
                            textProfName.setText(hospitalLoginResponse.getUserinfo().getName());
                            textPhoneNumber.setText(hospitalLoginResponse.getUserinfo().getPhoneNumber());
                            editName.setText(hospitalLoginResponse.getUserinfo().getName());
                            editAddressHos.setText(hospitalLoginResponse.getUserinfo().getAddress());
                            editPhoneNumber.setText(hospitalLoginResponse.getUserinfo().getPhoneNumber());
                            editHosRegId.setText(hospitalLoginResponse.getUserinfo().getRegisterID());
                            editOwnerName.setText(hospitalLoginResponse.getUserinfo().getOwnerName());
                            editOwnerPhoneNumber.setText(hospitalLoginResponse.getUserinfo().getOwnerPhoneNumber());
                            editEmail.setText(hospitalLoginResponse.getUserinfo().getEmailID());
                            //                        editPassword.setText(hospitalRegisterResponse.getUserinfo().getPassword());
                            String pro = hospitalLoginResponse.getUserinfo().getPhoto();
                            if (pro != null) {
                                new DownloadImageTask(imageProfilePic)
                                        .execute(getString(R.string.falcon_image_url) + pro);
                            } else {
                                imageProfilePic.setImageResource(R.mipmap.img_sample_pro_pic);
                            }
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();

                        }
                    }
                }

                @Override
                public void onFailure(Call<HospitalLoginResponse> call, Throwable t) {
                    if (isAdded()) {
                        hideProgress();
                        Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }

                }
            });
        } else {
            if (isAdded()) {
                userError.message = getString(R.string.network_error_message);
                showError(ERROR_SHOW_TYPE_DIALOG, userError);
            }
        }
    }

    private void hospInfoEdit() {

        HospitalRegister hospitalRegister = new HospitalRegister();
        hospitalRegister.setName(editName.getText().toString());
        hospitalRegister.setAddress(editAddressHos.getText().toString());
        hospitalRegister.setEmailID(editEmail.getText().toString());
        hospitalRegister.setOwnerName(editOwnerName.getText().toString());
        hospitalRegister.setOwnerPhoneNumber(editOwnerPhoneNumber.getText().toString());
        hospitalRegister.setLat(String.valueOf(mPickerLat));
        hospitalRegister.setLng(String.valueOf(mPickerLng));
        hospitalRegister.setPhoneNumber(editPhoneNumber.getText().toString());
        hospitalRegister.setPassword(editPassword.getText().toString());
        hospitalRegister.setRegisterID(editHosRegId.getText().toString());
        hospitalRegister.setDeviceID(sharedPreferences.getString(FalconConstants.DEVICEID, ""));
        hospitalRegister.setAppID(sharedPreferences.getString(FalconConstants.APPID, ""));
        hospitalRegister.setAppVersion(sharedPreferences.getString(FalconConstants.APPVERSION, ""));
        hospitalRegister.setFCMKey(fcmKeyPreferences.getString(FalconConstants.FCMTOKEN, ""));
        hospitalRegister.setId(sharedPreferences.getInt(FalconConstants.HOSPITALUSERID, 0));
        if (proFilePicture == null) {
            hospitalRegister.setPhoto(convertToBitMap());
        } else {
            hospitalRegister.setPhoto(proFilePicture);
        }

        if (Utils.isNetworkAvailable()) {
            showProgress();
            falconAPI.getHospInfoEdit(hospitalRegister).enqueue(new Callback<HospitalRegisterResponse>() {
                @Override
                public void onResponse(Call<HospitalRegisterResponse> call, Response<HospitalRegisterResponse> response) {
                    hideProgress();
                    hospitalRegisterResponse = response.body();
                    if (isAdded()) {
                        if (response.body() != null && response.isSuccessful() && hospitalRegisterResponse.getStatusCode() == 200) {
                            android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(getActivity());
                            alertDialog.setMessage(getString(R.string.successfully_update));
                            alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    getActivity().onBackPressed();
                                }
                            });
                            alertDialog.show();
                            editor.putInt(FalconConstants.HOSPITALUSERID, hospitalRegisterResponse.getUserinfo().getId()).commit();
                            editor.putString(FalconConstants.HOSPITALREGISTERID, hospitalRegisterResponse.getUserinfo().getRegisterID()).commit();
                            editor.putInt(FalconConstants.HOSPUNITS, hospitalRegisterResponse.getUserinfo().getIndUnits()).commit();
                            editor.putInt(FalconConstants.HOSPTOTALUNITS, hospitalRegisterResponse.getTotalUnits()).commit();
                            //                        editor.putInt(FalconConstants.HOSPLIVES, hospitalLoginResponse.getUserinfo().getLives()).commit();
                            editor.putString(FalconConstants.USERNAME, hospitalRegisterResponse.getUserinfo().getName()).commit();
                            editor.putString(FalconConstants.PHONENUMBER, hospitalRegisterResponse.getUserinfo().getPhoneNumber()).commit();
                            editor.putString(FalconConstants.PROFILEPICTURE, hospitalRegisterResponse.getUserinfo().getPhoto()).commit();
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();

                        }
                    }
                }

                @Override
                public void onFailure(Call<HospitalRegisterResponse> call, Throwable t) {
                    if (isAdded()) {
                        hideProgress();
                        Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }

                }
            });

        } else {
            userError.message = getString(R.string.network_error_message);
            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }
    }

    public boolean isHosProfileNotNull() {
        showErrorHospName();
        showErrorUserName();
        showErrorPhoneNumber();
        showErrorLocation();
        showErrorAdminName();
        if (!isFromBaseActivity) {
            showErrorPassword();
            if (showErrorHospName1() && showErrorUserName1()
                    && showErrorAdminName1() && showErrorPhoneNumber1() && showErrorLocation1() && showErrorPassword1()) {
                return true;
            } else {
                return false;
            }

        } else {
            if (showErrorHospName1() && showErrorUserName1()
                    && showErrorAdminName1()&& showErrorPhoneNumber1() && showErrorLocation1()) {
                return true;
            } else {
                return false;
            }

        }


    }

    private boolean showErrorHospName() {

        if (editName.getText().toString().isEmpty()) {
            textInputName.setError(getString(R.string.req_hos_name));
            return false;
        } else {
            textInputName.setErrorEnabled(false);
            return true;
        }
    }
    private boolean showErrorAdminName() {

        if (editOwnerName.getText().toString().isEmpty()) {
            textInputOwnerName.setError(getString(R.string.req_hos_admin_name));
            return false;
        } else {
            textInputOwnerName.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorLocation() {

        if (editAddressHos.getText().toString().isEmpty()) {
            textInputAddress.setError(getString(R.string.req_Location));
            return false;
        } else {
            textInputAddress.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorLocation1() {

        if (editAddressHos.getText().toString().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorPhoneNumber() {
        if (editPhoneNumber.getText().toString().isEmpty()) {
            textInputPhoneNum.setError(getString(R.string.req_pho));
            return false;
        } else if (editPhoneNumber.getText().toString().length() != 10) {
            textInputPhoneNum.setError(getString(R.string.req_phone_number));
            return false;
        } else {
            textInputPhoneNum.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorPhoneNumber1() {
        if (editPhoneNumber.getText().toString().isEmpty()) {
            return false;
        } else if (editPhoneNumber.getText().toString().length() > 10) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorHospName1() {

        if (editName.getText().toString().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorAdminName1() {

        if (editOwnerName.getText().toString().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorUserName() {
        if (editHosRegId.getText().toString().isEmpty()) {
            textInputHosRegId.setError(getString(R.string.req_reg_id));
            return false;
        } else {
            textInputHosRegId.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorUserName1() {
        if (editHosRegId.getText().toString().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }


    private boolean showErrorEmail() {
        if (editEmail.getText().toString().isEmpty()) {
            textInputEmail.setError(getString(R.string.req_Email));
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(editEmail.getText().toString()).matches()) {
            textInputEmail.setError(getActivity().getString(R.string.enter_valid_email_add));
            return false;
        } else {
            textInputEmail.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorEmail1() {
        if (editEmail.getText().toString().isEmpty()) {
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(editEmail.getText().toString()).matches()) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorPassword() {
        if (editPassword.getText().toString().isEmpty()) {
            textInputPassword.setError(getString(R.string.req_first_pasword));
            return false;
        } else if (editPassword.getText().length() < 8) {
            textInputPassword.setError(getActivity().getString(R.string.min_pass));
            return false;
        } else {
            textInputPassword.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorPassword1() {
        if (editPassword.getText().toString().isEmpty()) {
            return false;
        } else if (editPassword.getText().length() < 8) {
            return false;
        } else {
            return true;
        }
    }


}
