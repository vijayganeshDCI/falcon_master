package com.dci.falcon.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.activity.BaseActivity;
import com.dci.falcon.activity.NotificationActivity;
import com.dci.falcon.activity.TrackRequestActivity;
import com.dci.falcon.adapter.AcceptedRequestAdapter;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.model.AcceptRequest;
import com.dci.falcon.model.NotificationUpdate;
import com.dci.falcon.model.NotificationUpdateResponse;
import com.dci.falcon.model.TrackRequestItem;
import com.dci.falcon.model.AcceptRequestResponse;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomTextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

/**
 * Created by vijayaganesh on 11/10/2017.
 */

public class AcceptRequestFragment extends BaseFragment {


    FloatingActionButton floatingActionButton;
    NotificationActivity notificationActivity;
    AcceptRequestResponse acceptRequestResponse;
    BaseActivity baseActivity;
    AcceptedRequestAdapter trackRequestAdapter;
    List<TrackRequestItem> requestItemList;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    FalconAPI falconAPI;
    UserError userError;
    TrackRequestActivity trackRequestActivity;
    @BindView(R.id.image_empty_accept_req)
    ImageView imageEmptyAcceptReq;
    @BindView(R.id.text_label_accept_req)
    CustomTextView textLabelAcceptReq;
    @BindView(R.id.cons_empty_list)
    ConstraintLayout consEmptyList;
    @BindView(R.id.list_accepted_req)
    ListView listAcceptedReq;
    @BindView(R.id.swiperefresh_accept_req)
    SwipeRefreshLayout swiperefreshAcceptReq;
    @BindView(R.id.cons_accepted_req)
    ConstraintLayout consAcceptedReq;
    private Unbinder unbinder;
    private NotificationUpdateResponse notificationUpdateResponse;

    private Boolean isStarted = false;
    private Boolean isVisible = false;

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isVisible)
            getAcceptedRequest();

    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted)
            getAcceptedRequest();

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_accept_request, container, false);
        unbinder = ButterKnife.bind(this, view);
        FalconApplication.getContext().getComponent().inject(this);

        baseActivity = (BaseActivity) getActivity();
        getActivity().setTitle(getString(R.string.Notification));
        notificationActivity = (NotificationActivity) getActivity();
        imageEmptyAcceptReq.setImageResource(R.mipmap.icon_empty_request);
        textLabelAcceptReq.setText(getString(R.string.No_More_accept_req));
        userError = new UserError();
        updateNotification(0);
        floatingActionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab_request_blood);
        floatingActionButton.setVisibility(View.GONE);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                trackRequestActivity.push(new RequestBloodFragment(), getString(R.string.Request_Blood));
            }
        });
        requestItemList = new ArrayList<TrackRequestItem>();
        consEmptyList.setVisibility(View.VISIBLE);
        swiperefreshAcceptReq.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swiperefreshAcceptReq.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isAdded()) {
                            getAcceptedRequest();
                            swiperefreshAcceptReq.setRefreshing(false);
                        }
                    }
                }, 3000);
            }
        });

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void getAcceptedRequest() {
        AcceptRequest acceptRequest = new AcceptRequest();
        if (sharedPreferences.getInt(FalconConstants.ROLEID, 0) == 0) {

            acceptRequest.setUserID(sharedPreferences.getInt(FalconConstants.USEDID, 0));
            acceptRequest.setType(0);
        } else {

            acceptRequest.setUserID(sharedPreferences.getInt(FalconConstants.HOSPITALUSERID, 0));
            acceptRequest.setType(1);
        }

        if (Utils.isNetworkAvailable()) {
            showProgress();
            requestItemList.clear();
            falconAPI.acceptedRequest(acceptRequest).enqueue(new Callback<AcceptRequestResponse>() {
                @Override
                public void onResponse(Call<AcceptRequestResponse> call, Response<AcceptRequestResponse> response) {
                    hideProgress();
                    acceptRequestResponse = response.body();
                    if (isAdded()) {
                        if (response.isSuccessful() && response.body() != null && acceptRequestResponse.getStatusCode() == 200
                                && !acceptRequestResponse.getUser().isEmpty()) {
                            consEmptyList.setVisibility(View.GONE);

                            for (int i = 0; i < acceptRequestResponse.getUser().size(); i++) {
                                requestItemList.add(new TrackRequestItem(acceptRequestResponse.getUser().get(i).getNofID(),
                                        acceptRequestResponse.getUser().get(i).getBd_req().getBloodGroupID(),
                                        acceptRequestResponse.getUser().get(i).getBd_req().getUnits(),
                                        acceptRequestResponse.getUser().get(i).getF_user().getFirstName(),
                                        acceptRequestResponse.getUser().get(i).getScheduleTime(),
                                        acceptRequestResponse.getUser().get(i).getNoid().getUserResStatus(),
                                        acceptRequestResponse.getUser().get(i).getF_user().getProfilePicture(),
                                        acceptRequestResponse.getUser().get(i).getNotes(),
                                        acceptRequestResponse.getUser().get(i).getTravellingStatus(),
                                        acceptRequestResponse.getUser().get(i).getCreateDate(),
                                        acceptRequestResponse.getUser().get(i).getF_user().getPhoneNumber()));
                            }

                            trackRequestAdapter = new AcceptedRequestAdapter(getActivity(), requestItemList, baseActivity, AcceptRequestFragment.this);
                            listAcceptedReq.setAdapter(trackRequestAdapter);
                            Collections.sort(requestItemList, new Comparator<TrackRequestItem>() {
                                public int compare(TrackRequestItem obj1, TrackRequestItem obj2) {
                                    // ## Ascending order
                                    //                                return Integer.valueOf(obj2.getId()).compareTo(obj1.getId()); // To compare integer values
                                    // ## Descending order
                                    return obj2.getAcceptedDate().compareToIgnoreCase(obj1.getAcceptedDate()); // To compare string values
                                    // return Integer.valueOf(obj2.empId).compareTo(obj1.empId); // To compare integer values
                                }
                            });
                        } else {
                            consEmptyList.setVisibility(View.VISIBLE);

                        }
                    }
                }

                @Override
                public void onFailure(Call<AcceptRequestResponse> call, Throwable t) {
                    if (isAdded()) {
                        hideProgress();
                        consEmptyList.setVisibility(View.VISIBLE);
                        Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else {
            if (isAdded()) {
                consEmptyList.setVisibility(View.VISIBLE);
//                userError.message = getActivity().getString(R.string.network_error_message);
//                showError(ERROR_SHOW_TYPE_DIALOG, userError);
            }
        }
    }

    private void updateNotification(int status) {
        NotificationUpdate notificationUpdate = new NotificationUpdate();
        notificationUpdate.setStatus(status);
        if (sharedPreferences.getInt(FalconConstants.ROLEID, 0) == 0) {
            notificationUpdate.setUser_id(sharedPreferences.getInt(FalconConstants.USEDID, 0));
            notificationUpdate.setUser_type(1);
        } else {
            notificationUpdate.setUser_id(sharedPreferences.getInt(FalconConstants.HOSPITALUSERID, 0));
            notificationUpdate.setUser_type(0);
        }

        if (Utils.isNetworkAvailable()) {

            falconAPI.updateNotification(notificationUpdate).enqueue(new Callback<NotificationUpdateResponse>() {
                @Override
                public void onResponse(Call<NotificationUpdateResponse> call, Response<NotificationUpdateResponse> response) {
                    notificationUpdateResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && notificationUpdateResponse.getStatusCode() == 200) {
//                        notifyBilicking.setVisible(false);
//                        notify.setVisible(true);
                    }
                }

                @Override
                public void onFailure(Call<NotificationUpdateResponse> call, Throwable t) {

                }
            });

        } else {
            userError.message = getString(R.string.network_error_message);
            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }


    }
}