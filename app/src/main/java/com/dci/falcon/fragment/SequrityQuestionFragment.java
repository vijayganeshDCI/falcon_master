package com.dci.falcon.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.activity.LoginActivity;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.model.CheckQandA;
import com.dci.falcon.model.CheckSeqQandAResponse;
import com.dci.falcon.model.ForgetPassSequrityQuestionResponse;
import com.dci.falcon.model.SequrityQuestion;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomButton;
import com.dci.falcon.view.CustomEditText;
import com.dci.falcon.view.CustomTextView;
import com.dci.falcon.view.CustomTextViewBold;
import com.msg91.sendotp.library.PhoneNumberUtils;
import com.msg91.sendotp.library.internal.Iso2Phone;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

/**
 * Created by vijayaganesh on 11/30/2017.
 */

public class SequrityQuestionFragment extends BaseFragment {
    @BindView(R.id.image_icon_forget)
    ImageView imageIconForget;
    @BindView(R.id.text_label_forget_password)
    CustomTextViewBold textLabelForgetPassword;
    @BindView(R.id.text_label_enter_number)
    CustomTextView textLabelEnterNumber;
    @BindView(R.id.edit_seq_question)
    CustomEditText editSeqQuestion;
    @BindView(R.id.text_input_seq_question)
    TextInputLayout textInputSeqQuestion;
    @BindView(R.id.edit_seq_answer)
    CustomEditText editSeqAnswer;
    @BindView(R.id.text_input_seq_answer)
    TextInputLayout textInputSeqAnswer;
    @BindView(R.id.button_verify)
    CustomButton buttonVerify;
    @BindView(R.id.cons_seq_fragment)
    ConstraintLayout consSeqFragment;
    Unbinder unbinder;
    String phoneNumber;
    UserError userError;
    @Inject
    FalconAPI falconAPI;
    ForgetPassSequrityQuestionResponse forgetPassSequrityQuestionResponse;
    CheckSeqQandAResponse checkSeqQandAResponse;
    LoginActivity loginActivity;
    boolean isForChangeNumber;
    String newNumber;
    public static final String INTENT_PHONENUMBER = "phonenumber";
    public static final String INTENT_COUNTRY_CODE = "code";
    private String mCountryIso;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sequrity_question, container, false);
        unbinder = ButterKnife.bind(this, view);
        FalconApplication.getContext().getComponent().inject(this);
        userError = new UserError();
        loginActivity = (LoginActivity) getActivity();
//        editSeqAnswer.addTextChangedListener(textWatcher);
//        editSeqQuestion.addTextChangedListener(textWatcher);
        mCountryIso = PhoneNumberUtils.getDefaultCountryIso(getActivity());
        editor = sharedPreferences.edit();
        Bundle bundle = getArguments();
        if (bundle != null) {
            isForChangeNumber = bundle.getBoolean("forChangeNumber");
            if (isForChangeNumber) {
                //forChangeNumber
                phoneNumber = bundle.getString("oldNumber");
                newNumber = bundle.getString("newNumber");
                imageIconForget.setImageResource(R.mipmap.icon_change_number);
                textLabelForgetPassword.setText(getText(R.string.change_ur_number));
            } else {
                phoneNumber = bundle.getString("phoneNumber");
                imageIconForget.setImageResource(R.mipmap.icon_forget_password);
                textLabelForgetPassword.setText(getText(R.string.forget_password));
            }

        }
        getSecqurityQuestion();
        editSeqAnswer.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    if (isSeqAnswerNotNull())
                        checkSecQandA();
                }
                return false;
            }
        });
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.button_verify)
    public void onViewClicked() {
        if (isSeqAnswerNotNull())
            checkSecQandA();
    }

    private void getSecqurityQuestion() {
        SequrityQuestion sequrityQuestion = new SequrityQuestion();
        sequrityQuestion.setPhone_no(phoneNumber);

        if (Utils.isNetworkAvailable()) {
            showProgress();
            falconAPI.getSequrityQuestion(sequrityQuestion).enqueue(new Callback<ForgetPassSequrityQuestionResponse>() {
                @Override
                public void onResponse(Call<ForgetPassSequrityQuestionResponse> call, Response<ForgetPassSequrityQuestionResponse> response) {
                    hideProgress();
                    forgetPassSequrityQuestionResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && forgetPassSequrityQuestionResponse.getStatusCode() != 500) {
                        if (forgetPassSequrityQuestionResponse.getData().getQuestion()!=null)
                        editSeqQuestion.setText(forgetPassSequrityQuestionResponse.getData().getQuestion());
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.vaild_number_message), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ForgetPassSequrityQuestionResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.vaild_number_message), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            userError.message = getActivity().getString(R.string.network_error_message);
            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }

    }

    private boolean showErrorAnswer1() {
        if (editSeqAnswer.getText().toString().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorAnswer() {
        if (editSeqAnswer.getText().toString().isEmpty()) {
            textInputSeqAnswer.setError(getString(R.string.req_ans));
            return false;
        } else {
            textInputSeqAnswer.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorQuest1() {
        if (editSeqQuestion.getText().toString().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorQuest() {
        if (editSeqQuestion.getText().toString().isEmpty()) {
            textInputSeqQuestion.setError(getString(R.string.req_ques));
            return false;
        } else {
            textInputSeqQuestion.setErrorEnabled(false);
            return true;
        }
    }

    private boolean isSeqAnswerNotNull() {
        showErrorAnswer();
        showErrorQuest();

        if (showErrorAnswer1() && showErrorQuest1())
            return true;
        else
            return false;
    }

    private void checkSecQandA() {
        CheckQandA checkQandA = new CheckQandA();
        checkQandA.setPhone_no(phoneNumber);
        checkQandA.setAns(editSeqAnswer.getText().toString());
        final ResetPasswordIndiduvalUserFragment resetPasswordFragment = new ResetPasswordIndiduvalUserFragment();
        if (Utils.isNetworkAvailable()) {
            showProgress();
            falconAPI.checkSeqQandA(checkQandA).enqueue(new Callback<CheckSeqQandAResponse>() {
                @Override
                public void onResponse(Call<CheckSeqQandAResponse> call, Response<CheckSeqQandAResponse> response) {
                    hideProgress();
                    checkSeqQandAResponse = response.body();
                    if (response.isSuccessful() && response.body() != null && checkSeqQandAResponse.getStatusCode() != 500) {
                        if (isForChangeNumber) {
                            otpVerification(newNumber);
                        } else {
                            //ResetPassword
                            Bundle bundle = new Bundle();
                            bundle.putString("phoneNumber", phoneNumber);
                            resetPasswordFragment.setArguments(bundle);
                            loginActivity.push(resetPasswordFragment, getString(R.string.reset_password));
                        }
                        Toast.makeText(getActivity(), checkSeqQandAResponse.getStatus(), Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(getActivity(), checkSeqQandAResponse.getStatus(), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<CheckSeqQandAResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            userError.message = getActivity().getString(R.string.network_error_message);
            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }
    }

    private void otpVerification(String toChangeNumber) {
        OtpVerificationFragment otpVerificationFragment = new OtpVerificationFragment();
        Bundle bundle = new Bundle();
        editor.putString(FalconConstants.NEWPHONENUMBER, toChangeNumber).commit();
        editor.putString(FalconConstants.OLDPHONENUMBER, phoneNumber).commit();
        bundle.putInt("otpVerify", 2);
        bundle.putBoolean("isNewUser", false);
        bundle.putString(INTENT_PHONENUMBER, toChangeNumber);
        bundle.putString(INTENT_COUNTRY_CODE, Iso2Phone.getPhone(mCountryIso));
        otpVerificationFragment.setArguments(bundle);
        loginActivity.push(otpVerificationFragment, getString(R.string.Verification));

    }
//    TextWatcher textWatcher = new TextWatcher() {
//        @Override
//        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//        }
//
//        @Override
//        public void onTextChanged(CharSequence s, int start, int before, int count) {
//            ansvalidation();
//        }
//
//        @Override
//        public void afterTextChanged(Editable s) {
//
//        }
//    };
//
//    private void ansvalidation() {
//        if (!editSeqQuestion.getText().toString().isEmpty() && !editSeqAnswer.getText().toString().isEmpty()) {
//            buttonVerify.setEnabled(true);
//            buttonVerify.setBackgroundColor(getResources().getColor(R.color.falcon_red));
//        } else {
//            buttonVerify.setEnabled(false);
//            buttonVerify.setBackgroundColor(getResources().getColor(R.color.light_grey));
//        }
//    }

}
