package com.dci.falcon.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.activity.BaseActivity;
import com.dci.falcon.adapter.RespondRequestAdapter;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.model.AuthorizerRequestItem;
import com.dci.falcon.model.RespondReqResponse;
import com.dci.falcon.model.TrackRequest;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomTextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

/**
 * Created by vijayaganesh on 12/11/2017.
 */

public class RespondRequestFragment extends BaseFragment {
    @BindView(R.id.list_respond_to_req)
    ListView listRespondtoReq;
    @BindView(R.id.cons_authorize_req)
    ConstraintLayout consAuthorizeReq;
    Unbinder unbinder;
    @BindView(R.id.image_empty_res_req)
    ImageView imageEmptyResReq;
    @BindView(R.id.text_label_res_req)
    CustomTextView textLabelResReq;
    @BindView(R.id.cons_empty_list)
    ConstraintLayout consEmptyList;
    @BindView(R.id.swiperefresh_respond_req)
    SwipeRefreshLayout swiperefreshRespondReq;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    FalconAPI falconAPI;
    UserError userError;
    List<AuthorizerRequestItem> requestItemList;
    RespondRequestAdapter respondRequestAdapter;
    RespondReqResponse respondRequestResponse;
    BaseActivity baseActivity;
    private FloatingActionButton floatingActionButton;

    private Boolean isStarted = false;
    private Boolean isVisible = false;

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isVisible)
            getRespondRequest();

    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted)
            getRespondRequest();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_respond_request, container, false);
        unbinder = ButterKnife.bind(this, view);
        FalconApplication.getContext().getComponent().inject(this);

        userError = new UserError();
        baseActivity = (BaseActivity) getActivity();
        requestItemList = new ArrayList<AuthorizerRequestItem>();
        consEmptyList.setVisibility(View.VISIBLE);

        swiperefreshRespondReq.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swiperefreshRespondReq.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isAdded()) {
                            getRespondRequest();
                            swiperefreshRespondReq.setRefreshing(false);
                        }
                    }
                }, 3000);
            }
        });
        floatingActionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab_request_blood);
        floatingActionButton.setVisibility(View.GONE);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                notificationActivity.push(new RequestBloodFragment(), getString(R.string.Request_Blood));
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void getRespondRequest() {
        final TrackRequest respondRequest = new TrackRequest();
        respondRequest.setId(sharedPreferences.getInt(FalconConstants.USEDID, 0));
        if (Utils.isNetworkAvailable()) {
            showProgress();
            requestItemList.clear();
            falconAPI.respondRequest(respondRequest).enqueue(new Callback<RespondReqResponse>() {
                @Override
                public void onResponse(Call<RespondReqResponse> call, Response<RespondReqResponse> response) {
                    if (isAdded()) {
                        hideProgress();
                        consEmptyList.setVisibility(View.GONE);
                        respondRequestResponse = response.body();
                        if (response.body() != null && response.isSuccessful() && respondRequestResponse.getStatusCode() == 200
                                && !respondRequestResponse.getUser().isEmpty()) {
                            for (int i = 0; i < respondRequestResponse.getUser().size(); i++) {
                                if (respondRequestResponse.getUser().get(i).getReid().getStatus() != 2) {
                                    requestItemList.add(new AuthorizerRequestItem(respondRequestResponse.getUser().get(i).getName(),
                                            respondRequestResponse.getUser().get(i).getPhoneNumber(),
                                            respondRequestResponse.getUser().get(i).getReid().getBloodGroupID(),
                                            respondRequestResponse.getUser().get(i).getReid().getPatientName(),
                                            respondRequestResponse.getUser().get(i).getReid().getUnits(),
                                            respondRequestResponse.getUser().get(i).getReid().getHospitalAddress(),
                                            respondRequestResponse.getUser().get(i).getReid().getDateTime(),
                                            respondRequestResponse.getUser().get(i).getId(),
                                            respondRequestResponse.getUser().get(i).getUserResStatus(),
                                            respondRequestResponse.getUser().get(i).getReid().getId(),
                                            respondRequestResponse.getUser().get(i).getProfilePicture(),
                                            respondRequestResponse.getUser().get(i).getUniqueID(),
                                            respondRequestResponse.getUser().get(i).getF_user().getFirstName(),
                                            respondRequestResponse.getUser().get(i).getF_user().getId(),
                                            respondRequestResponse.getUser().get(i).getReid().getAlternateName(),
                                            respondRequestResponse.getUser().get(i).getReid().getAlternateNumber(),
                                            respondRequestResponse.getUser().get(i).getReid().getHospitalLat(),
                                            respondRequestResponse.getUser().get(i).getReid().getHospitalLng()));
                                }
                                respondRequestAdapter = new RespondRequestAdapter(getActivity(), requestItemList, baseActivity, RespondRequestFragment.this);
                                listRespondtoReq.setAdapter(respondRequestAdapter);
                                Collections.sort(requestItemList, new Comparator<AuthorizerRequestItem>() {
                                    public int compare(AuthorizerRequestItem obj1, AuthorizerRequestItem obj2) {
                                        // ## Ascending order
                                        //                                return Integer.valueOf(obj2.getId()).compareTo(obj1.getId()); // To compare integer values
                                        // ## Descending order
                                        return obj2.getDate().compareToIgnoreCase(obj1.getDate()); // To compare string values
                                        // return Integer.valueOf(obj2.empId).compareTo(obj1.empId); // To compare integer values
                                    }
                                });
                            }

                        } else {
                            consEmptyList.setVisibility(View.VISIBLE);
                        }
                    }

                }

                @Override
                public void onFailure(Call<RespondReqResponse> call, Throwable t) {
                    if (isAdded()) {
                        hideProgress();
                        consEmptyList.setVisibility(View.VISIBLE);
                        Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }

                }
            });


        } else {
            if (isAdded()) {
                consEmptyList.setVisibility(View.VISIBLE);
//                userError.message = getActivity().getString(R.string.network_error_message);
//                showError(ERROR_SHOW_TYPE_DIALOG, userError);
            }
        }
    }
}
