package com.dci.falcon.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dci.falcon.R;
import com.dci.falcon.activity.NotificationActivity;
import com.dci.falcon.adapter.NoticationPageAdapter;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.firebase.FirebaseMessaging;
import com.dci.falcon.model.NotificationUpdate;
import com.dci.falcon.model.NotificationUpdateResponse;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.CustomViewPager;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.Utils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

/**
 * Created by vijayaganesh on 11/14/2017.
 */

public class NotificationFragment extends BaseFragment {
    @BindView(R.id.tab_layout_notification)
    TabLayout tabLayoutNotification;
    @BindView(R.id.pager_notification)
    CustomViewPager pagerNotification;
    @BindView(R.id.cons_notification)
    ConstraintLayout consNotification;
    Unbinder unbinder;
    NoticationPageAdapter noticationPageAdapter;
    AuthorizerRequestFragment authorizerRequestFragment;
    AcceptRequestFragment acceptRequestFragment;
    RespondRequestFragment respondRequestFragment;
    FloatingActionButton floatingActionButton;
    NotificationActivity notificationActivity;
    LinearLayout tabChild;
    @Inject
    FalconAPI falconAPI;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private NotificationUpdateResponse notificationUpdateResponse;
    private UserError userError;
    private String notificationID = "1";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        unbinder = ButterKnife.bind(this, view);
        getActivity().setTitle(getString(R.string.Notification));
        notificationActivity = (NotificationActivity) getActivity();
        notificationActivity.getSupportActionBar().show();
        FalconApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        userError = new UserError();
        Intent intent = getActivity().getIntent();
        if (getActivity().getIntent() != null) {
            notificationID = intent.getStringExtra("notificationType");
        }


        pagerNotification.setPagingEnabled(false);
        tabLayoutNotification.addTab(tabLayoutNotification.newTab().setText(getString(R.string.Respond_Req)));
        tabLayoutNotification.addTab(tabLayoutNotification.newTab().setText(getString(R.string.Accept_Req)));
        tabLayoutNotification.addTab(tabLayoutNotification.newTab().setText(getString(R.string.Authuorize_Req)));
        changeTabsFont();
        tabChild = ((LinearLayout) tabLayoutNotification.getChildAt(0));
        noticationPageAdapter = new NoticationPageAdapter(getActivity().getSupportFragmentManager(), tabLayoutNotification.getTabCount());
        pagerNotification.setAdapter(noticationPageAdapter);

        if (sharedPreferences.getInt(FalconConstants.AUTHORIZER, 0) == 1) {
            switch (notificationID != null ? notificationID : "1") {
                case "1":
                    //Incoming request
                    pagerNotification.setCurrentItem(0);
                    break;
                case "2":
                    //Outgoing request
                    pagerNotification.setCurrentItem(1);
                    break;
                case "3":
                    //Authorizer request
                    pagerNotification.setCurrentItem(2);
                    break;
                default:
                    pagerNotification.setCurrentItem(0);
            }
        } else {
            switch (notificationID) {
                case "1":
                    //Incoming request
                    pagerNotification.setCurrentItem(0);
                    break;
                case "2":
                    //Outgoing request
                    pagerNotification.setCurrentItem(1);
                    break;
                default:
                    pagerNotification.setCurrentItem(0);
            }
        }

//        pagerNotification.setCurrentItem(0);
        authorizerRequestFragment = (AuthorizerRequestFragment) noticationPageAdapter.instantiateItem(pagerNotification, 2);
        acceptRequestFragment = (AcceptRequestFragment) noticationPageAdapter.instantiateItem(pagerNotification, 1);
        respondRequestFragment = (RespondRequestFragment) noticationPageAdapter.instantiateItem(pagerNotification, 0);
        pagerNotification.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayoutNotification));

        updateNotification(0);


//        if (sharedPreferences.getInt(FalconConstants.AUTHORIZER, 0) == 1) {
////            Donor/Authorizer
//            if (sharedPreferences.getInt(FalconConstants.USERTYPE, 0) == 0) {
//                for (int i = 0; i < tabChild.getChildCount(); i++) {
//                    if (i == 2) {
//                        tabChild.getChildAt(i).setVisibility(View.VISIBLE);
//                        pagerNotification.setCurrentItem(0);
//                    }
//                }
//            } else {
//                // NonDonor/Authorizer
//
//                for (int i = 0; i < tabChild.getChildCount(); i++) {
//                    if (i == 0) {
//                        tabChild.getChildAt(i).setVisibility(View.GONE);
//                        pagerNotification.setCurrentItem(1);
//                    }
//                }
//            }
//
//        } else {
////            NonAuthorizer/Donor
//            if (sharedPreferences.getInt(FalconConstants.USERTYPE, 0) == 0) {
//                for (int i = 0; i < tabChild.getChildCount(); i++) {
//                    if (i == 2) {
//                        tabChild.getChildAt(i).setVisibility(View.GONE);
//                        pagerNotification.setCurrentItem(0);
//                    }
//                }
//            }
//            else {
//                //NonAuthorizer/NonDonor
//                for (int i = 0; i < tabChild.getChildCount(); i++) {
//                    if (i == 0) {
//                        tabChild.getChildAt(i).setVisibility(View.VISIBLE);
//                    }
//                }
//            }

//        }
//
//             NonAuth-0
//                    Auth-1
        if (sharedPreferences.getInt(FalconConstants.AUTHORIZER, 0) == 1) {
            for (int i = 0; i < tabChild.getChildCount(); i++) {
                if (i == 2) {
                    tabChild.getChildAt(i).setVisibility(View.VISIBLE);
                }
            }
        } else {
            for (int i = 0; i < tabChild.getChildCount(); i++) {
                if (i == 2) {
                    tabChild.getChildAt(i).setVisibility(View.GONE);
                }
            }
        }
        tabLayoutNotification.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pagerNotification.setCurrentItem(tab.getPosition());
                authorizerRequestFragment = (AuthorizerRequestFragment) noticationPageAdapter.instantiateItem(pagerNotification, 2);
                acceptRequestFragment = (AcceptRequestFragment) noticationPageAdapter.instantiateItem(pagerNotification, 1);
                respondRequestFragment = (RespondRequestFragment) noticationPageAdapter.instantiateItem(pagerNotification, 0);
                updateNotification(0);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                authorizerRequestFragment = (AuthorizerRequestFragment) noticationPageAdapter.instantiateItem(pagerNotification, 2);
                acceptRequestFragment = (AcceptRequestFragment) noticationPageAdapter.instantiateItem(pagerNotification, 1);
                respondRequestFragment = (RespondRequestFragment) noticationPageAdapter.instantiateItem(pagerNotification, 0);
                updateNotification(0);
            }


            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                authorizerRequestFragment = (AuthorizerRequestFragment) noticationPageAdapter.instantiateItem(pagerNotification, 2);
                acceptRequestFragment = (AcceptRequestFragment) noticationPageAdapter.instantiateItem(pagerNotification, 1);
                respondRequestFragment = (RespondRequestFragment) noticationPageAdapter.instantiateItem(pagerNotification, 0);
            }

        });

        floatingActionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab_request_blood);
        floatingActionButton.setVisibility(View.GONE);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notificationActivity.push(new RequestBloodFragment(), getString(R.string.Request_Blood));
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void changeTabsFont() {

        ViewGroup vg = (ViewGroup) tabLayoutNotification.getChildAt(0);
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "font/Montserrat-Regular.ttf");
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(font, Typeface.NORMAL);
                }
            }
        }
    }

    private void updateNotification(int status) {
        NotificationUpdate notificationUpdate = new NotificationUpdate();
        notificationUpdate.setStatus(status);
        if (sharedPreferences.getInt(FalconConstants.ROLEID, 0) == 0) {
            notificationUpdate.setUser_id(sharedPreferences.getInt(FalconConstants.USEDID, 0));
            notificationUpdate.setUser_type(1);
        } else {
            notificationUpdate.setUser_id(sharedPreferences.getInt(FalconConstants.HOSPITALUSERID, 0));
            notificationUpdate.setUser_type(0);
        }

        if (Utils.isNetworkAvailable()) {

            falconAPI.updateNotification(notificationUpdate).enqueue(new Callback<NotificationUpdateResponse>() {
                @Override
                public void onResponse(Call<NotificationUpdateResponse> call, Response<NotificationUpdateResponse> response) {
                    notificationUpdateResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && notificationUpdateResponse.getStatusCode() == 200) {
//                        notifyBilicking.setVisible(false);
//                        notify.setVisible(true);
                    }
                }

                @Override
                public void onFailure(Call<NotificationUpdateResponse> call, Throwable t) {

                }
            });

        } else {
            userError.message = getString(R.string.network_error_message);
            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }


    }
}
