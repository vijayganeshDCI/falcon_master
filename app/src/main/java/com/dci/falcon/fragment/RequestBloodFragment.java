package com.dci.falcon.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.model.RequestBlood;
import com.dci.falcon.model.RequestBloodResponse;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomButton;
import com.dci.falcon.view.CustomEditText;
import com.dci.falcon.view.CustomTextView;
import com.dci.falcon.view.CustomTextViewBold;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.tasks.OnSuccessListener;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

/**
 * Created by vijayaganesh on 11/6/2017.
 */

public class RequestBloodFragment extends BaseFragment
        implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    @BindView(R.id.spinner_request_blood_group)
    Spinner spinnerRequestBloodGroup;
    @BindView(R.id.view_request)
    View viewRequest;
    @BindView(R.id.edit_hospital_detail)
    CustomEditText editHospitalDetail;
    @BindView(R.id.text_input_hospital_detail)
    TextInputLayout textInputHospitalDetail;
    @BindView(R.id.edit_post_donate_date)
    CustomEditText editPostDonateDate;
    @BindView(R.id.text_input_post_donate_date)
    TextInputLayout textInputPostDonateDate;
    @BindView(R.id.edit_alternate_contact_name)
    CustomEditText editAlternateContactName;
    @BindView(R.id.text_input_alternate_contact_name)
    TextInputLayout textInputAlternateContactName;
    @BindView(R.id.edit_alternate_contact_number)
    CustomEditText editAlternateContactNumber;
    @BindView(R.id.text_input_alternate_contact_number)
    TextInputLayout textInputAlternateContactNumber;
    Unbinder unbinder;
    @BindView(R.id.spinner_blood_group)
    Spinner spinnerBloodGroup;
    @BindView(R.id.view_blood_group)
    View viewBloodGroup;
    @BindView(R.id.edit_post_unit)
    CustomEditText editPostUnit;
    @BindView(R.id.text_input_post_unit)
    TextInputLayout textInputPostUnit;
    @BindView(R.id.edit_post_receipt_name)
    CustomEditText editPostReceiptName;
    @BindView(R.id.text_input_post_receipt_name)
    TextInputLayout textInputPostReceiptName;
    @BindView(R.id.edit_post_patient_id)
    CustomEditText editPostPatientId;
    @BindView(R.id.text_input_patient_id)
    TextInputLayout textInputPatientId;
    @BindView(R.id.edit_last_post_med_con)
    CustomEditText editLastPostMedCon;
    @BindView(R.id.text_input_post_med_con)
    TextInputLayout textInputPostMedCon;
    @BindView(R.id.cons_req_blood)
    ConstraintLayout consReqBlood;
    @BindView(R.id.scroll_req_blood)
    ScrollView scrollReqBlood;
    @BindView(R.id.button_request_blood)
    CustomButton buttonRequestBlood;
    @BindView(R.id.image_icon_request_blood)
    ImageView imageIconRequestBlood;
    @BindView(R.id.text_label_request_blood)
    CustomTextViewBold textLabelRequestBlood;
    @BindView(R.id.text_label_request_requ)
    CustomTextView textLabelRequestRequ;
    @BindView(R.id.image_mantitory_4)
    ImageView imageMantitory4;
    @BindView(R.id.text_label_manditatory)
    CustomTextView textLabelManditatory;
    @BindView(R.id.image_mantitory)
    ImageView imageMantitory;
    @BindView(R.id.image_mantitory1)
    ImageView imageMantitory1;
    @BindView(R.id.image_mantitory2)
    ImageView imageMantitory2;
    private Calendar currentD, calendardate, calendarTime, currentT;
    private String donateDate, donateTime, donateDateFormat;
    SimpleDateFormat simpleDateFormat;
    private static final int PLACE_PICKER_REQUEST = 3;
    private static final long FASTEST_INTERVAL = 1000 * 5;
    private static final long INTERVAL = 1000 * 10;
    private LocationRequest mLocationRequest;
    Location mCurrentLocation;
    GoogleApiClient mGoogleApiClient;
    private double mLat, mLng;
    private double hosLat, hosLng;
    private LatLngBounds bounds;
    FloatingActionButton floatingActionButton;
    int REQUEST_STORAGE = 1;
    String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_SMS, Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    FalconAPI falconAPI;
    String bloodGroup;
    int reqTo;
    RequestBloodResponse requestBloodResponse;
    UserError userError;
    String quickRequestBloodGroupID, quickRequestPatientName;
    boolean isFromQuickRequest = false;
    private FusedLocationProviderClient mFusedLocationClient;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_request_blood, container, false);
        unbinder = ButterKnife.bind(this, view);
        FalconApplication.getContext().getComponent().inject(this);
        getActivity().setTitle(R.string.Request_Blood);
        floatingActionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab_request_blood);
        floatingActionButton.setVisibility(View.GONE);
        simpleDateFormat = new SimpleDateFormat();
        editor = sharedPreferences.edit();
        userError = new UserError();
        editPostReceiptName.setFilters(new InputFilter[]{editTextRestrictLength(), editTextRestrictSmileys(), editTextRestrictSpecialCharacters()});
        editAlternateContactName.setFilters(new InputFilter[]{editTextRestrictLength(), editTextRestrictSmileys(), editTextRestrictSpecialCharacters()});
        ArrayAdapter<String> bloodType = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item, getResources().getStringArray(R.array.blood_group));
        ArrayAdapter<String> postBloodType = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item, getResources().getStringArray(R.array.post_blood_group));
        spinnerRequestBloodGroup.setAdapter(bloodType);
        spinnerBloodGroup.setAdapter(postBloodType);
        initiaieGoogleApiClient();
        Bundle bundle = getArguments();
        if (bundle != null) {
            quickRequestBloodGroupID = bundle.getString("bloodGroup");
            quickRequestPatientName = bundle.getString("patientName");
            isFromQuickRequest = bundle.getBoolean("isFromQuestRequest");
        }
        if (isFromQuickRequest) {
            getActivity().setTitle(R.string.Request_Blood);
            for (int i = 0; i < bloodType.getCount(); i++) {
                if (bloodType.getItem(i).toString().equals(quickRequestBloodGroupID)) {
                    spinnerRequestBloodGroup.setSelection(i);
                    break;
                }
            }
            editPostReceiptName.setText(quickRequestPatientName);
        }
//        editPostUnit.addTextChangedListener(textWatcher);
//        editPostReceiptName.addTextChangedListener(textWatcher);
//        editHospitalDetail.addTextChangedListener(textWatcher);


        spinnerBloodGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (((TextView) adapterView.getChildAt(0)).getText().equals("Send Request to")) {
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.hint_grey));
                }
                reqTo = adapterView.getSelectedItemPosition();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerRequestBloodGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                if (((TextView) adapterView.getChildAt(0)).getText().equals("Select Blood Group")) {
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.hint_grey));
                }
                bloodGroup = adapterView.getSelectedItem().toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        calendardate = Calendar.getInstance();
        calendardate.set(Calendar.MONTH, monthOfYear);
        calendardate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        calendardate.set(Calendar.YEAR, year);
        simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        donateDate = simpleDateFormat.format(calendardate.getTime());
        Date dob = null;
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd-MMM-yyyy");
        try {
            dob = simpleDateFormat1.parse(donateDate);
            simpleDateFormat.applyPattern("yyyy-MM-dd");
            donateDateFormat = simpleDateFormat.format(dob);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        timePickerDialog();

    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        calendarTime = Calendar.getInstance();
        calendarTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendarTime.set(Calendar.MINUTE, minute);
        simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        donateTime = simpleDateFormat.format(calendarTime.getTime());
        editPostDonateDate.setText(donateDate + " " + donateTime);
    }

    private void datePickerDialog() {
        currentD = Calendar.getInstance();
        DatePickerDialog datePickerDialog =
                DatePickerDialog.newInstance(this, currentD.get(Calendar.YEAR), currentD.get(Calendar.MONTH),
                        currentD.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.setAccentColor(getResources().getColor(R.color.falcon_red));
        datePickerDialog.setMinDate(currentD);
        datePickerDialog.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }

    private void timePickerDialog() {
        currentT = Calendar.getInstance();
        TimePickerDialog timePickerDialog =
                TimePickerDialog.newInstance(this, currentT.get(Calendar.HOUR_OF_DAY), currentT.get(Calendar.MINUTE), currentT.get(Calendar.SECOND), false);
        timePickerDialog.setAccentColor(getResources().getColor(R.color.falcon_red));
        timePickerDialog.show(getActivity().getFragmentManager(), "Timepickerdialog");
    }

    @OnClick({R.id.edit_hospital_detail, R.id.edit_post_donate_date, R.id.button_request_blood})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.edit_hospital_detail:
                if (!hasLocationPermissionGranted()) {
                    requestPermission();
                } else {
                    pickYourPlace();
                }
                break;
            case R.id.edit_post_donate_date:
                datePickerDialog();
                break;
            case R.id.button_request_blood:

                if (isRequestNotNull()) {
                    AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(getActivity());
                    alertDialog1.setMessage(getString(R.string.alert_give_blood_req));
                    alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            requestBlood();
                        }
                    });
                    alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            hideProgress();
                        }
                    });
                    alertDialog1.show();

                } else {
//                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                    alertDialog.setMessage(getString(R.string.alert_mes_profile));
//                    alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            dialogInterface.dismiss();
//                            hideProgress();
//                        }
//                    });
//                    alertDialog.show();
                }


                break;
        }
    }

    private void initiaieGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity()).addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void startLocationUpdates() {

        if (getActivity() != null && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

//        mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//        if (mCurrentLocation != null) {
//            mLat = mCurrentLocation.getLatitude();
//            mLng = mCurrentLocation.getLongitude();
//        }

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        mFusedLocationClient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    mLat = location.getLatitude();
                    mLng = location.getLongitude();
                }
            }
        });

        double radiusDegrees = 0.001;
        LatLng center = new LatLng(mLat, mLng);
        LatLng northEast = new LatLng(center.latitude + radiusDegrees, center.longitude + radiusDegrees);
        LatLng southWest = new LatLng(center.latitude - radiusDegrees, center.longitude - radiusDegrees);
        bounds = LatLngBounds.builder()
                .include(northEast)
                .include(southWest)
                .build();
    }

    private void pickYourPlace() {
        try {
            if (bounds != null) {
                PlacePicker.IntentBuilder intentBuilder =
                        new PlacePicker.IntentBuilder();
                intentBuilder.setLatLngBounds(bounds);
                Intent intent = intentBuilder.build(getActivity());
                startActivityForResult(intent, PLACE_PICKER_REQUEST);
            }

        } catch (GooglePlayServicesRepairableException
                | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PLACE_PICKER_REQUEST
                && resultCode == Activity.RESULT_OK) {

            final Place place = PlacePicker.getPlace(getActivity(), data);
            final String name = place.getName().toString();
            final String address = place.getAddress().toString();
            String attributions = (String) place.getAttributions();
            if (attributions == null) {
                attributions = "";
            }
            hosLat = place.getLatLng().latitude;
            hosLng = place.getLatLng().longitude;
            if (hosLat != 0 && hosLng != 0){
                if (!name.contains("9"))
                    editHospitalDetail.setText(name + "," + address);
                else
                    editHospitalDetail.setText(address);
            }
            else
                editHospitalDetail.setText("");

        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    public boolean hasLocationPermissionGranted() {
        return ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, REQUEST_STORAGE);
        }
    }

    public void requestBlood() {
        final RequestBlood requestBlood = new RequestBlood();
        requestBlood.setUserType(sharedPreferences.getInt(FalconConstants.ROLEID, 0));
//        RoleID--0-Indiduval
//        RoleID--1-Hos
        if (sharedPreferences.getInt(FalconConstants.ROLEID, 0) == 0) {
            requestBlood.setUserID(sharedPreferences.getInt(FalconConstants.USEDID, 0));
        } else {
            requestBlood.setHosID(sharedPreferences.getInt(FalconConstants.HOSPITALUSERID, 0));
        }
        requestBlood.setBloodGroupID(bloodGroup);
        requestBlood.setUnits(Integer.parseInt(editPostUnit.getText().toString()));
        requestBlood.setPatientName(editPostReceiptName.getText().toString());
        requestBlood.setPatientID(editPostPatientId.getText().toString());
        requestBlood.setHospitalAddress(editHospitalDetail.getText().toString());
        requestBlood.setHospitalLat(String.valueOf(hosLat));
        requestBlood.setHospitalLng(String.valueOf(hosLng));
        requestBlood.setDateTime(donateDateFormat+" "+donateTime);
        requestBlood.setMedicalCondition(editLastPostMedCon.getText().toString());
        requestBlood.setReqType(reqTo);
        requestBlood.setAlternateName(editAlternateContactName.getText().toString());
        requestBlood.setAlternateNumber(editAlternateContactNumber.getText().toString());

        if (Utils.isNetworkAvailable()) {
            showProgress();
            falconAPI.requestBlood(requestBlood).enqueue(new Callback<RequestBloodResponse>() {
                @Override
                public void onResponse(Call<RequestBloodResponse> call, Response<RequestBloodResponse> response) {
                    hideProgress();
                    requestBloodResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && requestBloodResponse.getStatusCode() == 200) {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setMessage(getString(R.string.successfully_request));
                        alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                getActivity().onBackPressed();
                            }
                        });
                        alertDialog.show();
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<RequestBloodResponse> call, Throwable t) {
                    if (isAdded()) {
                        hideProgress();
                        Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else {
            userError.message = getActivity().getString(R.string.network_error_message);
            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }
    }

//    private void requestBloodValidation() {
//        if (!bloodGroup.contains("Select Blood Group") && reqTo != 0 && !editPostUnit.getText().toString().isEmpty()
//                && !editPostReceiptName.getText().toString().isEmpty() && !editHospitalDetail.getText().toString().isEmpty()) {
//            buttonRequestBlood.setEnabled(true);
//            buttonRequestBlood.setBackgroundColor(getResources().getColor(R.color.falcon_red));
//        } else {
//            buttonRequestBlood.setEnabled(false);
//            buttonRequestBlood.setBackgroundColor(getResources().getColor(R.color.light_grey));
//        }
//    }

    private boolean isRequestNotNull() {
        showErrorBloodGroup();
        showErrorSendBloodGroup();
        showErrorUnits();
        showErrorPatientName();
        showErrorHos();
        showErrorDate();
        if (showErrorBloodGroup1() && showErrorSendBloodGroup1() && showErrorUnits1()
                && showErrorPatientName1() && showErrorHos1() && showErrorDate1()) {
            return true;
        } else {
            return false;
        }
    }

    private boolean showErrorUnits() {
        if (editPostUnit.getText().toString().isEmpty()) {
            textInputPostUnit.setError(getString(R.string.req_units));
            return false;
        } else {
            textInputPostUnit.setErrorEnabled(false);
            return true;
        }
    }

    private boolean showErrorUnits1() {
        if (editPostUnit.getText().toString().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorPatientName() {

        if (editPostReceiptName.getText().toString().isEmpty()) {
            textInputPostReceiptName.setError(getString(R.string.req_patient_name));
            return false;
        } else {
            textInputPostReceiptName.setErrorEnabled(false);
            return true;
        }

    }

    private boolean showErrorPatientName1() {

        if (editPostReceiptName.getText().toString().isEmpty()) {
            return false;
        } else {
            return true;
        }

    }

    private boolean showErrorHos() {

        if (editHospitalDetail.getText().toString().isEmpty()) {
            textInputHospitalDetail.setError(getString(R.string.req_Location));
            return false;
        } else {
            textInputHospitalDetail.setErrorEnabled(false);
            return true;
        }

    }

    private boolean showErrorHos1() {

        if (editHospitalDetail.getText().toString().isEmpty()) {
            return false;
        } else {
            return true;
        }

    }

    private boolean showErrorDate() {

        if (editPostDonateDate.getText().toString().isEmpty()) {
            textInputPostDonateDate.setError(getString(R.string.req_date));
            return false;
        } else {
            textInputPostDonateDate.setErrorEnabled(false);
            return true;
        }

    }

    private boolean showErrorDate1() {

        if (editPostDonateDate.getText().toString().isEmpty()) {
            return false;
        } else {
            return true;
        }

    }

    private boolean showErrorBloodGroup() {
        if (bloodGroup.contains("Select Blood Group")) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setMessage(getString(R.string.req_Blood));
            alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            alertDialog.show();
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorBloodGroup1() {
        if (bloodGroup.contains("Select Blood Group")) {
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorSendBloodGroup() {
        if (reqTo == 0) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setMessage(getString(R.string.req_req_Blood));
            alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            alertDialog.show();
            return false;
        } else {
            return true;
        }
    }

    private boolean showErrorSendBloodGroup1() {
        if (reqTo == 0) {
            return false;
        } else {
            return true;
        }
    }

//    TextWatcher textWatcher = new TextWatcher() {
//        @Override
//        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//        }
//
//        @Override
//        public void onTextChanged(CharSequence s, int start, int before, int count) {
//            requestBloodValidation();
//        }
//
//        @Override
//        public void afterTextChanged(Editable s) {
//
//        }
//    };

//    @OnClick(R.id.button_request_blood)
//    public void onViewClicked() {
//    }
}
