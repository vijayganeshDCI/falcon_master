package com.dci.falcon.fragment;

import android.app.Activity;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.activity.LanguageActivity;
import com.dci.falcon.activity.MakeDonationActivity;

import org.json.JSONException;
import org.json.JSONObject;

import instamojo.library.InstamojoPay;
import instamojo.library.InstapayListener;


/**
 * Created by vijayaganesh on 11/10/2017.
 */

public class MakeDonationFragment extends BaseFragment {
    FloatingActionButton floatingActionButton;
    MakeDonationActivity makeDonationActivity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_make_donation, container, false);
        getActivity().setTitle(getString(R.string.Make_Donation));
        makeDonationActivity = (MakeDonationActivity) getActivity();
        floatingActionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab_request_blood);
        floatingActionButton.setVisibility(View.GONE);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeDonationActivity.push(new RequestBloodFragment(), getString(R.string.Request_Blood));
            }
        });

        callInstamojoPay("vijayaganesh.jeevanandham@dci.in", "8870410990", "10", "official", "vijay");
        return view;
    }


    private void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername) {
        final Activity activity = getActivity();
        InstamojoPay instamojoPay = new InstamojoPay();
        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        getActivity().registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", purpose);
            pay.put("amount", amount);
            pay.put("name", buyername);
            pay.put("send_sms", true);
            pay.put("send_email", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(activity, pay, listener);
    }

    InstapayListener listener;


    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {
                Toast.makeText(getActivity(), response, Toast.LENGTH_LONG)
                        .show();
            }

            @Override
            public void onFailure(int code, String reason) {
                Toast.makeText(getActivity(), "Failed: " + reason, Toast.LENGTH_LONG)
                        .show();
            }
        };
    }


}
