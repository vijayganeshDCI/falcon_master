package com.dci.falcon.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.activity.FacilitatedDonationActivity;
import com.dci.falcon.activity.HomeActivity;
import com.dci.falcon.adapter.QuickRequestAdapter;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.model.AdditionalUsersResponse;
import com.dci.falcon.model.FamilyMembersInfoItem;
import com.dci.falcon.model.TrackRequest;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomTextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

/**
 * Created by vijayaganesh on 1/16/2018.
 */

public class AddtionalUsersFragment extends BaseFragment {
    @BindView(R.id.image_empty_member)
    ImageView imageEmptyMember;
    @BindView(R.id.text_label_empty_member)
    CustomTextView textLabelEmptyMember;
    @BindView(R.id.cons_empty_list)
    ConstraintLayout consEmptyList;
    @BindView(R.id.list_additional_member)
    ListView listAdditionalMember;
    @BindView(R.id.swiperefresh_additional_mem)
    SwipeRefreshLayout swiperefreshAdditionalMem;
    @BindView(R.id.cons_additional_members)
    ConstraintLayout consAdditionalMembers;
    Unbinder unbinder;
    @Inject
    FalconAPI falconAPI;
    @Inject
    SharedPreferences sharedPreferences;
    UserError userError;
    List<FamilyMembersInfoItem> membersInfoItemList;
    AdditionalUsersResponse additionalUsersResponse;
    QuickRequestAdapter quickRequestAdapter;
    HomeActivity homeActivity;
    FacilitatedDonationActivity facilitatedDonationActivity;
    private FloatingActionButton floatingActionButton;
    private int memberID;

    @Override
    public void onResume() {
        super.onResume();
        getAdditionalUsers();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fac_don_back, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_bar_back:
                getActivity().finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_additional_users, container, false);
        unbinder = ButterKnife.bind(this, view);
        getActivity().setTitle(getString(R.string.fac_dontions));
        FalconApplication.getContext().getComponent().inject(this);
        userError = new UserError();
        setHasOptionsMenu(true);
        if (getActivity() instanceof HomeActivity) {
            homeActivity = (HomeActivity) getActivity();
            setHasOptionsMenu(false);

        } else {
            facilitatedDonationActivity = (FacilitatedDonationActivity) getActivity();
            setHasOptionsMenu(true);
            view.setFocusableInTouchMode(true);
            view.requestFocus();
            view.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        if (keyCode == KeyEvent.KEYCODE_BACK) {
                            return true;
                        }
                    }
                    return false;
                }
            });
        }
        membersInfoItemList = new ArrayList<FamilyMembersInfoItem>();
        consEmptyList.setVisibility(View.VISIBLE);
        swiperefreshAdditionalMem.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swiperefreshAdditionalMem.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isAdded()) {
                            getAdditionalUsers();
                            swiperefreshAdditionalMem.setRefreshing(false);
                        }
                    }
                }, 3000);
            }
        });

        if (getActivity() instanceof HomeActivity) {
            floatingActionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab_request_blood);
            floatingActionButton.setVisibility(View.GONE);
            floatingActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                notificationActivity.push(new RequestBloodFragment(), getString(R.string.Request_Blood));
                }
            });
        }
        listAdditionalMember.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                FamilyMembersInfoItem familyMembersInfoItem = (FamilyMembersInfoItem) adapterView.getItemAtPosition(position);
                memberID = familyMembersInfoItem.getId();
                FacililatedDonationDetailFragment facililatedDonationDetailFragment = new FacililatedDonationDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("memberID", memberID);
                facililatedDonationDetailFragment.setArguments(bundle);
                if (getActivity() instanceof HomeActivity)
                    homeActivity.push(facililatedDonationDetailFragment, getString(R.string.fac_dontions));
                else
                    facilitatedDonationActivity.push(facililatedDonationDetailFragment, getString(R.string.fac_dontions));
            }
        });


        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void getAdditionalUsers() {
        TrackRequest trackRequest = new TrackRequest();
        if (sharedPreferences.getInt(FalconConstants.ROLEID, 0) == 0)
            trackRequest.setId(sharedPreferences.getInt(FalconConstants.USEDID, 0));
        else
            trackRequest.setId(sharedPreferences.getInt(FalconConstants.HOSPITALUSERID, 0));

        if (Utils.isNetworkAvailable()) {
            showProgress();
            membersInfoItemList.clear();
            falconAPI.getAdditionalUsers(trackRequest).enqueue(new Callback<AdditionalUsersResponse>() {
                @Override
                public void onResponse(Call<AdditionalUsersResponse> call, Response<AdditionalUsersResponse> response) {
                    hideProgress();
                    additionalUsersResponse = response.body();
                    if (isAdded()) {
                        consEmptyList.setVisibility(View.GONE);
                        if (response.body() != null && response.isSuccessful() && additionalUsersResponse.getStatusCode() == 200) {
                            for (int i = 0; i < additionalUsersResponse.getResult().size(); i++) {
                                membersInfoItemList.add(new FamilyMembersInfoItem(additionalUsersResponse.getResult().get(i).getName(),
                                        additionalUsersResponse.getResult().get(i).getBloodGroupID(),
                                        additionalUsersResponse.getResult().get(i).getPhoneNumber(),
                                        additionalUsersResponse.getResult().get(i).getId(), "",
                                        additionalUsersResponse.getResult().get(i).getProfilePicture(),
                                        additionalUsersResponse.getResult().get(i).getBloodCount(),"",false));
                                quickRequestAdapter = new QuickRequestAdapter(getActivity(), membersInfoItemList, AddtionalUsersFragment.this);
                                listAdditionalMember.setAdapter(quickRequestAdapter);
                                Collections.sort(membersInfoItemList, new Comparator<FamilyMembersInfoItem>() {
                                    public int compare(FamilyMembersInfoItem obj1, FamilyMembersInfoItem obj2) {
                                        // ## Ascending order
                                        //                                return Integer.valueOf(obj2.getId()).compareTo(obj1.getId()); // To compare integer values
                                        // ## Descending order
                                        return obj1.getFirstName().compareToIgnoreCase(obj2.getFirstName()); // To compare string values
                                        // return Integer.valueOf(obj2.empId).compareTo(obj1.empId); // To compare integer values
                                    }
                                });
                            }

                        } else {
                            consEmptyList.setVisibility(View.VISIBLE);
                        }
                    }
                }

                @Override
                public void onFailure(Call<AdditionalUsersResponse> call, Throwable t) {
                    if (isAdded()) {
                        hideProgress();
                        consEmptyList.setVisibility(View.VISIBLE);
                        Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else {
            if (isAdded()) {
                consEmptyList.setVisibility(View.VISIBLE);
                userError.message = getActivity().getString(R.string.network_error_message);
                showError(ERROR_SHOW_TYPE_DIALOG, userError);
            }
        }
    }

}
