package com.dci.falcon.fragment;

import android.Manifest;
import android.content.Context;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.activity.BaseActivity;
import com.dci.falcon.adapter.BloodRequestListAdapter;
import com.dci.falcon.adapter.RespondRequestAdapter;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.model.AuthorizerRequestItem;
import com.dci.falcon.model.RequestList;
import com.dci.falcon.model.RequestListResponse;
import com.dci.falcon.model.RespondReqResponse;
import com.dci.falcon.model.TrackRequest;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomTextView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executor;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;
import static com.facebook.accountkit.internal.AccountKitController.getApplicationContext;
import static com.facebook.accountkit.internal.AccountKitController.getApplicationName;

public class BloodRequestListFragment extends BaseFragment implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {


    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    FalconAPI falconAPI;
    @BindView(R.id.image_empty_res_req)
    ImageView imageEmptyResReq;
    @BindView(R.id.text_label_res_req)
    CustomTextView textLabelResReq;
    @BindView(R.id.cons_empty_list)
    ConstraintLayout consEmptyList;
    @BindView(R.id.list_respond_to_req)
    ListView listRespondToReq;
    @BindView(R.id.swiperefresh_respond_req)
    SwipeRefreshLayout swiperefreshRespondReq;
    @BindView(R.id.cons_authorize_req)
    ConstraintLayout consAuthorizeReq;
    Unbinder unbinder;
    UserError userError;
    List<AuthorizerRequestItem> requestItemList;
    BloodRequestListAdapter respondRequestAdapter;
    RequestListResponse respondRequestResponse;
    BaseActivity baseActivity;
    private FloatingActionButton floatingActionButton;
    LocationManager locationManager;
    private String lat, lng;
    //Define a request code to send to Google Play services
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double currentLatitude;
    private double currentLongitude;
    private LocationCallback mLocationCallback;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_respond_request, container, false);
        unbinder = ButterKnife.bind(this, view);
        FalconApplication.getContext().getComponent().inject(this);
        userError = new UserError();
        baseActivity = (BaseActivity) getActivity();
        requestItemList = new ArrayList<AuthorizerRequestItem>();
        consEmptyList.setVisibility(View.VISIBLE);
        getActivity().setTitle(R.string.request_list_title);
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                // The next two lines tell the new client that “this” current class will handle connection stuff
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                //fourth line adds the LocationServices API endpoint from GooglePlayServices
                .addApi(LocationServices.API)
                .build();

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds
        swiperefreshRespondReq.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swiperefreshRespondReq.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isAdded()) {
                            getBloodRequest(currentLatitude, currentLongitude);
                            swiperefreshRespondReq.setRefreshing(false);
                        }
                    }
                }, 3000);
            }
        });
        floatingActionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab_request_blood);
        floatingActionButton.setVisibility(View.GONE);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                notificationActivity.push(new RequestBloodFragment(), getString(R.string.Request_Blood));
            }
        });

        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(),
                android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);

        }

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                for (Location location : locationResult.getLocations()) {
                    Log.i("MainActivity", "Location: " + location.getLatitude() + " " + location.getLongitude());

                }
            }

            ;

        };

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        //Now lets connect to the API
        mGoogleApiClient.connect();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.v(this.getClass().getSimpleName(), "onPause()");

        //Disconnect from API onPause()
        if (mGoogleApiClient.isConnected()) {
            LocationServices.getFusedLocationProviderClient(getActivity()).removeLocationUpdates(mLocationCallback);
            mGoogleApiClient.disconnect();
        }


    }

    /**
     * If connected get desinationLat and long
     */
    @Override
    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            //
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        FusedLocationProviderClient location = LocationServices.getFusedLocationProviderClient(getActivity());
        location.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {

                if (location == null) {
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                            getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    LocationServices.getFusedLocationProviderClient(getActivity()).
                            requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());

                } else {
                    //If everything went fine lets get latitude and longitude
                    currentLatitude = location.getLatitude();
                    currentLongitude = location.getLongitude();
                    getBloodRequest(currentLatitude, currentLongitude);
//                    Toast.makeText(getActivity(), currentLatitude + " WORKS " + currentLongitude + "", Toast.LENGTH_LONG).show();
                }

            }
        });


    }


    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(getActivity(), CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    /**
     * If locationChanges change desinationLat and long
     *
     * @param location
     */
    @Override
    public void onLocationChanged(Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();

        Toast.makeText(getActivity(), currentLatitude + " WORKS " + currentLongitude + "", Toast.LENGTH_LONG).show();
    }

    void getLocation() {
        try {
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void getBloodRequest(Double lat, Double lng) {
        final RequestList requestList = new RequestList();
        requestList.setLan(String.valueOf(currentLongitude));
        requestList.setLat(String.valueOf(currentLatitude));
        if (Utils.isNetworkAvailable()) {
            showProgress();
            requestItemList.clear();
            falconAPI.getBloodRequestList(requestList).enqueue(new Callback<RequestListResponse>() {
                @Override
                public void onResponse(Call<RequestListResponse> call, Response<RequestListResponse> response) {
                    if (isAdded()) {
                        hideProgress();
                        consEmptyList.setVisibility(View.GONE);
                        respondRequestResponse = response.body();
                        if (response.body() != null && response.isSuccessful() && respondRequestResponse.getStatusCode() == 200
                                && !respondRequestResponse.getUser().isEmpty()) {
                            for (int i = 0; i < respondRequestResponse.getUser().size(); i++) {
                                requestItemList.add(new AuthorizerRequestItem(
                                        respondRequestResponse.getUser().get(i).getT_user() != null ?
                                                respondRequestResponse.getUser().get(i).getT_user().getFirstName() :
                                                respondRequestResponse.getUser().get(i).getT_hspt().getName(),
                                        respondRequestResponse.getUser().get(i).getT_user() != null ?
                                                respondRequestResponse.getUser().get(i).getT_user().getPhoneNumber() :
                                                respondRequestResponse.getUser().get(i).getT_hspt().getPhoneNumber(),
                                        respondRequestResponse.getUser().get(i).getReid().getBloodGroupID(),
                                        respondRequestResponse.getUser().get(i).getReid().getPatientName(),
                                        respondRequestResponse.getUser().get(i).getReid().getUnits(),
                                        respondRequestResponse.getUser().get(i).getReid().getHospitalAddress(),
                                        respondRequestResponse.getUser().get(i).getReid().getDateTime(),
                                        respondRequestResponse.getUser().get(i).getId(),
                                        respondRequestResponse.getUser().get(i).getUserResStatus(),
                                        respondRequestResponse.getUser().get(i).getReid().getId(),
                                        respondRequestResponse.getUser().get(i).getT_user() != null ?
                                                respondRequestResponse.getUser().get(i).getT_user().getProfilePicture() :
                                                respondRequestResponse.getUser().get(i).getT_hspt().getPhoto(),
                                        respondRequestResponse.getUser().get(i).getUniqueID(), "", 0,
                                        respondRequestResponse.getUser().get(i).getReid().getAlternateName(),
                                        respondRequestResponse.getUser().get(i).getReid().getAlternateNumber(),
                                        respondRequestResponse.getUser().get(i).getReid().getHospitalLat(),
                                        respondRequestResponse.getUser().get(i).getReid().getHospitalLng()));
                                respondRequestAdapter = new BloodRequestListAdapter(getActivity(), requestItemList, baseActivity,
                                        BloodRequestListFragment.this);
                                listRespondToReq.setAdapter(respondRequestAdapter);
                                Collections.sort(requestItemList, new Comparator<AuthorizerRequestItem>() {
                                    public int compare(AuthorizerRequestItem obj1, AuthorizerRequestItem obj2) {
                                        // ## Ascending order
                                        //                                return Integer.valueOf(obj2.getId()).compareTo(obj1.getId()); // To compare integer values
                                        // ## Descending order
                                        return obj2.getDate().compareToIgnoreCase(obj1.getDate()); // To compare string values
                                        // return Integer.valueOf(obj2.empId).compareTo(obj1.empId); // To compare integer values
                                    }
                                });
                            }

                        } else {
                            consEmptyList.setVisibility(View.VISIBLE);
                        }
                    }

                }

                @Override
                public void onFailure(Call<RequestListResponse> call, Throwable t) {
                    if (isAdded()) {
                        hideProgress();
                        consEmptyList.setVisibility(View.VISIBLE);
                        Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }

                }
            });


        } else {
            if (isAdded()) {
                consEmptyList.setVisibility(View.VISIBLE);
                userError.message = getActivity().getString(R.string.network_error_message);
                showError(ERROR_SHOW_TYPE_DIALOG, userError);
            }
        }
    }

//    @Override
//    public void onLocationChanged(Location location) {
//
//        desinationLat = String.valueOf(location.getLatitude());
//        desitinationLng = String.valueOf(location.getLongitude());
//
//        try {
//            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
//            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
////            locationText.setText(locationText.getText() + "\n" + addresses.get(0).getAddressLine(0) + ", " +
////                    addresses.get(0).getAddressLine(1) + ", " + addresses.get(0).getAddressLine(2));
//        } catch (Exception e) {
//
//        }
//
//    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Toast.makeText(getActivity(), "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
