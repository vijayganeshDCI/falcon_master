package com.dci.falcon.fragment;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dci.falcon.R;
import com.dci.falcon.activity.HomeActivity;
import com.dci.falcon.adapter.NoticationPageAdapter;
import com.dci.falcon.adapter.OtherUsersAdapter;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.CustomViewPager;
import com.dci.falcon.utills.FalconConstants;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by vijayaganesh on 1/16/2018.
 */

public class FacililatedDonationFragment extends BaseFragment {
    @BindView(R.id.tab_layout_fac_donation)
    TabLayout tabLayoutFacDonation;
    @BindView(R.id.pager_facilitated_donation)
    CustomViewPager pagerFacilitatedDonation;
    @BindView(R.id.cons_fac_donation)
    ConstraintLayout consFacDonation;
    Unbinder unbinder;
    private HomeActivity homeActivity;
    @Inject
    FalconAPI falconAPI;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private LinearLayout tabChild;
    OtherUsersAdapter otherUsersAdapter;
    FamilyMembersDonationsFragment facililatedDonationFragment;
    AddtionalUsersFragment addtionalUsersFragment;
    private FloatingActionButton floatingActionButton;

    @Override
    public void onResume() {
        super.onResume();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_facilitated_donations, container, false);
        unbinder = ButterKnife.bind(this, view);
        getActivity().setTitle(getString(R.string.fac_dontions));
        homeActivity = (HomeActivity) getActivity();
        homeActivity.getSupportActionBar().show();
        FalconApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        pagerFacilitatedDonation.setPagingEnabled(true);
        tabLayoutFacDonation.addTab(tabLayoutFacDonation.newTab().setText(getString(R.string.Family_users)));
        tabLayoutFacDonation.addTab(tabLayoutFacDonation.newTab().setText(getString(R.string.Other_users)));
        changeTabsFont();
        tabChild = ((LinearLayout) tabLayoutFacDonation.getChildAt(0));
        otherUsersAdapter = new OtherUsersAdapter(getActivity().getSupportFragmentManager(), tabLayoutFacDonation.getTabCount());
        pagerFacilitatedDonation.setAdapter(otherUsersAdapter);
        pagerFacilitatedDonation.setCurrentItem(0);
        facililatedDonationFragment = (FamilyMembersDonationsFragment) otherUsersAdapter.instantiateItem(pagerFacilitatedDonation, 0);
        addtionalUsersFragment = (AddtionalUsersFragment) otherUsersAdapter.instantiateItem(pagerFacilitatedDonation, 1);
        pagerFacilitatedDonation.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayoutFacDonation));

        tabLayoutFacDonation.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pagerFacilitatedDonation.setCurrentItem(tab.getPosition());
                facililatedDonationFragment = (FamilyMembersDonationsFragment) otherUsersAdapter.instantiateItem(pagerFacilitatedDonation, 0);
                addtionalUsersFragment = (AddtionalUsersFragment) otherUsersAdapter.instantiateItem(pagerFacilitatedDonation, 1);

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        floatingActionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab_request_blood);
        floatingActionButton.setVisibility(View.GONE);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeActivity.push(new RequestBloodFragment(), getString(R.string.Request_Blood));
            }
        });

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void changeTabsFont() {

        ViewGroup vg = (ViewGroup) tabLayoutFacDonation.getChildAt(0);
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "font/Montserrat-Regular.ttf");
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(font, Typeface.NORMAL);
                }
            }
        }
    }
}
