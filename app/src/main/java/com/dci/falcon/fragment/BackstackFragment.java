package com.dci.falcon.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

/**
 * Base class for fragments that may push a child fragment.
 * Created by sdevabhaktuni on 10/5/16.
 */

public class BackstackFragment extends BaseFragment {
  public interface ActivityCallback {
    void onChildBackStackChanged(final int backCount);
  }
  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    getChildFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
      @Override
      public void onBackStackChanged() {
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        FragmentManager fragmentManager = getChildFragmentManager();
        final int childBackCount = fragmentManager.getBackStackEntryCount();
        int backCount = childBackCount;
        if (childBackCount == 0) {
          fragmentManager = getFragmentManager();
          backCount = fragmentManager.getBackStackEntryCount();
        }
        FragmentManager.BackStackEntry backStackEntry = fragmentManager.getBackStackEntryAt(backCount - 1);
        actionBar.setTitle(backStackEntry.getName());
        if (getActivity() instanceof ActivityCallback) {
          ((ActivityCallback) getActivity()).onChildBackStackChanged(childBackCount);
        }
      }
    });
  }

  @Override
  public boolean onBackPressed() {
    FragmentManager fragmentManager = getChildFragmentManager();
    if (fragmentManager.getBackStackEntryCount() > 0) {
      fragmentManager.popBackStack();
      return true;
    } else {
      return super.onBackPressed();
    }
  }
}