package com.dci.falcon.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.activity.HistoryActivity;
import com.dci.falcon.adapter.HistoryAdapter;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.model.GetUserInfo;
import com.dci.falcon.model.HistoryResponse;
import com.dci.falcon.model.HistoryResponseItem;
import com.dci.falcon.model.HospitalHistoryResponse;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomTextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

/**
 * Created by vijayaganesh on 11/10/2017.
 */

public class HistoryFragment extends BaseFragment {
    FloatingActionButton floatingActionButton;
    HistoryActivity historyActivity;
    @BindView(R.id.list_history)
    ListView listHistory;
    @BindView(R.id.cons_history)
    ConstraintLayout consHistory;
    Unbinder unbinder;
    HistoryAdapter historyAdapter;
    List<HistoryResponseItem> historyItemList;
    @Inject
    FalconAPI falconAPI;
    @Inject
    SharedPreferences sharedPreferences;
    HistoryResponse historyResponse;
    UserError userError;
    @BindView(R.id.swiperefresh_history)
    SwipeRefreshLayout swiperefreshHistory;
    @BindView(R.id.image_empty_member)
    ImageView imageEmptyMember;
    @BindView(R.id.text_label_empty_member)
    CustomTextView textLabelEmptyMember;
    @BindView(R.id.cons_empty_list)
    ConstraintLayout consEmptyList;
    HospitalHistoryResponse hospitalHistoryResponse;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        getActivity().setTitle(getString(R.string.History));
        historyActivity = (HistoryActivity) getActivity();
        FalconApplication.getContext().getComponent().inject(this);
        userError = new UserError();
        floatingActionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab_request_blood);
        floatingActionButton.setVisibility(View.GONE);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                historyActivity.push(new RequestBloodFragment(), getString(R.string.Request_Blood));
            }
        });
        unbinder = ButterKnife.bind(this, view);
        historyItemList = new ArrayList<HistoryResponseItem>();
        if (sharedPreferences.getInt(FalconConstants.ROLEID, 0) == 0)
            getIndiUserHistory();
        else
            getHospUserHistory();

        swiperefreshHistory.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swiperefreshHistory.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (sharedPreferences.getInt(FalconConstants.ROLEID, 0) == 0)
                            getIndiUserHistory();
                        else
                            getHospUserHistory();
                        swiperefreshHistory.setRefreshing(false);
                    }
                }, 3000);
            }
        });

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void getIndiUserHistory() {
        GetUserInfo getUserInfo = new GetUserInfo();
        getUserInfo.setUserID(sharedPreferences.getInt(FalconConstants.USEDID, 0));

        if (Utils.isNetworkAvailable()) {
            showProgress();
            historyItemList.clear();
            falconAPI.getUserHistory(getUserInfo).enqueue(new Callback<HistoryResponse>() {
                @Override
                public void onResponse(Call<HistoryResponse> call, Response<HistoryResponse> response) {
                    hideProgress();
                    historyResponse = response.body();
                    if (isAdded()) {
                        if (response.body() != null && response.isSuccessful() && historyResponse.getStatusCode() == 200
                                && historyResponse.getHistory().size()>0) {
                            consEmptyList.setVisibility(View.GONE);
                            for (int i = 0; i < historyResponse.getHistory().size(); i++) {
                                historyItemList.add(new HistoryResponseItem(historyResponse.getHistory().get(i).getId()
                                        , historyResponse.getHistory().get(i).getUserID()
                                        , historyResponse.getHistory().get(i).getDescription()
                                        , historyResponse.getHistory().get(i).getCreatedDate(), "", "", "", "", "", "", "", ""));
                            }
                            historyAdapter = new HistoryAdapter(historyItemList, getActivity());
                            listHistory.setAdapter(historyAdapter);
                            Collections.sort(historyItemList, new Comparator<HistoryResponseItem>() {
                                public int compare(HistoryResponseItem obj1, HistoryResponseItem obj2) {
                                    // ## Ascending order
    //                                return Integer.valueOf(obj2.getId()).compareTo(obj1.getId()); // To compare integer values
                                    // ## Descending order
                                    return obj2.getCreatedDate().compareToIgnoreCase(obj1.getCreatedDate()); // To compare string values
                                    // return Integer.valueOf(obj2.empId).compareTo(obj1.empId); // To compare integer values
                                }
                            });

                        } else {
                            consEmptyList.setVisibility(View.VISIBLE);

                        }
                    }

                }

                @Override
                public void onFailure(Call<HistoryResponse> call, Throwable t) {
                    if (isAdded()) {
                        consEmptyList.setVisibility(View.VISIBLE);
                        Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                        hideProgress();
                    }
                }
            });
        } else {
            if (isAdded()) {
                consEmptyList.setVisibility(View.VISIBLE);
                userError.message = getActivity().getString(R.string.network_error_message);
                showError(ERROR_SHOW_TYPE_DIALOG, userError);
            }
        }

    }

    private void getHospUserHistory() {
        GetUserInfo getUserInfo = new GetUserInfo();
        getUserInfo.setUserID(sharedPreferences.getInt(FalconConstants.HOSPITALUSERID, 0));
        if (Utils.isNetworkAvailable()) {
            showProgress();
            falconAPI.getHospitalHistory(getUserInfo).enqueue(new Callback<HospitalHistoryResponse>() {
                @Override
                public void onResponse(Call<HospitalHistoryResponse> call, Response<HospitalHistoryResponse> response) {
                    hideProgress();
                    hospitalHistoryResponse = response.body();
                    if (isAdded()) {
                        if (response.isSuccessful() && response.body() != null && hospitalHistoryResponse.getStatusCode() == 200 &&
                                !hospitalHistoryResponse.getUser().isEmpty() &&
                                hospitalHistoryResponse.getUser().size()>0) {
                            consEmptyList.setVisibility(View.GONE);
                            for (int i = 0; i < hospitalHistoryResponse.getUser().size(); i++) {
                                historyItemList.add(new HistoryResponseItem(0,
                                        0, "", "", hospitalHistoryResponse.getUser().get(i).getF_user().getProfilePicture(),
                                        hospitalHistoryResponse.getUser().get(i).getF_user().getFirstName(),
                                        hospitalHistoryResponse.getUser().get(i).getBd_req().getBloodGroupID(),
                                        hospitalHistoryResponse.getUser().get(i).getUnits(),
                                        hospitalHistoryResponse.getUser().get(i).getBd_req().getPatientName(),
                                        hospitalHistoryResponse.getUser().get(i).getCreateDate(),
                                        hospitalHistoryResponse.getUser().get(i).getF_user().getPhoneNumber(),
                                        hospitalHistoryResponse.getUser().get(i).getBd_req().getPatientID()));
                            }

                            historyAdapter = new HistoryAdapter(historyItemList, getActivity());
                            listHistory.setAdapter(historyAdapter);
                            Collections.sort(historyItemList, new Comparator<HistoryResponseItem>() {
                                public int compare(HistoryResponseItem obj1, HistoryResponseItem obj2) {
                                    // ## Ascending order
    //                                return Integer.valueOf(obj2.getId()).compareTo(obj1.getId()); // To compare integer values
                                    // ## Descending order
                                    return obj2.getDonateTime().compareToIgnoreCase(obj1.getDonateTime()); // To compare string values
                                    // return Integer.valueOf(obj2.empId).compareTo(obj1.empId); // To compare integer values
                                }
                            });

                        } else {
                            consEmptyList.setVisibility(View.VISIBLE);

                        }
                    }
                }

                @Override
                public void onFailure(Call<HospitalHistoryResponse> call, Throwable t) {
                    if (isAdded()) {
                        consEmptyList.setVisibility(View.VISIBLE);
                        Toast.makeText(getActivity(), getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                        hideProgress();
                    }
                }
            });
        } else {
            if (isAdded()) {
                consEmptyList.setVisibility(View.VISIBLE);
                userError.message = getActivity().getString(R.string.network_error_message);
                showError(ERROR_SHOW_TYPE_DIALOG, userError);
            }
        }
    }

}
