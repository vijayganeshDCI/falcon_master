package com.dci.falcon.utills;

/**
 * Created by vijayaganesh on 2/16/2018.
 */

public interface NotiificationListener {

    boolean isNotifyReached( );
}
