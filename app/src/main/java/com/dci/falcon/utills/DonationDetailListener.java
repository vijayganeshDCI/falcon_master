package com.dci.falcon.utills;

import com.dci.falcon.model.DoNotDisturbList;
import com.dci.falcon.model.DonationDetails;

/**
 * Created by vijayaganesh on 11/8/2017.
 */

public interface DonationDetailListener {

    public boolean isDonationNotNull();

    public DonationDetails interfaceDonationDetails();


    public String donNotDisturbDate();

    public void getDonationDetail(DonationDetails donationDetails);

}
