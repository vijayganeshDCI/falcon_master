package com.dci.falcon.utills;

/**
 * Created by vijayaganesh on 10/23/2017.
 */

public class FalconConstants {

    public static final String ISLOOGED = "isLogged";
    public static final String ADDRESS = "address";
    public static final String MAPADDRESS = "MapAdress";
    public static final String LOGINSTATUSCODE = "LoginStatusCode";
    public static final String ROLEID = "RoleID";
    public static final String DEVICEID = "DeviceID";
    public static final String APPID = "AppID";
    public static final String APPVERSION = "AppVersion";
    public static final String FCMTOKEN = "FcmToken";
    public static final String FCMKEYSHAREDPERFRENCES = "Fcmsharedperf";
    public static final String NOTIFICATIONPERFRENCES = "NOTIFICATIONPERFRENCES";
    public static final String NOTIFICATION = "NOTIFICATION";
    public static final String PHONENUMBER = "PhoneNumber";
    public static final String PROFILEPICTURE = "ProfilePicture";
    public static final String NEWPHONENUMBER = "NewPhoneNumber";
    public static final String OLDPHONENUMBER = "OldPhoneNumber";
    public static final String AUTHORIZER = "Authorizer";
    public static final String AUTHORIZERSTATUS = "AuthorizerStatus";
    public static final String USEDID = "UserId";
    public static final String HOSPITALUSERID = "HosUserId";
    public static final String HOSPITALREGISTERID = "HosRegisterId";
    public static final String INDIDUVALLIVES = "IndiduvalLives";
    public static final String HOSPUNITS = "HospUnits";
    public static final String HOSPLIVES = "HospLives";

    public static final String FALCONTOTALUNITS = "TotalUnits";
    public static final String INDIDUVALUNITS = "IndiduvalUnits";
    public static final String FACILITATEDDONATIONTOTALUNITS = "FACILITATEDDONATIONTOTALUNITS";

    public static final String HOSPTOTALUNITS = "HospTotalUnits";
    public static final String FACILITATEDHOSPDONATIONTOTALUNITS = "FACILITATEDHOSPDONATIONTOTALUNITS";

    public static final String BLOODGROUP = "BloodGroup";
    public static final String USERNAME = "userName";
    public static final String ORGID = "OrgID";
    public static final String SEQID = "SeqID";
    public static final String NOTNULL = "notnull";
    public static final String USERTYPE = "userType";
    public static final String REMAINIG_DAYS_TO_DONATE_BLOOD = "REMAINIG_DAYS_TO_DONATE_BLOOD";
}
