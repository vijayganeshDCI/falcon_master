package com.dci.falcon.utills;

import android.content.Context;

import com.dci.falcon.model.UserDetails;

/**
 * Created by vijayaganesh on 12/4/2017.
 */

public interface ProfileListener {

    UserDetails getUserDetails();
}
