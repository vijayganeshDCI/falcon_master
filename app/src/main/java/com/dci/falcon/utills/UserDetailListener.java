package com.dci.falcon.utills;

import android.content.Context;

import com.dci.falcon.fragment.UserDetailFragment;
import com.dci.falcon.model.UserDetails;

/**
 * Created by vijayaganesh on 11/6/2017.
 */

public interface UserDetailListener {

    public boolean isUserDetailNotNull();

    public UserDetails interfaceUserDetails();

    public void getUserDetails( UserDetails userDetails);



}
