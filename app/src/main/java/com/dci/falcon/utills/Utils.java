package com.dci.falcon.utills;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.dci.falcon.app.FalconApplication;

public class Utils {

    public static boolean isNetworkAvailable() {
        ConnectivityManager mConnectivityManager =
                (ConnectivityManager) FalconApplication.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = mConnectivityManager.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }
}
