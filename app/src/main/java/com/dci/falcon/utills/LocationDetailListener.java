package com.dci.falcon.utills;

import com.dci.falcon.model.LocationDetails;

/**
 * Created by vijayaganesh on 11/8/2017.
 */

public interface LocationDetailListener {
    public boolean isLocationNotNull();
    public LocationDetails interafceLocationDetails();
    public void getLocationDetail(LocationDetails locationDetails);


}
