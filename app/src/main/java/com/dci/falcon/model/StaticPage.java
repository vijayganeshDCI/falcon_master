package com.dci.falcon.model;

/**
 * Created by vijayaganesh on 11/30/2017.
 */

public class StaticPage {

    public String getPage_name() {
        return page_name;
    }

    public void setPage_name(String page_name) {
        this.page_name = page_name;
    }

    private String page_name;
}
