package com.dci.falcon.model;

import java.util.List;

/**
 * Created by vijayaganesh on 11/24/2017.
 */

public class DoNotDisturbList {

    private List<DonNotDisturbItem> DND;

    public List<DonNotDisturbItem> getDND() {
        return DND;
    }

    public void setDND(List<DonNotDisturbItem> DND) {
        this.DND = DND;
    }
}
