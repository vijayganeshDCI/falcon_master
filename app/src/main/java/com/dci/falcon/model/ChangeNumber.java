package com.dci.falcon.model;

/**
 * Created by vijayaganesh on 11/29/2017.
 */

public class ChangeNumber {

    public String getOld_phone_no() {
        return old_phone_no;
    }

    public void setOld_phone_no(String old_phone_no) {
        this.old_phone_no = old_phone_no;
    }

    public String getNew_phone_no() {
        return new_phone_no;
    }

    public void setNew_phone_no(String new_phone_no) {
        this.new_phone_no = new_phone_no;
    }

    private String old_phone_no;
    private String new_phone_no;


}
