package com.dci.falcon.model;

/**
 * Created by vijayaganesh on 12/21/2017.
 */

public class MemberDelete {

    public int getMember_id() {
        return member_id;
    }

    public void setMember_id(int member_id) {
        this.member_id = member_id;
    }

    private int member_id;
}
