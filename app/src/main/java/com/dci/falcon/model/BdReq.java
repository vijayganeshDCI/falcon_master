package com.dci.falcon.model;

public class BdReq{
	private int reqType;
	private int status;
	private String alternateNumber;
	private String medicalCondition;
	private String createdAt;
	private String patientName;
	private String alternateName;
	private int hosID;
	private String units;
	private String dateTime;
	private String hospitalLat;
	private int memberID;
	private String updatedAt;
	private int userID;
	private String bloodGroupID;
	private String patientID;
	private int id;
	private String hospitalAddress;
	private int userType;
	private String hospitalLng;
}
