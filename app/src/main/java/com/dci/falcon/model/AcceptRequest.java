package com.dci.falcon.model;

/**
 * Created by vijayaganesh on 1/12/2018.
 */

public class AcceptRequest {
    public int getUserID() {
        return UserID;
    }

    public void setUserID(int userID) {
        UserID = userID;
    }

    private int UserID;
    private int Type;

    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }
}
