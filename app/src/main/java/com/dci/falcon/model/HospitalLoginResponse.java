package com.dci.falcon.model;

public class HospitalLoginResponse {

    private String Status;
    private String Message;
    private int StatusCode;

    public int getTotal_user_blood_count() {
        return total_user_blood_count;
    }

    public void setTotal_user_blood_count(int total_user_blood_count) {
        this.total_user_blood_count = total_user_blood_count;
    }

    private int total_user_blood_count;


    public int getTotalUnits() {
        return totalUnits;
    }

    public void setTotalUnits(int totalUnits) {
        this.totalUnits = totalUnits;
    }

    private int totalUnits;
    private HospitalLoginUserinfo userinfo;

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int statusCode) {
        StatusCode = statusCode;
    }

    public HospitalLoginUserinfo getUserinfo() {
        return userinfo;
    }

    public void setUserinfo(HospitalLoginUserinfo userinfo) {
        this.userinfo = userinfo;
    }

    public float getTime_percentage() {
        return time_percentage;
    }

    public void setTime_percentage(float time_percentage) {
        this.time_percentage = time_percentage;
    }

    public float getPercentage() {
        return percentage;
    }

    public void setPercentage(float percentage) {
        this.percentage = percentage;
    }

    private float time_percentage;
    private float percentage;


}
