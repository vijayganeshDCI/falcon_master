package com.dci.falcon.model;

public class AuthorizerRequestHospitalItem {
    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public int getToID() {
        return ToID;
    }

    public void setToID(int toID) {
        ToID = toID;
    }

    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public HospitalLoginUserinfo getT_hspt() {
        return t_hspt;
    }

    public void setT_hspt(HospitalLoginUserinfo t_hspt) {
        this.t_hspt = t_hspt;
    }

    public int getFromID() {
        return FromID;
    }

    public void setFromID(int fromID) {
        FromID = fromID;
    }

    public int getRecord_from() {
        return record_from;
    }

    public void setRecord_from(int record_from) {
        this.record_from = record_from;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public int getUserResStatus() {
        return UserResStatus;
    }

    public void setUserResStatus(int userResStatus) {
        UserResStatus = userResStatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public RequestBloodInfo getReid() {
        return reid;
    }

    public void setReid(RequestBloodInfo reid) {
        this.reid = reid;
    }

    public String getCreatedtime() {
        return createdtime;
    }

    public void setCreatedtime(String createdtime) {
        createdtime = createdtime;
    }

    private int Status;
    private int ToID;
    private int Type;
    private String Message;
    private HospitalLoginUserinfo t_hspt;
    private int FromID;
    private int record_from;
    private String CreatedDate;
    private int UserResStatus;
    private int id;
    private RequestBloodInfo reid;
    private String createdtime;


}
