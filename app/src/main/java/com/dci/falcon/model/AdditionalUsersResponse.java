package com.dci.falcon.model;

import java.util.List;

public class AdditionalUsersResponse{
	public List<AdditionalUserItem> getResult() {
		return result;
	}

	public void setResult(List<AdditionalUserItem> result) {
		this.result = result;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public int getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(int statusCode) {
		StatusCode = statusCode;
	}

	private List<AdditionalUserItem> result;
	private String Status;
	private int StatusCode;
}