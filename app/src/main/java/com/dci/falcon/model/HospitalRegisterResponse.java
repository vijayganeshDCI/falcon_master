package com.dci.falcon.model;

public class HospitalRegisterResponse {
    private String Message;
    private int StatusCode;

    public int getTotalUnits() {
        return totalUnits;
    }

    public void setTotalUnits(int totalUnits) {
        this.totalUnits = totalUnits;
    }

    private int totalUnits;
    private HospitalRegisterUserInfo userinfo;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int statusCode) {
        StatusCode = statusCode;
    }

    public HospitalRegisterUserInfo getUserinfo() {
        return userinfo;
    }

    public void setUserinfo(HospitalRegisterUserInfo userinfo) {
        this.userinfo = userinfo;
    }


}
