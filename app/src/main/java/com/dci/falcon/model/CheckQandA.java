package com.dci.falcon.model;

/**
 * Created by vijayaganesh on 11/30/2017.
 */

public class CheckQandA {
    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getAns() {
        return ans;
    }

    public void setAns(String ans) {
        this.ans = ans;
    }

    private String phone_no;
    private String ans;

}
