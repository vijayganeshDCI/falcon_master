package com.dci.falcon.model;

import java.util.List;

public class RespondReqResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public List<RespondReqUserItem> getUser() {
		return User;
	}

	public void setUser(List<RespondReqUserItem> user) {
		User = user;
	}

	public int getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(int statusCode) {
		StatusCode = statusCode;
	}

	private String Status;
	private List<RespondReqUserItem> User;
	private int StatusCode;
}