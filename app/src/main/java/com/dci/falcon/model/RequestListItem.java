package com.dci.falcon.model;

public class RequestListItem {
	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public int getFromID() {
		return FromID;
	}

	public void setFromID(int fromID) {
		FromID = fromID;
	}

	public int getNofType() {
		return NofType;
	}

	public void setNofType(int nofType) {
		NofType = nofType;
	}

	public String getCreatedtime() {
		return createdtime;
	}

	public void setCreatedtime(String createdtime) {
		this.createdtime = createdtime;
	}

	public int getToID() {
		return ToID;
	}

	public void setToID(int toID) {
		ToID = toID;
	}

	public int getType() {
		return Type;
	}

	public void setType(int type) {
		Type = type;
	}

	public RequestList_User_Item getT_user() {
		return t_user;
	}

	public void setT_user(RequestList_User_Item t_user) {
		this.t_user = t_user;
	}

	public int getRecord_from() {
		return record_from;
	}

	public void setRecord_from(int record_from) {
		this.record_from = record_from;
	}

	public String getCreatedDate() {
		return CreatedDate;
	}

	public void setCreatedDate(String createdDate) {
		CreatedDate = createdDate;
	}

	public int getUserResStatus() {
		return UserResStatus;
	}

	public void setUserResStatus(int userResStatus) {
		UserResStatus = userResStatus;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public RequestBloodInfo getReid() {
		return reid;
	}

	public void setReid(RequestBloodInfo reid) {
		this.reid = reid;
	}

	public String getUniqueID() {
		return uniqueID;
	}

	public void setUniqueID(String uniqueID) {
		this.uniqueID = uniqueID;
	}

	private int Status;
	private String Message;
	private int FromID;
	private int NofType;
	private String createdtime;
	private int ToID;
	private int Type;
	private RequestList_User_Item t_user;

	public HospitalLoginUserinfo getT_hspt() {
		return t_hspt;
	}

	public void setT_hspt(HospitalLoginUserinfo t_hspt) {
		this.t_hspt = t_hspt;
	}

	private HospitalLoginUserinfo t_hspt;
	private int record_from;
	private String CreatedDate;
	private int UserResStatus;
	private int id;
	private RequestBloodInfo reid;
	private String uniqueID;
}
