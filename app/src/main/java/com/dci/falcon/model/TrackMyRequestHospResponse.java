package com.dci.falcon.model;

import java.util.List;

public class TrackMyRequestHospResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public List<TrackHospUserInfo> getUser() {
		return User;
	}

	public void setUser(List<TrackHospUserInfo> user) {
		User = user;
	}

	public int getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(int statusCode) {
		StatusCode = statusCode;
	}

	private String Status;
	private List<TrackHospUserInfo> User;
	private int StatusCode;
}