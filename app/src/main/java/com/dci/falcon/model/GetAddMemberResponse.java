package com.dci.falcon.model;

public class GetAddMemberResponse {
    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public IndiLoginUserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(IndiLoginUserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public MemberInfo getMemberInfo() {
        return memberInfo;
    }

    public void setMemberInfo(MemberInfo memberInfo) {
        this.memberInfo = memberInfo;
    }

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int statusCode) {
        StatusCode = statusCode;
    }

    private String Status;
    private IndiLoginUserInfo userInfo;
    private MemberInfo memberInfo;
    private int StatusCode;


}
