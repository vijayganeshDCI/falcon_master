package com.dci.falcon.model;

import java.util.List;

public class TrackMyRequestResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public List<TrackMyRequestUserItem> getUser() {
		return User;
	}

	public void setUser(List<TrackMyRequestUserItem> user) {
		User = user;
	}

	public int getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(int statusCode) {
		StatusCode = statusCode;
	}

	private String Status;
	private List<TrackMyRequestUserItem> User;
	private int StatusCode;
}