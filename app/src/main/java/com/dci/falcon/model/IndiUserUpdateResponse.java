package com.dci.falcon.model;

import java.util.List;

public class IndiUserUpdateResponse{
	public List<IndiUpdateDndInfoItem> getDndInfo() {
		return dndInfo;
	}

	public void setDndInfo(List<IndiUpdateDndInfoItem> dndInfo) {
		this.dndInfo = dndInfo;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public IndiUpdateUserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(IndiUpdateUserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public IndiUpdateOrgInfo getOrgInfo() {
		return orgInfo;
	}

	public void setOrgInfo(IndiUpdateOrgInfo orgInfo) {
		this.orgInfo = orgInfo;
	}

	public int getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(int statusCode) {
		StatusCode = statusCode;
	}

	public int getTotalUnits() {
		return totalUnits;
	}

	public void setTotalUnits(int totalUnits) {
		this.totalUnits = totalUnits;
	}

	private List<IndiUpdateDndInfoItem> dndInfo;
	private String Status;
	private IndiUpdateUserInfo userInfo;
	private String Message;
	private IndiUpdateOrgInfo orgInfo;
	private int StatusCode;
	private int totalUnits;
}