package com.dci.falcon.model;

public class StaticPageData {
	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getContent() {
		return Content;
	}

	public void setContent(String content) {
		Content = content;
	}

	public String getCreated_att() {
		return created_att;
	}

	public void setCreated_att(String created_att) {
		this.created_att = created_att;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPageName() {
		return PageName;
	}

	public void setPageName(String pageName) {
		PageName = pageName;
	}

	private int Status;
	private String updated_at;
	private String Content;
	private String created_att;
	private int id;
	private String PageName;


}
