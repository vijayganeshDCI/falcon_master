package com.dci.falcon.model;

public class TrackRequestBloodInfo {
	public int getReqType() {
		return ReqType;
	}

	public void setReqType(int reqType) {
		ReqType = reqType;
	}

	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}

	public String getAlternateNumber() {
		return AlternateNumber;
	}

	public void setAlternateNumber(String alternateNumber) {
		AlternateNumber = alternateNumber;
	}

	public String getMedicalCondition() {
		return MedicalCondition;
	}

	public void setMedicalCondition(String medicalCondition) {
		MedicalCondition = medicalCondition;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getPatientName() {
		return PatientName;
	}

	public void setPatientName(String patientName) {
		PatientName = patientName;
	}

	public String getAlternateName() {
		return AlternateName;
	}

	public void setAlternateName(String alternateName) {
		AlternateName = alternateName;
	}

	public int getHosID() {
		return HosID;
	}

	public void setHosID(int hosID) {
		HosID = hosID;
	}

	public String getUnits() {
		return Units;
	}

	public void setUnits(String units) {
		Units = units;
	}

	public String getDateTime() {
		return DateTime;
	}

	public void setDateTime(String dateTime) {
		DateTime = dateTime;
	}

	public String getHospitalLat() {
		return HospitalLat;
	}

	public void setHospitalLat(String hospitalLat) {
		HospitalLat = hospitalLat;
	}

	public int getMemberID() {
		return MemberID;
	}

	public void setMemberID(int memberID) {
		MemberID = memberID;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public int getUserID() {
		return UserID;
	}

	public void setUserID(int userID) {
		UserID = userID;
	}

	public String getBloodGroupID() {
		return BloodGroupID;
	}

	public void setBloodGroupID(String bloodGroupID) {
		BloodGroupID = bloodGroupID;
	}

	public String getPatientID() {
		return PatientID;
	}

	public void setPatientID(String patientID) {
		PatientID = patientID;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getHospitalAddress() {
		return HospitalAddress;
	}

	public void setHospitalAddress(String hospitalAddress) {
		HospitalAddress = hospitalAddress;
	}

	public int getUserType() {
		return UserType;
	}

	public void setUserType(int userType) {
		UserType = userType;
	}

	public String getHospitalLng() {
		return HospitalLng;
	}

	public void setHospitalLng(String hospitalLng) {
		HospitalLng = hospitalLng;
	}

	private int ReqType;
	private int Status;
	private String AlternateNumber;
	private String MedicalCondition;
	private String created_at;
	private String PatientName;
	private String AlternateName;
	private int HosID;
	private String Units;
	private String DateTime;
	private String HospitalLat;
	private int MemberID;
	private String updated_at;
	private int UserID;
	private String BloodGroupID;
	private String PatientID;
	private int id;
	private String HospitalAddress;
	private int UserType;
	private String HospitalLng;
}
