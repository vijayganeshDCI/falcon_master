package com.dci.falcon.model;

import java.util.List;

public class FacilitatedDonationDetailResponse {
    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public List<BloodReqIndItem> getBloodReqInd() {
        return bloodReqInd;
    }

    public void setBloodReqInd(List<BloodReqIndItem> bloodReqInd) {
        this.bloodReqInd = bloodReqInd;
    }

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int statusCode) {
        StatusCode = statusCode;
    }

    public List<BloodReqHosItem> getBloodReqHos() {
        return bloodReqHos;
    }

    public void setBloodReqHos(List<BloodReqHosItem> bloodReqHos) {
        this.bloodReqHos = bloodReqHos;
    }

    private String Status;
    private List<BloodReqIndItem> bloodReqInd;
    private int StatusCode;
    private List<BloodReqHosItem> bloodReqHos;
}