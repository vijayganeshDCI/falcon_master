package com.dci.falcon.model;

import java.util.List;

public class OrgResponse {
    private List<OrgListItem> orgList;
    private int StatusCode;
    private String status;

    public List<OrgListItem> getOrgList() {
        return orgList;
    }

    public void setOrgList(List<OrgListItem> orgList) {
        this.orgList = orgList;
    }

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int statusCode) {
        StatusCode = statusCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}