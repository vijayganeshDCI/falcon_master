package com.dci.falcon.model;

/**
 * Created by vijayaganesh on 12/6/2017.
 */

public class RequestBlood {

    public String getBloodGroupID() {
        return BloodGroupID;
    }

    public void setBloodGroupID(String bloodGroupID) {
        BloodGroupID = bloodGroupID;
    }

    public String getPatientName() {
        return PatientName;
    }

    public void setPatientName(String patientName) {
        PatientName = patientName;
    }

    public String getPatientID() {
        return PatientID;
    }

    public void setPatientID(String patientID) {
        PatientID = patientID;
    }

    public String getHospitalAddress() {
        return HospitalAddress;
    }

    public void setHospitalAddress(String hospitalAddress) {
        HospitalAddress = hospitalAddress;
    }

    public String getHospitalLat() {
        return HospitalLat;
    }

    public void setHospitalLat(String hospitalLat) {
        HospitalLat = hospitalLat;
    }

    public String getHospitalLng() {
        return HospitalLng;
    }

    public void setHospitalLng(String hospitalLng) {
        HospitalLng = hospitalLng;
    }

    public String getDateTime() {
        return DateTime;
    }

    public void setDateTime(String dateTime) {
        DateTime = dateTime;
    }

    public String getMedicalCondition() {
        return MedicalCondition;
    }

    public void setMedicalCondition(String medicalCondition) {
        MedicalCondition = medicalCondition;
    }

    public String getAlternateName() {
        return AlternateName;
    }

    public void setAlternateName(String alternateName) {
        AlternateName = alternateName;
    }

    public String getAlternateNumber() {
        return AlternateNumber;
    }

    public void setAlternateNumber(String alternateNumber) {
        AlternateNumber = alternateNumber;
    }

    public int getHosID() {
        return HosID;
    }

    public void setHosID(int hosID) {
        HosID = hosID;
    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int userID) {
        UserID = userID;
    }

    public int getUserType() {
        return UserType;
    }

    public void setUserType(int userType) {
        UserType = userType;
    }

    public int getReqType() {
        return ReqType;
    }

    public void setReqType(int reqType) {
        ReqType = reqType;
    }

    public int getUnits() {
        return Units;
    }

    public void setUnits(int units) {
        Units = units;
    }

    private String BloodGroupID;
    private String PatientName;
    private String PatientID;
    private String HospitalAddress;
    private String HospitalLat;
    private String HospitalLng;
    private String DateTime;
    private String MedicalCondition;
    private String AlternateName;
    private String AlternateNumber;
    private int HosID;
    private int UserID;
    private int UserType;
    private int ReqType;
    private int Units;



}
