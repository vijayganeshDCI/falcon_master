package com.dci.falcon.model;

public class AuthorizerRequestUserItem {
    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public int getToID() {
        return ToID;
    }

    public void setToID(int toID) {
        ToID = toID;
    }

    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public IndiLoginUserInfo getT_user() {
        return t_user;
    }

    public void setT_user(IndiLoginUserInfo t_user) {
        this.t_user = t_user;
    }

    public int getFromID() {
        return FromID;
    }

    public void setFromID(int fromID) {
        FromID = fromID;
    }

    public int getRecord_from() {
        return record_from;
    }

    public void setRecord_from(int record_from) {
        this.record_from = record_from;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public int getUserResStatus() {
        return UserResStatus;
    }

    public void setUserResStatus(int userResStatus) {
        UserResStatus = userResStatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public RequestBloodInfo getReid() {
        return reid;
    }

    public void setReid(RequestBloodInfo reid) {
        this.reid = reid;
    }

    public String getCreatedtime() {
        return createdtime;
    }

    public void setCreatedtime(String createdtime) {
        this.createdtime = createdtime;
    }

    private int Status;
    private int ToID;
    private int Type;
    private String Message;
    private IndiLoginUserInfo t_user;
    private int FromID;
    private int record_from;
    private String CreatedDate;
    private int UserResStatus;
    private int id;
    private RequestBloodInfo reid;
    private String createdtime;

    public String getUniqueID() {
        return uniqueID;
    }

    public void setUniqueID(String uniqueID) {
        this.uniqueID = uniqueID;
    }

    private String uniqueID;
}
