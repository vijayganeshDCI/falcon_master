package com.dci.falcon.model;

import java.util.List;

public class RequestListResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public List<RequestListItem> getUser() {
		return User;
	}

	public void setUser(List<RequestListItem> user) {
		User = user;
	}

	public int getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(int statusCode) {
		StatusCode = statusCode;
	}

	private String Status;
	private List<RequestListItem> User;
	private int StatusCode;
}