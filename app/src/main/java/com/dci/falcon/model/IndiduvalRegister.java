package com.dci.falcon.model;

import java.util.List;

/**
 * Created by vijayaganesh on 11/23/2017.
 */

public class IndiduvalRegister {

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private int id;

    private String DeviceID;
    private String AppVersion;
    private String AppID;
    private boolean NonsmartPhone;
    private int UserType;
    private String PhoneNumber;
    private String FirstName;
    private String LastName;
    private String BloodGroupID;
    private int Age;
    private String EmailID;
    private String Password;
    private String DOB;
    private int Gender;
    private int OrganizationType;
    private String OrgName;
    private String OrgPhoneNumber;
    private String OrgAddressDetail;
    private String OrgDetail;
    private String NearHospitalLocationHomeLat;
    private String NearHospitalLocationHomeLng;
    private String NearHospitalLocationOfficeLat;
    private String NearHospitalLocationOfficeLng;
    private String NearHospitalLocationFreLat;
    private String NearHospitalLocationFreLng;
    private String FCMKey;
    private int SecQuesID;
    private String SecQuesAns;
    private String ProfilePicture;
    private String NearHospitalLocationHome;
    private String NearHospitalLocationOffice;
    private String NearHospitalLocationFre;
    private String LastDonationDate;
    private int ActiveStatus;
    private int OrganizationID;

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int userID) {
        UserID = userID;
    }

    public int getAddUser() {
        return addUser;
    }

    public void setAddUser(int addUser) {
        this.addUser = addUser;
    }

    public int getReqUserType() {
        return ReqUserType;
    }

    public void setReqUserType(int reqUserType) {
        ReqUserType = reqUserType;
    }

    private int UserID;
    private int addUser;
    private int ReqUserType;

    public boolean isAuthorizerStatus() {
        return AuthorizerStatus;
    }

    public void setAuthorizerStatus(boolean authorizerStatus) {
        AuthorizerStatus = authorizerStatus;
    }

    public int getAuthorizer() {
        return Authorizer;
    }

    public void setAuthorizer(int authorizer) {
        Authorizer = authorizer;
    }

    private boolean AuthorizerStatus;
    private int Authorizer;
    /*private List<DonNotDisturbItem> DND;*/
    private String DND;

    public String getDonateAt() {
        return DonateAt;
    }

    public void setDonateAt(String donateAt) {
        DonateAt = donateAt;
    }

    private String DonateAt;

    public String getDND() {
        return DND;
    }

    public void setDND(String DND) {
        this.DND = DND;
    }

    public int getUserType() {
        return UserType;
    }

    public void setUserType(int userType) {
        UserType = userType;
    }

    public boolean isNonsmartPhone() {
        return NonsmartPhone;
    }

    public void setNonsmartPhone(boolean nonsmartPhone) {
        NonsmartPhone = nonsmartPhone;
    }

    public String getDeviceID() {
        return DeviceID;
    }

    public void setDeviceID(String deviceID) {
        DeviceID = deviceID;
    }

    public String getAppVersion() {
        return AppVersion;
    }

    public void setAppVersion(String appVersion) {
        AppVersion = appVersion;
    }

    public String getAppID() {
        return AppID;
    }

    public void setAppID(String appID) {
        AppID = appID;
    }


    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getBloodGroupID() {
        return BloodGroupID;
    }

    public void setBloodGroupID(String bloodGroupID) {
        BloodGroupID = bloodGroupID;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int age) {
        Age = age;
    }

    public String getEmailID() {
        return EmailID;
    }

    public void setEmailID(String emailID) {
        EmailID = emailID;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public int getGender() {
        return Gender;
    }

    public void setGender(int gender) {
        Gender = gender;
    }

    public int getOrganizationType() {
        return OrganizationType;
    }

    public void setOrganizationType(int organizationType) {
        OrganizationType = organizationType;
    }

    public String getOrgName() {
        return OrgName;
    }

    public void setOrgName(String orgName) {
        OrgName = orgName;
    }

    public String getOrgPhoneNumber() {
        return OrgPhoneNumber;
    }

    public void setOrgPhoneNumber(String orgPhoneNumber) {
        OrgPhoneNumber = orgPhoneNumber;
    }

    public String getOrgAddressDetail() {
        return OrgAddressDetail;
    }

    public void setOrgAddressDetail(String orgAddressDetail) {
        OrgAddressDetail = orgAddressDetail;
    }

    public String getOrgDetail() {
        return OrgDetail;
    }

    public void setOrgDetail(String orgDetail) {
        OrgDetail = orgDetail;
    }

    public String getNearHospitalLocationHomeLat() {
        return NearHospitalLocationHomeLat;
    }

    public void setNearHospitalLocationHomeLat(String nearHospitalLocationHomeLat) {
        NearHospitalLocationHomeLat = nearHospitalLocationHomeLat;
    }

    public String getNearHospitalLocationHomeLng() {
        return NearHospitalLocationHomeLng;
    }

    public void setNearHospitalLocationHomeLng(String nearHospitalLocationHomeLng) {
        NearHospitalLocationHomeLng = nearHospitalLocationHomeLng;
    }

    public String getNearHospitalLocationOfficeLat() {
        return NearHospitalLocationOfficeLat;
    }

    public void setNearHospitalLocationOfficeLat(String nearHospitalLocationOfficeLat) {
        NearHospitalLocationOfficeLat = nearHospitalLocationOfficeLat;
    }

    public String getNearHospitalLocationOfficeLng() {
        return NearHospitalLocationOfficeLng;
    }

    public void setNearHospitalLocationOfficeLng(String nearHospitalLocationOfficeLng) {
        NearHospitalLocationOfficeLng = nearHospitalLocationOfficeLng;
    }

    public String getNearHospitalLocationFreLat() {
        return NearHospitalLocationFreLat;
    }

    public void setNearHospitalLocationFreLat(String nearHospitalLocationFreLat) {
        NearHospitalLocationFreLat = nearHospitalLocationFreLat;
    }

    public String getNearHospitalLocationFreLng() {
        return NearHospitalLocationFreLng;
    }

    public void setNearHospitalLocationFreLng(String nearHospitalLocationFreLng) {
        NearHospitalLocationFreLng = nearHospitalLocationFreLng;
    }

    public String getFCMKey() {
        return FCMKey;
    }

    public void setFCMKey(String FCMKey) {
        this.FCMKey = FCMKey;
    }

    public int getSecQuesID() {
        return SecQuesID;
    }

    public void setSecQuesID(int secQuesID) {
        SecQuesID = secQuesID;
    }

    public String getSecQuesAns() {
        return SecQuesAns;
    }

    public void setSecQuesAns(String secQuesAns) {
        SecQuesAns = secQuesAns;
    }

    public String getProfilePicture() {
        return ProfilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        ProfilePicture = profilePicture;
    }

    public String getNearHospitalLocationHome() {
        return NearHospitalLocationHome;
    }

    public void setNearHospitalLocationHome(String nearHospitalLocationHome) {
        NearHospitalLocationHome = nearHospitalLocationHome;
    }

    public String getNearHospitalLocationOffice() {
        return NearHospitalLocationOffice;
    }

    public void setNearHospitalLocationOffice(String nearHospitalLocationOffice) {
        NearHospitalLocationOffice = nearHospitalLocationOffice;
    }

    public String getNearHospitalLocationFre() {
        return NearHospitalLocationFre;
    }

    public void setNearHospitalLocationFre(String nearHospitalLocationFre) {
        NearHospitalLocationFre = nearHospitalLocationFre;
    }

    public String getLastDonationDate() {
        return LastDonationDate;
    }

    public void setLastDonationDate(String lastDonationDate) {
        LastDonationDate = lastDonationDate;
    }

    public int isActiveStatus() {
        return ActiveStatus;
    }

    public void setActiveStatus(int activeStatus) {
        ActiveStatus = activeStatus;
    }

    public int getOrganizationID() {
        return OrganizationID;
    }

    public void setOrganizationID(int organizationID) {
        OrganizationID = organizationID;
    }


  /*  public List<DonNotDisturbItem> getDND() {
        return DND;
    }

    public void setDND(List<DonNotDisturbItem> DND) {
        this.DND = DND;
    }*/

}


