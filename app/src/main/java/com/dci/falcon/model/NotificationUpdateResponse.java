package com.dci.falcon.model;

public class NotificationUpdateResponse{
	public String getPending_notification() {
		return pending_notification;
	}

	public void setPending_notification(String pending_notification) {
		this.pending_notification = pending_notification;
	}

	public int getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(int statusCode) {
		StatusCode = statusCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	private String pending_notification;
	private int StatusCode;
	private String status;
}
