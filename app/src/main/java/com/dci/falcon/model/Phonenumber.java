package com.dci.falcon.model;

/**
 * Created by vijayaganesh on 1/3/2018.
 */

public class Phonenumber {

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    private String PhoneNumber;

    public String getFCMKey() {
        return FCMKey;
    }

    public void setFCMKey(String FCMKey) {
        this.FCMKey = FCMKey;
    }

    private String FCMKey;
}
