package com.dci.falcon.model;

import java.util.List;

public class TrackMyRequestDetailResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public List<TrackMyRequestDetailItem> getTrackMyRequestDetail() {
		return trackMyRequestDetail;
	}

	public void setTrackMyRequestDetail(List<TrackMyRequestDetailItem> trackMyRequestDetail) {
		this.trackMyRequestDetail = trackMyRequestDetail;
	}

	public int getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(int statusCode) {
		StatusCode = statusCode;
	}

	private String Status;
	private List<TrackMyRequestDetailItem> trackMyRequestDetail;
	private int StatusCode;
}