package com.dci.falcon.model;

import java.util.List;

/**
 * Created by vijayaganesh on 11/9/2017.
 */

public class DonationDetails {


    private boolean isActiveChecked;
    private String lastDonated;
    private int activeStatus;
    private String donateAt;

    public List<IndiLoginDNDInfo> getDndInfo() {
        return dndInfo;
    }

    public void setDndInfo(List<IndiLoginDNDInfo> dndInfo) {
        this.dndInfo = dndInfo;
    }

    private List<IndiLoginDNDInfo> dndInfo;

    public int getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(int activeStatus) {
        this.activeStatus = activeStatus;
    }

    public String getDonateAt() {
        return donateAt;
    }

    public void setDonateAt(String donateAt) {
        this.donateAt = donateAt;
    }


    public boolean isActiveChecked() {
        return isActiveChecked;
    }

    public void setActiveChecked(boolean activeChecked) {
        isActiveChecked = activeChecked;
    }

    public String getLastDonated() {
        return lastDonated;
    }

    public void setLastDonated(String lastDonated) {
        this.lastDonated = lastDonated;
    }


}
