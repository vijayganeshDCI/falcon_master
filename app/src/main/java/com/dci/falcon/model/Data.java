package com.dci.falcon.model;

public class Data{
	private String emailID;
	private String registerID;
	private String ownerName;
	private String address;
	private String lng;
	private String fCMKey;
	private String createdAt;
	private String name;
	private String appVersion;
	private String appID;
	private String updatedAt;
	private String lastlogin;
	private int id;
	private String password;
	private String deviceID;
	private String photo;
	private int verification;
	private String iMEI;
	private String city;
	private String ownerPhoneNumber;
	private String state;
	private String phoneNumber;
	private int country;
	private String lat;
	private int status;
}
