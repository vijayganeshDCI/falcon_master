package com.dci.falcon.model;

/**
 * Created by vijayaganesh on 12/13/2017.
 */

public class AuthorizerRequestItem {




    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getBloodgroup() {
        return bloodgroup;
    }

    public void setBloodgroup(String bloodgroup) {
        this.bloodgroup = bloodgroup;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public AuthorizerRequestItem(String name, String phoneNumber, String bloodgroup, String patientName, String units, String address, String date,
                                 int id,int status,int reidID,String profilePicture,String UniqueID,String toUserName,int userID,String alternateName,
                                 String alternatePhoneNumber,String lat,String lang) {
        Name = name;
        this.toUserName = toUserName;
        PhoneNumber = phoneNumber;
        this.bloodgroup = bloodgroup;
        this.patientName = patientName;
        this.units = units;
        this.address = address;
        this.date = date;
        this.id = id;
        this.status=status;
        this.reidID=reidID;
        this.ProfilePicture=profilePicture;
        this.uniqueID=UniqueID;
        this.userID=userID;
        this.alternateName=alternateName;
        this.alternatePhoneNumber=alternatePhoneNumber;
        this.lat=lat;
        this.lang=lang;
    }

    private String Name;
    private String PhoneNumber;
    private String bloodgroup;
    private String patientName;
    private String units;
    private String address;
    private String date;

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    private String lat;
    private String lang;

    public String getAlternateName() {
        return alternateName;
    }

    public void setAlternateName(String alternateName) {
        this.alternateName = alternateName;
    }

    public String getAlternatePhoneNumber() {
        return alternatePhoneNumber;
    }

    public void setAlternatePhoneNumber(String alternatePhoneNumber) {
        this.alternatePhoneNumber = alternatePhoneNumber;
    }

    private String alternateName;
    private String alternatePhoneNumber;

    public String getToUserName() {
        return toUserName;
    }

    public void setToUserName(String toUserName) {
        this.toUserName = toUserName;
    }

    private String toUserName;

    public String getUniqueID() {
        return uniqueID;
    }

    public void setUniqueID(String uniqueID) {
        this.uniqueID = uniqueID;
    }

    private String uniqueID;

    public String getProfilePicture() {
        return ProfilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        ProfilePicture = profilePicture;
    }

    private String ProfilePicture;
    private int id;

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    private int userID;





    public int getReidID() {
        return reidID;
    }

    public void setReidID(int reidID) {
        this.reidID = reidID;
    }

    private int reidID;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    private int status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


}
