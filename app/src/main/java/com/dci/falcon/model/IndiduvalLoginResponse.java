package com.dci.falcon.model;

import java.util.List;

public class IndiduvalLoginResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public List<IndiLoginDNDInfo> getDndInfo() {
		return dndInfo;
	}

	public void setDndInfo(List<IndiLoginDNDInfo> dndInfo) {
		this.dndInfo = dndInfo;
	}

	public IndiLoginUserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(IndiLoginUserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}



	public int getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(int statusCode) {
		StatusCode = statusCode;
	}

	private String Status;
	private List<IndiLoginDNDInfo> dndInfo;
	private IndiLoginUserInfo userInfo;
	private IndiLoginOrgInfo org;
	private int StatusCode;

	public int getTotalUnits() {
		return totalUnits;
	}

	public void setTotalUnits(int totalUnits) {
		this.totalUnits = totalUnits;
	}

	private int totalUnits;
	private String Message;

	public IndiLoginOrgInfo getOrg() {
		return org;
	}

	public void setOrg(IndiLoginOrgInfo org) {
		this.org = org;
	}


}