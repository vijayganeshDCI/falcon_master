package com.dci.falcon.model;

/**
 * Created by vijayaganesh on 1/5/2018.
 */

public class Logout {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    private int id;
    private int type;
}
