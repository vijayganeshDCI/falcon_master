package com.dci.falcon.model;

/**
 * Created by vijayaganesh on 1/9/2018.
 */

public class RejectRequest {

    public int getNofID() {
        return NofID;
    }

    public void setNofID(int nofID) {
        NofID = nofID;
    }

    private int NofID;
}
