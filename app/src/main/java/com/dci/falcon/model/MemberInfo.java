package com.dci.falcon.model;

public class MemberInfo{
	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}

	public int getMembersFromID() {
		return MembersFromID;
	}

	public void setMembersFromID(int membersFromID) {
		MembersFromID = membersFromID;
	}

	public int getReferenceMobileNumber() {
		return ReferenceMobileNumber;
	}

	public void setReferenceMobileNumber(int referenceMobileNumber) {
		ReferenceMobileNumber = referenceMobileNumber;
	}

	public String getRelationshipID() {
		return RelationshipID;
	}

	public void setRelationshipID(String relationshipID) {
		RelationshipID = relationshipID;
	}

	public String getCreatedDate() {
		return CreatedDate;
	}

	public void setCreatedDate(String createdDate) {
		CreatedDate = createdDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getMembersToID() {
		return MembersToID;
	}

	public void setMembersToID(int membersToID) {
		MembersToID = membersToID;
	}

	public int getNotifications() {
		return Notifications;
	}

	public void setNotifications(int notifications) {
		Notifications = notifications;
	}

	private int Status;
	private int MembersFromID;
	private int ReferenceMobileNumber;
	private String RelationshipID;
	private String CreatedDate;
	private int id;
	private int MembersToID;
	private int Notifications;

	}
