package com.dci.falcon.model;

/**
 * Created by vijayaganesh on 12/13/2017.
 */

public class AuthorizerRequestUpdate {

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserResStatus() {
        return UserResStatus;
    }

    public void setUserResStatus(int userResStatus) {
        UserResStatus = userResStatus;
    }

    private int id;
    private int UserResStatus;

    public int getBloodID() {
        return bloodID;
    }

    public void setBloodID(int bloodID) {
        this.bloodID = bloodID;
    }

    private int bloodID;


}
