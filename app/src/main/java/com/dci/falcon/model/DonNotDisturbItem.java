package com.dci.falcon.model;

public class DonNotDisturbItem {


    private String EndTime;
    private String StartTime;
    private String Day;

    public DonNotDisturbItem(String endTime, String startTime, String day) {
        EndTime = endTime;
        StartTime = startTime;
        Day = day;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getDay() {
        return Day;
    }

    public void setDay(String day) {
        Day = day;
    }

}
