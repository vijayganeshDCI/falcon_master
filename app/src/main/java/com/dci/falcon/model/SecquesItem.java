package com.dci.falcon.model;

public class SecquesItem {
    private int Status;
    private String created_at;
    private String Question;
    private int id;

    public SecquesItem(int status, String created_at, String question, int id) {
        Status = status;
        this.created_at = created_at;
        Question = question;
        this.id = id;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


}
