package com.dci.falcon.model;

/**
 * Created by vijayaganesh on 11/9/2017.
 */

public class LocationDetails {

    private String nearHome;
    private String nearOffice;
    private String nearFrequent;
    private String nearHomeLat;
    private String nearHomeLng;
    private String nearOfficeLat;
    private String nearOfficeLng;
    private String nearFrequentLat;
    private String nearFrequentLng;

    public String getNearHomeLat() {
        return nearHomeLat;
    }

    public void setNearHomeLat(String nearHomeLat) {
        this.nearHomeLat = nearHomeLat;
    }

    public String getNearHomeLng() {
        return nearHomeLng;
    }

    public void setNearHomeLng(String nearHomeLng) {
        this.nearHomeLng = nearHomeLng;
    }

    public String getNearOfficeLat() {
        return nearOfficeLat;
    }

    public void setNearOfficeLat(String nearOfficeLat) {
        this.nearOfficeLat = nearOfficeLat;
    }

    public String getNearOfficeLng() {
        return nearOfficeLng;
    }

    public void setNearOfficeLng(String nearOfficeLng) {
        this.nearOfficeLng = nearOfficeLng;
    }

    public String getNearFrequentLat() {
        return nearFrequentLat;
    }

    public void setNearFrequentLat(String nearFrequentLat) {
        this.nearFrequentLat = nearFrequentLat;
    }

    public String getNearFrequentLng() {
        return nearFrequentLng;
    }

    public void setNearFrequentLng(String nearFrequentLng) {
        this.nearFrequentLng = nearFrequentLng;
    }


    public String getNearHome() {
        return nearHome;
    }

    public void setNearHome(String nearHome) {
        this.nearHome = nearHome;
    }

    public String getNearOffice() {
        return nearOffice;
    }

    public void setNearOffice(String nearOffice) {
        this.nearOffice = nearOffice;
    }

    public String getNearFrequent() {
        return nearFrequent;
    }

    public void setNearFrequent(String nearFrequent) {
        this.nearFrequent = nearFrequent;
    }
}
