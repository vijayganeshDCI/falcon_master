package com.dci.falcon.model;

public class AddMemberResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public int getMember_id() {
		return member_id;
	}

	public void setMember_id(int member_id) {
		this.member_id = member_id;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public int getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(int statusCode) {
		StatusCode = statusCode;
	}

	public AddMemberUserInfo getUserinfo() {
		return userinfo;
	}

	public void setUserinfo(AddMemberUserInfo userinfo) {
		this.userinfo = userinfo;
	}

	private String Status;
	private int member_id;
	private String Message;
	private int StatusCode;
	private AddMemberUserInfo userinfo;


}
