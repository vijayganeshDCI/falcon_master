package com.dci.falcon.model;

public class IndiRegUserInfo {
    private String EmailID;
    private String NearHospitalLocationFreLat;
    private String FCMKey;
    private String created_at;
    private String Gender;
    private String ActiveStatus;
    private String AppVersion;
    private String AppID;
    private String updated_at;
    private String DOB;
    private String SecQuesID;
    private String BloodGroupID;
    private int Authorizer;
    private String NearHospitalLocationOffice;
    private String NearHospitalLocationHomeLng;
    private String NearHospitalLocationHome;
    private int id;
    private String Age;
    private String ProfilePicture;
    private String Password;
    private String NearHospitalLocationHomeLat;
    private String LastDonationDate;
    private int Status;
    private String DeviceID;
    private String FirstName;
    private String NearHospitalLocationOfficeLat;
    private String SecQuesAns;
    private String OrganizationID;
    private String OrganizationType;
    private String PhoneNumber;
    private String NearHospitalLocationOfficeLng;
    private String LastName;
    private String NearHospitalLocationFreLng;
    private int UserType;
    private String NearHospitalLocationFre;
    private String DonateAt;
    private String NonsmartPhone;

    public int getUnits() {
        return Units;
    }

    public void setUnits(int units) {
        Units = units;
    }

    public int getLives() {
        return Lives;
    }

    public void setLives(int lives) {
        Lives = lives;
    }

    private int Units;
    private int Lives;
    private int indUnits;

    public int getIndUnits() {
        return indUnits;
    }

    public void setIndUnits(int indUnits) {
        this.indUnits = indUnits;
    }


    public String getDonateAt() {
        return DonateAt;
    }

    public void setDonateAt(String donateAt) {
        DonateAt = donateAt;
    }

    public String getEmailID() {
        return EmailID;
    }

    public void setEmailID(String emailID) {
        EmailID = emailID;
    }

    public String getNearHospitalLocationFreLat() {
        return NearHospitalLocationFreLat;
    }

    public void setNearHospitalLocationFreLat(String nearHospitalLocationFreLat) {
        NearHospitalLocationFreLat = nearHospitalLocationFreLat;
    }

    public String getFCMKey() {
        return FCMKey;
    }

    public void setFCMKey(String FCMKey) {
        this.FCMKey = FCMKey;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getActiveStatus() {
        return ActiveStatus;
    }

    public void setActiveStatus(String activeStatus) {
        ActiveStatus = activeStatus;
    }

    public String getAppVersion() {
        return AppVersion;
    }

    public void setAppVersion(String appVersion) {
        AppVersion = appVersion;
    }

    public String getAppID() {
        return AppID;
    }

    public void setAppID(String appID) {
        AppID = appID;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getSecQuesID() {
        return SecQuesID;
    }

    public void setSecQuesID(String secQuesID) {
        SecQuesID = secQuesID;
    }

    public String getBloodGroupID() {
        return BloodGroupID;
    }

    public void setBloodGroupID(String bloodGroupID) {
        BloodGroupID = bloodGroupID;
    }

    public int getAuthorizer() {
        return Authorizer;
    }

    public void setAuthorizer(int authorizer) {
        Authorizer = authorizer;
    }

    public String getNearHospitalLocationOffice() {
        return NearHospitalLocationOffice;
    }

    public void setNearHospitalLocationOffice(String nearHospitalLocationOffice) {
        NearHospitalLocationOffice = nearHospitalLocationOffice;
    }

    public String getNearHospitalLocationHomeLng() {
        return NearHospitalLocationHomeLng;
    }

    public void setNearHospitalLocationHomeLng(String nearHospitalLocationHomeLng) {
        NearHospitalLocationHomeLng = nearHospitalLocationHomeLng;
    }

    public String getNearHospitalLocationHome() {
        return NearHospitalLocationHome;
    }

    public void setNearHospitalLocationHome(String nearHospitalLocationHome) {
        NearHospitalLocationHome = nearHospitalLocationHome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAge() {
        return Age;
    }

    public void setAge(String age) {
        Age = age;
    }

    public String getProfilePicture() {
        return ProfilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        ProfilePicture = profilePicture;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getNearHospitalLocationHomeLat() {
        return NearHospitalLocationHomeLat;
    }

    public void setNearHospitalLocationHomeLat(String nearHospitalLocationHomeLat) {
        NearHospitalLocationHomeLat = nearHospitalLocationHomeLat;
    }

    public String getLastDonationDate() {
        return LastDonationDate;
    }

    public void setLastDonationDate(String lastDonationDate) {
        LastDonationDate = lastDonationDate;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public String getDeviceID() {
        return DeviceID;
    }

    public void setDeviceID(String deviceID) {
        DeviceID = deviceID;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getNearHospitalLocationOfficeLat() {
        return NearHospitalLocationOfficeLat;
    }

    public void setNearHospitalLocationOfficeLat(String nearHospitalLocationOfficeLat) {
        NearHospitalLocationOfficeLat = nearHospitalLocationOfficeLat;
    }

    public String getSecQuesAns() {
        return SecQuesAns;
    }

    public void setSecQuesAns(String secQuesAns) {
        SecQuesAns = secQuesAns;
    }

    public String getOrganizationID() {
        return OrganizationID;
    }

    public void setOrganizationID(String organizationID) {
        OrganizationID = organizationID;
    }

    public String getOrganizationType() {
        return OrganizationType;
    }

    public void setOrganizationType(String organizationType) {
        OrganizationType = organizationType;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getNearHospitalLocationOfficeLng() {
        return NearHospitalLocationOfficeLng;
    }

    public void setNearHospitalLocationOfficeLng(String nearHospitalLocationOfficeLng) {
        NearHospitalLocationOfficeLng = nearHospitalLocationOfficeLng;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getNearHospitalLocationFreLng() {
        return NearHospitalLocationFreLng;
    }

    public void setNearHospitalLocationFreLng(String nearHospitalLocationFreLng) {
        NearHospitalLocationFreLng = nearHospitalLocationFreLng;
    }

    public int getUserType() {
        return UserType;
    }

    public void setUserType(int userType) {
        UserType = userType;
    }

    public String getNearHospitalLocationFre() {
        return NearHospitalLocationFre;
    }

    public void setNearHospitalLocationFre(String nearHospitalLocationFre) {
        NearHospitalLocationFre = nearHospitalLocationFre;
    }

    public String getNonsmartPhone() {
        return NonsmartPhone;
    }

    public void setNonsmartPhone(String nonsmartPhone) {
        NonsmartPhone = nonsmartPhone;
    }


}
