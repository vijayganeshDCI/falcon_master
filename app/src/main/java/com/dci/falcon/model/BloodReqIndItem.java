package com.dci.falcon.model;

public class BloodReqIndItem{
	public String getEmailID() {
		return EmailID;
	}

	public void setEmailID(String emailID) {
		EmailID = emailID;
	}

	public String getNearHospitalLocationFreLat() {
		return NearHospitalLocationFreLat;
	}

	public void setNearHospitalLocationFreLat(String nearHospitalLocationFreLat) {
		NearHospitalLocationFreLat = nearHospitalLocationFreLat;
	}

	public int getFromID() {
		return FromID;
	}

	public void setFromID(int fromID) {
		FromID = fromID;
	}

	public int getGender() {
		return Gender;
	}

	public void setGender(int gender) {
		Gender = gender;
	}

	public String getCreateDate() {
		return CreateDate;
	}

	public void setCreateDate(String createDate) {
		CreateDate = createDate;
	}

	public String getDateTime() {
		return DateTime;
	}

	public void setDateTime(String dateTime) {
		DateTime = dateTime;
	}

	public int getActiveStatus() {
		return ActiveStatus;
	}

	public void setActiveStatus(int activeStatus) {
		ActiveStatus = activeStatus;
	}

	public int getBrID() {
		return BrID;
	}

	public void setBrID(int brID) {
		BrID = brID;
	}

	public String getAppID() {
		return AppID;
	}

	public void setAppID(String appID) {
		AppID = appID;
	}

	public String getDOB() {
		return DOB;
	}

	public void setDOB(String DOB) {
		this.DOB = DOB;
	}

	public int getSecQuesID() {
		return SecQuesID;
	}

	public void setSecQuesID(int secQuesID) {
		SecQuesID = secQuesID;
	}

	public int getAuthorizer() {
		return Authorizer;
	}

	public void setAuthorizer(int authorizer) {
		Authorizer = authorizer;
	}

	public String getNearHospitalLocationHome() {
		return NearHospitalLocationHome;
	}

	public void setNearHospitalLocationHome(String nearHospitalLocationHome) {
		NearHospitalLocationHome = nearHospitalLocationHome;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}

	public int getReqType() {
		return ReqType;
	}

	public void setReqType(int reqType) {
		ReqType = reqType;
	}

	public int getNonsmartPhone() {
		return NonsmartPhone;
	}

	public void setNonsmartPhone(int nonsmartPhone) {
		NonsmartPhone = nonsmartPhone;
	}

	public String getPatientName() {
		return PatientName;
	}

	public void setPatientName(String patientName) {
		PatientName = patientName;
	}

	public int getOrganizationID() {
		return OrganizationID;
	}

	public void setOrganizationID(int organizationID) {
		OrganizationID = organizationID;
	}

	public String getUnits() {
		return Units;
	}

	public void setUnits(String units) {
		Units = units;
	}

	public String getHospitalLat() {
		return HospitalLat;
	}

	public void setHospitalLat(String hospitalLat) {
		HospitalLat = hospitalLat;
	}

	public String getScheduleTime() {
		return ScheduleTime;
	}

	public void setScheduleTime(String scheduleTime) {
		ScheduleTime = scheduleTime;
	}

	public String getPatientID() {
		return PatientID;
	}

	public void setPatientID(String patientID) {
		PatientID = patientID;
	}

	public String getLastLogin() {
		return LastLogin;
	}

	public void setLastLogin(String lastLogin) {
		LastLogin = lastLogin;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getNearHospitalLocationFreLng() {
		return NearHospitalLocationFreLng;
	}

	public void setNearHospitalLocationFreLng(String nearHospitalLocationFreLng) {
		NearHospitalLocationFreLng = nearHospitalLocationFreLng;
	}

	public int getUserType() {
		return UserType;
	}

	public void setUserType(int userType) {
		UserType = userType;
	}

	public String getHospitalLng() {
		return HospitalLng;
	}

	public void setHospitalLng(String hospitalLng) {
		HospitalLng = hospitalLng;
	}

	public String getNearHospitalLocationFre() {
		return NearHospitalLocationFre;
	}

	public void setNearHospitalLocationFre(String nearHospitalLocationFre) {
		NearHospitalLocationFre = nearHospitalLocationFre;
	}

	public String getDonateAt() {
		return DonateAt;
	}

	public void setDonateAt(String donateAt) {
		DonateAt = donateAt;
	}

	public String getFCMKey() {
		return FCMKey;
	}

	public void setFCMKey(String FCMKey) {
		this.FCMKey = FCMKey;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getAppVersion() {
		return AppVersion;
	}

	public void setAppVersion(String appVersion) {
		AppVersion = appVersion;
	}

	public int getToID() {
		return ToID;
	}

	public void setToID(int toID) {
		ToID = toID;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public int getUserID() {
		return UserID;
	}

	public void setUserID(int userID) {
		UserID = userID;
	}

	public String getBloodGroupID() {
		return BloodGroupID;
	}

	public void setBloodGroupID(String bloodGroupID) {
		BloodGroupID = bloodGroupID;
	}

	public String getNearHospitalLocationOffice() {
		return NearHospitalLocationOffice;
	}

	public void setNearHospitalLocationOffice(String nearHospitalLocationOffice) {
		NearHospitalLocationOffice = nearHospitalLocationOffice;
	}

	public String getNearHospitalLocationHomeLng() {
		return NearHospitalLocationHomeLng;
	}

	public void setNearHospitalLocationHomeLng(String nearHospitalLocationHomeLng) {
		NearHospitalLocationHomeLng = nearHospitalLocationHomeLng;
	}

	public int getLoginStatus() {
		return LoginStatus;
	}

	public void setLoginStatus(int loginStatus) {
		LoginStatus = loginStatus;
	}

	public String getNotes() {
		return Notes;
	}

	public void setNotes(String notes) {
		Notes = notes;
	}

	public int getAge() {
		return Age;
	}

	public void setAge(int age) {
		Age = age;
	}

	public String getProfilePicture() {
		return ProfilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		ProfilePicture = profilePicture;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public String getNearHospitalLocationHomeLat() {
		return NearHospitalLocationHomeLat;
	}

	public void setNearHospitalLocationHomeLat(String nearHospitalLocationHomeLat) {
		NearHospitalLocationHomeLat = nearHospitalLocationHomeLat;
	}

	public String getLastDonationDate() {
		return LastDonationDate;
	}

	public void setLastDonationDate(String lastDonationDate) {
		LastDonationDate = lastDonationDate;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getDeviceID() {
		return DeviceID;
	}

	public void setDeviceID(String deviceID) {
		DeviceID = deviceID;
	}

	public String getNearHospitalLocationOfficeLat() {
		return NearHospitalLocationOfficeLat;
	}

	public void setNearHospitalLocationOfficeLat(String nearHospitalLocationOfficeLat) {
		NearHospitalLocationOfficeLat = nearHospitalLocationOfficeLat;
	}

	public String getAlternateNumber() {
		return AlternateNumber;
	}

	public void setAlternateNumber(String alternateNumber) {
		AlternateNumber = alternateNumber;
	}

	public String getMedicalCondition() {
		return MedicalCondition;
	}

	public void setMedicalCondition(String medicalCondition) {
		MedicalCondition = medicalCondition;
	}

	public String getIMEI() {
		return IMEI;
	}

	public void setIMEI(String IMEI) {
		this.IMEI = IMEI;
	}

	public String getAlternateName() {
		return AlternateName;
	}

	public void setAlternateName(String alternateName) {
		AlternateName = alternateName;
	}

	public String getSecQuesAns() {
		return SecQuesAns;
	}

	public void setSecQuesAns(String secQuesAns) {
		SecQuesAns = secQuesAns;
	}

	public int getOrganizationType() {
		return OrganizationType;
	}

	public void setOrganizationType(int organizationType) {
		OrganizationType = organizationType;
	}

	public int getHosID() {
		return HosID;
	}

	public void setHosID(int hosID) {
		HosID = hosID;
	}

	public int getMemberID() {
		return MemberID;
	}

	public void setMemberID(int memberID) {
		MemberID = memberID;
	}

	public int getTravellingStatus() {
		return TravellingStatus;
	}

	public void setTravellingStatus(int travellingStatus) {
		TravellingStatus = travellingStatus;
	}

	public String getPhoneNumber() {
		return PhoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		PhoneNumber = phoneNumber;
	}

	public String getNearHospitalLocationOfficeLng() {
		return NearHospitalLocationOfficeLng;
	}

	public void setNearHospitalLocationOfficeLng(String nearHospitalLocationOfficeLng) {
		NearHospitalLocationOfficeLng = nearHospitalLocationOfficeLng;
	}

	public String getHospitalAddress() {
		return HospitalAddress;
	}

	public void setHospitalAddress(String hospitalAddress) {
		HospitalAddress = hospitalAddress;
	}

	public int getNofID() {
		return NofID;
	}

	public void setNofID(int nofID) {
		NofID = nofID;
	}

	private String EmailID;
	private String NearHospitalLocationFreLat;
	private int FromID;
	private int Gender;
	private String CreateDate;
	private String DateTime;
	private int ActiveStatus;
	private int BrID;
	private String AppID;
	private String DOB;
	private int SecQuesID;
	private int Authorizer;
	private String NearHospitalLocationHome;
	private int id;
	private int Status;
	private int ReqType;
	private int NonsmartPhone;
	private String PatientName;
	private int OrganizationID;
	private String Units;
	private String HospitalLat;
	private String ScheduleTime;
	private String PatientID;
	private String LastLogin;
	private String LastName;
	private String NearHospitalLocationFreLng;
	private int UserType;
	private String HospitalLng;
	private String NearHospitalLocationFre;
	private String DonateAt;
	private String FCMKey;
	private String created_at;
	private String AppVersion;
	private int ToID;
	private String updated_at;
	private int UserID;
	private String BloodGroupID;
	private String NearHospitalLocationOffice;
	private String NearHospitalLocationHomeLng;
	private int LoginStatus;
	private String Notes;
	private int Age;
	private String ProfilePicture;
	private String Password;
	private String NearHospitalLocationHomeLat;
	private String LastDonationDate;
	private String FirstName;
	private String DeviceID;
	private String NearHospitalLocationOfficeLat;
	private String AlternateNumber;
	private String MedicalCondition;
	private String IMEI;
	private String AlternateName;
	private String SecQuesAns;
	private int OrganizationType;
	private int HosID;
	private int MemberID;
	private int TravellingStatus;
	private String PhoneNumber;
	private String NearHospitalLocationOfficeLng;
	private String HospitalAddress;
	private int NofID;
}
