package com.dci.falcon.model;

/**
 * Created by vijayaganesh on 11/22/2017.
 */

public class HospitalLogin {

    private String RegisterID;
    private String Password;

    public String getFCMKey() {
        return FCMKey;
    }

    public void setFCMKey(String FCMKey) {
        this.FCMKey = FCMKey;
    }

    private String FCMKey;

    public String getRegisterID() {
        return RegisterID;
    }

    public void setRegisterID(String registerID) {
        RegisterID = registerID;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }
}
