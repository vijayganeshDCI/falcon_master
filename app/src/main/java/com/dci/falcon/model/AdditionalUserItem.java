package com.dci.falcon.model;

public class AdditionalUserItem {
	public String getBloodGroupID() {
		return BloodGroupID;
	}

	public void setBloodGroupID(String bloodGroupID) {
		BloodGroupID = bloodGroupID;
	}

	public String getPhoneNumber() {
		return PhoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		PhoneNumber = phoneNumber;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProfilePicture() {
		return ProfilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		ProfilePicture = profilePicture;
	}

	public int getBloodCount() {
		return bloodCount;
	}

	public void setBloodCount(int bloodCount) {
		this.bloodCount = bloodCount;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	private String BloodGroupID;
	private String PhoneNumber;
	private int id;
	private String ProfilePicture;
	private int bloodCount;
	private String Name;
}
