package com.dci.falcon.model;

public class QuickRegistrationResponse{
	public String getDndInfo() {
		return dndInfo;
	}

	public void setDndInfo(String dndInfo) {
		this.dndInfo = dndInfo;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public IndiRegUserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(IndiRegUserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public String getOrgInfo() {
		return orgInfo;
	}

	public void setOrgInfo(String orgInfo) {
		this.orgInfo = orgInfo;
	}

	public int getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(int statusCode) {
		StatusCode = statusCode;
	}

	public int getTotalUnits() {
		return totalUnits;
	}

	public void setTotalUnits(int totalUnits) {
		this.totalUnits = totalUnits;
	}

	private String dndInfo;
	private String Status;
	private IndiRegUserInfo userInfo;
	private String Message;
	private String orgInfo;
	private int StatusCode;
	private int totalUnits;
}
