package com.dci.falcon.model;

public class UpdateMemberResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getMember_id() {
		return member_id;
	}

	public void setMember_id(String member_id) {
		this.member_id = member_id;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public int getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(int statusCode) {
		StatusCode = statusCode;
	}

	public IndiLoginUserInfo getUserinfo() {
		return userinfo;
	}

	public void setUserinfo(IndiLoginUserInfo userinfo) {
		this.userinfo = userinfo;
	}

	private String Status;
	private String member_id;
	private String Message;
	private int StatusCode;
	private IndiLoginUserInfo userinfo;


}
