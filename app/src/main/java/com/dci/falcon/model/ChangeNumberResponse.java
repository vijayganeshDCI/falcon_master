package com.dci.falcon.model;

import java.util.List;

public class ChangeNumberResponse {
    private List<ChangeNumberDataItem> data;

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int statusCode) {
        StatusCode = statusCode;
    }

    private int StatusCode;
    private String status;

    public void setData(List<ChangeNumberDataItem> data) {
        this.data = data;
    }

    public List<ChangeNumberDataItem> getData() {
        return data;
    }


    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }


}