package com.dci.falcon.model;

public class CheckSeqQandAResponse {
    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int statusCode) {
        StatusCode = statusCode;
    }

    private int StatusCode;
    private String status;


    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
