package com.dci.falcon.model;

/**
 * Created by vijayaganesh on 12/4/2017.
 */

public class GetUserInfo {

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int userID) {
        UserID = userID;
    }

    private int UserID;
}
