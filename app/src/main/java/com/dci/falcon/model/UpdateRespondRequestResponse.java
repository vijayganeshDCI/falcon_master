package com.dci.falcon.model;

public class UpdateRespondRequestResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public int getToID() {
		return ToID;
	}

	public void setToID(int toID) {
		ToID = toID;
	}

	public int getType() {
		return Type;
	}

	public void setType(int type) {
		Type = type;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public int getFromID() {
		return FromID;
	}

	public void setFromID(int fromID) {
		FromID = fromID;
	}

	public int getRecord_from() {
		return record_from;
	}

	public void setRecord_from(int record_from) {
		this.record_from = record_from;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public int getUserResStatus() {
		return UserResStatus;
	}

	public void setUserResStatus(int userResStatus) {
		UserResStatus = userResStatus;
	}

	public int getNofType() {
		return NofType;
	}

	public void setNofType(int nofType) {
		NofType = nofType;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(int statusCode) {
		StatusCode = statusCode;
	}

	public String getCreatedtime() {
		return createdtime;
	}

	public void setCreatedtime(String createdtime) {
		this.createdtime = createdtime;
	}

	private String Status;
	private int ToID;
	private int Type;
	private String Message;
	private int FromID;
	private int record_from;
	private String createdDate;
	private int UserResStatus;
	private int NofType;
	private int id;
	private int StatusCode;
	private String createdtime;


}
