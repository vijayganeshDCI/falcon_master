package com.dci.falcon.model;

/**
 * Created by vijayaganesh on 1/5/2018.
 */

public class TrackMyRequestItem {

    public TrackMyRequestItem(String patientName, String bloodUnits, String bloodGroup,String hospAddress, String dateTime, int id,int status) {
        this.patientName = patientName;
        this.bloodUnits = bloodUnits;
        this.bloodGroup = bloodGroup;
        this.hospAddress = hospAddress;
        this.dateTime = dateTime;
        this.id = id;
        this.Status=status;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getBloodUnits() {
        return bloodUnits;
    }

    public void setBloodUnits(String bloodUnits) {
        this.bloodUnits = bloodUnits;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }


    public String getHospAddress() {
        return hospAddress;
    }

    public void setHospAddress(String hospAddress) {
        this.hospAddress = hospAddress;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private String patientName;
    private String bloodUnits;
    private String bloodGroup;
    private String hospAddress;
    private String dateTime;
    private int id;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    private int Status;
}
