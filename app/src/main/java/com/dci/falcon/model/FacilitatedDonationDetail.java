package com.dci.falcon.model;

/**
 * Created by vijayaganesh on 1/8/2018.
 */

public class FacilitatedDonationDetail {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getLocations() {
        return locations;
    }

    public void setLocations(String locations) {
        this.locations = locations;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public FacilitatedDonationDetail(String name, String profilePicture, String bloodGroup, String units, String locations, String date, String phoneNumber,String patientID,String requestorname) {
        this.name = name;
        this.profilePicture = profilePicture;
        this.bloodGroup = bloodGroup;
        this.units = units;
        this.locations = locations;
        this.date = date;
        this.phoneNumber = phoneNumber;
        this.patientID=patientID;
        this.Requestorname=requestorname;
    }

    public String getRequestorname() {
        return Requestorname;
    }

    public void setRequestorname(String requestorname) {
        Requestorname = requestorname;
    }

    public String getPatientID() {
        return patientID;
    }

    public void setPatientID(String patientID) {
        this.patientID = patientID;
    }

    private String name;
    private String Requestorname;
    private String profilePicture;
    private String bloodGroup;
    private String units;
    private String locations;
    private String date;
    private String phoneNumber;
    private String patientID;

    public String getPatientName() {
        return patientID;
    }

    public void setPatientName(String patientName) {
        this.patientID = patientName;
    }
}
