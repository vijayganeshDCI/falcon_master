package com.dci.falcon.model;

/**
 * Created by vijayaganesh on 1/8/2018.
 */

public class UserActivityUpdate {

    public String getUniqueID() {
        return uniqueID;
    }

    public void setUniqueID(String uniqueID) {
        this.uniqueID = uniqueID;
    }

    public int getUnits() {
        return units;
    }

    public void setUnits(int units) {
        this.units = units;
    }

    private String uniqueID;
    private int units;
}
