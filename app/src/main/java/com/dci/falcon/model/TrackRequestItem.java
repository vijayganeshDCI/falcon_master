package com.dci.falcon.model;

/**
 * Created by vijayaganesh on 11/14/2017.
 */

public class TrackRequestItem {


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBloodGroupId() {
        return bloodGroupId;
    }

    public void setBloodGroupId(String bloodGroupId) {
        this.bloodGroupId = bloodGroupId;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getDonorName() {
        return donorName;
    }

    public void setDonorName(String donorName) {
        this.donorName = donorName;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }


    public TrackRequestItem(int id, String bloodGroupId, String units, String donorName, String dateTime
            , int status, String ProfilePicture, String Notes, int TravellingStatus, String acceptedDate
            , String phoneNumber) {
        this.id = id;
        this.bloodGroupId = bloodGroupId;
        this.units = units;
        this.donorName = donorName;
        this.dateTime = dateTime;
        this.status = status;
        this.profilePicture = ProfilePicture;
        this.notes = Notes;
        this.travellingStatus = TravellingStatus;
        this.acceptedDate = acceptedDate;
        this.phoneNumber = phoneNumber;
    }

    private int id;
    private String bloodGroupId;
    private String units;
    private String donorName;
    private String dateTime;
    private int status;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    private String phoneNumber;

    public String getAcceptedDate() {
        return acceptedDate;
    }

    public void setAcceptedDate(String acceptedDate) {
        this.acceptedDate = acceptedDate;
    }

    private String acceptedDate;

    public int getTravellingStatus() {
        return travellingStatus;
    }

    public void setTravellingStatus(int travellingStatus) {
        this.travellingStatus = travellingStatus;
    }

    private int travellingStatus;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    private String notes;

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    private String profilePicture;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
