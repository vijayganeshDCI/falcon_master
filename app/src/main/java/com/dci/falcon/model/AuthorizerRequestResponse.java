package com.dci.falcon.model;

import java.util.List;

public class AuthorizerRequestResponse {
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public List<AuthorizerRequestUserItem> getUser() {
		return User;
	}

	public void setUser(List<AuthorizerRequestUserItem> user) {
		User = user;
	}

	public List<AuthorizerRequestHospitalItem> getHospital() {
		return Hospital;
	}

	public void setHospital(List<AuthorizerRequestHospitalItem> hospital) {
		Hospital = hospital;
	}

	public int getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(int statusCode) {
		StatusCode = statusCode;
	}

	private String Status;
	private List<AuthorizerRequestUserItem> User;
	private List<AuthorizerRequestHospitalItem> Hospital;
	private int StatusCode;


}