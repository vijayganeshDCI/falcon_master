package com.dci.falcon.model;

import java.util.List;

public class ResetPasswordResponse {
    public List<ResetPasswordResponseDataItem> getData() {
        return data;
    }

    public void setData(List<ResetPasswordResponseDataItem> data) {
        this.data = data;
    }

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int statusCode) {
        StatusCode = statusCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private List<ResetPasswordResponseDataItem> data;
    private int StatusCode;
    private String status;


}