package com.dci.falcon.model;

public class FamilyMembersInfoItem {
	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getBloodGroupID() {
		return BloodGroupID;
	}

	public void setBloodGroupID(String bloodGroupID) {
		BloodGroupID = bloodGroupID;
	}

	public String getPhoneNumber() {
		return PhoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		PhoneNumber = phoneNumber;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getProfilePicture() {
		return ProfilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		ProfilePicture = profilePicture;
	}

	public FamilyMembersInfoItem(String firstName, String bloodGroupID, String phoneNumber, int id, String lastName, String profilePicture
			,int bloodCount1,String relationType,boolean logged_in_status) {
		FirstName = firstName;
		BloodGroupID = bloodGroupID;
		PhoneNumber = phoneNumber;
		this.id = id;
		LastName = lastName;
		ProfilePicture = profilePicture;
		bloodCount=bloodCount1;
		this.Relationship=relationType;
		this.logged_in_status=logged_in_status;
	}

	private String FirstName;
	private String BloodGroupID;
	private String PhoneNumber;
	private int id;
	private String LastName;
	private String ProfilePicture;

	public boolean isLogged_in_status() {
		return logged_in_status;
	}

	public void setLogged_in_status(boolean logged_in_status) {
		this.logged_in_status = logged_in_status;
	}

	private boolean logged_in_status;


	public String getRelationship() {
		return Relationship;
	}

	public void setRelationship(String relationship) {
		Relationship = relationship;
	}

	private String Relationship;

	public int getBloodCount() {
		return bloodCount;
	}

	public void setBloodCount(int bloodCount) {
		this.bloodCount = bloodCount;
	}

	private int bloodCount;


}
