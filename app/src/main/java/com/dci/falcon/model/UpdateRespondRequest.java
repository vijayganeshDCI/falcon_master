package com.dci.falcon.model;

/**
 * Created by vijayaganesh on 12/15/2017.
 */

public class UpdateRespondRequest {


    public int getTravellingStatus() {
        return TravellingStatus;
    }

    public void setTravellingStatus(int travellingStatus) {
        TravellingStatus = travellingStatus;
    }

    public String getScheduleTime() {
        return ScheduleTime;
    }

    public void setScheduleTime(String scheduleTime) {
        ScheduleTime = scheduleTime;
    }

    public String getNotes() {
        return Notes;
    }

    public void setNotes(String notes) {
        Notes = notes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserResStatus() {
        return UserResStatus;
    }

    public void setUserResStatus(int userResStatus) {
        UserResStatus = userResStatus;
    }

    private int TravellingStatus;
    private String ScheduleTime;
    private String Notes;
    private int id;
    private int UserResStatus;
}
