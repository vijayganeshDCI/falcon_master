package com.dci.falcon.model;

public class RequestList_User_Item {

	public String getEmailID() {
		return EmailID;
	}

	public void setEmailID(String emailID) {
		EmailID = emailID;
	}

	public String getNearHospitalLocationFreLat() {
		return NearHospitalLocationFreLat;
	}

	public void setNearHospitalLocationFreLat(String nearHospitalLocationFreLat) {
		NearHospitalLocationFreLat = nearHospitalLocationFreLat;
	}

	public String getDonateAt() {
		return DonateAt;
	}

	public void setDonateAt(String donateAt) {
		DonateAt = donateAt;
	}

	public String getFCMKey() {
		return FCMKey;
	}

	public void setFCMKey(String FCMKey) {
		this.FCMKey = FCMKey;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public int getGender() {
		return Gender;
	}

	public void setGender(int gender) {
		Gender = gender;
	}

	public int getActiveStatus() {
		return ActiveStatus;
	}

	public void setActiveStatus(int activeStatus) {
		ActiveStatus = activeStatus;
	}

	public String getAppVersion() {
		return AppVersion;
	}

	public void setAppVersion(String appVersion) {
		AppVersion = appVersion;
	}

	public int getPending_notification() {
		return pending_notification;
	}

	public void setPending_notification(int pending_notification) {
		this.pending_notification = pending_notification;
	}

	public int getRefererID() {
		return refererID;
	}

	public void setRefererID(int refererID) {
		this.refererID = refererID;
	}

	public String getAppID() {
		return AppID;
	}

	public void setAppID(String appID) {
		AppID = appID;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getDOB() {
		return DOB;
	}

	public void setDOB(String DOB) {
		this.DOB = DOB;
	}

	public int getSecQuesID() {
		return SecQuesID;
	}

	public void setSecQuesID(int secQuesID) {
		SecQuesID = secQuesID;
	}

	public String getBloodGroupID() {
		return BloodGroupID;
	}

	public void setBloodGroupID(String bloodGroupID) {
		BloodGroupID = bloodGroupID;
	}

	public int getAuthorizer() {
		return Authorizer;
	}

	public void setAuthorizer(int authorizer) {
		Authorizer = authorizer;
	}

	public String getNearHospitalLocationOffice() {
		return NearHospitalLocationOffice;
	}

	public void setNearHospitalLocationOffice(String nearHospitalLocationOffice) {
		NearHospitalLocationOffice = nearHospitalLocationOffice;
	}

	public String getNearHospitalLocationHome() {
		return NearHospitalLocationHome;
	}

	public void setNearHospitalLocationHome(String nearHospitalLocationHome) {
		NearHospitalLocationHome = nearHospitalLocationHome;
	}

	public String getNearHospitalLocationHomeLng() {
		return NearHospitalLocationHomeLng;
	}

	public void setNearHospitalLocationHomeLng(String nearHospitalLocationHomeLng) {
		NearHospitalLocationHomeLng = nearHospitalLocationHomeLng;
	}

	public int getLoginStatus() {
		return LoginStatus;
	}

	public void setLoginStatus(int loginStatus) {
		LoginStatus = loginStatus;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAge() {
		return Age;
	}

	public void setAge(int age) {
		Age = age;
	}

	public String getProfilePicture() {
		return ProfilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		ProfilePicture = profilePicture;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public String getNearHospitalLocationHomeLat() {
		return NearHospitalLocationHomeLat;
	}

	public void setNearHospitalLocationHomeLat(String nearHospitalLocationHomeLat) {
		NearHospitalLocationHomeLat = nearHospitalLocationHomeLat;
	}

	public String getLastDonationDate() {
		return LastDonationDate;
	}

	public void setLastDonationDate(String lastDonationDate) {
		LastDonationDate = lastDonationDate;
	}

	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getDeviceID() {
		return DeviceID;
	}

	public void setDeviceID(String deviceID) {
		DeviceID = deviceID;
	}

	public String getNearHospitalLocationOfficeLat() {
		return NearHospitalLocationOfficeLat;
	}

	public void setNearHospitalLocationOfficeLat(String nearHospitalLocationOfficeLat) {
		NearHospitalLocationOfficeLat = nearHospitalLocationOfficeLat;
	}

	public int getNonsmartPhone() {
		return NonsmartPhone;
	}

	public void setNonsmartPhone(int nonsmartPhone) {
		NonsmartPhone = nonsmartPhone;
	}

	public String getIMEI() {
		return IMEI;
	}

	public void setIMEI(String IMEI) {
		this.IMEI = IMEI;
	}

	public String getSecQuesAns() {
		return SecQuesAns;
	}

	public void setSecQuesAns(String secQuesAns) {
		SecQuesAns = secQuesAns;
	}

	public int getOrganizationID() {
		return OrganizationID;
	}

	public void setOrganizationID(int organizationID) {
		OrganizationID = organizationID;
	}

	public int getOrganizationType() {
		return OrganizationType;
	}

	public void setOrganizationType(int organizationType) {
		OrganizationType = organizationType;
	}

	public String getLastLogin() {
		return LastLogin;
	}

	public void setLastLogin(String lastLogin) {
		LastLogin = lastLogin;
	}

	public String getPhoneNumber() {
		return PhoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		PhoneNumber = phoneNumber;
	}

	public String getNearHospitalLocationOfficeLng() {
		return NearHospitalLocationOfficeLng;
	}

	public void setNearHospitalLocationOfficeLng(String nearHospitalLocationOfficeLng) {
		NearHospitalLocationOfficeLng = nearHospitalLocationOfficeLng;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getNearHospitalLocationFreLng() {
		return NearHospitalLocationFreLng;
	}

	public void setNearHospitalLocationFreLng(String nearHospitalLocationFreLng) {
		NearHospitalLocationFreLng = nearHospitalLocationFreLng;
	}

	public int getUserType() {
		return UserType;
	}

	public void setUserType(int userType) {
		UserType = userType;
	}

	public String getNearHospitalLocationFre() {
		return NearHospitalLocationFre;
	}

	public void setNearHospitalLocationFre(String nearHospitalLocationFre) {
		NearHospitalLocationFre = nearHospitalLocationFre;
	}

	private String EmailID;
	private String NearHospitalLocationFreLat;
	private String DonateAt;
	private String FCMKey;
	private String created_at;
	private int Gender;
	private int ActiveStatus;
	private String AppVersion;
	private int pending_notification;
	private int refererID;
	private String AppID;
	private String updated_at;
	private String DOB;
	private int SecQuesID;
	private String BloodGroupID;
	private int Authorizer;
	private String NearHospitalLocationOffice;
	private String NearHospitalLocationHome;
	private String NearHospitalLocationHomeLng;
	private int LoginStatus;
	private int id;
	private int Age;
	private String ProfilePicture;
	private String Password;
	private String NearHospitalLocationHomeLat;
	private String LastDonationDate;
	private int Status;
	private String FirstName;
	private String DeviceID;
	private String NearHospitalLocationOfficeLat;
	private int NonsmartPhone;
	private String IMEI;
	private String SecQuesAns;
	private int OrganizationID;
	private int OrganizationType;
	private String LastLogin;
	private String PhoneNumber;
	private String NearHospitalLocationOfficeLng;
	private String LastName;
	private String NearHospitalLocationFreLng;
	private int UserType;
	private String NearHospitalLocationFre;
}
