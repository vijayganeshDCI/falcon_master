package com.dci.falcon.model;

public class BloodReqHosItem{
	public String getEmailID() {
		return EmailID;
	}

	public void setEmailID(String emailID) {
		EmailID = emailID;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getLng() {
		return Lng;
	}

	public void setLng(String lng) {
		Lng = lng;
	}

	public int getFromID() {
		return FromID;
	}

	public void setFromID(int fromID) {
		FromID = fromID;
	}

	public String getCreateDate() {
		return CreateDate;
	}

	public void setCreateDate(String createDate) {
		CreateDate = createDate;
	}

	public String getDateTime() {
		return DateTime;
	}

	public void setDateTime(String dateTime) {
		DateTime = dateTime;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public int getBrID() {
		return BrID;
	}

	public void setBrID(int brID) {
		BrID = brID;
	}

	public String getAppID() {
		return AppID;
	}

	public void setAppID(String appID) {
		AppID = appID;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}

	public String getRegisterID() {
		return RegisterID;
	}

	public void setRegisterID(String registerID) {
		RegisterID = registerID;
	}

	public String getOwnerName() {
		return OwnerName;
	}

	public void setOwnerName(String ownerName) {
		OwnerName = ownerName;
	}

	public String getFCMKey() {
		return FCMKey;
	}

	public void setFCMKey(String FCMKey) {
		this.FCMKey = FCMKey;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getAppVersion() {
		return AppVersion;
	}

	public void setAppVersion(String appVersion) {
		AppVersion = appVersion;
	}

	public int getToID() {
		return ToID;
	}

	public void setToID(int toID) {
		ToID = toID;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getLastlogin() {
		return Lastlogin;
	}

	public void setLastlogin(String lastlogin) {
		Lastlogin = lastlogin;
	}

	public int getUserID() {
		return UserID;
	}

	public void setUserID(int userID) {
		UserID = userID;
	}

	public String getBloodGroupID() {
		return BloodGroupID;
	}

	public void setBloodGroupID(String bloodGroupID) {
		BloodGroupID = bloodGroupID;
	}

	public String getNotes() {
		return Notes;
	}

	public void setNotes(String notes) {
		Notes = notes;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public String getDeviceID() {
		return DeviceID;
	}

	public void setDeviceID(String deviceID) {
		DeviceID = deviceID;
	}

	public String getAlternateNumber() {
		return AlternateNumber;
	}

	public void setAlternateNumber(String alternateNumber) {
		AlternateNumber = alternateNumber;
	}

	public String getMedicalCondition() {
		return MedicalCondition;
	}

	public void setMedicalCondition(String medicalCondition) {
		MedicalCondition = medicalCondition;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public int getVerification() {
		return Verification;
	}

	public void setVerification(int verification) {
		Verification = verification;
	}

	public String getIMEI() {
		return IMEI;
	}

	public void setIMEI(String IMEI) {
		this.IMEI = IMEI;
	}

	public String getAlternateName() {
		return AlternateName;
	}

	public void setAlternateName(String alternateName) {
		AlternateName = alternateName;
	}

	public int getHosID() {
		return HosID;
	}

	public void setHosID(int hosID) {
		HosID = hosID;
	}

	public int getMemberID() {
		return MemberID;
	}

	public void setMemberID(int memberID) {
		MemberID = memberID;
	}

	public int getTravellingStatus() {
		return TravellingStatus;
	}

	public void setTravellingStatus(int travellingStatus) {
		TravellingStatus = travellingStatus;
	}

	public String getPhoneNumber() {
		return PhoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		PhoneNumber = phoneNumber;
	}

	public String getHospitalAddress() {
		return HospitalAddress;
	}

	public void setHospitalAddress(String hospitalAddress) {
		HospitalAddress = hospitalAddress;
	}

	public String getLat() {
		return Lat;
	}

	public void setLat(String lat) {
		Lat = lat;
	}

	public int getNofID() {
		return NofID;
	}

	public void setNofID(int nofID) {
		NofID = nofID;
	}

	public int getReqType() {
		return ReqType;
	}

	public void setReqType(int reqType) {
		ReqType = reqType;
	}

	public String getPatientName() {
		return PatientName;
	}

	public void setPatientName(String patientName) {
		PatientName = patientName;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getUnits() {
		return Units;
	}

	public void setUnits(String units) {
		Units = units;
	}

	public String getOwnerPhoneNumber() {
		return OwnerPhoneNumber;
	}

	public void setOwnerPhoneNumber(String ownerPhoneNumber) {
		OwnerPhoneNumber = ownerPhoneNumber;
	}

	public String getHospitalLat() {
		return HospitalLat;
	}

	public void setHospitalLat(String hospitalLat) {
		HospitalLat = hospitalLat;
	}

	public String getScheduleTime() {
		return ScheduleTime;
	}

	public void setScheduleTime(String scheduleTime) {
		ScheduleTime = scheduleTime;
	}

	public String getState() {
		return State;
	}

	public void setState(String state) {
		State = state;
	}

	public String getPatientID() {
		return PatientID;
	}

	public void setPatientID(String patientID) {
		PatientID = patientID;
	}

	public int getCountry() {
		return Country;
	}

	public void setCountry(int country) {
		Country = country;
	}

	public int getUserType() {
		return UserType;
	}

	public void setUserType(int userType) {
		UserType = userType;
	}

	public String getHospitalLng() {
		return HospitalLng;
	}

	public void setHospitalLng(String hospitalLng) {
		HospitalLng = hospitalLng;
	}

	private String EmailID;
	private String Address;
	private String Lng;
	private int FromID;
	private String CreateDate;
	private String DateTime;
	private String Name;
	private int BrID;
	private String AppID;
	private int id;
	private int Status;
	private int ReqType;
	private String PatientName;
	private String City;
	private String Units;
	private String OwnerPhoneNumber;
	private String HospitalLat;
	private String ScheduleTime;
	private String State;
	private String PatientID;
	private int Country;
	private int UserType;
	private String HospitalLng;
	private int status;
	private String RegisterID;
	private String OwnerName;
	private String FCMKey;
	private String created_at;
	private String AppVersion;
	private int ToID;
	private String updated_at;
	private String Lastlogin;
	private int UserID;
	private String BloodGroupID;
	private String Notes;
	private String Password;
	private String DeviceID;
	private String AlternateNumber;
	private String MedicalCondition;
	private String photo;
	private int Verification;
	private String IMEI;
	private String AlternateName;
	private int HosID;
	private int MemberID;
	private int TravellingStatus;
	private String PhoneNumber;
	private String HospitalAddress;
	private String Lat;
	private int NofID;
}
