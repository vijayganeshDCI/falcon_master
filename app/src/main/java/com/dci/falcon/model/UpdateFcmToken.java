package com.dci.falcon.model;

/**
 * Created by vijayaganesh on 2/8/2018.
 */

public class UpdateFcmToken {

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFCMKey() {
        return FCMKey;
    }

    public void setFCMKey(String FCMKey) {
        this.FCMKey = FCMKey;
    }

    private int id;
    private String FCMKey;
}
