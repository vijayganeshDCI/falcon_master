package com.dci.falcon.model;

public class IndiLoginOrgInfo{
	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}

	public String getDetails() {
		return Details;
	}

	public void setDetails(String details) {
		Details = details;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getAddressDetails() {
		return AddressDetails;
	}

	public void setAddressDetails(String addressDetails) {
		AddressDetails = addressDetails;
	}

	public String getPhoneNumber() {
		return PhoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		PhoneNumber = phoneNumber;
	}

	public int getCreateUserID() {
		return CreateUserID;
	}

	public void setCreateUserID(int createUserID) {
		CreateUserID = createUserID;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	private int Status;
	private String Details;
	private String updated_at;
	private String AddressDetails;
	private String PhoneNumber;
	private int CreateUserID;
	private String created_at;
	private int id;
	private String Name;


}
