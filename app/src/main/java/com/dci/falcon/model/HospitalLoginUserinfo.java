package com.dci.falcon.model;

public class HospitalLoginUserinfo {
	private String EmailID;
	private String RegisterID;
	private String OwnerName;
	private String Address;
	private String Lng;
	private String FCMKey;
	private String createdAt;
	private String Name;
	private String AppVersion;
	private String AppID;
	private String updatedAt;
	private String Lastlogin;
	private int id;
	private String Password;
	private String DeviceID;
	private String photo;
	private int Verification;
	private String IMEI;
	private String City;
	private String OwnerPhoneNumber;
	private String State;
	private String PhoneNumber;
	private String Lat;
	private int status;
	private int Country;

	public int getPending_notification() {
		return pending_notification;
	}

	public void setPending_notification(int pending_notification) {
		this.pending_notification = pending_notification;
	}

	private int pending_notification;

	public int getLives() {
		return Lives;
	}

	public void setLives(int lives) {
		Lives = lives;
	}

	private int Lives;

	public int getIndUnits() {
		return indUnits;
	}

	public void setIndUnits(int indUnits) {
		this.indUnits = indUnits;
	}

	private int indUnits;

	public int getCountry() {
		return Country;
	}

	public void setCountry(int country) {
		Country = country;
	}

	public String getEmailID() {
		return EmailID;
	}

	public void setEmailID(String emailID) {
		EmailID = emailID;
	}

	public String getRegisterID() {
		return RegisterID;
	}

	public void setRegisterID(String registerID) {
		RegisterID = registerID;
	}

	public String getOwnerName() {
		return OwnerName;
	}

	public void setOwnerName(String ownerName) {
		OwnerName = ownerName;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getLng() {
		return Lng;
	}

	public void setLng(String lng) {
		Lng = lng;
	}

	public String getFCMKey() {
		return FCMKey;
	}

	public void setFCMKey(String FCMKey) {
		this.FCMKey = FCMKey;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getAppVersion() {
		return AppVersion;
	}

	public void setAppVersion(String appVersion) {
		AppVersion = appVersion;
	}

	public String getAppID() {
		return AppID;
	}

	public void setAppID(String appID) {
		AppID = appID;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getLastlogin() {
		return Lastlogin;
	}

	public void setLastlogin(String lastlogin) {
		Lastlogin = lastlogin;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public String getDeviceID() {
		return DeviceID;
	}

	public void setDeviceID(String deviceID) {
		DeviceID = deviceID;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public int getVerification() {
		return Verification;
	}

	public void setVerification(int verification) {
		Verification = verification;
	}

	public String getIMEI() {
		return IMEI;
	}

	public void setIMEI(String IMEI) {
		this.IMEI = IMEI;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getOwnerPhoneNumber() {
		return OwnerPhoneNumber;
	}

	public void setOwnerPhoneNumber(String ownerPhoneNumber) {
		OwnerPhoneNumber = ownerPhoneNumber;
	}

	public String getState() {
		return State;
	}

	public void setState(String state) {
		State = state;
	}

	public String getPhoneNumber() {
		return PhoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		PhoneNumber = phoneNumber;
	}

	public String getLat() {
		return Lat;
	}

	public void setLat(String lat) {
		Lat = lat;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}


}
