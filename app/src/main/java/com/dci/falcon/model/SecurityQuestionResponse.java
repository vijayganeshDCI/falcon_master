package com.dci.falcon.model;

import java.util.List;

public class SecurityQuestionResponse {
    private List<SecquesItem> secques;
    private int StatusCode;
    private String status;

    public List<SecquesItem> getSecques() {
        return secques;
    }

    public void setSecques(List<SecquesItem> secques) {
        this.secques = secques;
    }

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int statusCode) {
        StatusCode = statusCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}