package com.dci.falcon.model;

/**
 * Created by vijayaganesh on 11/14/2017.
 */

public class HistoryResponseItem {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int userID) {
        UserID = userID;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }


    public HistoryResponseItem(int id, int userID, String description, String createdDate, String profilePicture, String name, String bloodGroup, String units, String patientName, String donateTime, String phoneNumber,String patienttID1) {
        this.id = id;
        UserID = userID;
        Description = description;
        CreatedDate = createdDate;
        this.profilePicture = profilePicture;
        this.name = name;
        this.bloodGroup = bloodGroup;
        this.units = units;
        this.patientName = patientName;
        this.donateTime = donateTime;
        this.phoneNumber = phoneNumber;
        this.patienttID=patienttID1;
    }

    private int id;
    private int UserID;
    private String Description;
    private String CreatedDate;
    private String profilePicture;
    private String name;
    private String bloodGroup;
    private String units;
    private String patientName;
    private String donateTime;
    private String phoneNumber;

    public String getPatienttID() {
        return patienttID;
    }

    public void setPatienttID(String patienttID) {
        this.patienttID = patienttID;
    }

    private String patienttID;

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getDonateTime() {
        return donateTime;
    }

    public void setDonateTime(String donateTime) {
        this.donateTime = donateTime;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


}
