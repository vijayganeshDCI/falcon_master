package com.dci.falcon.model;

/**
 * Created by vijayaganesh on 12/4/2017.
 */

public class AddMember {
    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getEmailID() {
        return EmailID;
    }

    public void setEmailID(String emailID) {
        EmailID = emailID;
    }

    public String getProfilePicture() {
        return ProfilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        ProfilePicture = profilePicture;
    }

    public String getRelationshipID() {
        return RelationshipID;
    }

    public void setRelationshipID(String relationshipID) {
        RelationshipID = relationshipID;
    }


    public String getNearHospitalLocationHomeLat() {
        return NearHospitalLocationHomeLat;
    }

    public void setNearHospitalLocationHomeLat(String nearHospitalLocationHomeLat) {
        NearHospitalLocationHomeLat = nearHospitalLocationHomeLat;
    }

    public String getNearHospitalLocationHomeLng() {
        return NearHospitalLocationHomeLng;
    }

    public void setNearHospitalLocationHomeLng(String nearHospitalLocationHomeLng) {
        NearHospitalLocationHomeLng = nearHospitalLocationHomeLng;
    }

    public String getNearHospitalLocationOffice() {
        return NearHospitalLocationOffice;
    }

    public void setNearHospitalLocationOffice(String nearHospitalLocationOffice) {
        NearHospitalLocationOffice = nearHospitalLocationOffice;
    }

    public String getNearHospitalLocationOfficeLat() {
        return NearHospitalLocationOfficeLat;
    }

    public void setNearHospitalLocationOfficeLat(String nearHospitalLocationOfficeLat) {
        NearHospitalLocationOfficeLat = nearHospitalLocationOfficeLat;
    }

    public String getNearHospitalLocationOfficeLng() {
        return NearHospitalLocationOfficeLng;
    }

    public void setNearHospitalLocationOfficeLng(String nearHospitalLocationOfficeLng) {
        NearHospitalLocationOfficeLng = nearHospitalLocationOfficeLng;
    }

    public String getNearHospitalLocationFre() {
        return NearHospitalLocationFre;
    }

    public void setNearHospitalLocationFre(String nearHospitalLocationFre) {
        NearHospitalLocationFre = nearHospitalLocationFre;
    }

    public String getNearHospitalLocationFreLat() {
        return NearHospitalLocationFreLat;
    }

    public void setNearHospitalLocationFreLat(String nearHospitalLocationFreLat) {
        NearHospitalLocationFreLat = nearHospitalLocationFreLat;
    }

    public String getNearHospitalLocationFreLng() {
        return NearHospitalLocationFreLng;
    }

    public void setNearHospitalLocationFreLng(String nearHospitalLocationFreLng) {
        NearHospitalLocationFreLng = nearHospitalLocationFreLng;
    }

    public String getBloodGroupID() {
        return BloodGroupID;
    }

    public void setBloodGroupID(String bloodGroupID) {
        BloodGroupID = bloodGroupID;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int age) {
        Age = age;
    }

    public int getGender() {
        return Gender;
    }

    public void setGender(int gender) {
        Gender = gender;
    }

    public String getNearHospitalLocationHome() {
        return NearHospitalLocationHome;
    }

    public void setNearHospitalLocationHome(String nearHospitalLocationHome) {
        NearHospitalLocationHome = nearHospitalLocationHome;
    }


    private String FirstName;
    private String LastName;
    private String PhoneNumber;
    private String DOB;
    private String EmailID;
    private String ProfilePicture;
    private String RelationshipID;
    private String NearHospitalLocationHome;
    private String NearHospitalLocationHomeLat;
    private String NearHospitalLocationHomeLng;
    private String NearHospitalLocationOffice;
    private String NearHospitalLocationOfficeLat;
    private String NearHospitalLocationOfficeLng;
    private String NearHospitalLocationFre;
    private String NearHospitalLocationFreLat;
    private String NearHospitalLocationFreLng;
    private String BloodGroupID;
    private int user_id;
    private int Age;
    private int Gender;
    private int refererID;
    private int UserType;
    private int member_id;

    public String getLastDonationDate() {
        return LastDonationDate;
    }

    public void setLastDonationDate(String lastDonationDate) {
        LastDonationDate = lastDonationDate;
    }

    private String LastDonationDate;

    public int getActiveStatus() {
        return ActiveStatus;
    }

    public void setActiveStatus(int activeStatus) {
        ActiveStatus = activeStatus;
    }

    private int ActiveStatus;

    public int getRefererID() {
        return refererID;
    }

    public void setRefererID(int refererID) {
        this.refererID = refererID;
    }

    public int getUserType() {
        return UserType;
    }

    public void setUserType(int userType) {
        UserType = userType;
    }

    public int getMember_id() {
        return member_id;
    }

    public void setMember_id(int member_id) {
        this.member_id = member_id;
    }


}
