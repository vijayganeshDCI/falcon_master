package com.dci.falcon.model;

import java.util.List;

public class GetFamilyMemberResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public List<FamilyMembersInfoItem> getMembersInfo() {
		return MembersInfo;
	}

	public void setMembersInfo(List<FamilyMembersInfoItem> membersInfo) {
		MembersInfo = membersInfo;
	}

	public int getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(int statusCode) {
		StatusCode = statusCode;
	}

	private String Status;
	private List<FamilyMembersInfoItem> MembersInfo;
	private int StatusCode;

	public int getTotal_count() {
		return total_count;
	}

	public void setTotal_count(int total_count) {
		this.total_count = total_count;
	}

	private int total_count;


}