package com.dci.falcon.model;

/**
 * Created by vijayaganesh on 1/4/2018.
 */

public class QuickRegistration {

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getLocationHome() {
        return locationHome;
    }

    public void setLocationHome(String locationHome) {
        this.locationHome = locationHome;
    }

    public String getHomelat() {
        return homelat;
    }

    public void setHomelat(String homelat) {
        this.homelat = homelat;
    }

    public String getHomelng() {
        return homelng;
    }

    public void setHomelng(String homelng) {
        this.homelng = homelng;
    }


    private String profilePicture;
    private String firstName;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    private String lastName;
    private String phoneNumber;
    private String bloodGroup;
    private int userType;
    private String locationHome;
    private String homelat;
    private String homelng;

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    private int gender;
    private int age;
    private String dob;

    public int isAuthorizerStatus() {
        return authorizerStatus;
    }

    public void setAuthorizerStatus(int authorizerStatus) {
        this.authorizerStatus = authorizerStatus;
    }

    private int authorizerStatus;
}
