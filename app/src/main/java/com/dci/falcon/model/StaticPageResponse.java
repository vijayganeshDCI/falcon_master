package com.dci.falcon.model;

public class StaticPageResponse {
    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int statusCode) {
        StatusCode = statusCode;
    }

    private StaticPageData data;
    private int StatusCode;

    public void setData(StaticPageData data) {
        this.data = data;
    }

    public StaticPageData getData() {
        return data;
    }


}
