package com.dci.falcon.model;

public class AddMemberUserInfo {
	public String getEmailID() {
		return EmailID;
	}

	public void setEmailID(String emailID) {
		EmailID = emailID;
	}

	public String getNearHospitalLocationFreLat() {
		return NearHospitalLocationFreLat;
	}

	public void setNearHospitalLocationFreLat(String nearHospitalLocationFreLat) {
		NearHospitalLocationFreLat = nearHospitalLocationFreLat;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getNearHospitalLocationOfficeLat() {
		return NearHospitalLocationOfficeLat;
	}

	public void setNearHospitalLocationOfficeLat(String nearHospitalLocationOfficeLat) {
		NearHospitalLocationOfficeLat = nearHospitalLocationOfficeLat;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getGender() {
		return Gender;
	}

	public void setGender(String gender) {
		Gender = gender;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getDOB() {
		return DOB;
	}

	public void setDOB(String DOB) {
		this.DOB = DOB;
	}

	public String getBloodGroupID() {
		return BloodGroupID;
	}

	public void setBloodGroupID(String bloodGroupID) {
		BloodGroupID = bloodGroupID;
	}

	public String getNearHospitalLocationOffice() {
		return NearHospitalLocationOffice;
	}

	public void setNearHospitalLocationOffice(String nearHospitalLocationOffice) {
		NearHospitalLocationOffice = nearHospitalLocationOffice;
	}

	public String getPhoneNumber() {
		return PhoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		PhoneNumber = phoneNumber;
	}

	public String getNearHospitalLocationHome() {
		return NearHospitalLocationHome;
	}

	public void setNearHospitalLocationHome(String nearHospitalLocationHome) {
		NearHospitalLocationHome = nearHospitalLocationHome;
	}

	public String getNearHospitalLocationHomeLng() {
		return NearHospitalLocationHomeLng;
	}

	public void setNearHospitalLocationHomeLng(String nearHospitalLocationHomeLng) {
		NearHospitalLocationHomeLng = nearHospitalLocationHomeLng;
	}

	public String getNearHospitalLocationOfficeLng() {
		return NearHospitalLocationOfficeLng;
	}

	public void setNearHospitalLocationOfficeLng(String nearHospitalLocationOfficeLng) {
		NearHospitalLocationOfficeLng = nearHospitalLocationOfficeLng;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getNearHospitalLocationFreLng() {
		return NearHospitalLocationFreLng;
	}

	public void setNearHospitalLocationFreLng(String nearHospitalLocationFreLng) {
		NearHospitalLocationFreLng = nearHospitalLocationFreLng;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAge() {
		return Age;
	}

	public void setAge(String age) {
		Age = age;
	}

	public String getProfilePicture() {
		return ProfilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		ProfilePicture = profilePicture;
	}

	public String getNearHospitalLocationFre() {
		return NearHospitalLocationFre;
	}

	public void setNearHospitalLocationFre(String nearHospitalLocationFre) {
		NearHospitalLocationFre = nearHospitalLocationFre;
	}

	public String getNearHospitalLocationHomeLat() {
		return NearHospitalLocationHomeLat;
	}

	public void setNearHospitalLocationHomeLat(String nearHospitalLocationHomeLat) {
		NearHospitalLocationHomeLat = nearHospitalLocationHomeLat;
	}

	private String EmailID;
	private String FirstName;
	private String created_at;
	private String updated_at;
	private String Gender;
	private String DOB;
	private String BloodGroupID;
	private String PhoneNumber;
	private String NearHospitalLocationOffice;
	private String NearHospitalLocationOfficeLat;
	private String NearHospitalLocationOfficeLng;
	private String NearHospitalLocationHome;
	private String NearHospitalLocationHomeLat;
	private String NearHospitalLocationHomeLng;
	private String LastName;
	private String NearHospitalLocationFre;
	private String NearHospitalLocationFreLat;
	private String NearHospitalLocationFreLng;
	private int id;
	private String Age;
	private String ProfilePicture;


}
