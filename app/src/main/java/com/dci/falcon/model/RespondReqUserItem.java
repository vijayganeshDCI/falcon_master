package com.dci.falcon.model;

public class RespondReqUserItem {
	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public int getFromID() {
		return FromID;
	}

	public void setFromID(int fromID) {
		FromID = fromID;
	}

	public int getNofType() {
		return NofType;
	}

	public void setNofType(int nofType) {
		NofType = nofType;
	}

	public String getCreatedtime() {
		return createdtime;
	}

	public void setCreatedtime(String createdtime) {
		this.createdtime = createdtime;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public int getToID() {
		return ToID;
	}

	public void setToID(int toID) {
		ToID = toID;
	}

	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

	public int getType() {
		return Type;
	}

	public void setType(int type) {
		Type = type;
	}

	public int getRecord_from() {
		return record_from;
	}

	public void setRecord_from(int record_from) {
		this.record_from = record_from;
	}

	public String getCreatedDate() {
		return CreatedDate;
	}

	public void setCreatedDate(String createdDate) {
		CreatedDate = createdDate;
	}

	public int getUserResStatus() {
		return UserResStatus;
	}

	public void setUserResStatus(int userResStatus) {
		UserResStatus = userResStatus;
	}

	public String getPhoneNumber() {
		return PhoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		PhoneNumber = phoneNumber;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public RequestBloodInfo getReid() {
		return reid;
	}

	public void setReid(RequestBloodInfo reid) {
		this.reid = reid;
	}

	public String getUniqueID() {
		return uniqueID;
	}

	public void setUniqueID(String uniqueID) {
		this.uniqueID = uniqueID;
	}

	private int Status;
	private String Message;
	private int FromID;
	private int NofType;
	private String createdtime;
	private String Name;
	private int ToID;
	private String profilePicture;
	private int Type;
	private int record_from;
	private String CreatedDate;
	private int UserResStatus;
	private String PhoneNumber;
	private int id;
	private RequestBloodInfo reid;

	public IndiLoginUserInfo getF_user() {
		return f_user;
	}

	public void setF_user(IndiLoginUserInfo f_user) {
		this.f_user = f_user;
	}

	private IndiLoginUserInfo f_user;
	private String uniqueID;
}
