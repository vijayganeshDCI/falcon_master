package com.dci.falcon.model;

public class RequestBloodResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public String getNotifictionStatus() {
		return NotifictionStatus;
	}

	public void setNotifictionStatus(String notifictionStatus) {
		NotifictionStatus = notifictionStatus;
	}

	public int getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(int statusCode) {
		StatusCode = statusCode;
	}

	public RequestBloodInfo getInfo() {
		return info;
	}

	public void setInfo(RequestBloodInfo info) {
		this.info = info;
	}

	private String Status;
	private String Message;
	private String NotifictionStatus;
	private int StatusCode;
	private RequestBloodInfo info;


}
