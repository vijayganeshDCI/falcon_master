package com.dci.falcon.model;

import java.util.List;

public class HistoryResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public List<HistoryResponseItem> getHistory() {
		return history;
	}

	public void setHistory(List<HistoryResponseItem> history) {
		this.history = history;
	}

	public int getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(int statusCode) {
		StatusCode = statusCode;
	}

	private String Status;
	private String Message;
	private List<HistoryResponseItem> history;
	private int StatusCode;


}