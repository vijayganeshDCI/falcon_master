package com.dci.falcon.model;

public class RequestBloodInfo {
	public String getReqType() {
		return ReqType;
	}

	public void setReqType(String reqType) {
		ReqType = reqType;
	}

	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}

	public String getAlternateNumber() {
		return AlternateNumber;
	}

	public void setAlternateNumber(String alternateNumber) {
		AlternateNumber = alternateNumber;
	}

	public String getMedicalCondition() {
		return MedicalCondition;
	}

	public void setMedicalCondition(String medicalCondition) {
		MedicalCondition = medicalCondition;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getPatientName() {
		return PatientName;
	}

	public void setPatientName(String patientName) {
		PatientName = patientName;
	}

	public String getAlternateName() {
		return AlternateName;
	}

	public void setAlternateName(String alternateName) {
		AlternateName = alternateName;
	}

	public String getHosID() {
		return HosID;
	}

	public void setHosID(String hosID) {
		HosID = hosID;
	}

	public String getUnits() {
		return Units;
	}

	public void setUnits(String units) {
		Units = units;
	}

	public String getDateTime() {
		return DateTime;
	}

	public void setDateTime(String dateTime) {
		DateTime = dateTime;
	}

	public String getHospitalLat() {
		return HospitalLat;
	}

	public void setHospitalLat(String hospitalLat) {
		HospitalLat = hospitalLat;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getUserID() {
		return UserID;
	}

	public void setUserID(String userID) {
		UserID = userID;
	}

	public String getBloodGroupID() {
		return BloodGroupID;
	}

	public void setBloodGroupID(String bloodGroupID) {
		BloodGroupID = bloodGroupID;
	}

	public String getPatientID() {
		return PatientID;
	}

	public void setPatientID(String patientID) {
		PatientID = patientID;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getHospitalAddress() {
		return HospitalAddress;
	}

	public void setHospitalAddress(String hospitalAddress) {
		HospitalAddress = hospitalAddress;
	}

	public String getUserType() {
		return UserType;
	}

	public void setUserType(String userType) {
		UserType = userType;
	}

	public String getHospitalLng() {
		return HospitalLng;
	}

	public void setHospitalLng(String hospitalLng) {
		HospitalLng = hospitalLng;
	}

	private String ReqType;
	private int Status;
	private String AlternateNumber;
	private String MedicalCondition;
	private String created_at;
	private String PatientName;
	private String AlternateName;
	private String HosID;
	private String Units;
	private String DateTime;
	private String HospitalLat;
	private String updated_at;
	private String UserID;
	private String BloodGroupID;
	private String PatientID;
	private int id;
	private String HospitalAddress;
	private String UserType;
	private String HospitalLng;

}
