package com.dci.falcon.model;

import java.util.List;

public class IndiduvalRegisterResponse {
    private List<IndiRegDndInfoItem> dndInfo;
    private String Status;
    private IndiRegUserInfo userInfo;
    private String Message;
    private IndiRegOrgInfo orgInfo;
    private int StatusCode;
    private int totalIndUnits;

    public int getTotalIndUnits() {
        return totalIndUnits;
    }

    public void setTotalIndUnits(int totalIndUnits) {
        this.totalIndUnits = totalIndUnits;
    }



    public List<IndiRegDndInfoItem> getDndInfo() {
        return dndInfo;
    }

    public void setDndInfo(List<IndiRegDndInfoItem> dndInfo) {
        this.dndInfo = dndInfo;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public IndiRegUserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(IndiRegUserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public IndiRegOrgInfo getOrgInfo() {
        return orgInfo;
    }

    public void setOrgInfo(IndiRegOrgInfo orgInfo) {
        this.orgInfo = orgInfo;
    }

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int statusCode) {
        StatusCode = statusCode;
    }


}