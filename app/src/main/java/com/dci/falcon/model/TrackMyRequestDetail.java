package com.dci.falcon.model;

/**
 * Created by vijayaganesh on 1/5/2018.
 */

public class TrackMyRequestDetail {

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public TrackMyRequestDetail(String profilePicture, String name, String dateTime, String phoneNumber,int status) {
        this.profilePicture = profilePicture;
        Name = name;
        this.dateTime = dateTime;
        this.phoneNumber = phoneNumber;
        this.status=status;
    }

    private String profilePicture;
    private String Name;
    private String dateTime;
    private String phoneNumber;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    private int status;
}
