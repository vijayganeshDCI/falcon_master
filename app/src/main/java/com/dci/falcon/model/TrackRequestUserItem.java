package com.dci.falcon.model;

public class TrackRequestUserItem {
	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}

	public int getFromID() {
		return FromID;
	}

	public void setFromID(int fromID) {
		FromID = fromID;
	}

	public TrackRequestBloodInfo getBd_req() {
		return bd_req;
	}

	public void setBd_req(TrackRequestBloodInfo bd_req) {
		this.bd_req = bd_req;
	}

	public String getCreateDate() {
		return CreateDate;
	}

	public void setCreateDate(String createDate) {
		CreateDate = createDate;
	}

	public String getUnits() {
		return Units;
	}

	public void setUnits(String units) {
		Units = units;
	}



	public int getToID() {
		return ToID;
	}

	public void setToID(int toID) {
		ToID = toID;
	}

	public int getBrID() {
		return BrID;
	}

	public void setBrID(int brID) {
		BrID = brID;
	}

	public String getScheduleTime() {
		return ScheduleTime;
	}

	public void setScheduleTime(String scheduleTime) {
		ScheduleTime = scheduleTime;
	}

	public int getTravellingStatus() {
		return TravellingStatus;
	}

	public void setTravellingStatus(int travellingStatus) {
		TravellingStatus = travellingStatus;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public TrackRequestUserInfo getF_user() {
		return f_user;
	}

	public void setF_user(TrackRequestUserInfo f_user) {
		this.f_user = f_user;
	}

	public String getNotes() {
		return Notes;
	}

	public void setNotes(String notes) {
		Notes = notes;
	}

	public int getNofID() {
		return NofID;
	}

	public void setNofID(int nofID) {
		NofID = nofID;
	}

	private int Status;
	private int FromID;
	private TrackRequestBloodInfo bd_req;
	private String CreateDate;
	private String Units;

	public TrackRequestNoid getNoid() {
		return noid;
	}

	public void setNoid(TrackRequestNoid noid) {
		this.noid = noid;
	}

	private TrackRequestNoid noid;
	private int ToID;
	private int BrID;
	private String ScheduleTime;
	private int TravellingStatus;
	private int id;
	private TrackRequestUserInfo f_user;
	private String Notes;
	private int NofID;
}
