package com.dci.falcon.model;

import android.support.design.widget.FloatingActionButton;

import java.util.List;

public class GetIndiUserResponse {
    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public List<IndiLoginDNDInfo> getDndInfo() {
        return dndInfo;
    }

    public void setDndInfo(List<IndiLoginDNDInfo> dndInfo) {
        this.dndInfo = dndInfo;
    }

    public IndiLoginUserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(IndiLoginUserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public IndiLoginOrgInfo getOrg() {
        return org;
    }

    public void setOrg(IndiLoginOrgInfo org) {
        this.org = org;
    }

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int statusCode) {
        StatusCode = statusCode;
    }

    private String Status;
    private List<IndiLoginDNDInfo> dndInfo;
    private IndiLoginUserInfo userInfo;
    private IndiLoginOrgInfo org;
    private int StatusCode;

    public int getTotalUnits() {
        return totalUnits;
    }

    public void setTotalUnits(int totalUnits) {
        this.totalUnits = totalUnits;
    }

    private int total_user_blood_count;
    private int totalUnits;

    public int getIndUnits() {
        return indUnits;
    }

    public void setIndUnits(int indUnits) {
        this.indUnits = indUnits;
    }

    private int indUnits;

    public int getBlood_ramaing_days() {
        return blood_ramaing_days;
    }

    public void setBlood_ramaing_days(int blood_ramaing_days) {
        this.blood_ramaing_days = blood_ramaing_days;
    }

    private int blood_ramaing_days;

    public int getTotal_user_blood_count() {
        return total_user_blood_count;
    }

    public void setTotal_user_blood_count(int total_user_blood_count) {
        this.total_user_blood_count = total_user_blood_count;
    }

    public float getTime_percentage() {
        return time_percentage;
    }

    public void setTime_percentage(float time_percentage) {
        this.time_percentage = time_percentage;
    }

    public float getPercentage() {
        return percentage;
    }

    public void setPercentage(float percentage) {
        this.percentage = percentage;
    }

    private float time_percentage;
    private float percentage;



}