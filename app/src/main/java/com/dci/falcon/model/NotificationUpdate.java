package com.dci.falcon.model;

/**
 * Created by vijayaganesh on 2/17/2018.
 */

public class NotificationUpdate {

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getUser_type() {
        return user_type;
    }

    public void setUser_type(int user_type) {
        this.user_type = user_type;
    }

    private int user_id;
    private int status;
    private int user_type;

}
