package com.dci.falcon.model;

public class ChangeHospPasswordResponse{
	public HospitalLoginUserinfo getData() {
		return data;
	}

	public void setData(HospitalLoginUserinfo data) {
		this.data = data;
	}

	public int getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(int statusCode) {
		StatusCode = statusCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	private HospitalLoginUserinfo data;
	private int StatusCode;
	private String status;
}
