package com.dci.falcon.model;

import java.util.List;

public class HospitalHistoryResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public List<TrackRequestUserItem> getUser() {
		return User;
	}

	public void setUser(List<TrackRequestUserItem> user) {
		User = user;
	}

	public int getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(int statusCode) {
		StatusCode = statusCode;
	}

	private String Status;
	private List<TrackRequestUserItem> User;
	private int StatusCode;
}