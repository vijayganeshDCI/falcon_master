package com.dci.falcon.model;

/**
 * Created by vijayaganesh on 11/8/2017.
 */

public class UserDetails {

    private String firstName;
    private String lastName;
    private String bloodGroup;
    private int age;
    private String emailID;
    private String password;
    private String dateOfBirth;
    private int gender;
    private String orgName;
    private String orgDetail;
    private String orgaddress;
    private String orgphoneNumber;
    private int sequrityQuestionID;
    private String sequrityQuestionAnswer;
    private int orgType;
    private int orgID;

    public int getAuthorizer() {
        return authorizer;
    }

    public void setAuthorizer(int authorizer) {
        this.authorizer = authorizer;
    }

    private int authorizer;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    private int Status;

    public String getOrgphoneNumber() {
        return orgphoneNumber;
    }

    public void setOrgphoneNumber(String orgphoneNumber) {
        this.orgphoneNumber = orgphoneNumber;
    }

    public int getSequrityQuestionID() {
        return sequrityQuestionID;
    }

    public void setSequrityQuestionID(int sequrityQuestionID) {
        this.sequrityQuestionID = sequrityQuestionID;
    }

    public String getSequrityQuestionAnswer() {
        return sequrityQuestionAnswer;
    }

    public void setSequrityQuestionAnswer(String sequrityQuestionAnswer) {
        this.sequrityQuestionAnswer = sequrityQuestionAnswer;
    }

    public int getOrgType() {
        return orgType;
    }

    public void setOrgType(int orgType) {
        this.orgType = orgType;
    }

    public int getOrgID() {
        return orgID;
    }

    public void setOrgID(int orgID) {
        this.orgID = orgID;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getOrgaddress() {
        return orgaddress;
    }

    public void setOrgaddress(String orgaddress) {
        this.orgaddress = orgaddress;
    }



    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getOrgDetail() {
        return orgDetail;
    }

    public void setOrgDetail(String orgDetail) {
        this.orgDetail = orgDetail;
    }

}
