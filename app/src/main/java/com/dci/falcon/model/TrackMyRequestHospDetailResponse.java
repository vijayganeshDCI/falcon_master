package com.dci.falcon.model;

import java.util.List;

/**
 * Created by vijayaganesh on 1/8/2018.
 */

public class TrackMyRequestHospDetailResponse {

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public List<TrackMyRequestDetailItem> getTrackMyRequestDetail() {
        return hosTrackMyRequestDetail;
    }

    public void setTrackMyRequestDetail(List<TrackMyRequestDetailItem> trackMyRequestDetail) {
        this.hosTrackMyRequestDetail = trackMyRequestDetail;
    }

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int statusCode) {
        StatusCode = statusCode;
    }

    private String Status;
    private List<TrackMyRequestDetailItem> hosTrackMyRequestDetail;
    private int StatusCode;
}
