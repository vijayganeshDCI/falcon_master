package com.dci.falcon.model;

public class TrackMyRequestDetailItem{
	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}

	public int getToID() {
		return ToID;
	}

	public void setToID(int toID) {
		ToID = toID;
	}

	public int getType() {
		return Type;
	}

	public void setType(int type) {
		Type = type;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public int getFromID() {
		return FromID;
	}

	public void setFromID(int fromID) {
		FromID = fromID;
	}

	public int getRecord_from() {
		return record_from;
	}

	public void setRecord_from(int record_from) {
		this.record_from = record_from;
	}

	public String getCreatedDate() {
		return CreatedDate;
	}

	public void setCreatedDate(String createdDate) {
		CreatedDate = createdDate;
	}

	public int getUserResStatus() {
		return UserResStatus;
	}

	public void setUserResStatus(int userResStatus) {
		UserResStatus = userResStatus;
	}

	public int getNofType() {
		return NofType;
	}

	public void setNofType(int nofType) {
		NofType = nofType;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public TrackRequestUserInfo getF_user() {
		return f_user;
	}

	public void setF_user(TrackRequestUserInfo f_user) {
		this.f_user = f_user;
	}

	public String getCreatedtime() {
		return createdtime;
	}

	public void setCreatedtime(String createdtime) {
		this.createdtime = createdtime;
	}

	private int Status;
	private int ToID;
	private int Type;
	private String Message;
	private int FromID;
	private int record_from;
	private String CreatedDate;
	private int UserResStatus;
	private int NofType;
	private int id;
	private TrackRequestUserInfo f_user;
	private String createdtime;
}
