package com.dci.falcon.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.CheckBox;

/**
 * Created by vijayaganesh on 10/31/2017.
 */

public class CustomCheckBox extends android.support.v7.widget.AppCompatCheckBox {
    public CustomCheckBox(Context context) {
        super(context);
        setFont();
    }

    public CustomCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont();
    }

    public CustomCheckBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFont();
    }

    private void setFont() {
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "font/Montserrat-Bold.ttf");
        setTypeface(font, Typeface.NORMAL);
    }
}
