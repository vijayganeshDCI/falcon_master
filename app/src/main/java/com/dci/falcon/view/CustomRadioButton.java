package com.dci.falcon.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.RadioButton;

/**
 * Created by vijayaganesh on 10/31/2017.
 */

public class CustomRadioButton extends android.support.v7.widget.AppCompatRadioButton {
    public CustomRadioButton(Context context) {
        super(context);
        setFont();
    }

    public CustomRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont();
    }

    public CustomRadioButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFont();
    }

    private void setFont() {
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "font/Montserrat-Bold.ttf");
        setTypeface(font, Typeface.NORMAL);
    }
}
