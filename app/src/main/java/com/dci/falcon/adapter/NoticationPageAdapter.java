package com.dci.falcon.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.dci.falcon.fragment.AcceptRequestFragment;
import com.dci.falcon.fragment.AuthorizerRequestFragment;
import com.dci.falcon.fragment.RespondRequestFragment;

/**
 * Created by vijayaganesh on 11/14/2017.
 */

public class NoticationPageAdapter extends FragmentStatePagerAdapter {


    int numOfItems;

    public NoticationPageAdapter(FragmentManager fm, int numOfItems) {
        super(fm);
        this.numOfItems = numOfItems;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                RespondRequestFragment respondRequestFragment = new RespondRequestFragment();
                return respondRequestFragment;
            case 1:
                AcceptRequestFragment acceptRequestFragment = new AcceptRequestFragment();
                return acceptRequestFragment;
            case 2:
                AuthorizerRequestFragment authorizerRequestFragment = new AuthorizerRequestFragment();
                return authorizerRequestFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numOfItems;
    }
}
