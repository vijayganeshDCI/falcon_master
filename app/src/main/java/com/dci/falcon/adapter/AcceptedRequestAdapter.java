package com.dci.falcon.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.icu.text.DecimalFormat;
import android.os.AsyncTask;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.activity.BaseActivity;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.fragment.AcceptRequestFragment;
import com.dci.falcon.model.CloseRequirement;
import com.dci.falcon.model.LogoutResponse;
import com.dci.falcon.model.RejectRequest;
import com.dci.falcon.model.TrackRequestItem;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomButton;
import com.dci.falcon.view.CustomTextView;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

/**
 * Created by vijayaganesh on 11/14/2017.
 */

public class AcceptedRequestAdapter extends BaseAdapter {

    LayoutInflater layoutInflater;
    Context context;
    List<TrackRequestItem> respondRequestItemList;
    BaseActivity baseActivity;
    AcceptRequestFragment acceptRequestFragment;
    @Inject
    FalconAPI falconAPI;
    LogoutResponse logoutResponse;
    @Inject
    SharedPreferences sharedPreferences;
    UserError userError;
    private SimpleDateFormat simpleDateFormat;
    ViewHolder viewHolder = null;

    public AcceptedRequestAdapter(Context context, List<TrackRequestItem> respondRequestItemList, BaseActivity baseActivity, AcceptRequestFragment acceptRequestFragment1) {
        this.context = context;
        this.respondRequestItemList = respondRequestItemList;
        this.baseActivity = baseActivity;
        this.acceptRequestFragment = acceptRequestFragment1;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        FalconApplication.getContext().getComponent().inject(this);
        userError = new UserError();
        simpleDateFormat = new SimpleDateFormat();

    }


    @Override
    public int getCount() {
        return respondRequestItemList.size();
    }

    @Override
    public TrackRequestItem getItem(int position) {
        return respondRequestItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.accept_req_list_item, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.textUserDescrip.setText("Request: " + respondRequestItemList.get(position).getBloodGroupId() + " Blood - " +
                (!respondRequestItemList.get(position).getUnits().equals("1") ? respondRequestItemList.get(position).getUnits() + " Units"
                        : respondRequestItemList.get(position).getUnits() + " Unit"));
        viewHolder.textDonorName.setText("Donor: " + respondRequestItemList.get(position).getDonorName());
        viewHolder.textPhoneNumber.setText(respondRequestItemList.get(position).getPhoneNumber());
        Date dob1 = null;
        SimpleDateFormat simpleDateFormat11 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            dob1 = simpleDateFormat11.parse(respondRequestItemList.get(position).getAcceptedDate());
            simpleDateFormat.applyPattern("dd-MMM-yyyy HH:mm:ss");
            viewHolder.textAcceptedDate.setText(simpleDateFormat.format(dob1));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (!respondRequestItemList.get(position).getDateTime().equalsIgnoreCase("00:00:00")) {
            viewHolder.textDate.setVisibility(View.VISIBLE);
            viewHolder.textDate.setText("Expected Arrival on: " + respondRequestItemList.get(position).getDateTime());
        } else {
            viewHolder.textDate.setVisibility(View.GONE);
        }

        if (respondRequestItemList.get(position).getStatus() == 0 || respondRequestItemList.get(position).getStatus() == 1) {
            viewHolder.buttonNo.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_cancel_red, 0, 0, 0);
            viewHolder.buttonNo.setTextColor(context.getResources().getColor(R.color.falcon_red));
            viewHolder.buttonNo.setEnabled(true);
        } else {
            viewHolder.buttonNo.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_cancel, 0, 0, 0);
            viewHolder.buttonNo.setTextColor(context.getResources().getColor(R.color.hint_grey));
            viewHolder.buttonNo.setEnabled(false);
        }

        if (respondRequestItemList.get(position).getTravellingStatus() == 0) {

            viewHolder.textStatus.setText(context.getString(R.string.active));
            viewHolder.textStatus.setTextColor(context.getResources().getColor(R.color.accept_green));

        } else if (respondRequestItemList.get(position).getTravellingStatus() == 1) {
            viewHolder.textStatus.setText(context.getString(R.string.ontheway));
            viewHolder.textStatus.setTextColor(context.getResources().getColor(R.color.accept_green));

        } else if (respondRequestItemList.get(position).getStatus() == 3) {
            viewHolder.textStatus.setText(context.getString(R.string.closed));
            viewHolder.textStatus.setTextColor(context.getResources().getColor(R.color.falcon_red));

        }


        if (!respondRequestItemList.get(position).getNotes().isEmpty()) {
            viewHolder.textNotes.setVisibility(View.VISIBLE);
            viewHolder.textNotes.setText(respondRequestItemList.get(position).getNotes());
        } else {
            viewHolder.textNotes.setVisibility(View.GONE);
        }

        if (respondRequestItemList.get(position).getProfilePicture() != null) {
            Picasso.with(context)
                    .load(context.getString(R.string.falcon_image_url) + respondRequestItemList.get(position).getProfilePicture())
                    .placeholder(R.mipmap.img_sample_pro_pic)   // optional
//                                    .error(R.drawable.ic_error_fallback)      // optional
//                                    .resize(250, 200)                        // optional
//                                    .rotate(90)                             // optional
                    .into((viewHolder.imageProfilePic));
        } else {
            viewHolder.imageProfilePic.setImageResource(R.mipmap.img_sample_pro_pic);
        }

        viewHolder.buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(context);
                alertDialog.setMessage(context.getString(R.string.reject_req));
                alertDialog.setPositiveButton(R.string.Yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
//                        dialogInterface.dismiss();
                        rejectRequest(respondRequestItemList.get(position).getId(), respondRequestItemList.get(position).getDonorName());
                    }
                });
                alertDialog.setNegativeButton(R.string.No, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();

                    }
                });
                alertDialog.show();
            }
        });

        return convertView;
    }


    public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    static class ViewHolder {
        @BindView(R.id.image_profile_pic)
        CircularImageView imageProfilePic;
        @BindView(R.id.text_user_descrip)
        CustomTextView textUserDescrip;
        @BindView(R.id.text_date)
        CustomTextView textDate;
        @BindView(R.id.button_no)
        CustomButton buttonNo;
        @BindView(R.id.res_reg_list_item)
        ConstraintLayout resRegListItem;
        @BindView(R.id.card_respond_req)
        CardView cardRespondReq;
        @BindView(R.id.cons_respond_req_item)
        ConstraintLayout consRespondReqItem;
        @BindView(R.id.text_donor_name)
        CustomTextView textDonorName;
        @BindView(R.id.text_status)
        CustomTextView textStatus;
        @BindView(R.id.text_notes)
        CustomTextView textNotes;
        @BindView(R.id.text_accepted_date)
        CustomTextView textAcceptedDate;
        @BindView(R.id.text_user_phone_number)
        CustomTextView textPhoneNumber;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    private void closeRequirement(int id) {
        CloseRequirement closeRequirement = new CloseRequirement();
        closeRequirement.setNofID(id);
        if (Utils.isNetworkAvailable()) {
            baseActivity.showProgress();
            falconAPI.closeRequirement(closeRequirement).enqueue(new Callback<LogoutResponse>() {
                @Override
                public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {
                    baseActivity.hideProgress();
                    logoutResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && logoutResponse.getStatusCode() == 200) {
                        android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(context);
                        alertDialog.setMessage(context.getString(R.string.close_req));
                        alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
//                                dialogInterface.dismiss();
                                if (sharedPreferences.getInt(FalconConstants.ROLEID, 0) == 0)
//                                    dialogInterface.dismiss();
                                acceptRequestFragment.getAcceptedRequest();

//                                else
//                                    trackMyRequestFragment.getHospitalTrackMyrequest();
                            }
                        });
                        alertDialog.show();
                    } else
                        Toast.makeText(context, context.getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onFailure(Call<LogoutResponse> call, Throwable t) {
                    baseActivity.hideProgress();
                    Toast.makeText(context, context.getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            userError.message = context.getString(R.string.network_error_message);
            baseActivity.showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }

    }

    private void rejectRequest(int id, final String donorName) {
        final RejectRequest rejectRequest = new RejectRequest();
        rejectRequest.setNofID(id);
        if (Utils.isNetworkAvailable()) {
            baseActivity.showProgress();
            falconAPI.rejectRequest(rejectRequest).enqueue(new Callback<LogoutResponse>() {
                @Override
                public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {
                    baseActivity.hideProgress();
                    logoutResponse = response.body();
                    if (response.isSuccessful() && response.body() != null && logoutResponse.getStatusCode() == 200) {
                        Toast.makeText(context, context.getString(R.string.rejected) + " " + donorName, Toast.LENGTH_SHORT).show();
                        acceptRequestFragment.getAcceptedRequest();
//                        viewHolder.buttonNo.setText(context.getString(R.string.rejected));
//                        baseActivity.onBackPressed();

                    }

                }

                @Override
                public void onFailure(Call<LogoutResponse> call, Throwable t) {
                    baseActivity.hideProgress();
                    Toast.makeText(context, context.getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            userError.message = context.getString(R.string.network_error_message);
            baseActivity.showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }

    }


}
