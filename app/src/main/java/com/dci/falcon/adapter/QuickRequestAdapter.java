package com.dci.falcon.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.dci.falcon.R;
import com.dci.falcon.fragment.QuickRequestFragment;
import com.dci.falcon.fragment.RequestBloodFragment;
import com.dci.falcon.model.FamilyMembersInfoItem;
import com.dci.falcon.view.CustomTextView;
import com.github.siyamed.shapeimageview.CircularImageView;

import java.io.InputStream;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by vijayaganesh on 11/15/2017.
 */

public class QuickRequestAdapter extends BaseAdapter {

    Context context;
    List<FamilyMembersInfoItem> quickRequestItemList;
    LayoutInflater layoutInflater;
    Fragment fragment;

    public QuickRequestAdapter(Context context, List<FamilyMembersInfoItem> quickRequestItemList, Fragment fragment1) {
        this.context = context;
        this.quickRequestItemList = quickRequestItemList;
        this.fragment = fragment1;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    @Override
    public int getCount() {
        return quickRequestItemList.size();
    }

    @Override
    public FamilyMembersInfoItem getItem(int position) {
        return quickRequestItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.quick_request_item, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.textBloodGroup.setText("Blood Group:" + quickRequestItemList.get(position).getBloodGroupID());
        viewHolder.textName.setText(quickRequestItemList.get(position).getFirstName());
        if (fragment instanceof QuickRequestFragment) {
            viewHolder.imageButtonQuickRequest.setVisibility(View.VISIBLE);
            viewHolder.imageButtonRightArrow.setVisibility(View.GONE);
            viewHolder.textUnits.setVisibility(View.GONE);
            viewHolder.textRelation.setVisibility(View.VISIBLE);
            viewHolder.textRelation.setText(quickRequestItemList.get(position).getRelationship());
        } else {
            viewHolder.imageButtonQuickRequest.setVisibility(View.GONE);
            viewHolder.imageButtonRightArrow.setVisibility(View.VISIBLE);
            viewHolder.textUnits.setVisibility(View.VISIBLE);
            viewHolder.textUnits.setText("Totally Donated: " + quickRequestItemList.get(position).getBloodCount());
            viewHolder.textRelation.setVisibility(View.GONE);

        }
        if (fragment instanceof QuickRequestFragment) {
            viewHolder.imageButtonQuickRequest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Fragment requestBloodFragment = new RequestBloodFragment();
                    FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.frame_home_container, requestBloodFragment).addToBackStack("requestBloodFragment").commit();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isFromQuestRequest", true);
                    bundle.putString("bloodGroup", quickRequestItemList.get(position).getBloodGroupID());
                    bundle.putString("patientName", quickRequestItemList.get(position).getFirstName());
                    requestBloodFragment.setArguments(bundle);
                }
            });
        }
        if (quickRequestItemList.get(position).getProfilePicture() != null) {
            new DownloadImageTask((viewHolder.imageProfilePic)).execute(context.getString(R.string.falcon_image_url) + quickRequestItemList.get(position).getProfilePicture());
        } else {
            viewHolder.imageProfilePic.setImageResource(R.mipmap.img_sample_pro_pic);
        }

        return convertView;
    }

    public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    static class ViewHolder {
        @BindView(R.id.image_profile_pic)
        CircularImageView imageProfilePic;
        @BindView(R.id.text_hosp_name)
        CustomTextView textName;
        @BindView(R.id.text_units)
        CustomTextView textUnits;
        @BindView(R.id.text_blood_relation)
        CustomTextView textRelation;
        @BindView(R.id.text_blood_group)
        CustomTextView textBloodGroup;
        @BindView(R.id.cons_quick_req_item_inner)
        ConstraintLayout consQuickReqItemInner;
        @BindView(R.id.quick_request_item)
        ConstraintLayout quickRequestItem;
        @BindView(R.id.image_button_quick_request)
        ImageButton imageButtonQuickRequest;
        @BindView(R.id.image_button_right_arrow)
        ImageButton imageButtonRightArrow;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }


}
