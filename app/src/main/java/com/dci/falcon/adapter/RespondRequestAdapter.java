package com.dci.falcon.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.activity.BaseActivity;
import com.dci.falcon.activity.MapActivity;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.fragment.FalconMapFragment;
import com.dci.falcon.fragment.RequestBloodFragment;
import com.dci.falcon.fragment.RespondRequestFragment;
import com.dci.falcon.model.AuthorizerRequestItem;
import com.dci.falcon.model.UpdateRespondRequest;
import com.dci.falcon.model.UpdateRespondRequestResponse;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomButton;
import com.dci.falcon.view.CustomTextView;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

/**
 * Created by vijayaganesh on 12/14/2017.
 */

public class RespondRequestAdapter extends BaseAdapter {


    Context context;
    List<AuthorizerRequestItem> respondRequestItemList;
    BaseActivity baseActivity;
    RespondRequestFragment respondRequestFragment;
    LayoutInflater layoutInflater;
    @Inject
    FalconAPI falconAPI;
    UserError userError;
    UpdateRespondRequestResponse updateRespondRequestResponse;
    private SimpleDateFormat simpleDateFormat;
    @Inject
    SharedPreferences sharedPreferences;

    public RespondRequestAdapter(Context context, List<AuthorizerRequestItem> respondRequestItemList, BaseActivity baseActivity, RespondRequestFragment respondRequestFragment) {
        this.context = context;
        this.respondRequestItemList = respondRequestItemList;
        this.baseActivity = baseActivity;
        this.respondRequestFragment = respondRequestFragment;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        FalconApplication.getContext().getComponent().inject(this);
        userError = new UserError();
        simpleDateFormat = new SimpleDateFormat();

    }


    @Override
    public int getCount() {
        return respondRequestItemList.size();
    }

    @Override
    public AuthorizerRequestItem getItem(int position) {
        return respondRequestItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.respond_request_item, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.textUserName.setVisibility(View.VISIBLE);
        viewHolder.textToUserName.setVisibility(View.VISIBLE);
        viewHolder.textDateTime.setVisibility(View.VISIBLE);
        viewHolder.textFindRoute.setVisibility(View.GONE);


        viewHolder.textUserName.setText("Requestor : " + respondRequestItemList.get(position).getName());

        if (sharedPreferences.getInt(FalconConstants.USEDID, 0) == respondRequestItemList.get(position).getUserID()) {
            viewHolder.textToUserName.setText("To : Yourself ");
        } else {
            viewHolder.textToUserName.setText("To : " + respondRequestItemList.get(position).getToUserName());
        }

        viewHolder.textUserPhoneNumber.setText(respondRequestItemList.get(position).getPhoneNumber());
        viewHolder.textRequestId.setText("Request ID : " + respondRequestItemList.get(position).getUniqueID());
        viewHolder.textPatientName.setText("Patient : " + respondRequestItemList.get(position).getPatientName());
        viewHolder.textUserRequirement.setText("Request : " + respondRequestItemList.get(position).getBloodgroup() + " Blood - " +
                (!respondRequestItemList.get(position).getUnits().equals("1") ? respondRequestItemList.get(position).getUnits() + " Units"
                        : respondRequestItemList.get(position).getUnits() + " Unit"));
        viewHolder.textUserRequirement.setVisibility(View.VISIBLE);

        if (respondRequestItemList.get(position).getAlternateName() != null && !respondRequestItemList.get(position).getAlternateName().isEmpty()) {
            viewHolder.textAlternateName.setText("Alternate Name : " + respondRequestItemList.get(position).getAlternateName());
        }

        if (respondRequestItemList.get(position).getAlternatePhoneNumber() != null && !respondRequestItemList.get(position).getAlternatePhoneNumber().isEmpty()) {
            viewHolder.textAlternateNumber.setText("Alternate Number : " + respondRequestItemList.get(position).getAlternatePhoneNumber());
        }

        Date dob = null;
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            dob = simpleDateFormat1.parse(respondRequestItemList.get(position).getDate());
            simpleDateFormat.applyPattern("dd-MMM-yyyy HH:mm:ss");
            viewHolder.textDateTime.setText(simpleDateFormat.format(dob));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        viewHolder.textAddress.setText(respondRequestItemList.get(position).getAddress());

        viewHolder.buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setMessage(context.getString(R.string.accept_request));
                alertDialog.setPositiveButton(R.string.Yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
//                        Fragment updateRequestStatusFragment = new UpdateRequestStatusFragment();
//                        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
//                        fragmentManager.beginTransaction().replace(R.id.frame_notify_container, updateRequestStatusFragment).commit();
//                        Bundle bundle = new Bundle();
//                        bundle.putInt("id", respondRequestItemList.get(position).getId());
//                        bundle.putInt("status", 1);
//                        updateRequestStatusFragment.setArguments(bundle);
                        updateRespondRequestStatus(respondRequestItemList.get(position).getId(), 1);
                    }

                });
                alertDialog.setNegativeButton(R.string.No, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        });

        viewHolder.buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setMessage(context.getString(R.string.decline_request));
                alertDialog.setPositiveButton(R.string.Yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        updateRespondRequestStatus(respondRequestItemList.get(position).getId(), 2);
                    }
                });
                alertDialog.setNegativeButton(R.string.No, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        });
        viewHolder.buttonNoICant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setMessage(context.getString(R.string.decline_request));
                alertDialog.setPositiveButton(R.string.Yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        updateRespondRequestStatus(respondRequestItemList.get(position).getId(), 2);
                    }
                });
                alertDialog.setNegativeButton(R.string.No, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        });
        final ViewHolder finalViewHolder = viewHolder;
        viewHolder.buttonYesIWill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setMessage(context.getString(R.string.ready_to_respond));
                alertDialog.setPositiveButton(R.string.Yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finalViewHolder.consStatus.setVisibility(View.VISIBLE);
                        finalViewHolder.consAccept.setVisibility(View.GONE);
                        finalViewHolder.imageProfilePic.setVisibility(View.VISIBLE);
                        finalViewHolder.textUserPhoneNumber.setVisibility(View.VISIBLE);
                        finalViewHolder.textRequestId.setVisibility(View.VISIBLE);
                        finalViewHolder.textPatientName.setVisibility(View.VISIBLE);
                        finalViewHolder.textAddress.setVisibility(View.VISIBLE);
                        finalViewHolder.textFindRoute.setVisibility(View.VISIBLE);
                        if (respondRequestItemList.get(position).getAlternateName() != null && !respondRequestItemList.get(position).getAlternateName().isEmpty()) {
                            finalViewHolder.textAlternateName.setVisibility(View.VISIBLE);
                        } else {
                            finalViewHolder.textAlternateName.setVisibility(View.GONE);
                        }

                        if (respondRequestItemList.get(position).getAlternatePhoneNumber() != null && !respondRequestItemList.get(position).getAlternatePhoneNumber().isEmpty()) {
                            finalViewHolder.textAlternateNumber.setVisibility(View.VISIBLE);
                        } else {
                            finalViewHolder.textAlternateNumber.setVisibility(View.GONE);
                        }
                    }
                });
                alertDialog.setNegativeButton(R.string.No, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        });

        if (respondRequestItemList.get(position).getStatus() == 1) {
            viewHolder.buttonNo.setEnabled(false);
            viewHolder.buttonNo.setTextColor(context.getResources().getColor(R.color.hint_grey));
            viewHolder.buttonNo.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_cancel, 0, 0, 0);
            viewHolder.buttonYes.setEnabled(false);
            viewHolder.buttonYes.setTextColor(context.getResources().getColor(R.color.hint_grey));
            viewHolder.buttonYes.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_ok, 0, 0, 0);
            viewHolder.textStatus.setText(context.getString(R.string.Accepted));
            viewHolder.textStatus.setVisibility(View.VISIBLE);
            viewHolder.textStatus.setTextColor(context.getResources().getColor(R.color.accept_green));
            viewHolder.textUserPhoneNumber.setVisibility(View.VISIBLE);
            viewHolder.textRequestId.setVisibility(View.VISIBLE);
            viewHolder.textPatientName.setVisibility(View.VISIBLE);
            viewHolder.textAddress.setVisibility(View.VISIBLE);
            viewHolder.imageProfilePic.setVisibility(View.VISIBLE);
            viewHolder.consAccept.setVisibility(View.GONE);
            viewHolder.consStatus.setVisibility(View.VISIBLE);
            if (respondRequestItemList.get(position).getAlternateName() != null && !respondRequestItemList.get(position).getAlternateName().isEmpty()) {
                viewHolder.textAlternateName.setVisibility(View.VISIBLE);
            } else {
                viewHolder.textAlternateName.setVisibility(View.GONE);
            }

            if (respondRequestItemList.get(position).getAlternatePhoneNumber() != null && !respondRequestItemList.get(position).getAlternatePhoneNumber().isEmpty()) {
                viewHolder.textAlternateNumber.setVisibility(View.VISIBLE);
            } else {
                viewHolder.textAlternateNumber.setVisibility(View.GONE);
            }
        } else if (respondRequestItemList.get(position).getStatus() == 2) {
            viewHolder.buttonNo.setEnabled(false);
            viewHolder.buttonNo.setTextColor(context.getResources().getColor(R.color.hint_grey));
            viewHolder.buttonNo.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_cancel, 0, 0, 0);
            viewHolder.buttonYes.setEnabled(false);
            viewHolder.buttonYes.setTextColor(context.getResources().getColor(R.color.hint_grey));
            viewHolder.buttonYes.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_ok, 0, 0, 0);
            viewHolder.textStatus.setText(context.getString(R.string.closed));
            viewHolder.textStatus.setVisibility(View.VISIBLE);
            viewHolder.textStatus.setTextColor(context.getResources().getColor(R.color.falcon_red));
            viewHolder.textUserPhoneNumber.setVisibility(View.VISIBLE);
            viewHolder.textRequestId.setVisibility(View.VISIBLE);
            viewHolder.textPatientName.setVisibility(View.VISIBLE);
            viewHolder.textAddress.setVisibility(View.VISIBLE);
            viewHolder.imageProfilePic.setVisibility(View.VISIBLE);
            viewHolder.consAccept.setVisibility(View.GONE);
            viewHolder.consStatus.setVisibility(View.VISIBLE);
            if (respondRequestItemList.get(position).getAlternateName() != null && !respondRequestItemList.get(position).getAlternateName().isEmpty()) {
                viewHolder.textAlternateName.setVisibility(View.VISIBLE);
            } else {
                viewHolder.textAlternateName.setVisibility(View.GONE);
            }

            if (respondRequestItemList.get(position).getAlternatePhoneNumber() != null && !respondRequestItemList.get(position).getAlternatePhoneNumber().isEmpty()) {
                viewHolder.textAlternateNumber.setVisibility(View.VISIBLE);
            } else {
                viewHolder.textAlternateNumber.setVisibility(View.GONE);
            }
        } else {
            viewHolder.buttonNo.setEnabled(true);
            viewHolder.buttonNo.setTextColor(context.getResources().getColor(R.color.falcon_red));
            viewHolder.buttonNo.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_cancel_red, 0, 0, 0);
            viewHolder.buttonYes.setEnabled(true);
            viewHolder.buttonYes.setTextColor(context.getResources().getColor(R.color.accept_green));
            viewHolder.buttonYes.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_ok_green, 0, 0, 0);
            viewHolder.textStatus.setVisibility(View.VISIBLE);
            viewHolder.textStatus.setText(context.getString(R.string.active));
            viewHolder.textStatus.setTextColor(context.getResources().getColor(R.color.accept_green));
            viewHolder.textUserPhoneNumber.setVisibility(View.GONE);
            viewHolder.textAlternateNumber.setVisibility(View.GONE);
            viewHolder.textAlternateName.setVisibility(View.GONE);
            viewHolder.textRequestId.setVisibility(View.GONE);
            viewHolder.textPatientName.setVisibility(View.GONE);
            viewHolder.textAddress.setVisibility(View.GONE);
            viewHolder.imageProfilePic.setVisibility(View.GONE);
            viewHolder.consAccept.setVisibility(View.VISIBLE);
            viewHolder.consStatus.setVisibility(View.GONE);

        }
        if (respondRequestItemList.get(position).getProfilePicture() != null) {

            Picasso.with(context)
                    .load(context.getString(R.string.falcon_image_url) + respondRequestItemList.get(position).getProfilePicture())
                    .placeholder(R.mipmap.img_sample_pro_pic)   // optional
//                                    .error(R.drawable.ic_error_fallback)      // optional
//                                    .resize(250, 200)                        // optional
//                                    .rotate(90)                             // optional
                    .into((viewHolder.imageProfilePic));

        } else {
            viewHolder.imageProfilePic.setImageResource(R.mipmap.img_sample_pro_pic);
        }

        viewHolder.textFindRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, MapActivity.class);
                intent.putExtra("lang",respondRequestItemList.get(position).getLang());
                intent.putExtra("lat",respondRequestItemList.get(position).getLat());
                context.startActivity(intent);

            }
        });

        return convertView;
    }

    public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }


    static class ViewHolder {
        @BindView(R.id.image_profile_pic)
        CircularImageView imageProfilePic;
        @BindView(R.id.text_user_name)
        CustomTextView textUserName;
        @BindView(R.id.text_find_route)
        CustomTextView textFindRoute;
        @BindView(R.id.text_user_phone_number)
        CustomTextView textUserPhoneNumber;
        @BindView(R.id.text_patient_name)
        CustomTextView textPatientName;
        @BindView(R.id.text_user_requirement)
        CustomTextView textUserRequirement;
        @BindView(R.id.text_date_time)
        CustomTextView textDateTime;
        @BindView(R.id.button_yes)
        CustomButton buttonYes;
        @BindView(R.id.button_no)
        CustomButton buttonNo;
        @BindView(R.id.res_reg_list_item)
        ConstraintLayout resRegListItem;
        @BindView(R.id.card_respond_req)
        CardView cardRespondReq;
        @BindView(R.id.cons_respond_req_item)
        ConstraintLayout consRespondReqItem;
        @BindView(R.id.text_status)
        CustomTextView textStatus;
        @BindView(R.id.text_request_id)
        CustomTextView textRequestId;
        @BindView(R.id.text_user_address)
        CustomTextView textAddress;
        @BindView(R.id.text_to_user_name)
        CustomTextView textToUserName;
        @BindView(R.id.button_yes_i_will)
        CustomButton buttonYesIWill;
        @BindView(R.id.button_no_i_cant)
        CustomButton buttonNoICant;
        @BindView(R.id.cons_accept)
        ConstraintLayout consAccept;
        @BindView(R.id.cons_status)
        ConstraintLayout consStatus;
        @BindView(R.id.text_user_alternate_name)
        CustomTextView textAlternateName;
        @BindView(R.id.text_user_alternate_number)
        CustomTextView textAlternateNumber;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    private void updateRespondRequestStatus(int id, int status) {
        UpdateRespondRequest updateRespondRequest = new UpdateRespondRequest();
        updateRespondRequest.setId(id);
        updateRespondRequest.setNotes("");
        updateRespondRequest.setScheduleTime("");
        updateRespondRequest.setUserResStatus(status);
        updateRespondRequest.setTravellingStatus(0);
        if (Utils.isNetworkAvailable()) {
            baseActivity.showProgress();
            falconAPI.updateRespondRequest(updateRespondRequest).enqueue(new Callback<UpdateRespondRequestResponse>() {
                @Override
                public void onResponse(Call<UpdateRespondRequestResponse> call, Response<UpdateRespondRequestResponse> response) {
                    baseActivity.hideProgress();
                    updateRespondRequestResponse = response.body();
                    if (response.isSuccessful() && response.isSuccessful() && updateRespondRequestResponse.getStatusCode() == 200) {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                        alertDialog.setMessage(context.getString(R.string.successfully_update));
                        alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
//                                dialogInterface.dismiss();
                                respondRequestFragment.getRespondRequest();
//                                baseActivity.onBackPressed();
                            }

                        });
                        alertDialog.show();
                    } else {
                        Toast.makeText(context, context.getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<UpdateRespondRequestResponse> call, Throwable t) {
                    baseActivity.hideProgress();
                    Toast.makeText(context, context.getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();

                }
            });
        } else {
            userError.message = context.getString(R.string.network_error_message);
            baseActivity.showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }
    }

}
