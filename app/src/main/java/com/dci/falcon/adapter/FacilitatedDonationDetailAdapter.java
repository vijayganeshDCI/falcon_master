package com.dci.falcon.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.icu.text.DecimalFormat;
import android.os.AsyncTask;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.dci.falcon.R;
import com.dci.falcon.model.FacilitatedDonationDetail;
import com.dci.falcon.view.CustomButton;
import com.dci.falcon.view.CustomTextView;
import com.github.siyamed.shapeimageview.CircularImageView;

import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by vijayaganesh on 1/8/2018.
 */

public class FacilitatedDonationDetailAdapter extends BaseAdapter {
    private SimpleDateFormat simpleDateFormat;

    public FacilitatedDonationDetailAdapter(Context context, List<FacilitatedDonationDetail> facilitatedDonationDetailList) {
        this.context = context;
        this.facilitatedDonationDetailList = facilitatedDonationDetailList;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        simpleDateFormat=new SimpleDateFormat();
    }

    Context context;
    List<FacilitatedDonationDetail> facilitatedDonationDetailList;
    LayoutInflater layoutInflater;

    @Override
    public int getCount() {
        return facilitatedDonationDetailList.size();
    }

    @Override
    public FacilitatedDonationDetail getItem(int position) {
        return facilitatedDonationDetailList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.facilitated_donation_detail_item, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.textUserName.setText("Requestor : "+facilitatedDonationDetailList.get(position).getRequestorname());
        viewHolder.textPatientName.setText(facilitatedDonationDetailList.get(position).getName()!=null?"Patient : " + facilitatedDonationDetailList.get(position).getName()+" - "
        +facilitatedDonationDetailList.get(position).getPatientName():"Patient : " + facilitatedDonationDetailList.get(position).getName());
        viewHolder.textPhoneNumber.setText(facilitatedDonationDetailList.get(position).getPhoneNumber());
        viewHolder.textLocation.setText(facilitatedDonationDetailList.get(position).getLocations());
        Date dob = null;
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        try {
            dob = simpleDateFormat1.parse(facilitatedDonationDetailList.get(position).getDate());
            simpleDateFormat.applyPattern("dd-MMM-yyyy");
            viewHolder.textDate.setText(simpleDateFormat.format(dob) );
        } catch (ParseException e) {
            e.printStackTrace();
        }
        viewHolder.textNotes.setText("Donated : " + facilitatedDonationDetailList.get(position).getBloodGroup() + " Blood - "
                + facilitatedDonationDetailList.get(position).getUnits()+" Units");

        if (facilitatedDonationDetailList.get(position).getProfilePicture() != null) {
            new DownloadImageTask((viewHolder.imageProfilePic)).execute(context.getString(R.string.falcon_image_url) + facilitatedDonationDetailList.get(position).getProfilePicture());
        } else {
            viewHolder.imageProfilePic.setImageResource(R.mipmap.img_sample_pro_pic);
        }
        return convertView;
    }


    public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    static class ViewHolder {
        @BindView(R.id.image_profile_pic)
        CircularImageView imageProfilePic;
        @BindView(R.id.text_user_name)
        CustomTextView textUserName;
        @BindView(R.id.text_phone_number)
        CustomTextView textPhoneNumber;
        @BindView(R.id.text_location)
        CustomTextView textLocation;
        @BindView(R.id.text_date)
        CustomTextView textDate;
        @BindView(R.id.text_notes)
        CustomTextView textNotes;
        @BindView(R.id.text_patient_name)
        CustomTextView textPatientName;
        @BindView(R.id.button_no)
        CustomButton buttonNo;
        @BindView(R.id.facilitated_detail_list_item)
        ConstraintLayout facilitatedDetailListItem;
        @BindView(R.id.card_facilited_detail)
        CardView cardFacilitedDetail;
        @BindView(R.id.cons_facilitated_dona_detail_item)
        ConstraintLayout consFacilitatedDonaDetailItem;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
