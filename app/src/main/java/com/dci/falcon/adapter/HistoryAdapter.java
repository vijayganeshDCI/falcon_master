package com.dci.falcon.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.dci.falcon.R;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.model.HistoryResponseItem;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.view.CustomTextView;
import com.github.siyamed.shapeimageview.CircularImageView;

import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by vijayaganesh on 11/14/2017.
 */

public class HistoryAdapter extends BaseAdapter {

    List<HistoryResponseItem> historyItemList;
    Context context;
    LayoutInflater layoutInflater;
    @Inject
    SharedPreferences sharedPreferences;
    SimpleDateFormat simpleDateFormat;

    public HistoryAdapter(List<HistoryResponseItem> historyItemList, Context context) {
        this.historyItemList = historyItemList;
        this.context = context;
        FalconApplication.getContext().getComponent().inject(this);
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        simpleDateFormat=new SimpleDateFormat();
    }

    @Override
    public int getCount() {
        return historyItemList.size();
    }

    @Override
    public HistoryResponseItem getItem(int position) {
        return historyItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.history_list_item, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (sharedPreferences.getInt(FalconConstants.ROLEID, 0) == 0) {
            viewHolder.textHistoryDescription.setText(historyItemList.get(position).getDescription());
            Date dob = null;
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
            try {
                dob = simpleDateFormat1.parse(historyItemList.get(position).getCreatedDate());
                simpleDateFormat.applyPattern("dd-MMM-yyyy");
                viewHolder.textDate.setText(simpleDateFormat.format(dob));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            viewHolder.textPatientName.setVisibility(View.GONE);
            viewHolder.imageProfilePic.setVisibility(View.GONE);
            viewHolder.textDonatedDetail.setVisibility(View.GONE);
            viewHolder.textPhoneNumber.setVisibility(View.GONE);
        } else {
            viewHolder.textHistoryDescription.setText("Donor : " + historyItemList.get(position).getName());
            viewHolder.textPhoneNumber.setText(historyItemList.get(position).getPhoneNumber());
            viewHolder.textPatientName.setText(
                    historyItemList.get(position).getPatienttID()!=null?"Patient : " + historyItemList.get(position).getPatientName()+" - "+
            historyItemList.get(position).getPatienttID():"Patient : " + historyItemList.get(position).getPatientName());
            viewHolder.textDonatedDetail.setText("Donated : " + historyItemList.get(position).getBloodGroup() + " blood - " +
                    (!historyItemList.get(position).getUnits().equals("1")?historyItemList.get(position).getUnits() + " Units "
                    :historyItemList.get(position).getUnits() + " Unit "));
            Date dob = null;
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
            try {
                dob = simpleDateFormat1.parse(historyItemList.get(position).getDonateTime());
                simpleDateFormat.applyPattern("dd-MMM-yyyy");
                viewHolder.textDate.setText(simpleDateFormat.format(dob));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            viewHolder.textPatientName.setVisibility(View.VISIBLE);
            viewHolder.imageProfilePic.setVisibility(View.GONE);
            viewHolder.textDonatedDetail.setVisibility(View.VISIBLE);
            viewHolder.textPhoneNumber.setVisibility(View.GONE);
            if (historyItemList.get(position).getProfilePicture() != null) {
                new DownloadImageTask((viewHolder.imageProfilePic)).execute(context.getString(R.string.falcon_image_url) + historyItemList.get(position).getProfilePicture());
            } else {
                viewHolder.imageProfilePic.setImageResource(R.mipmap.img_sample_pro_pic);
            }

        }


        return convertView;
    }


    public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    static class ViewHolder {
        @BindView(R.id.image_profile_pic)
        CircularImageView imageProfilePic;
        @BindView(R.id.text_history_description)
        CustomTextView textHistoryDescription;
        @BindView(R.id.text_phone_number)
        CustomTextView textPhoneNumber;
        @BindView(R.id.text_type)
        CustomTextView textPatientName;
        @BindView(R.id.text_donated_detail)
        CustomTextView textDonatedDetail;
        @BindView(R.id.text_date)
        CustomTextView textDate;
        @BindView(R.id.cons_history_item)
        ConstraintLayout consHistoryItem;
        @BindView(R.id.card_history_list_item)
        CardView cardHistoryListItem;
        @BindView(R.id.cons_history_list_item)
        ConstraintLayout consHistoryListItem;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
