package com.dci.falcon.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.icu.text.DecimalFormat;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.activity.BaseActivity;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.fragment.TrackMyRequestFragment;
import com.dci.falcon.model.CloseRequirement;
import com.dci.falcon.model.LogoutResponse;
import com.dci.falcon.model.TrackMyRequestItem;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.FalconConstants;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomButton;
import com.dci.falcon.view.CustomTextView;
import com.github.siyamed.shapeimageview.CircularImageView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

/**
 * Created by vijayaganesh on 1/5/2018.
 */

public class TrackMyRequestAdapter extends BaseAdapter {

    LayoutInflater layoutInflater;
    Context context;
    List<TrackMyRequestItem> respondRequestItemList;
    BaseActivity baseActivity;
    TrackMyRequestFragment trackMyRequestFragment;
    @Inject
    FalconAPI falconAPI;
    LogoutResponse logoutResponse;
    @Inject
    SharedPreferences sharedPreferences;
    UserError userError;
    private SimpleDateFormat simpleDateFormat;

    public TrackMyRequestAdapter(Context context, List<TrackMyRequestItem> respondRequestItemList, BaseActivity baseActivity, TrackMyRequestFragment trackMyRequestFragment) {
        this.context = context;
        this.respondRequestItemList = respondRequestItemList;
        this.baseActivity = baseActivity;
        this.trackMyRequestFragment = trackMyRequestFragment;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        FalconApplication.getContext().getComponent().inject(this);
        userError = new UserError();
        simpleDateFormat = new SimpleDateFormat();
    }


    @Override
    public int getCount() {
        return respondRequestItemList.size();
    }

    @Override
    public TrackMyRequestItem getItem(int position) {
        return respondRequestItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.track_my_request_list_item, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.textRequest.setText("Request : " + respondRequestItemList.get(position).getBloodGroup() + " Blood - "
                + (!respondRequestItemList.get(position).getBloodUnits().equals("1") ?
                respondRequestItemList.get(position).getBloodUnits() + " Units"
                : respondRequestItemList.get(position).getBloodUnits() + " Unit"));
        viewHolder.textPatientName.setText("Patient : " + respondRequestItemList.get(position).getPatientName());
        viewHolder.textHosAdd.setText(respondRequestItemList.get(position).getHospAddress());
        Date dob = null;
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            dob = simpleDateFormat1.parse(respondRequestItemList.get(position).getDateTime());
            simpleDateFormat.applyPattern("dd-MMM-yyyy HH:mm:ss");
            viewHolder.textDateTime.setText(simpleDateFormat.format(dob));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        switch (respondRequestItemList.get(position).getStatus()) {
            case 0:
//                Not Active
                viewHolder.textStatus.setText(context.getString(R.string.active));
                viewHolder.textStatus.setTextColor(context.getResources().getColor(R.color.accept_green));
                viewHolder.buttonNo.setEnabled(true);
                viewHolder.buttonNo.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_cancel_red, 0, 0, 0);
                viewHolder.buttonNo.setTextColor(context.getResources().getColor(R.color.falcon_red));
                break;
            case 1:
//                Active
                viewHolder.textStatus.setText(context.getString(R.string.active));
                viewHolder.textStatus.setTextColor(context.getResources().getColor(R.color.accept_green));
                viewHolder.buttonNo.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_cancel_red, 0, 0, 0);
                viewHolder.buttonNo.setTextColor(context.getResources().getColor(R.color.falcon_red));
                viewHolder.buttonNo.setEnabled(true);
                break;
            case 2:
                //Completed
                viewHolder.textStatus.setText(context.getString(R.string.closed));
                viewHolder.textStatus.setTextColor(context.getResources().getColor(R.color.accept_green));
                viewHolder.buttonNo.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_cancel, 0, 0, 0);
                viewHolder.buttonNo.setTextColor(context.getResources().getColor(R.color.hint_grey));
                viewHolder.buttonNo.setEnabled(false);
                break;
            case 3:
                //time exceed
                viewHolder.textStatus.setText(context.getString(R.string.time_expired));
                viewHolder.textStatus.setTextColor(context.getResources().getColor(R.color.falcon_red));
                viewHolder.buttonNo.setTextColor(context.getResources().getColor(R.color.hint_grey));
                viewHolder.buttonNo.setEnabled(false);
                viewHolder.buttonNo.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_cancel, 0, 0, 0);
                break;
        }

        viewHolder.buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(context);
                alertDialog.setMessage(context.getString(R.string.close_req));
                alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        closeRequirement(respondRequestItemList.get(position).getId());
                    }
                });
                alertDialog.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterfac, int i) {
                        dialogInterfac.dismiss();
                    }
                });
                alertDialog.show();
            }
        });

        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.image_profile_pic)
        CircularImageView imageProfilePic;
        @BindView(R.id.text_request)
        CustomTextView textRequest;
        @BindView(R.id.text_patient_name)
        CustomTextView textPatientName;
        @BindView(R.id.text_hos_add)
        CustomTextView textHosAdd;
        @BindView(R.id.text_date_time)
        CustomTextView textDateTime;
        @BindView(R.id.text_status)
        CustomTextView textStatus;
        @BindView(R.id.button_no)
        CustomButton buttonNo;
        @BindView(R.id.track_reg_list_item)
        ConstraintLayout trackRegListItem;
        @BindView(R.id.card_track_my_req)
        CardView cardTrackMyReq;
        @BindView(R.id.cons_track_my_request)
        ConstraintLayout consTrackMyRequest;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    private void closeRequirement(int id) {
        CloseRequirement closeRequirement = new CloseRequirement();
        closeRequirement.setNofID(id);
        if (Utils.isNetworkAvailable()) {
            baseActivity.showProgress();
            falconAPI.closeRequirement(closeRequirement).enqueue(new Callback<LogoutResponse>() {
                @Override
                public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {
                    baseActivity.hideProgress();
                    logoutResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && logoutResponse.getStatusCode() == 200) {
                        if (sharedPreferences.getInt(FalconConstants.ROLEID, 0) == 0)
                            trackMyRequestFragment.getTrackMyRequest();
                        else
                            trackMyRequestFragment.getHospitalTrackMyrequest();

                    } else
                        Toast.makeText(context, context.getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onFailure(Call<LogoutResponse> call, Throwable t) {
                    baseActivity.hideProgress();
                    Toast.makeText(context, context.getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            userError.message = context.getString(R.string.network_error_message);
            baseActivity.showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }

    }
}
