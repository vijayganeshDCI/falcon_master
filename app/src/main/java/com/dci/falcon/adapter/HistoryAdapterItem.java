package com.dci.falcon.adapter;

import android.support.v7.widget.RecyclerView;

import com.dci.falcon.model.HistoryResponseItem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by vijayaganesh on 12/11/2017.
 */

public abstract class HistoryAdapterItem <VH extends RecyclerView.ViewHolder>
            extends RecyclerView.Adapter<VH> {
        private List<HistoryResponseItem> items = new ArrayList<>();

        public HistoryAdapterItem() {
            setHasStableIds(true);
        }

        public void addAll(Collection<? extends HistoryResponseItem> collection) {
            if (collection != null) {
                items.addAll(collection);
                notifyDataSetChanged();
            }
        }

        public void clear() {
            items.clear();
            notifyDataSetChanged();
        }

        public void remove(int position) {
            items.remove(position);
            notifyDataSetChanged();
        }

        public HistoryResponseItem getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return getItem(position).hashCode();
        }

        @Override
        public int getItemCount() {
            return items.size();
        }
    }

