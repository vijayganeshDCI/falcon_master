package com.dci.falcon.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.dci.falcon.R;
import com.dci.falcon.model.OrgListItem;
import com.dci.falcon.view.CustomTextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by vijayaganesh on 11/23/2017.
 */

public class OrgListAdapter extends BaseAdapter {

    Context context;
    List<OrgListItem> orgListItems;
    LayoutInflater layoutInflater;

    public OrgListAdapter(Context context, List<OrgListItem> orgListItems) {
        this.context = context;
        this.orgListItems = orgListItems;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return orgListItems.size();
    }

    @Override
    public OrgListItem getItem(int position) {
        return orgListItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.custom_spinner_item, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.textSpinnerItem.setText(orgListItems.get(position).getName());
        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.text_spinner_item)
        CustomTextView textSpinnerItem;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
