package com.dci.falcon.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.icu.text.DecimalFormat;
import android.os.AsyncTask;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.dci.falcon.R;
import com.dci.falcon.model.TrackMyRequestDetail;
import com.dci.falcon.view.CustomButton;
import com.dci.falcon.view.CustomTextView;
import com.github.siyamed.shapeimageview.CircularImageView;

import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by vijayaganesh on 1/5/2018.
 */

public class TrackMyRequestDetailAdapter extends BaseAdapter {

    private SimpleDateFormat simpleDateFormat;

    public TrackMyRequestDetailAdapter(Context context, List<TrackMyRequestDetail> trackMyRequestDetails) {
        this.context = context;
        this.trackMyRequestDetails = trackMyRequestDetails;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        simpleDateFormat=new SimpleDateFormat();
    }

    Context context;
    List<TrackMyRequestDetail> trackMyRequestDetails;
    LayoutInflater layoutInflater;

    @Override
    public int getCount() {
        return trackMyRequestDetails.size();
    }

    @Override
    public TrackMyRequestDetail getItem(int position) {
        return trackMyRequestDetails.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.track_my_quest_detail_list, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.textUserName.setText("Donor : " + trackMyRequestDetails.get(position).getName());
        viewHolder.textPhoneNumber.setText(trackMyRequestDetails.get(position).getPhoneNumber());
        Date dob = null;
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            dob = simpleDateFormat1.parse(trackMyRequestDetails.get(position).getDateTime());
            simpleDateFormat.applyPattern("dd-MMM-yyyy HH:mm:ss");
            viewHolder.textDate.setText(simpleDateFormat.format(dob));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        switch (trackMyRequestDetails.get(position).getStatus()) {
            case 0:
                //Open
                viewHolder.textStatus.setText(context.getString(R.string.active));
                viewHolder.textStatus.setTextColor(context.getResources().getColor(R.color.accept_green));
                break;
            case 1:
                //Accepted
                viewHolder.textStatus.setText(context.getString(R.string.Accepted));
                viewHolder.textStatus.setTextColor(context.getResources().getColor(R.color.accept_green));

                break;
            case 2:
                //Rejected
                viewHolder.textStatus.setText(context.getString(R.string.closed));
                viewHolder.textStatus.setTextColor(context.getResources().getColor(R.color.falcon_red));

                break;
        }

        if (trackMyRequestDetails.get(position).getProfilePicture() != null) {
            new DownloadImageTask((viewHolder.imageProfilePic)).execute(context.getString(R.string.falcon_image_url) + trackMyRequestDetails.get(position).getProfilePicture());
        } else {
            viewHolder.imageProfilePic.setImageResource(R.mipmap.img_sample_pro_pic);
        }
        return convertView;
    }


    public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    static class ViewHolder {
        @BindView(R.id.image_profile_pic)
        CircularImageView imageProfilePic;
        @BindView(R.id.text_user_name)
        CustomTextView textUserName;
        @BindView(R.id.text_phone_number)
        CustomTextView textPhoneNumber;
        @BindView(R.id.text_date)
        CustomTextView textDate;
        @BindView(R.id.text_status)
        CustomTextView textStatus;
        @BindView(R.id.text_notes)
        CustomTextView textNotes;
        @BindView(R.id.button_no)
        CustomButton buttonNo;
        @BindView(R.id.track_detail_reg_list_item)
        ConstraintLayout trackDetailRegListItem;
        @BindView(R.id.card_track_detail_req)
        CardView cardTrackDetailReq;
        @BindView(R.id.cons_track_my_request_detail)
        ConstraintLayout consTrackMyRequestDetail;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
