package com.dci.falcon.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.dci.falcon.R;
import com.dci.falcon.activity.BaseActivity;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.fragment.AddMemberFragment;
import com.dci.falcon.fragment.RequestBloodFragment;
import com.dci.falcon.fragment.ViewInformationFragment;
import com.dci.falcon.model.FamilyMembersInfoItem;
import com.dci.falcon.model.MemberDelete;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomTextView;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.io.InputStream;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

public class ViewMemberInfoAdapter extends RecyclerSwipeAdapter<ViewMemberInfoAdapter.SimpleViewHolder> {


    Context context;
    List<FamilyMembersInfoItem> viewInfoMemberListItems;
    BaseActivity baseActivity;
    ViewInformationFragment viewInformationFragment;
    UserError userError;
    @Inject
    FalconAPI falconAPI;

    public ViewMemberInfoAdapter(Context context, List<FamilyMembersInfoItem> viewInfoMemberListItems, BaseActivity baseActivity, ViewInformationFragment viewInformationFragment) {
        this.context = context;
        this.viewInfoMemberListItems = viewInfoMemberListItems;
        this.baseActivity = baseActivity;
        this.viewInformationFragment = viewInformationFragment;
        userError = new UserError();
        FalconApplication.getContext().getComponent().inject(this);

    }


    @Override
    public int getItemCount() {
        return viewInfoMemberListItems.size();
    }


    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.member_item_view, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder viewHolder, final int position) {
        FamilyMembersInfoItem item = viewInfoMemberListItems.get(position);

        viewHolder.textMemName.setText(viewInfoMemberListItems.get(position).getFirstName());
        viewHolder.textViewPhoneNumber.setText(viewInfoMemberListItems.get(position).getPhoneNumber());
        viewHolder.textMemBloodGroup.setText("Blood Group:" + " " + viewInfoMemberListItems.get(position).getBloodGroupID());
        viewHolder.textRelation.setText(viewInfoMemberListItems.get(position).getRelationship());

        if (viewInfoMemberListItems.get(position).getProfilePicture() != null) {
            Picasso.with(context)
                    .load(context.getString(R.string.falcon_image_url) + viewInfoMemberListItems.get(position).getProfilePicture())
                    .placeholder(R.mipmap.img_sample_pro_pic)   // optional
//                                    .error(R.drawable.ic_error_fallback)      // optional
//                                    .resize(250, 200)                        // optional
//                                    .rotate(90)                             // optional
                    .into((viewHolder.imageViewProPic));
        } else {
            viewHolder.imageViewProPic.setImageResource(R.mipmap.img_sample_pro_pic);
        }

        viewHolder.swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);
        viewHolder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {
            }
        });

        if (viewInfoMemberListItems.get(position).isLogged_in_status()) {
//            Logged in
            viewHolder.imageEdit.setImageResource(R.mipmap.icon_user_information);
            viewHolder.textEdit.setText(R.string.info);
        } else {
//            not yet log in
            viewHolder.imageEdit.setImageResource(R.mipmap.icon_edit);
            viewHolder.textEdit.setText(R.string.edit);
        }

        viewHolder.memberDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(context);
                alertDialog.setMessage(context.getString(R.string.remove));
                alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        memberDelete(viewInfoMemberListItems.get(position).getId(), position);
                    }
                });
                alertDialog.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        });

        viewHolder.memberEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewInfoMemberListItems.get(position).isLogged_in_status()) {
                    Fragment addMemberFragment = new AddMemberFragment();
                    FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.frame_home_container, addMemberFragment).addToBackStack("requestBloodFragment").commit();
                    Bundle bundle = new Bundle();
                    bundle.putInt("memberID", viewInfoMemberListItems.get(position).getId());
                    bundle.putBoolean("isFromViewInfo", true);
                    bundle.putBoolean("isLogged", viewInfoMemberListItems.get(position).isLogged_in_status());
                    addMemberFragment.setArguments(bundle);
                } else {
                    android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(context);
                    alertDialog.setMessage(context.getString(R.string.edit_profile));
                    alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Fragment addMemberFragment = new AddMemberFragment();
                            FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                            fragmentManager.beginTransaction().replace(R.id.frame_home_container, addMemberFragment).addToBackStack("requestBloodFragment").commit();
                            Bundle bundle = new Bundle();
                            bundle.putInt("memberID", viewInfoMemberListItems.get(position).getId());
                            bundle.putBoolean("isFromViewInfo", true);
                            bundle.putBoolean("isLogged", viewInfoMemberListItems.get(position).isLogged_in_status());
                            addMemberFragment.setArguments(bundle);
                        }
                    });
                    alertDialog.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog.show();
                }

            }
        });
        viewHolder.memberQuickBloodRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(context);
                alertDialog.setMessage(context.getString(R.string.alert_give_blood_req_from_view_info) + " " + viewInfoMemberListItems.get(position).getFirstName() + " ?");
                alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        RequestBloodFragment requestBloodFragment = new RequestBloodFragment();
                        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.frame_home_container, requestBloodFragment).addToBackStack("requestBloodFragment").commit();
                        Bundle bundle = new Bundle();
                        bundle.putBoolean("isFromQuestRequest", true);
                        bundle.putString("bloodGroup", viewInfoMemberListItems.get(position).getBloodGroupID());
                        bundle.putString("patientName", viewInfoMemberListItems.get(position).getFirstName());
                        requestBloodFragment.setArguments(bundle);

                    }
                });
                alertDialog.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        });
        mItemManger.bindView(viewHolder.itemView, position);
    }


    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }


    public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {
        SwipeLayout swipeLayout;
        public CircularImageView imageViewProPic;
        public CustomTextView textMemName, textRelation;
        public CustomTextView textMemBloodGroup;
        public CustomTextView textViewPhoneNumber;
        public LinearLayout memberEdit;
        public LinearLayout memberDelete;
        public LinearLayout memberQuickBloodRequest;
        public ImageView imageEdit;
        public CustomTextView textEdit;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            imageViewProPic = (CircularImageView) itemView.findViewById(R.id.image_profile_pic);
            imageEdit = (ImageView) itemView.findViewById(R.id.image_member_edit);
            textMemName = (CustomTextView) itemView.findViewById(R.id.text_member_name);
            textEdit = (CustomTextView) itemView.findViewById(R.id.text_member_edit);
            textRelation = (CustomTextView) itemView.findViewById(R.id.text_member_blood_realtion);
            textMemBloodGroup = (CustomTextView) itemView.findViewById(R.id.text_member_blood_group);
            textViewPhoneNumber = (CustomTextView) itemView.findViewById(R.id.text_member_phone_number);
            memberEdit = (LinearLayout) itemView.findViewById(R.id.linear_member_edit);
            memberDelete = (LinearLayout) itemView.findViewById(R.id.linear_member_delete);
            memberQuickBloodRequest = (LinearLayout) itemView.findViewById(R.id.linear_member_quick_req);
        }
    }

    private void memberDelete(int memberID, final int pos) {
        MemberDelete memberDelete = new MemberDelete();
        memberDelete.setMember_id(memberID);
        if (Utils.isNetworkAvailable()) {
            baseActivity.showProgress();
            falconAPI.memberDelete(memberDelete).enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    baseActivity.hideProgress();
                    if (response.isSuccessful() && response.body() != null) {
                        android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(context);
                        alertDialog.setMessage(context.getString(R.string.successfully_deleted));
                        alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                viewInfoMemberListItems.remove(pos);
                                viewInformationFragment.getFamilyMembers();
                                notifyDataSetChanged();
                            }
                        });
                        alertDialog.show();
                    } else {
                        Toast.makeText(context, context.getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    baseActivity.hideProgress();
                    Toast.makeText(context, context.getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();

                }
            });
        } else {
            userError.message = context.getString(R.string.network_error_message);
            baseActivity.showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }

    }

}
