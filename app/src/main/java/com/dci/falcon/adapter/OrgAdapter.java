package com.dci.falcon.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.dci.falcon.R;
import com.dci.falcon.model.OrgListItem;
import com.dci.falcon.view.CustomTextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by vijayaganesh on 11/29/2017.
 */

public class OrgAdapter extends ArrayAdapter<OrgListItem> {

    Context context;
    List<OrgListItem> orgListItems;
    LayoutInflater layoutInflater;

    public OrgAdapter(@NonNull Context context, @LayoutRes int resource,List<OrgListItem> orgListItems) {
        super(context, resource, orgListItems);
        this.context = context;
        this.orgListItems = orgListItems;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return orgListItems.size();
    }

    @Override
    public OrgListItem getItem(int position) {
        return orgListItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.custom_spinner_item, parent,false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.textSpinnerItem.setText(orgListItems.get(position).getName());
        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.text_spinner_item)
        CustomTextView textSpinnerItem;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
