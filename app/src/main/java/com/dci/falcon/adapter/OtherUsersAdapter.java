package com.dci.falcon.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.dci.falcon.fragment.AcceptRequestFragment;
import com.dci.falcon.fragment.AddtionalUsersFragment;
import com.dci.falcon.fragment.FamilyMembersDonationsFragment;

/**
 * Created by vijayaganesh on 1/16/2018.
 */

public class OtherUsersAdapter extends FragmentStatePagerAdapter {
    int numOfItems;

    public OtherUsersAdapter(FragmentManager fm, int numOfItems) {
        super(fm);
        this.numOfItems = numOfItems;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                FamilyMembersDonationsFragment familyMembersDonationsFragment = new FamilyMembersDonationsFragment();
                return familyMembersDonationsFragment;
            case 1:
                AddtionalUsersFragment addtionalUsersFragment = new AddtionalUsersFragment();
                return addtionalUsersFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numOfItems;
    }
}
