package com.dci.falcon.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.dci.falcon.R;
import com.dci.falcon.activity.BaseActivity;
import com.dci.falcon.app.FalconApplication;
import com.dci.falcon.app.UserError;
import com.dci.falcon.fragment.AuthorizerRequestFragment;
import com.dci.falcon.model.AuthorizerRequestItem;
import com.dci.falcon.model.AuthorizerRequestResponse;
import com.dci.falcon.model.AuthorizerRequestUpdate;
import com.dci.falcon.model.AuthorizerRequestUpdateResponse;
import com.dci.falcon.retrofit.FalconAPI;
import com.dci.falcon.utills.Utils;
import com.dci.falcon.view.CustomButton;
import com.dci.falcon.view.CustomTextView;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.falcon.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

/**
 * Created by vijayaganesh on 12/13/2017.
 */

public class AuthorizerRequestAdapter extends BaseAdapter {

    LayoutInflater layoutInflater;
    Context context;
    List<AuthorizerRequestItem> authorizerRequestItemList;
    @Inject
    FalconAPI falconAPI;
    @Inject
    SharedPreferences sharedPreferences;
    AuthorizerRequestResponse authorizerRequestResponse;
    AuthorizerRequestUpdateResponse authorizerRequestUpdateResponse;
    UserError userError;
    BaseActivity baseActivity;
    AuthorizerRequestFragment authorizerRequestFragment;
    private SimpleDateFormat simpleDateFormat;

    public AuthorizerRequestAdapter(Context context, List<AuthorizerRequestItem> authorizerRequestItemList, BaseActivity baseActivity, AuthorizerRequestFragment authorizerRequestFragment) {
        this.context = context;
        this.authorizerRequestItemList = authorizerRequestItemList;
        this.baseActivity = baseActivity;
        this.authorizerRequestFragment = authorizerRequestFragment;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        FalconApplication.getContext().getComponent().inject(this);
        userError = new UserError();
        simpleDateFormat = new SimpleDateFormat();
    }


    @Override
    public int getCount() {
        return authorizerRequestItemList.size();
    }

    @Override
    public AuthorizerRequestItem getItem(int position) {
        return authorizerRequestItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.authorizer_request_item, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.textUserName.setVisibility(View.VISIBLE);
        viewHolder.textUserRequirement.setVisibility(View.VISIBLE);
        viewHolder.textDateTime.setVisibility(View.VISIBLE);


        viewHolder.textUserName.setText("Requestor : " + authorizerRequestItemList.get(position).getName());

        viewHolder.textUserPhoneNumber.setText(authorizerRequestItemList.get(position).getPhoneNumber());

        viewHolder.textPatientName.setText("Patient : " + authorizerRequestItemList.get(position).getPatientName());

        viewHolder.textUserRequirement.setText("Request : " + authorizerRequestItemList.get(position).getBloodgroup() + " Blood - " +
                (!authorizerRequestItemList.get(position).getUnits().equals("1") ? authorizerRequestItemList.get(position).getUnits() + " Units"
                        : authorizerRequestItemList.get(position).getUnits() + " Unit"));

        viewHolder.textAddress.setText(authorizerRequestItemList.get(position).getAddress());

        if(authorizerRequestItemList.get(position).getAlternateName()!=null && !authorizerRequestItemList.get(position).getAlternateName().isEmpty() ) {
            viewHolder.textAlternateName.setText("Alternate Name : "+authorizerRequestItemList.get(position).getAlternateName());
        }

        if (authorizerRequestItemList.get(position).getAlternatePhoneNumber()!=null && !authorizerRequestItemList.get(position).getAlternatePhoneNumber().isEmpty()){
            viewHolder.textAlternateNumber.setText("Alternate Number : "+authorizerRequestItemList.get(position).getAlternatePhoneNumber());
        }


        Date dob = null;
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            dob = simpleDateFormat1.parse(authorizerRequestItemList.get(position).getDate());
            simpleDateFormat.applyPattern("dd-MMM-yyyy HH:mm:ss");
            viewHolder.textDateTime.setText(simpleDateFormat.format(dob));

        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (authorizerRequestItemList.get(position).getStatus() == 1) {
            viewHolder.buttonAuthorizeTenMin.setVisibility(View.GONE);
        } else {
            viewHolder.buttonAuthorizeTenMin.setVisibility(View.VISIBLE);
        }

        if (authorizerRequestItemList.get(position).getStatus() == 0) {
            viewHolder.textUserPhoneNumber.setVisibility(View.GONE);
            viewHolder.textAlternateNumber.setVisibility(View.GONE);
            viewHolder.textAlternateName.setVisibility(View.GONE);
            viewHolder.textPatientName.setVisibility(View.GONE);
            viewHolder.textAddress.setVisibility(View.GONE);
            viewHolder.imageProfilePic.setVisibility(View.GONE);
            viewHolder.consStatus.setVisibility(View.GONE);
            viewHolder.consAccept.setVisibility(View.VISIBLE);
        } else {
            viewHolder.textUserPhoneNumber.setVisibility(View.VISIBLE);
            if(authorizerRequestItemList.get(position).getAlternateName()!=null && !authorizerRequestItemList.get(position).getAlternateName().isEmpty()){
                viewHolder.textAlternateName.setVisibility(View.VISIBLE);
            }else {
                viewHolder.textAlternateName.setVisibility(View.GONE);
            }
            if (authorizerRequestItemList.get(position).getAlternatePhoneNumber()!=null && !authorizerRequestItemList.get(position).getAlternatePhoneNumber().isEmpty()){
                viewHolder.textAlternateNumber.setVisibility(View.VISIBLE);
            }else {
                viewHolder.textAlternateNumber.setVisibility(View.GONE);
            }
            viewHolder.textPatientName.setVisibility(View.VISIBLE);
            viewHolder.textAddress.setVisibility(View.VISIBLE);
            viewHolder.imageProfilePic.setVisibility(View.VISIBLE);
            viewHolder.consStatus.setVisibility(View.VISIBLE);
            viewHolder.consAccept.setVisibility(View.GONE);
        }


        viewHolder.buttonAuthorizeTenMin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog(context.getString(R.string.authorize_in_10_min_update), 1, authorizerRequestItemList.get(position).getId(), authorizerRequestItemList.get(position).getReidID());
            }
        });

        viewHolder.buttonBusy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog(context.getString(R.string.busy_update), 2, authorizerRequestItemList.get(position).getId(), authorizerRequestItemList.get(position).getReidID());
            }
        });
        viewHolder.buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog(context.getString(R.string.verified_update), 3, authorizerRequestItemList.get(position).getId(), authorizerRequestItemList.get(position).getReidID());


            }
        });
        viewHolder.buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog(context.getString(R.string.spam_update), 4, authorizerRequestItemList.get(position).getId(), authorizerRequestItemList.get(position).getReidID());
            }
        });

        if (authorizerRequestItemList.get(position).getProfilePicture() != null) {
            Picasso.with(context)
                    .load(context.getString(R.string.falcon_image_url) + authorizerRequestItemList.get(position).getProfilePicture())
                    .placeholder(R.mipmap.img_sample_pro_pic)   // optional
//                                    .error(R.drawable.ic_error_fallback)      // optional
//                                    .resize(250, 200)                        // optional
//                                    .rotate(90)                             // optional
                    .into((viewHolder.imageProfilePic));
        } else {
            viewHolder.imageProfilePic.setImageResource(R.mipmap.img_sample_pro_pic);
        }

        final ViewHolder finalViewHolder = viewHolder;
        viewHolder.buttonYesIWill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setMessage(context.getString(R.string.ready_to_auth));
                alertDialog.setPositiveButton(R.string.Yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finalViewHolder.consStatus.setVisibility(View.VISIBLE);
                        finalViewHolder.consAccept.setVisibility(View.GONE);
                        finalViewHolder.textUserPhoneNumber.setVisibility(View.VISIBLE);
                        finalViewHolder.textPatientName.setVisibility(View.VISIBLE);
                        finalViewHolder.textAddress.setVisibility(View.VISIBLE);
                        finalViewHolder.imageProfilePic.setVisibility(View.VISIBLE);
                        if(authorizerRequestItemList.get(position).getAlternateName()!=null && !authorizerRequestItemList.get(position).getAlternateName().isEmpty()){
                            finalViewHolder.textAlternateName.setVisibility(View.VISIBLE);
                        }else {
                            finalViewHolder.textAlternateName.setVisibility(View.GONE);
                        }
                        if (authorizerRequestItemList.get(position).getAlternatePhoneNumber()!=null && !authorizerRequestItemList.get(position).getAlternatePhoneNumber().isEmpty()){
                            finalViewHolder.textAlternateNumber.setVisibility(View.VISIBLE);
                        }else {
                            finalViewHolder.textAlternateNumber.setVisibility(View.GONE);
                        }
                    }
                });
                alertDialog.setNegativeButton(R.string.No, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();

            }
        });
        viewHolder.buttonNoICant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog(context.getString(R.string.busy_update), 2, authorizerRequestItemList.get(position).getId(), authorizerRequestItemList.get(position).getReidID());
            }
        });

        return convertView;
    }

    public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    private void alertDialog(String mes, final int status, final int id, final int bloodID) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setMessage(mes);
        alertDialog.setPositiveButton(R.string.Yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                updateAuthorizerStatus(status, id, bloodID);
            }
        });
        alertDialog.setNegativeButton(R.string.No, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.show();

    }

    private void updateAuthorizerStatus(int status, int id, int bloodID) {
        AuthorizerRequestUpdate authorizerRequestUpdate = new AuthorizerRequestUpdate();
        authorizerRequestUpdate.setId(id);
        authorizerRequestUpdate.setUserResStatus(status);
        authorizerRequestUpdate.setBloodID(bloodID);
        if (Utils.isNetworkAvailable()) {
            baseActivity.showProgress();
            falconAPI.authorizerRequestUpdate(authorizerRequestUpdate).enqueue(new Callback<AuthorizerRequestUpdateResponse>() {
                @Override
                public void onResponse(Call<AuthorizerRequestUpdateResponse> call, Response<AuthorizerRequestUpdateResponse> response) {
                    baseActivity.hideProgress();
                    authorizerRequestUpdateResponse = response.body();
                    if (response.isSuccessful() && response.body() != null && authorizerRequestUpdateResponse.getStatusCode() == 200) {
                        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                        alertDialog.setMessage(context.getString(R.string.successfully_update));
                        alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
//                                dialogInterface.dismiss();
                                authorizerRequestFragment.getAuthorizerRequest();
//                                baseActivity.onBackPressed();

                            }
                        });
                        alertDialog.show();
                    } else {
                        Toast.makeText(context, context.getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AuthorizerRequestUpdateResponse> call, Throwable t) {
                    baseActivity.hideProgress();
                    Toast.makeText(context, context.getString(R.string.server_error_message), Toast.LENGTH_SHORT).show();

                }
            });
        } else {
            userError.message = context.getString(R.string.network_error_message);
            baseActivity.showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }

    }


    static class ViewHolder {
        @BindView(R.id.image_profile_pic)
        CircularImageView imageProfilePic;
        @BindView(R.id.text_user_name)
        CustomTextView textUserName;
        @BindView(R.id.text_user_phone_number)
        CustomTextView textUserPhoneNumber;
        @BindView(R.id.text_patient_name)
        CustomTextView textPatientName;
        @BindView(R.id.text_user_requirement)
        CustomTextView textUserRequirement;
        @BindView(R.id.text_date_time)
        CustomTextView textDateTime;
        @BindView(R.id.text_user_address)
        CustomTextView textAddress;
        @BindView(R.id.text_user_alternate_name)
        CustomTextView textAlternateName;
        @BindView(R.id.text_user_alternate_number)
        CustomTextView textAlternateNumber;
        @BindView(R.id.button_yes)
        CustomButton buttonYes;
        @BindView(R.id.button_no)
        CustomButton buttonNo;
        @BindView(R.id.res_reg_list_item)
        ConstraintLayout resRegListItem;
        @BindView(R.id.card_respond_req)
        CardView cardRespondReq;
        @BindView(R.id.cons_respond_req_item)
        ConstraintLayout consRespondReqItem;
        @BindView(R.id.button_authorize_ten_min)
        CustomButton buttonAuthorizeTenMin;
        @BindView(R.id.button_busy)
        CustomButton buttonBusy;
        @BindView(R.id.button_yes_i_will)
        CustomButton buttonYesIWill;
        @BindView(R.id.button_no_i_cant)
        CustomButton buttonNoICant;
        @BindView(R.id.cons_accept)
        ConstraintLayout consAccept;
        @BindView(R.id.cons_status)
        ConstraintLayout consStatus;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}
