package com.dci.falcon.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.dci.falcon.fragment.DonationFragment;
import com.dci.falcon.fragment.LocationFragment;
import com.dci.falcon.fragment.UserDetailFragment;

/**
 * Created by vijayaganesh on 11/1/2017.
 */

public class ProfilePageAdapter extends FragmentStatePagerAdapter {

    int numOfTabs;
    UserDetailFragment userDetailFragment;
    LocationFragment locationFragment;
    DonationFragment donationFragment;


    public ProfilePageAdapter(FragmentManager fm, int mNumOfTabs) {
        super(fm);
        this.numOfTabs = mNumOfTabs;
        userDetailFragment = new UserDetailFragment();
        locationFragment = new LocationFragment();
        donationFragment = new DonationFragment();
    }


    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return userDetailFragment;
            case 1:
                return locationFragment;
            case 2:
                return donationFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }
}
